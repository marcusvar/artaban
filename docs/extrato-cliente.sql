SELECT
  DATE_FORMAT(ART_Venda.DataHoraEmissao,'%d/%m/%Y %h:%i') AS Emissao, 
  ART_Passagem.Valor, 
  ART_Passagem.Desconto, 
  ART_Fidelizacao.ValorOperacao AS Fidelizacao,
  (ART_Passagem.Valor - ART_Passagem.Desconto - ART_Fidelizacao.ValorOperacao) AS Total,
  ART_Viagem.DataInicio AS DataViagem,
  ART_Viagem.HoraInicio AS HoraViagem 
FROM
  ART_VendaItem
JOIN ART_Venda ON ART_VendaItem.Venda_Id = ART_Venda.Id
JOIN ART_Passagem ON ART_VendaItem.Passagem_Id = ART_Passagem.Id
JOIN ART_Pessoa ON ART_Passagem.Pessoa_Id = ART_Pessoa.Id
JOIN ART_Fidelizacao ON ART_Venda.Id = ART_Fidelizacao.Venda_Id
JOIN ART_Viagem ON ART_Passagem.Viagem_Id = ART_Viagem.Id
WHERE ART_Pessoa.Id = 3
ORDER BY ART_Venda.DataHoraEmissao;