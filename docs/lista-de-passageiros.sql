SELECT 
  ART_LinhaItem.Sequencia, UPPER(CONCAT(SUBSTRING(ART_HorarioItem.HoraEmbarque,1,5), ' - ',
  ART_Localidade.Nome, ' - ', ART_PontoReferencia.Descricao)) AS Embarque,
  ART_Passagem.Poltrona, 
  ART_Pessoa.Nome AS Passageiro, 
  (SELECT 
      ap.Nome
   FROM 
      ART_VendaItem avi
   JOIN ART_Venda av ON avi.Venda_Id = av.Id
   JOIN ART_Pessoa ap ON av.Pessoa_Id_Cliente = ap.Id
   WHERE
     avi.Passagem_Id = ART_Passagem.Id
   ) AS Empresa,
  Empresa.Nome AS EmpresaReserva,
  (SELECT
      GROUP_CONCAT(ART_PessoaTelefone.telefone SEPARATOR '<br />')
   FROM ART_PessoaTelefone
   WHERE ART_PessoaTelefone.Pessoa_Id = ART_Passagem.Pessoa_Id
   GROUP BY ART_PessoaTelefone.Pessoa_Id
  ) AS Telefone,
  ART_Pessoa.RGIE AS RG,
  IF(ART_Passagem.Parcial='C','',IF(ART_Passagem.Parcial='V', 'Volta','Ida')) AS Forma,
  ART_Passagem.Valor,
  ART_Passagem.DescontoFidelizacao,
  ART_Passagem.Desconto AS DescontoPassagem,
  ART_Passagem.Tipo
FROM
  ART_Passagem
JOIN ART_Pessoa ON ART_Passagem.Pessoa_Id = ART_Pessoa.Id
JOIN ART_ViagemItem ON ART_Passagem.ViagemItem_Id_Origem = ART_ViagemItem.Id
JOIN ART_HorarioItem ON ART_ViagemItem.HorarioItem_Id = ART_HorarioItem.Id
JOIN ART_LinhaItem ON ART_HorarioItem.LinhaItem_Id = ART_LinhaItem.ID
JOIN ART_PontoReferencia ON ART_LinhaItem.PontoReferencia_Id = ART_PontoReferencia.Id
JOIN ART_Localidade ON ART_PontoReferencia.Localidade_Id = ART_Localidade.Id
LEFT JOIN ART_Pessoa AS Empresa ON ART_Passagem.Empresa_Id = Empresa.Id
WHERE
  ART_Passagem.Viagem_Id = 28 AND
  ART_Passagem.Situacao <> 'Cancelada' AND
  ART_Passagem.Poltrona <> ''
ORDER BY ART_LinhaItem.Sequencia, ART_Passagem.Poltrona