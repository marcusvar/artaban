DELETE FROM `artaban`.`ART_PassagemAdicional`;
INSERT INTO `artaban`.`ART_PassagemAdicional` (`Passagem_Id`, `TipoAdicionalTarifa_Id`, `Valor`, `created_at`, `updated_at`) 
SELECT Id, TipoAdicionalTarifa_Id, Adicional, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
FROM ART_Passagem
WHERE Adicional <> 0;