SET @nome:='100';
SELECT * FROM(
SELECT 
  ART_Pessoa.Id,
  ART_Pessoa.Nome,
  NULL AS IdResponsavel,
  NULL AS Responsavel
FROM
  ART_Pessoa
WHERE
  SUBSTR(ART_Pessoa.Tipo,1,2)='PF' AND
  ART_Pessoa.Nome like CONCAT('%',@nome,'%')
UNION
SELECT
  IFNULL(Emp.Id,ART_Pessoa.Id) Id,
  IFNULL(Emp.Nome,ART_Pessoa.Nome) Nome,
  Emp.Id IdResponsavel,
  (SELECT Nome FROM ART_Pessoa ap WHERE ap.Id=ART_Pessoa.Id) AS Responsavel
FROM
  ART_Pessoa
LEFT JOIN ART_Pessoa Emp ON Emp.Pessoa_Id_Responsavel = ART_Pessoa.Id
WHERE
  NOT ISNULL(Emp.Pessoa_Id_Responsavel) AND
  (Emp.Nome like CONCAT('%',@nome,'%') OR ART_Pessoa.Nome LIKE CONCAT('%',@nome,'%'))
) AS Consulta
WHERE   
  NOT ISNULL(Consulta.Id)
ORDER BY 
  Consulta.Nome ASC