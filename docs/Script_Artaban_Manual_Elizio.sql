DROP TABLE IF EXISTS ART_Pais;
CREATE TABLE ART_Pais (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Nome VARCHAR(50) NOT NULL,
  Sigla CHAR(2) NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL
)
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_UF;
CREATE TABLE ART_UF (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Nome VARCHAR(50) NOT NULL,
  Sigla CHAR(2) NOT NULL,
  Pais_Id INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_UF_Pais_Id FOREIGN KEY (Pais_Id)
    REFERENCES ART_Pais(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Localidade;
CREATE TABLE ART_Localidade (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Nome VARCHAR(70) NOT NULL,
  Tipo ENUM('Munic�pio','Distrito','Povoado','Regi�o administrativa') DEFAULT NULL,
  Fuso SMALLINT(6) DEFAULT NULL,
  UF_Id INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Localidade_UF_Id FOREIGN KEY (UF_Id)
    REFERENCES ART_UF(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Pessoa;
CREATE TABLE ART_Pessoa(
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Nome VARCHAR(70) NOT NULL COMMENT 'Nome da pessoa ou Raz�o social da empresa',
  NomeUsual VARCHAR(50) DEFAULT NULL COMMENT 'Nome usual da pessoa ou Nome fantasia da empresa',
  Documento VARCHAR(18) DEFAULT NULL COMMENT 'CPF para pessoa f�sica e CNPJ para pessoa jur�dica',
  RGIE VARCHAR(20) DEFAULT NULL COMMENT 'RG para pessoa f�sica e IE para pessoa jur�dica',
  Email VARCHAR(50) DEFAULT NULL COMMENT 'E-mail da pessoal ou empresarial',
  Sexo ENUM('M', 'F') DEFAULT NULL ,
  DataNascimento DATE DEFAULT NULL,
  Telefone VARCHAR(15) DEFAULT NULL COMMENT 'Telefone da empresa',
  Celular VARCHAR(15) DEFAULT NULL COMMENT 'N�mero de celular da empresa/contato respons�vel',
  Senha VARCHAR(45) DEFAULT NULL COMMENT 'Senha do painel de controle do hambiente da Kinghost',
  Pessoa_Id INT(11) DEFAULT NULL COMMENT 'Relacionamento recursivo para multiempresa',
  Tipo ENUM('PF Brasileiro', 'PJ Brasileiro', 'PF Estrangeiro', 'PJ Estrangeiro') NOT NULL,
  EmPotencial TINYINT(1) DEFAULT 0,
  VisualizarFidelizacao TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Verifica se o cliente tem permiss�o para fazer resgate de fideliza��o',
  Pessoa_Id_Responsavel INTEGER DEFAULT NULL COMMENT 'Relacionamento da pessoa respons�vel pela empresa',
  WebSite VARCHAR(100) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Pessoa_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(ID) ON DELETE RESTRICT ON UPDATE CASCADE, 
  CONSTRAINT FK_Pessoa_Pessoa_Id_Responsavel FOREIGN KEY (Pessoa_Id_Responsavel)
    REFERENCES ART_Pessoa(ID) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci
COMMENT = 'Cadastro de Pessoas (Empresas e clientes)';


DROP TABLE IF EXISTS ART_PessoaTelefone;
CREATE TABLE ART_PessoaTelefone(
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Pessoa_Id INTEGER NOT NULL,
  Descricao VARCHAR(15) NOT NULL,
  Telefone VARCHAR(15) NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_PessoaTelefone_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(ID) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_PessoaVinculo;
CREATE TABLE ART_PessoaVinculo(
  Pessoa_Id INTEGER NOT NULL,
  Pessoa_Id_Vinculo INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  PRIMARY KEY(Pessoa_Id, Pessoa_Id_Vinculo),
  CONSTRAINT FK_PessoaVinculo_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(ID) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_PessoaVinculo_Pessoa_Id_Vinculo FOREIGN KEY (Pessoa_Id_Vinculo)
    REFERENCES ART_Pessoa(ID) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_PessoaEndereco;
CREATE TABLE ART_PessoaEndereco(
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Logradouro VARCHAR(50) NOT NULL,
  Numero VARCHAR(10) NOT NULL,
  Bairro VARCHAR(50) DEFAULT NULL,
  Complemento VARCHAR(50) DEFAULT NULL,
  Cep CHAR(9) DEFAULT NULL,
  Localidade_Id INTEGER NOT NULL,
  Pessoa_Id INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_PessoaEndereco_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(ID) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_PessoaEndereco_Localidade_Id FOREIGN KEY (Localidade_Id)
    REFERENCES ART_Localidade(ID) ON DELETE RESTRICT ON UPDATE CASCADE  
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;

DROP TABLE IF EXISTS ART_TipoPoltrona;
CREATE TABLE ART_TipoPoltrona (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Descricao VARCHAR(40) NOT NULL,
  Pessoa_Id INTEGER NOT NULL COMMENT 'Relacionamento com a empresa',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_TipoPoltrona_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;

DROP TABLE IF EXISTS ART_MapaVeiculo;
CREATE TABLE ART_MapaVeiculo (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Descricao VARCHAR(40) NOT NULL,
  Pessoa_Id INTEGER NOT NULL COMMENT 'Relacionamento com a empresa',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_MapaVeiculo_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_MapaVeiculoPoltrona;;
CREATE TABLE ART_MapaVeiculoPoltrona (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  MapaVeiculo_Id INTEGER NOT NULL,
  TipoPoltrona_Id INTEGER NOT NULL,
  Numero INTEGER NOT NULL COMMENT 'N�mero da poltrona',
  Linha INTEGER NOT NULL COMMENT 'Linha em que a poltrona esta posicionada',
  Coluna INTEGER NOT NULL COMMENT 'Coluna em que a poltrona esta posicionada',
  Andar INTEGER NOT NULL COMMENT 'Andar onde esta localizada a poltrona', 
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_MapaVeiculoPoltrona_MapaVeiculo_Id FOREIGN KEY (MapaVeiculo_Id)
    REFERENCES ART_MapaVeiculo(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_TipoPoltrona_TipoPoltrona_Id FOREIGN KEY (TipoPoltrona_Id)
    REFERENCES ART_TipoPoltrona(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Veiculo;
CREATE TABLE ART_Veiculo (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Numero VARCHAR(15) DEFAULT NULL,
  Placa VARCHAR(8) NOT NULL,
  MapaVeiculo_Id INTEGER NOT NULL,
  Pessoa_Id INTEGER NOT NULL COMMENT 'Relacionamento com a empresa',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Veiculo_MapaVeiculo FOREIGN KEY (MapaVeiculo_Id)
    REFERENCES ART_MapaVeiculo(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Veiculo_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_PontoReferencia;
CREATE TABLE ART_PontoReferencia (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Descricao VARCHAR(45) NOT NULL,
  Tipo ENUM('Ponto de Refer�ncia','Ag�ncia','Garagem','Dep�sito','Ponto de Apoio','Transportadora','Ag�ncia de Turismo','Ag�ncia Terceirizada','Departamento') DEFAULT NULL,
  Localidade_Id INT(11) NOT NULL COMMENT 'Localidade da qual pertence o ponto de refer�ncia.',
  Pessoa_Id INT(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_PontoReferencia_localidade_Id FOREIGN KEY (Localidade_Id)
    REFERENCES ART_Localidade(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_PontoReferencia FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Linha;
CREATE TABLE ART_Linha (
  ID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  PontoReferencia_Id_Destino INTEGER NOT NULL COMMENT 'Destino da linha',
  PontoReferencia_Id_Via INTEGER NOT NULL COMMENT 'Via da linha',
  Pessoa_Id INTEGER NOT NULL COMMENT 'Relacionamento com a empresa',
  Situacao ENUM('Normal', 'Bloqueada') DEFAULT 'Normal' NOT NULL, 
  Duracao INTEGER NOT NULL COMMENT 'Dura��o da viagem em dias',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Linha_PontoReferencia_Id_Destino FOREIGN KEY (PontoReferencia_Id_Destino)
    REFERENCES ART_PontoReferencia(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Linha_PontoReferencia_Id_Via FOREIGN KEY (PontoReferencia_Id_Via)
    REFERENCES ART_PontoReferencia(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT F_Linha_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_LinhaItem;
CREATE TABLE ART_LinhaItem (
  ID INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Linha_Id INTEGER NOT NULL,
  PontoReferencia_Id INTEGER NOT NULL,
  Sequencia TINYINT NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_LinhaItem_Linha_Id FOREIGN KEY (Linha_Id)
    REFERENCES ART_Linha(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_LinhaItem_PontoReferencia_Id FOREIGN KEY (PontoReferencia_Id)
    REFERENCES ART_PontoReferencia(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Horario;
CREATE TABLE ART_Horario (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Linha_Id INTEGER NOT NULL,
  Horario TIME NOT NULL COMMENT 'Hor�rio inicial de partida do ponto de refer�ncia deste hor�rio',
  Sentido ENUM('Ida','Volta') DEFAULT NULL COMMENT 'Sentido em que o hor�rio ir� trafegar com rela��o ao cadastrado na linha',
  Frequencia CHAR(7) NOT NULL COMMENT 'Frequ�ncia da viagem (DSTQQSS) ',
  MapaVeiculo_Id INTEGER DEFAULT NULL,
  Feriado TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Feriado - Excluir, Incluir e Ignorar (0,1,2)',
  Confirmado ENUM('Sim','N�o') NOT NULL DEFAULT 'N�o' COMMENT 'Define se o hor�rio est� confirmado para gerar viagem',
  Situacao ENUM('Normal', 'Bloqueado') DEFAULT 'Normal' NOT NULL,
  Duracao INTEGER NOT NULL COMMENT 'Dura��o da viagem em dias',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Horario_Linha_Id FOREIGN KEY (Linha_Id)
    REFERENCES ART_Linha(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT fk_Horario_MapaVeiculo FOREIGN KEY (MapaVeiculo_Id)
    REFERENCES ART_MapaVeiculo(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_HorarioItem;
CREATE TABLE ART_HorarioItem (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Horario_Id INTEGER NOT NULL,
  LinhaItem_Id INTEGER NOT NULL,
  HoraEmbarque TIME NOT NULL,
  DiaEmbarque TINYINT NOT NULL DEFAULT 0,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_HorarioItem_Horario_Id FOREIGN KEY (Horario_Id)
    REFERENCES ART_Horario(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_HorarioItem_LinhaItem_Id FOREIGN KEY (LinhaItem_Id)
    REFERENCES ART_LinhaItem(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Viagem;
CREATE TABLE ART_Viagem (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  DataInicio DATE NOT NULL COMMENT 'Data do inicio da viagem',
  HoraInicio TIME NOT NULL COMMENT 'Hora do inicio da viagem',
  Horario_Id INTEGER NOT NULL,
  MapaVeiculo_Id INTEGER NOT NULL,
  Confirmada ENUM('Sim','N�o') NOT NULL DEFAULT 'N�o' COMMENT 'Define se a viagem est� confirmada para aparecer na consulta',
  Situacao ENUM('Normal', 'Bloqueada') DEFAULT 'Normal' NOT NULL,
  Duracao INTEGER NOT NULL COMMENT 'Dura��o da viagem em dias',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Viatem_Horario_Id FOREIGN KEY (Horario_Id)
    REFERENCES ART_Horario(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT Fk_Viagem_MapaVeiculo_Id FOREIGN KEY (MapaVeiculo_Id)
    REFERENCES ART_MapaVeiculo(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_ViagemItem;
CREATE TABLE ART_ViagemItem (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Viagem_Id INTEGER NOT NULL,
  HorarioItem_Id INTEGER NOT NULL,
  DataHoraEmbarque DATETIME NOT NULL,  
  Ativo TINYINT(1) DEFAULT '1' NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_ViatemItem_Viagem_Id FOREIGN KEY (Viagem_Id)
    REFERENCES ART_Viagem(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_ViatemItem_HorarioItem_Id FOREIGN KEY (HorarioItem_Id)
    REFERENCES ART_HorarioItem(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_TipoAdicionalTarifa;
CREATE TABLE ART_TipoAdicionalTarifa (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Descricao VARCHAR(40) NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_AdicionalTarifa;
CREATE TABLE ART_AdicionalTarifa (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Linha_Id INTEGER NOT NULL COMMENT 'O valor adicional ser� para a linha independente do trecho',
  TipoAdicionalTarifa_Id INTEGER NOT NULL,
  DataVigencia DATE NOT NULL COMMENT 'Data do in�cio da vig�ncia',
  Valor DOUBLE(15, 2) DEFAULT NULL,
  Ida TINYINT(1) NOT NULL DEFAULT '1',
  Volta TINYINT(1) NOT NULL DEFAULT '1',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_AdicionalTarifa_Linha_Id FOREIGN KEY (Linha_Id)
    REFERENCES ART_Linha(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Tarifa_TipoAdicionalTarifa FOREIGN KEY (TipoAdicionalTarifa_Id)
    REFERENCES ART_TipoAdicionalTarifa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Tarifa;
CREATE TABLE ART_Tarifa (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  LinhaItem_Id INTEGER NOT NULL COMMENT 'Tarifa do ponto de refer�ncia para o destino da linha',
  TipoPoltrona_Id INTEGER NOT NULL COMMENT 'O valor pode diferenciar pelo tipo de poltrona',
  DataVigencia DATE NOT NULL COMMENT 'Data do in�cio da vig�ncia',
  Valor Double(15, 2) DEFAULT 0,
  Valor2 Double(15, 2) DEFAULT 0,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Tarifa_LinhaItem_Id FOREIGN KEY (LinhaItem_Id)
    REFERENCES ART_LinhaItem(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Tarifa_TipoPoltrona FOREIGN KEY (TipoPoltrona_Id)
    REFERENCES ART_TipoPoltrona(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Desconto;
CREATE TABLE ART_Desconto (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Descricao VARCHAR(40) NOT NULL,
  Percentagem FLOAT NOT NULL,
  Confirmado ENUM('Sim', 'N�o') DEFAULT 'N�o' NOT NULL,
  AgenciaWeb TINYINT(1) NOT NULL DEFAULT 0 COMMENT "0 Ambos 1 Ag�ncia 2 Web",
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_DescontoItinerario;
CREATE TABLE ART_DescontoItinerario (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Desconto_Id INTEGER NOT NULL,
  PontoReferencia_Id_Inicial INTEGER NOT NULL,
  PontoReferencia_Id_Final INTEGER NOT NULL,
  Percentagem FLOAT NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_DescontoItinerario_Desconto_Id FOREIGN KEY (Desconto_Id)
    REFERENCES ART_Desconto(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_DescontoItinerario_PontoReferencia_Id_Inicial FOREIGN KEY (PontoReferencia_Id_Inicial)
    REFERENCES ART_PontoReferencia(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_DescontoItinerario_PontoReferencia_Id_Final FOREIGN KEY (PontoReferencia_Id_Final)
    REFERENCES ART_PontoReferencia(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_DescontoPoltrona;
CREATE TABLE ART_DescontoPoltrona (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Desconto_Id INTEGER NOT NULL,
  Poltrona TINYINT(3) NOT NULL,
  Percentagem FLOAT NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_DescontoPoltrona_Desconto_Id FOREIGN KEY (Desconto_Id)
    REFERENCES ART_Desconto(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_DescontoLinha;
CREATE TABLE ART_DescontoLinha (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Desconto_Id INTEGER NOT NULL,
  Linha_Id INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_DescontoLinha_Desconto_Id FOREIGN KEY (Desconto_Id)
    REFERENCES ART_Desconto(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_DescontoLinha_Linha_Id FOREIGN KEY (Linha_Id)
    REFERENCES ART_Linha(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_DescontoHorario;
CREATE TABLE ART_DescontoHorario (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Desconto_Id INTEGER NOT NULL,
  Horario_Id INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_DescontoHorario_Desconto_Id FOREIGN KEY (Desconto_Id)
    REFERENCES ART_Desconto(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_DescontoHorario_Horario_Id FOREIGN KEY (Horario_Id)
    REFERENCES ART_Horario(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_DescontoViagem;
CREATE TABLE ART_DescontoViagem (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Desconto_Id INTEGER NOT NULL,
  Viagem_Id INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_DescontoViagem_Desconto_Id FOREIGN KEY (Desconto_Id)
    REFERENCES ART_Desconto(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_DescontoViagem_Viagem_Id FOREIGN KEY (Viagem_Id)
    REFERENCES ART_Viagem(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Venda;
CREATE TABLE ART_Venda (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  PontoReferencia_Id INTEGER NOT NULL COMMENT 'Ponto de refer�ncia emissor da(s) passagem(ns)',
  Pessoa_Id_Cliente INTEGER NOT NULL COMMENT 'Cliente que esta comprando passagem',
  Pessoa_Id INTEGER NOT NULL COMMENT 'Relacionamento com a empresa',
  Usuario_Id INTEGER DEFAULT NULL COMMENT 'Relacionamento com o usu�rio - Agente emitente',
  DataHoraEmissao TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,  
  ValorDesconto FLOAT(10, 2) DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Venda_PontoReferencia_Id FOREIGN KEY (PontoReferencia_Id)
    REFERENCES ART_PontoReferencia(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Venda_Cliente_Id FOREIGN KEY (Pessoa_Id_Cliente)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Venda_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Passagem;
CREATE TABLE ART_Passagem (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  DataHoraEmissao TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  Valor FLOAT(10, 2) DEFAULT NULL,
  Poltrona CHAR(2) DEFAULT NULL,
  Viagem_Id INTEGER NOT NULL,
  ViagemItem_Id_Origem INTEGER NOT NULL,
  Pessoa_Id INTEGER NOT NULL COMMENT 'Passageiro',
  Tipo ENUM('R', 'P') NOT NULL COMMENT 'R = Reserva, P = Passagem', 
  Parcial ENUM('C', 'I', 'V') NOT NULL DEFAULT 'C' COMMENT 'C = Cheia, I = Ida, V = Volta', 
  Situacao ENUM('Normal', 'Cancelada') DEFAULT 'Normal' NOT NULL, 
  ReservaExpiracao DATETIME DEFAULT NULL COMMENT 'Quando for reserva informar a data de expira��o da reserva',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Passagem_Viagem_Id FOREIGN KEY (Viagem_Id)
    REFERENCES ART_Viagem(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Passagem_ViagemItem_Id_Origem FOREIGN KEY (ViagemItem_Id_Origem)
    REFERENCES ART_ViagemItem(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Passagem_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_VendaItem;
CREATE TABLE ART_VendaItem (
  Id INTEGER NOT NULL NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Venda_Id INTEGER NOT NULL,
  Passagem_Id INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_VendaItem_Venda_Id FOREIGN KEY (Venda_Id)
    REFERENCES ART_Venda(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_VendaItem_Passagem_Id FOREIGN KEY (Passagem_Id)
    REFERENCES ART_Passagem(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Fidelizacao;
CREATE TABLE ART_Fidelizacao (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Operacao ENUM('D�bito','Cr�dito') DEFAULT NULL,
  Descricao VARCHAR(50) DEFAULT NULL,
  DataOperacao DATE NOT NULL,
  ValorOperacao FLOAT(10, 2) NOT NULL,
  Pessoa_Id INTEGER NOT NULL COMMENT 'Cliente',
  Venda_Id INTEGER DEFAULT NULL COMMENT 'Relacionamento com a venda quando for utilizado o saldo',
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Fidelizacao_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Fidelizacao_Venda_Id FOREIGN KEY (Venda_Id)
    REFERENCES ART_Venda(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci
COMMENT = 'Fideliza��o de clientes (Mega Polo)';


DROP TABLE IF EXISTS ART_Parametro;
CREATE TABLE ART_Parametro (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Nome VARCHAR(50) NOT NULL,
  Valor VARCHAR(200) DEFAULT NULL,
  Descricao VARCHAR(100) NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Feriados;
CREATE TABLE ART_Feriados (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  UF_Id INTEGER, 
  TIPO ENUM('Fixo', 'M�vel') DEFAULT 'Fixo' NOT NULL,
  Dia TINYINT(2) UNSIGNED DEFAULT NULL,
  Mes TINYINT(2) UNSIGNED DEFAULT NULL,
  Descricao VARCHAR(50) NOT NULL,
  DataMovel DATE DEFAULT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Feriado_UF_Id FOREIGN KEY (UF_Id)
    REFERENCES ART_UF(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;

DROP TABLE IF EXISTS ART_Semafaro;
CREATE TABLE ART_Semafaro (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Viagem_Id INTEGER NOT NULL,  
  Poltrona INTEGER NOT NULL,
  ViagemItem_Id_Origem INTEGER NOT NULL,
  DataHora DATETIME NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Semafaro_Viagem_Id FOREIGN KEY (Viagem_Id)
    REFERENCES ART_Viagem(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Semafaro_ViagemItem_Id FOREIGN KEY (ViagemItem_Id_Origem)
    REFERENCES ART_ViagemItem(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;


DROP TABLE IF EXISTS ART_Usuario;
CREATE TABLE ART_Usuario (
  Id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
  Nome VARCHAR(180) NOT NULL,
  Email VARCHAR(80) NOT NULL,
  Senha VARCHAR(80) NOT NULL,
  Pessoa_Id INTEGER NOT NULL, 
  PontoReferencia_Id_Agencia INTEGER NOT NULL,
  created_at DATETIME DEFAULT NULL,
  updated_at DATETIME DEFAULT NULL,
  deleted_at DATETIME DEFAULT NULL,
  CONSTRAINT FK_Usuario_Pessoa_Id FOREIGN KEY (Pessoa_Id)
    REFERENCES ART_Pessoa(Id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT FK_Usuario_PontoReferencia_Id_Agencia FOREIGN KEY (PontoReferencia_Id_Agencia)
    REFERENCES ART_PontoReferencia(Id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8
COLLATE utf8_swedish_ci;