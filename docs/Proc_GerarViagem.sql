 DELIMITER ^
DROP PROCEDURE IF EXISTS Proc_GerarViagem^
CREATE PROCEDURE Proc_GerarViagem(
  pDataInicio DATE,
  pDataFim    DATE,
  pHorario_Id  INTEGER)
BEGIN
  DECLARE vValidacao TINYINT;
  DECLARE vFeriado TINYINT;
  DECLARE vHorarioFeriado TINYINT;
  DECLARE vFrequencia TINYINT;

  DECLARE vDomingo TINYINT;
  DECLARE vSegunda TINYINT;
  DECLARE vTerca TINYINT;
  DECLARE vQuarta TINYINT;
  DECLARE vQuinta TINYINT;
  DECLARE vSexta TINYINT;
  DECLARE vSabado TINYINT;

  DECLARE vDataInicio DATE;
  DECLARE vHorario TIME;
  DECLARE vPontoReferenciaDestino_Id INTEGER;
  DECLARE vTipoVeiculo_Id INTEGER;
  DECLARE vQtdDias INTEGER;
  DECLARE vQtd INTEGER;
  DECLARE vViagem_Id INTEGER;

  DECLARE viHorarioItem_Id INTEGER;
  DECLARE viHorario TIME;
  DECLARE vEOF TINYINT;
  DECLARE vFim TINYINT;

  DECLARE Cur_HorarioItem CURSOR FOR
    SELECT Id AS HorarioItem_Id, Horario
    FROM ART_HorarioItem
    WHERE Horario_Id = pHorario_Id;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET vEOF = TRUE;

  SET vValidacao = 0;

  /*Validar se a Linha est� ok - Retorno 1*/	
  SELECT IF(ART_Linha.Situacao = 'Normal', 0, 1) INTO vValidacao
  FROM ART_Linha 
  JOIN ART_Horario ON ART_Horario.Linha_Id = ART_Linha.Id
  WHERE ART_Horario.Id = pHorario_Id;
  
  IF vValidacao = 0 THEN
    /*Validar Situa��o e se Hor�rio esta Confirmado - Retorno 2*/
    SELECT IF((IF(Situacao = 'Normal', 0, 2) + IF(Confirmado = 'Sim', 0, 2)) = 0, 0,2) INTO vValidacao
    FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;
  END IF;

  IF vValidacao = 0 THEN
    SET vDataInicio = pDataInicio;

    SELECT ART_Horario.Horario INTO vHorario FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT ART_Horario.TipoVeiculo_Id INTO vTipoVeiculo_Id FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT (DATEDIFF(pDataFim, pDataInicio)) INTO vQtdDias;
    
    SELECT ART_PontoReferencia.Id INTO vPontoReferenciaDestino_Id 
    FROM ART_Horario 
    JOIN ART_LinhaItem ON ART_LinhaItem.ID = ART_Horario.LinhaItem_Id_Destino
    JOIN ART_PontoReferencia ON ART_PontoReferencia.Id = ART_LinhaItem.PontoReferencia_Id
    WHERE ART_Horario.Id = pHorario_Id;

    SET vQTD = 0;
    WHILE vQtd <= vQtdDias DO
      
      /*Verifica se o hor�rio tr�fega em feriados*/
      SELECT Feriado INTO vHorarioFeriado FROM ART_Horario WHERE Id = pHorario_Id;

      SET vFeriado = 0;
      IF vHorarioFeriado = 0 THEN
        /*Se o hor�rio tr�fega em feriados ent�o verifica se a data � feriado*/
        SELECT EXISTS(
        SELECT ART_Feriados.Id FROM ART_Feriados
        WHERE ART_Feriados.DataMovel = DATE_ADD(vDataInicio, INTERVAL vQtd DAY) OR
        (ART_Feriados.Dia = DAY(DATE_ADD(vDataInicio, INTERVAL vQtd DAY)) AND
        ART_Feriados.Mes = MONTH(DATE_ADD(vDataInicio, INTERVAL vQtd DAY)))) INTO vFeriado;
      END IF;

      /*Verificar a frequ�ncia do hor�rio*/
      SELECT SUBSTRING(Frequencia FROM 1 FOR 1) INTO vDomingo FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 2 FOR 1) INTO vSegunda FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 3 FOR 1) INTO vTerca FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 4 FOR 1) INTO vQuarta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 5 FOR 1) INTO vQuinta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 6 FOR 1) INTO vSexta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 7 FOR 1) INTO vSabado FROM ART_Horario WHERE Id = pHorario_Id;
            
      SELECT 
        CASE DATE_FORMAT(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), '%w')
          WHEN 0 THEN IF(vDomingo = 1, "1", "0")
          WHEN 1 THEN IF(vSegunda = 1, "1", "0")
          WHEN 2 THEN IF(vTerca = 1, "1", "0")
          WHEN 3 THEN IF(vQuarta = 1, "1", "0")
          WHEN 4 THEN IF(vQuinta = 1, "1", "0")
          WHEN 5 THEN IF(vSexta = 1, "1", "0")
          WHEN 6 THEN IF(vSabado = 1, "1", "0")
      END INTO vFrequencia;
      
      IF (vFeriado = 0) AND (vFrequencia = 1) THEN
        /* Grava na tabela de viagens */
        INSERT INTO ART_Viagem(DataInicio, HoraInicio, Horario_Id, PontoReferencia_Id_Destino, TipoVeiculo_Id, Confirmada, Situacao)
          VALUES(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), vHorario, pHorario_Id, vPontoReferenciaDestino_Id, vTipoVeiculo_Id, "Sim", "Normal");
        
        /* Busca o Id da viagem gravada */
        SET vViagem_Id = (SELECT LAST_INSERT_ID());
          
        /* Grava na tabela de viagem item  */
        OPEN Cur_HorarioItem;
        REPEAT
          SET vEOF = FALSE;
          FETCH Cur_HorarioItem INTO viHorarioItem_Id, viHorario;
          SET vFim = vEOF;
          IF NOT vFim THEN
            INSERT INTO ART_ViagemItem(Viagem_Id, HorarioItem_Id, Horario, Ativo) 
            VALUES(vViagem_Id, viHorarioItem_Id, viHorario, 1); 
          END IF;
        UNTIL vFim END REPEAT;
        CLOSE Cur_HorarioItem;
      END IF;
      
      SET vQtd = vQtd + 1;
    END WHILE;
    SELECT "Procedimento executado com sucesso." AS Mensagem;
  ELSE
  BEGIN
    IF vValidacao = 1 THEN
      SELECT "A linha deste hor�rio est� bloqueada." AS Mensagem;
    ELSE IF vValidacao = 2 THEN
      SELECT "Este Hor�rio est� Bloqueado ou n�o confirmado." AS Mensagem;
    END IF;  
    END IF;  
  END;  
  END IF;  
END^

DELIMITER ;
