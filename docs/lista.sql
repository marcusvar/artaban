SELECT 
ART_ViagemItem.HorarioItem_Id,
  CONCAT(IFNULL(LinhaItem.Sequencia, ART_LinhaItem.Sequencia), 
         IFNULL(PontoReferencia.Id, ART_PontoReferencia.Id)) AS Sequencia, 
  CONCAT(ART_LinhaItem.Sequencia, ART_PontoReferencia.Id) AS SeqRecolher,
DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%H:%i') AS DataHoraEmbarque,
(SELECT HoraEmbarque 
	 FROM ART_HorarioItem hi
     WHERE 
		hi.LinhaItem_id = ART_HorarioItem.LinhaItem_Id_Transbordo AND
		hi.Horario_Id = ART_HorarioItem.Horario_Id),
  UPPER(CONCAT(DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%H:%i'), ' - ',
        ART_Localidade.Nome, ' - ', ART_PontoReferencia.Descricao)) AS Embarque,  
  UPPER(CONCAT(
	SUBSTRING(IF(ART_ViagemItem.Recolher,
	(COALESCE( 
	(SELECT HoraEmbarque 
	 FROM ART_HorarioItem hi
     WHERE 
		hi.LinhaItem_id = ART_HorarioItem.LinhaItem_Id_Transbordo AND
		hi.Horario_Id = ART_HorarioItem.Horario_Id
    ), DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%H:%i:%s')), ART_ViagemItem.DataHoraEmbarque)),1,5), ' - ',
        IFNULL(Localidade.Nome,ART_Localidade.Nome), ' - ', 
	    IFNULL(PontoReferencia.Descricao, ART_PontoReferencia.Descricao))) AS Baldeacao,
  ART_ViagemItem.HorarioItem_Id_Transbordo,            
  ART_Passagem.Poltrona, 
  ART_ViagemItem.Recolher,
  ART_PontoReferencia.Tipo,
  ART_Pessoa.Nome AS Passageiro, 
ART_ViagemItem.DataHoraEmbarque,
ART_ViagemItem.Recolher,
  (SELECT 
      ap.Nome
   FROM 
      ART_VendaItem avi
   JOIN ART_Venda av ON avi.Venda_Id = av.Id
   JOIN ART_Pessoa ap ON av.Pessoa_Id_Cliente = ap.Id
   WHERE
     avi.Passagem_Id = ART_Passagem.Id
   LIMIT 1  
   ) AS Empresa,
  Empresa.Nome AS EmpresaReserva,
    (SELECT
      GROUP_CONCAT(ART_PessoaTelefone.telefone SEPARATOR '<br />')
   FROM ART_PessoaTelefone
   WHERE ART_PessoaTelefone.Pessoa_Id = ART_Passagem.Pessoa_Id
   GROUP BY ART_PessoaTelefone.Pessoa_Id
  ) AS Telefone,
  ART_Pessoa.RGIE AS RG,
  Empresa.Documento AS CNPJ,
  IF(ART_Passagem.Parcial='C','',IF(ART_Passagem.Parcial='V', 'Volta','Ida')) AS Forma,
  ART_Passagem.Valor,
  ART_Passagem.DescontoFidelizacao,
  ART_Passagem.Desconto AS DescontoPassagem,
  ART_Passagem.Tipo,
  ART_Passagem.Adicional
FROM
  ART_Passagem
JOIN ART_Pessoa ON ART_Passagem.Pessoa_Id = ART_Pessoa.Id
JOIN ART_ViagemItem ON ART_Passagem.ViagemItem_Id_Origem = ART_ViagemItem.Id
JOIN ART_HorarioItem ON ART_ViagemItem.HorarioItem_Id = ART_HorarioItem.Id
JOIN ART_LinhaItem ON ART_HorarioItem.LinhaItem_Id = ART_LinhaItem.ID
JOIN ART_PontoReferencia ON ART_LinhaItem.PontoReferencia_Id = ART_PontoReferencia.Id
JOIN ART_Localidade ON ART_PontoReferencia.Localidade_Id = ART_Localidade.Id
LEFT JOIN ART_LinhaItem AS LinhaItem ON ART_ViagemItem.HorarioItem_Id_Transbordo = LinhaItem.ID
LEFT JOIN ART_PontoReferencia AS PontoReferencia ON LinhaItem.PontoReferencia_Id = PontoReferencia.Id
LEFT JOIN ART_Localidade AS Localidade ON PontoReferencia.Localidade_Id = Localidade.Id

LEFT JOIN ART_Pessoa AS Empresa ON ART_Passagem.Empresa_Id = Empresa.Id
WHERE
  ART_Passagem.Viagem_Id = 115 AND
  ART_Passagem.Situacao <> 'Cancelada' AND
  ART_Passagem.Poltrona <> ''
ORDER BY Baldeacao #DataHoraEmbarque ART_ViagemItem.DataHoraEmbarque #'Embarque'