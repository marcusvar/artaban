SET @inicial := '2015-05-06';
SET @opcao := FALSE;
SET @periodo := 60;


SELECT
#    IFNULL(ART_Localidade.Nome, '|Sem Cidade') AS Cidade,
    ART_Localidade.Nome AS Cidade,
#===========================================================================
    CONCAT(Pessoa.Nome, IF(NOT ISNULL(
        (SELECT Pessoa_Id_Responsavel FROM ART_Pessoa 
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel)
    ), CONCAT('<br />',

    (SELECT
    GROUP_CONCAT(ART_Pessoa.Nome SEPARATOR '<br /> ')
        FROM ART_Pessoa
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel
    )),'')) AS Cliente,
#===========================================================================
/*
Passageiro.Nome,
ART_Passagem.DataHoraEmissao,
ART_Passagem.Poltrona,
ART_Viagem.Descricao,
*/
    CONCAT('', IF(NOT ISNULL(
        (SELECT Pessoa_Id_Responsavel FROM ART_Pessoa 
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel)
    ), CONCAT('<br />',

    (SELECT
    GROUP_CONCAT(ART_Pessoa.NomeUsual SEPARATOR '<br /> ')
        FROM ART_Pessoa
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel
    )),'')) AS NomeUsual,
#===========================================================================    
    (SELECT
        GROUP_CONCAT(ART_PessoaTelefone.Descricao,': ',ART_PessoaTelefone.telefone SEPARATOR '<br /> ')
        FROM ART_PessoaTelefone
        WHERE ART_PessoaTelefone.Pessoa_Id = Pessoa.Id
        GROUP BY ART_PessoaTelefone.Pessoa_Id
    ) AS Telefones,
#===========================================================================
    DATE_FORMAT(MAX(ART_Passagem.DataHoraEmissao), '%Y-%m-%d') AS UltimaCompra,
#===========================================================================
    DATE_FORMAT(MAX(ART_Viagem.DataInicio), '%d/%m/%Y') AS DataViagem,
#===========================================================================
    DATEDIFF(@inicial, MAX(ART_Viagem.DataInicio)) AS Diferenca
#===========================================================================
FROM 
    ART_Pessoa AS Pessoa
LEFT JOIN ART_PessoaVinculo ON Pessoa.Id = ART_PessoaVinculo.Pessoa_Id
LEFT JOIN ART_Pessoa AS Vinculo ON ART_PessoaVinculo.Pessoa_Id_Vinculo = Vinculo.Id
LEFT JOIN ART_PessoaEndereco ON ART_PessoaEndereco.Pessoa_Id = Pessoa.Id
LEFT JOIN ART_Localidade on ART_Localidade.Id = ART_PessoaEndereco.Localidade_Id
JOIN ART_Passagem ON ( Pessoa.Id = ART_Passagem.Pessoa_Id OR Vinculo.Id = ART_Passagem.Pessoa_Id ) AND ART_Passagem.Situacao <> 'Cancelada'
LEFT JOIN ART_Viagem ON ART_Viagem.Id = ART_Passagem.Viagem_Id
#LEFT JOIN ART_Pessoa AS Passageiro ON ART_Passagem.Pessoa_Id = Passageiro.Id
WHERE 
    Pessoa.Pessoa_Id = 2 
GROUP BY Pessoa.Id
HAVING IF(@opcao, Diferenca <= @periodo, Diferenca > @periodo)
ORDER BY ART_Localidade.Nome, Pessoa.Nome ASC;