SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `veritasweb06` DEFAULT CHARACTER SET latin1 ;
USE `veritasweb06` ;

-- -----------------------------------------------------
-- Table `veritasweb06`.`art_pessoa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_pessoa` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL COMMENT 'Nome da pessoa ou Razão social da empresa',
  `NomeUsual` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'Nome usual da pessoa ou Nome fantasia da empresa',
  `Documento` VARCHAR(18) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'CPF para pessoa física e CNPJ para pessoa jurídica',
  `RGIE` VARCHAR(20) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'RG para pessoa física e IE para pessoa jurídica',
  `Email` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'E-mail da pessoal ou empresarial',
  `Sexo` ENUM('M','F') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `DataNascimento` DATE NULL DEFAULT NULL,
  `Telefone` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'Telefone da empresa',
  `Celular` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'Número de celular da empresa/contato responsável',
  `Senha` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'Senha do painel de controle do hambiente da Kinghost',
  `Pessoa_Id` INT(11) NULL DEFAULT NULL COMMENT 'Relacionamento recursivo para multiempresa',
  `Tipo` ENUM('PF Brasileiro','PJ Brasileiro','PF Estrangeiro','PJ Estrangeiro') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `EmPotencial` TINYINT(1) NULL DEFAULT '0',
  `VisualizarFidelizacao` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Verifica se o cliente tem permissão para fazer resgate de fidelização',
  `Pessoa_Id_Responsavel` INT(11) NULL DEFAULT NULL COMMENT 'Relacionamento da pessoa responsável pela empresa',
  `WebSite` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Pessoa_Pessoa_Id` (`Pessoa_Id` ASC),
  INDEX `FK_Pessoa_Pessoa_Id_Responsavel` (`Pessoa_Id_Responsavel` ASC),
  CONSTRAINT `FK_Pessoa_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Pessoa_Pessoa_Id_Responsavel`
    FOREIGN KEY (`Pessoa_Id_Responsavel`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci
COMMENT = 'Cadastro de Pessoas (Empresas e clientes)';


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_pais` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Sigla` CHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_uf`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_uf` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Sigla` CHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Pais_Id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_UF_Pais_Id` (`Pais_Id` ASC),
  CONSTRAINT `FK_UF_Pais_Id`
    FOREIGN KEY (`Pais_Id`)
    REFERENCES `veritasweb06`.`art_pais` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_localidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_localidade` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(70) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Tipo` ENUM('Município','Distrito','Povoado','Região administrativa') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Fuso` SMALLINT(6) NULL DEFAULT NULL,
  `UF_Id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Localidade_UF_Id` (`UF_Id` ASC),
  CONSTRAINT `FK_Localidade_UF_Id`
    FOREIGN KEY (`UF_Id`)
    REFERENCES `veritasweb06`.`art_uf` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_pontoreferencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_pontoreferencia` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Descricao` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Tipo` ENUM('Ponto de Referência','Agência','Garagem','Depósito','Ponto de Apoio','Transportadora','Agência de Turismo','Agência Terceirizada','Departamento') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Localidade_Id` INT(11) NOT NULL COMMENT 'Localidade da qual pertence o ponto de referência.',
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_PontoReferencia_localidade_Id` (`Localidade_Id` ASC),
  INDEX `FK_PontoReferencia` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_PontoReferencia`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_PontoReferencia_localidade_Id`
    FOREIGN KEY (`Localidade_Id`)
    REFERENCES `veritasweb06`.`art_localidade` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_linha`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_linha` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `PontoReferencia_Id_Destino` INT(11) NOT NULL COMMENT 'Destino da linha',
  `PontoReferencia_Id_Via` INT(11) NOT NULL COMMENT 'Via da linha',
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `Situacao` ENUM('Normal','Bloqueada') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Normal',
  `Duracao` INT(11) NOT NULL COMMENT 'Duração da viagem em dias',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK_Linha_PontoReferencia_Id_Destino` (`PontoReferencia_Id_Destino` ASC),
  INDEX `FK_Linha_PontoReferencia_Id_Via` (`PontoReferencia_Id_Via` ASC),
  INDEX `F_Linha_Pessoa_Id` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_Linha_PontoReferencia_Id_Destino`
    FOREIGN KEY (`PontoReferencia_Id_Destino`)
    REFERENCES `veritasweb06`.`art_pontoreferencia` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Linha_PontoReferencia_Id_Via`
    FOREIGN KEY (`PontoReferencia_Id_Via`)
    REFERENCES `veritasweb06`.`art_pontoreferencia` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `F_Linha_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_tipoadicionaltarifa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_tipoadicionaltarifa` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Descricao` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_adicionaltarifa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_adicionaltarifa` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Linha_Id` INT(11) NOT NULL COMMENT 'O valor adicional será para a linha independente do trecho',
  `TipoAdicionalTarifa_Id` INT(11) NOT NULL,
  `DataVigencia` DATE NOT NULL COMMENT 'Data do início da vigência',
  `Valor` DOUBLE(15,2) NULL DEFAULT NULL,
  `Ida` TINYINT(1) NOT NULL DEFAULT '1',
  `Volta` TINYINT(1) NOT NULL DEFAULT '1',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_AdicionalTarifa_Linha_Id` (`Linha_Id` ASC),
  INDEX `FK_Tarifa_TipoAdicionalTarifa` (`TipoAdicionalTarifa_Id` ASC),
  CONSTRAINT `FK_AdicionalTarifa_Linha_Id`
    FOREIGN KEY (`Linha_Id`)
    REFERENCES `veritasweb06`.`art_linha` (`ID`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Tarifa_TipoAdicionalTarifa`
    FOREIGN KEY (`TipoAdicionalTarifa_Id`)
    REFERENCES `veritasweb06`.`art_tipoadicionaltarifa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_desconto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_desconto` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Descricao` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Percentagem` FLOAT NOT NULL,
  `Confirmado` ENUM('Sim','Não') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Não',
  `AgenciaWeb` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 Ambos 1 Agência 2 Web',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_mapaveiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_mapaveiculo` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Descricao` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_MapaVeiculo_Pessoa_Id` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_MapaVeiculo_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_horario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_horario` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Linha_Id` INT(11) NOT NULL,
  `Horario` TIME NOT NULL COMMENT 'Horário inicial de partida do ponto de referência deste horário',
  `Sentido` ENUM('Ida','Volta') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL COMMENT 'Sentido em que o horário irá trafegar com relação ao cadastrado na linha',
  `Frequencia` CHAR(7) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL COMMENT 'Frequência da viagem (DSTQQSS) ',
  `MapaVeiculo_Id` INT(11) NULL DEFAULT NULL,
  `Feriado` TINYINT(1) NOT NULL DEFAULT '1' COMMENT 'Feriado - Excluir, Incluir e Ignorar (0,1,2)',
  `Confirmado` ENUM('Sim','Não') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Não' COMMENT 'Define se o horário está confirmado para gerar viagem',
  `Situacao` ENUM('Normal','Bloqueado') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Normal',
  `Duracao` INT(11) NOT NULL COMMENT 'Duração da viagem em dias',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Horario_Linha_Id` (`Linha_Id` ASC),
  INDEX `fk_Horario_MapaVeiculo` (`MapaVeiculo_Id` ASC),
  CONSTRAINT `FK_Horario_Linha_Id`
    FOREIGN KEY (`Linha_Id`)
    REFERENCES `veritasweb06`.`art_linha` (`ID`)
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Horario_MapaVeiculo`
    FOREIGN KEY (`MapaVeiculo_Id`)
    REFERENCES `veritasweb06`.`art_mapaveiculo` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_descontohorario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_descontohorario` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Desconto_Id` INT(11) NOT NULL,
  `Horario_Id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_DescontoHorario_Desconto_Id` (`Desconto_Id` ASC),
  INDEX `FK_DescontoHorario_Horario_Id` (`Horario_Id` ASC),
  CONSTRAINT `FK_DescontoHorario_Desconto_Id`
    FOREIGN KEY (`Desconto_Id`)
    REFERENCES `veritasweb06`.`art_desconto` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_DescontoHorario_Horario_Id`
    FOREIGN KEY (`Horario_Id`)
    REFERENCES `veritasweb06`.`art_horario` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_descontoitinerario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_descontoitinerario` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Desconto_Id` INT(11) NOT NULL,
  `PontoReferencia_Id_Inicial` INT(11) NOT NULL,
  `PontoReferencia_Id_Final` INT(11) NOT NULL,
  `Percentagem` FLOAT NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_DescontoItinerario_Desconto_Id` (`Desconto_Id` ASC),
  INDEX `FK_DescontoItinerario_PontoReferencia_Id_Inicial` (`PontoReferencia_Id_Inicial` ASC),
  INDEX `FK_DescontoItinerario_PontoReferencia_Id_Final` (`PontoReferencia_Id_Final` ASC),
  CONSTRAINT `FK_DescontoItinerario_Desconto_Id`
    FOREIGN KEY (`Desconto_Id`)
    REFERENCES `veritasweb06`.`art_desconto` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_DescontoItinerario_PontoReferencia_Id_Final`
    FOREIGN KEY (`PontoReferencia_Id_Final`)
    REFERENCES `veritasweb06`.`art_pontoreferencia` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_DescontoItinerario_PontoReferencia_Id_Inicial`
    FOREIGN KEY (`PontoReferencia_Id_Inicial`)
    REFERENCES `veritasweb06`.`art_pontoreferencia` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_descontolinha`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_descontolinha` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Desconto_Id` INT(11) NOT NULL,
  `Linha_Id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_DescontoLinha_Desconto_Id` (`Desconto_Id` ASC),
  INDEX `FK_DescontoLinha_Linha_Id` (`Linha_Id` ASC),
  CONSTRAINT `FK_DescontoLinha_Desconto_Id`
    FOREIGN KEY (`Desconto_Id`)
    REFERENCES `veritasweb06`.`art_desconto` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_DescontoLinha_Linha_Id`
    FOREIGN KEY (`Linha_Id`)
    REFERENCES `veritasweb06`.`art_linha` (`ID`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_descontopoltrona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_descontopoltrona` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Desconto_Id` INT(11) NOT NULL,
  `Poltrona` TINYINT(3) NOT NULL,
  `Percentagem` FLOAT NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_DescontoPoltrona_Desconto_Id` (`Desconto_Id` ASC),
  CONSTRAINT `FK_DescontoPoltrona_Desconto_Id`
    FOREIGN KEY (`Desconto_Id`)
    REFERENCES `veritasweb06`.`art_desconto` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_viagem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_viagem` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `DataInicio` DATE NOT NULL COMMENT 'Data do inicio da viagem',
  `HoraInicio` TIME NOT NULL COMMENT 'Hora do inicio da viagem',
  `Horario_Id` INT(11) NOT NULL,
  `MapaVeiculo_Id` INT(11) NOT NULL,
  `Confirmada` ENUM('Sim','Não') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Não' COMMENT 'Define se a viagem está confirmada para aparecer na consulta',
  `Situacao` ENUM('Normal','Bloqueada') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Normal',
  `Duracao` INT(11) NOT NULL COMMENT 'Duração da viagem em dias',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Viatem_Horario_Id` (`Horario_Id` ASC),
  INDEX `Fk_Viagem_MapaVeiculo_Id` (`MapaVeiculo_Id` ASC),
  CONSTRAINT `Fk_Viagem_MapaVeiculo_Id`
    FOREIGN KEY (`MapaVeiculo_Id`)
    REFERENCES `veritasweb06`.`art_mapaveiculo` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Viatem_Horario_Id`
    FOREIGN KEY (`Horario_Id`)
    REFERENCES `veritasweb06`.`art_horario` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_descontoviagem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_descontoviagem` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Desconto_Id` INT(11) NOT NULL,
  `Viagem_Id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_DescontoViagem_Desconto_Id` (`Desconto_Id` ASC),
  INDEX `FK_DescontoViagem_Viagem_Id` (`Viagem_Id` ASC),
  CONSTRAINT `FK_DescontoViagem_Desconto_Id`
    FOREIGN KEY (`Desconto_Id`)
    REFERENCES `veritasweb06`.`art_desconto` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_DescontoViagem_Viagem_Id`
    FOREIGN KEY (`Viagem_Id`)
    REFERENCES `veritasweb06`.`art_viagem` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_feriados`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_feriados` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `UF_Id` INT(11) NULL DEFAULT NULL,
  `TIPO` ENUM('Fixo','Móvel') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Fixo',
  `Dia` TINYINT(2) UNSIGNED NULL DEFAULT NULL,
  `Mes` TINYINT(2) UNSIGNED NULL DEFAULT NULL,
  `Descricao` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `DataMovel` DATE NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Feriado_UF_Id` (`UF_Id` ASC),
  CONSTRAINT `FK_Feriado_UF_Id`
    FOREIGN KEY (`UF_Id`)
    REFERENCES `veritasweb06`.`art_uf` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_venda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_venda` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `PontoReferencia_Id` INT(11) NOT NULL COMMENT 'Ponto de referência emissor da(s) passagem(ns)',
  `Pessoa_Id_Cliente` INT(11) NOT NULL COMMENT 'Cliente que esta comprando passagem',
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `Usuario_Id` INT(11) NULL DEFAULT NULL COMMENT 'Relacionamento com o usuário - Agente emitente',
  `DataHoraEmissao` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ValorDesconto` FLOAT(10,2) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Venda_PontoReferencia_Id` (`PontoReferencia_Id` ASC),
  INDEX `FK_Venda_Cliente_Id` (`Pessoa_Id_Cliente` ASC),
  INDEX `FK_Venda_Pessoa_Id` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_Venda_Cliente_Id`
    FOREIGN KEY (`Pessoa_Id_Cliente`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Venda_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Venda_PontoReferencia_Id`
    FOREIGN KEY (`PontoReferencia_Id`)
    REFERENCES `veritasweb06`.`art_pontoreferencia` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_fidelizacao`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_fidelizacao` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Operacao` ENUM('Débito','Crédito') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Descricao` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `DataOperacao` DATE NOT NULL,
  `ValorOperacao` FLOAT(10,2) NOT NULL,
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Cliente',
  `Venda_Id` INT(11) NULL DEFAULT NULL COMMENT 'Relacionamento com a venda quando for utilizado o saldo',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Fidelizacao_Pessoa_Id` (`Pessoa_Id` ASC),
  INDEX `FK_Fidelizacao_Venda_Id` (`Venda_Id` ASC),
  CONSTRAINT `FK_Fidelizacao_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Fidelizacao_Venda_Id`
    FOREIGN KEY (`Venda_Id`)
    REFERENCES `veritasweb06`.`art_venda` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci
COMMENT = 'Fidelização de clientes (Mega Polo)';


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_linhaitem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_linhaitem` (
  `ID` INT(11) NOT NULL AUTO_INCREMENT,
  `Linha_Id` INT(11) NOT NULL,
  `PontoReferencia_Id` INT(11) NOT NULL,
  `Sequencia` TINYINT(4) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK_LinhaItem_Linha_Id` (`Linha_Id` ASC),
  INDEX `FK_LinhaItem_PontoReferencia_Id` (`PontoReferencia_Id` ASC),
  CONSTRAINT `FK_LinhaItem_Linha_Id`
    FOREIGN KEY (`Linha_Id`)
    REFERENCES `veritasweb06`.`art_linha` (`ID`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_LinhaItem_PontoReferencia_Id`
    FOREIGN KEY (`PontoReferencia_Id`)
    REFERENCES `veritasweb06`.`art_pontoreferencia` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_horarioitem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_horarioitem` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Horario_Id` INT(11) NOT NULL,
  `LinhaItem_Id` INT(11) NOT NULL,
  `HoraEmbarque` TIME NOT NULL,
  `DiaEmbarque` TINYINT(4) NOT NULL DEFAULT '0',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_HorarioItem_Horario_Id` (`Horario_Id` ASC),
  INDEX `FK_HorarioItem_LinhaItem_Id` (`LinhaItem_Id` ASC),
  CONSTRAINT `FK_HorarioItem_Horario_Id`
    FOREIGN KEY (`Horario_Id`)
    REFERENCES `veritasweb06`.`art_horario` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_HorarioItem_LinhaItem_Id`
    FOREIGN KEY (`LinhaItem_Id`)
    REFERENCES `veritasweb06`.`art_linhaitem` (`ID`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_tipopoltrona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_tipopoltrona` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Descricao` VARCHAR(40) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_TipoPoltrona_Pessoa_Id` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_TipoPoltrona_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_mapaveiculopoltrona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_mapaveiculopoltrona` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `MapaVeiculo_Id` INT(11) NOT NULL,
  `TipoPoltrona_Id` INT(11) NOT NULL,
  `Numero` INT(11) NOT NULL COMMENT 'Número da poltrona',
  `Linha` INT(11) NOT NULL COMMENT 'Linha em que a poltrona esta posicionada',
  `Coluna` INT(11) NOT NULL COMMENT 'Coluna em que a poltrona esta posicionada',
  `Andar` INT(11) NOT NULL COMMENT 'Andar onde esta localizada a poltrona',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_MapaVeiculoPoltrona_MapaVeiculo_Id` (`MapaVeiculo_Id` ASC),
  INDEX `FK_TipoPoltrona_TipoPoltrona_Id` (`TipoPoltrona_Id` ASC),
  CONSTRAINT `FK_MapaVeiculoPoltrona_MapaVeiculo_Id`
    FOREIGN KEY (`MapaVeiculo_Id`)
    REFERENCES `veritasweb06`.`art_mapaveiculo` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_TipoPoltrona_TipoPoltrona_Id`
    FOREIGN KEY (`TipoPoltrona_Id`)
    REFERENCES `veritasweb06`.`art_tipopoltrona` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_parametro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_parametro` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Valor` VARCHAR(200) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Descricao` VARCHAR(100) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_viagemitem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_viagemitem` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Viagem_Id` INT(11) NOT NULL,
  `HorarioItem_Id` INT(11) NOT NULL,
  `DataHoraEmbarque` DATETIME NOT NULL,
  `Ativo` TINYINT(1) NOT NULL DEFAULT '1',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_ViatemItem_Viagem_Id` (`Viagem_Id` ASC),
  INDEX `FK_ViatemItem_HorarioItem_Id` (`HorarioItem_Id` ASC),
  CONSTRAINT `FK_ViatemItem_HorarioItem_Id`
    FOREIGN KEY (`HorarioItem_Id`)
    REFERENCES `veritasweb06`.`art_horarioitem` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_ViatemItem_Viagem_Id`
    FOREIGN KEY (`Viagem_Id`)
    REFERENCES `veritasweb06`.`art_viagem` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_passagem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_passagem` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `DataHoraEmissao` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Valor` FLOAT(10,2) NULL DEFAULT NULL,
  `Poltrona` CHAR(2) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Viagem_Id` INT(11) NOT NULL,
  `ViagemItem_Id_Origem` INT(11) NOT NULL,
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Passageiro',
  `Tipo` ENUM('R','P') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL COMMENT 'R = Reserva, P = Passagem',
  `Parcial` ENUM('C','I','V') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'C' COMMENT 'C = Cheia, I = Ida, V = Volta',
  `Situacao` ENUM('Normal','Cancelada') CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL DEFAULT 'Normal',
  `ReservaExpiracao` DATETIME NULL DEFAULT NULL COMMENT 'Quando for reserva informar a data de expiração da reserva',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Passagem_Viagem_Id` (`Viagem_Id` ASC),
  INDEX `FK_Passagem_ViagemItem_Id_Origem` (`ViagemItem_Id_Origem` ASC),
  INDEX `FK_Passagem_Pessoa_Id` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_Passagem_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Passagem_ViagemItem_Id_Origem`
    FOREIGN KEY (`ViagemItem_Id_Origem`)
    REFERENCES `veritasweb06`.`art_viagemitem` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Passagem_Viagem_Id`
    FOREIGN KEY (`Viagem_Id`)
    REFERENCES `veritasweb06`.`art_viagem` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_pessoaendereco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_pessoaendereco` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Logradouro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Numero` VARCHAR(10) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Bairro` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Complemento` VARCHAR(50) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Cep` CHAR(9) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Localidade_Id` INT(11) NOT NULL,
  `Pessoa_Id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_PessoaEndereco_Pessoa_Id` (`Pessoa_Id` ASC),
  INDEX `FK_PessoaEndereco_Localidade_Id` (`Localidade_Id` ASC),
  CONSTRAINT `FK_PessoaEndereco_Localidade_Id`
    FOREIGN KEY (`Localidade_Id`)
    REFERENCES `veritasweb06`.`art_localidade` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_PessoaEndereco_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_pessoatelefone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_pessoatelefone` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Pessoa_Id` INT(11) NOT NULL,
  `Descricao` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Telefone` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_PessoaTelefone_Pessoa_Id` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_PessoaTelefone_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_pessoavinculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_pessoavinculo` (
  `Pessoa_Id` INT(11) NOT NULL,
  `Pessoa_Id_Vinculo` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Pessoa_Id`, `Pessoa_Id_Vinculo`),
  INDEX `FK_PessoaVinculo_Pessoa_Id_Vinculo` (`Pessoa_Id_Vinculo` ASC),
  CONSTRAINT `FK_PessoaVinculo_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_PessoaVinculo_Pessoa_Id_Vinculo`
    FOREIGN KEY (`Pessoa_Id_Vinculo`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_semafaro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_semafaro` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Viagem_Id` INT(11) NOT NULL,
  `Poltrona` INT(11) NOT NULL,
  `ViagemItem_Id_Origem` INT(11) NOT NULL,
  `DataHora` DATETIME NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Semafaro_Viagem_Id` (`Viagem_Id` ASC),
  INDEX `FK_Semafaro_ViagemItem_Id` (`ViagemItem_Id_Origem` ASC),
  CONSTRAINT `FK_Semafaro_ViagemItem_Id`
    FOREIGN KEY (`ViagemItem_Id_Origem`)
    REFERENCES `veritasweb06`.`art_viagemitem` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Semafaro_Viagem_Id`
    FOREIGN KEY (`Viagem_Id`)
    REFERENCES `veritasweb06`.`art_viagem` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_tarifa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_tarifa` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `LinhaItem_Id` INT(11) NOT NULL COMMENT 'Tarifa do ponto de referência para o destino da linha',
  `TipoPoltrona_Id` INT(11) NOT NULL COMMENT 'O valor pode diferenciar pelo tipo de poltrona',
  `DataVigencia` DATE NOT NULL COMMENT 'Data do início da vigência',
  `Valor` DOUBLE(15,2) NULL DEFAULT '0.00',
  `Valor2` DOUBLE(15,2) NULL DEFAULT '0.00',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Tarifa_LinhaItem_Id` (`LinhaItem_Id` ASC),
  INDEX `FK_Tarifa_TipoPoltrona` (`TipoPoltrona_Id` ASC),
  CONSTRAINT `FK_Tarifa_LinhaItem_Id`
    FOREIGN KEY (`LinhaItem_Id`)
    REFERENCES `veritasweb06`.`art_linhaitem` (`ID`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Tarifa_TipoPoltrona`
    FOREIGN KEY (`TipoPoltrona_Id`)
    REFERENCES `veritasweb06`.`art_tipopoltrona` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_usuario` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Nome` VARCHAR(180) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Email` VARCHAR(80) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Senha` VARCHAR(80) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `Pessoa_Id` INT(11) NOT NULL,
  `PontoReferencia_Id_Agencia` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Usuario_Pessoa_Id` (`Pessoa_Id` ASC),
  INDEX `FK_Usuario_PontoReferencia_Id_Agencia` (`PontoReferencia_Id_Agencia` ASC),
  CONSTRAINT `FK_Usuario_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Usuario_PontoReferencia_Id_Agencia`
    FOREIGN KEY (`PontoReferencia_Id_Agencia`)
    REFERENCES `veritasweb06`.`art_pontoreferencia` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_veiculo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_veiculo` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Numero` VARCHAR(15) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NULL DEFAULT NULL,
  `Placa` VARCHAR(8) CHARACTER SET 'utf8' COLLATE 'utf8_swedish_ci' NOT NULL,
  `MapaVeiculo_Id` INT(11) NOT NULL,
  `Pessoa_Id` INT(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_Veiculo_MapaVeiculo` (`MapaVeiculo_Id` ASC),
  INDEX `FK_Veiculo_Pessoa_Id` (`Pessoa_Id` ASC),
  CONSTRAINT `FK_Veiculo_MapaVeiculo`
    FOREIGN KEY (`MapaVeiculo_Id`)
    REFERENCES `veritasweb06`.`art_mapaveiculo` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Veiculo_Pessoa_Id`
    FOREIGN KEY (`Pessoa_Id`)
    REFERENCES `veritasweb06`.`art_pessoa` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;


-- -----------------------------------------------------
-- Table `veritasweb06`.`art_vendaitem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `veritasweb06`.`art_vendaitem` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT,
  `Venda_Id` INT(11) NOT NULL,
  `Passagem_Id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`Id`),
  INDEX `FK_VendaItem_Venda_Id` (`Venda_Id` ASC),
  INDEX `FK_VendaItem_Passagem_Id` (`Passagem_Id` ASC),
  CONSTRAINT `FK_VendaItem_Passagem_Id`
    FOREIGN KEY (`Passagem_Id`)
    REFERENCES `veritasweb06`.`art_passagem` (`Id`)
    ON UPDATE CASCADE,
  CONSTRAINT `FK_VendaItem_Venda_Id`
    FOREIGN KEY (`Venda_Id`)
    REFERENCES `veritasweb06`.`art_venda` (`Id`)
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_swedish_ci;

USE `veritasweb06` ;

-- -----------------------------------------------------
-- procedure Proc_ConsHorarios
-- -----------------------------------------------------

DELIMITER $$
USE `veritasweb06`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Proc_ConsHorarios`(
  pDataViagem        DATE,
  pOrigem            INTEGER,
  pDestino           INTEGER,
  pAgenciaWeb        INTEGER,
  pInicio            INTEGER,
  pLimite            INTEGER)
BEGIN
  DECLARE vDataValidacao DATE;

  IF pDataViagem IS NULL THEN
    SET vDataValidacao = CURRENT_DATE;
  ELSE  
    SET vDataValidacao = pDataViagem;
  END IF;  

SELECT 
  ART_Viagem.Id,	
  ART_Viagem.DataInicio,
  ART_Viagem.HoraInicio,	
  ART_Viagem.Horario_Id,	
  ART_Horario.Horario AS Horario,
  ART_ViagemItem.DataHoraEmbarque,
  PontoReferenciaDestino.Id AS PontoReferenciaDestino_Id,	
  PontoReferenciaDestino.Descricao AS PontoReferenciaDestino_Descricao,	
  ART_Viagem.TipoVeiculo_Id,	
  ART_TipoVeiculo.Descricao AS TipoVeiculo_Descricao,
  ART_Viagem.Confirmada,	
  ART_Viagem.Situacao,
  ART_Viagem.Duracao,
  ART_Viagem.Descricao
  
/*
ART_Viagem.Id
ART_PontoReferencia.Id
pAgenciaWeb 
Currentedate

  CALL PROC_ConsTarifa(14, 1, 2, "2014-03-05");
*/  
  
FROM
  ART_Viagem
  JOIN ART_ViagemItem ON ART_ViagemItem.Viagem_Id = ART_Viagem.Id 
  JOIN ART_Horario ON ART_Horario.Id = ART_Viagem.Horario_Id
  JOIN ART_HorarioItem ON ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
  JOIN ART_LinhaItem ON ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id
  JOIN ART_TipoVeiculo ON ART_TipoVeiculo.Id = ART_Viagem.TipoVeiculo_Id 
  JOIN ART_Linha ON ART_Linha.Id = ART_Horario.Linha_Id  
  JOIN ART_PontoReferencia AS PontoReferenciaDestino ON PontoReferenciaDestino.Id = ART_Linha.PontoReferencia_Id_Destino
  JOIN ART_PontoReferencia ON ART_PontoReferencia.Id = ART_LinhaItem.PontoReferencia_Id AND
    ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id AND 
    ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
WHERE
  ART_Linha.PontoReferencia_Id_Destino = pDestino AND
  ART_PontoReferencia.Id = pOrigem AND
  (pDataViagem IS NULL OR ART_Viagem.DataInicio = pDataViagem) AND
  ART_Linha.PontoReferencia_Id_Destino <> pOrigem AND
  ART_Viagem.DataInicio >= vDataValidacao
ORDER BY
  ART_ViagemItem.DataHoraEmbarque
LIMIT
  pInicio, pLimite;  
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Proc_ConsHorariosQTd
-- -----------------------------------------------------

DELIMITER $$
USE `veritasweb06`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Proc_ConsHorariosQTd`(
  pDataViagem        DATE,
  pOrigem            INTEGER,
  pDestino           INTEGER)
BEGIN
  DECLARE vDataValidacao DATE;

  IF pDataViagem IS NULL THEN
    SET vDataValidacao = CURRENT_DATE;
  ELSE  
    SET vDataValidacao = pDataViagem;
  END IF;  

SELECT 
  Count(ART_Viagem.Id) AS Total
FROM
  ART_Viagem
  JOIN ART_ViagemItem ON ART_ViagemItem.Viagem_Id = ART_Viagem.Id 
  JOIN ART_Horario ON ART_Horario.Id = ART_Viagem.Horario_Id
  JOIN ART_HorarioItem ON ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
  JOIN ART_LinhaItem ON ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id
  JOIN ART_TipoVeiculo ON ART_TipoVeiculo.Id = ART_Viagem.TipoVeiculo_Id 
  JOIN ART_Linha ON ART_Linha.Id = ART_Horario.Linha_Id  
  JOIN ART_PontoReferencia AS PontoReferenciaDestino ON PontoReferenciaDestino.Id = ART_Linha.PontoReferencia_Id_Destino
  JOIN ART_PontoReferencia ON ART_PontoReferencia.Id = ART_LinhaItem.PontoReferencia_Id AND
    ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id AND 
    ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
WHERE
  ART_Linha.PontoReferencia_Id_Destino = pDestino AND
  ART_PontoReferencia.Id = pOrigem AND
  (pDataViagem IS NULL OR ART_Viagem.DataInicio = pDataViagem) AND
  ART_Linha.PontoReferencia_Id_Destino <> pOrigem AND
  ART_Viagem.DataInicio >= vDataValidacao;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Proc_ConsMapaLugarViagem
-- -----------------------------------------------------

DELIMITER $$
USE `veritasweb06`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Proc_ConsMapaLugarViagem`(
  pViagem_Id INTEGER)
BEGIN
  SELECT
    Consulta.Numero,
    Consulta.Linha,
    Consulta.Coluna,
    Consulta.Andar,
    IF(Consulta.Passagem_Id IS NULL, "mlLivre", 
    (SELECT CONCAT(
    IF(ART_Passagem.Tipo = "P", "mlOcupada", "mlReserva"),
    IF(ART_Pessoa.Sexo = "M", "M", "F"))
    FROM ART_Passagem 
    JOIN ART_Pessoa ON ART_Pessoa.Id = ART_Passagem.Pessoa_Id 
    WHERE ART_Passagem.Id = Consulta.Passagem_Id)
    ) AS Situacao,    
    Consulta.Parcial,
    "" AS Promocao,
    Consulta.Passagem_Id
  FROM
    (SELECT
      ART_TipoVeiculoMapa.Numero,
      ART_TipoVeiculoMapa.Linha,
      ART_TipoVeiculoMapa.Coluna,
      ART_TipoVeiculoMapa.Andar,
      ART_Passagem.Id AS Passagem_Id,
      ART_Passagem.Parcial
    FROM
      ART_Viagem
      JOIN ART_TipoVeiculo ON ART_TipoVeiculo.Id = ART_Viagem.TipoVeiculo_Id
      JOIN ART_TipoVeiculoMapa ON ART_TipoVeiculoMapa.TipoVeiculo_Id = ART_TipoVeiculo.Id
      LEFT JOIN ART_Passagem ON ART_Passagem.Viagem_Id = ART_Viagem.Id AND 
        ART_Passagem.Situacao = "Normal" AND ART_Passagem.Poltrona = ART_TipoVeiculoMapa.Numero  
    WHERE
      ART_Viagem.Id = pViagem_Id) AS Consulta
  ORDER BY
    Consulta.Andar,
    Consulta.Linha,
    Consulta.Coluna;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Proc_ConsTarifa
-- -----------------------------------------------------

DELIMITER $$
USE `veritasweb06`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Proc_ConsTarifa`(
  pViagem_Id  INTEGER,
  pOrigem_Id  INTEGER,
  pAgenciaWeb INTEGER,
pDataCompra DATE)
BEGIN
  SELECT
    PontoReferenciaOrigem_Descricao,
    PontoReferenciaDestino_Descricao,
    Valor AS TarifaPiso1, 
    Valor2 AS TarifaPiso2,
    AdicionalTarifa,
    Desconto,
    ((Valor + AdicionalTarifa) - Desconto) AS TotalTarifaPiso1,
    ((Valor2 + AdicionalTarifa) - Desconto) AS TotalTarifaPiso2
  FROM
    (SELECT
      Origem.Descricao AS PontoReferenciaOrigem_Descricao,
      Destino.Descricao AS PontoReferenciaDestino_Descricao,
      /* Tarifa piso1 e tarifa piso2 */  
      ART_Tarifa.Valor,
      ART_Tarifa.Valor2,
      /* Adicional da tarifa */
      IFNULL((SELECT SUM(Valor) FROM ART_AdicionalTarifa 
      WHERE ART_AdicionalTarifa.Linha_Id = ART_Linha.Id
      AND ART_AdicionalTarifa.DataVigencia >= pDataCompra), 0) AS AdicionalTarifa,  
      /* Desconto */
      IFNULL((SELECT ART_Desconto.Percentagem FROM ART_Desconto 
      WHERE (ART_Desconto.AgenciaWeb = pAgenciaWeb or ART_Desconto.AgenciaWeb = "0")
      AND ART_Desconto.Confirmado), 0) AS Desconto
    FROM
      ART_Viagem
      JOIN ART_ViagemItem ON ART_ViagemItem.Viagem_Id = ART_Viagem.Id
      JOIN ART_HorarioItem ON ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id  
      JOIN ART_LinhaItem ON ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id
      JOIN ART_Linha ON ART_Linha.Id = ART_LinhaItem.Linha_Id
      JOIN ART_PontoReferencia AS Origem ON Origem.Id = ART_LinhaItem.PontoReferencia_Id
      JOIN ART_PontoReferencia AS Destino ON Destino.Id = ART_Linha.PontoReferencia_Id_Destino  
      JOIN ART_Tarifa ON ART_Tarifa.LinhaItem_Id = ART_LinhaItem.Id AND
        ART_Tarifa.TipoVeiculo_Id = ART_Viagem.TipoVeiculo_Id
    WHERE
      ART_Viagem.Id = pViagem_Id AND
      Origem.Id = pOrigem_Id) AS Consulta;      
      
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Proc_GerarViagem
-- -----------------------------------------------------

DELIMITER $$
USE `veritasweb06`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Proc_GerarViagem`(
  pDataInicio DATE,
  pDataFim    DATE,
  pHorario_Id  INTEGER)
BEGIN
  DECLARE vValidacao TINYINT;
  DECLARE vFeriado TINYINT;
  DECLARE vHorarioFeriado TINYINT;
  DECLARE vFrequencia TINYINT;

  DECLARE vDomingo TINYINT;
  DECLARE vSegunda TINYINT;
  DECLARE vTerca TINYINT;
  DECLARE vQuarta TINYINT;
  DECLARE vQuinta TINYINT;
  DECLARE vSexta TINYINT;
  DECLARE vSabado TINYINT;

  DECLARE vDataInicio DATE;
  DECLARE vHorario TIME;
  DECLARE vTipoVeiculo_Id INTEGER;
  DECLARE vQtdDias INTEGER;
  DECLARE vQtd INTEGER;
  DECLARE vViagem_Id INTEGER;
  DECLARE vDuracao INTEGER;
  DECLARE vDescricao VARCHAR(50);

  DECLARE viHorarioItem_Id INTEGER;
  DECLARE viHoraEmbarque TIME;
  DECLARE viDiaEmbarque TINYINT;
  DECLARE vEOF TINYINT;
  DECLARE vFim TINYINT;

  DECLARE Cur_HorarioItem CURSOR FOR
    SELECT Id AS HorarioItem_Id, HoraEmbarque, DiaEmbarque
    FROM ART_HorarioItem
    WHERE Horario_Id = pHorario_Id;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET vEOF = TRUE;

  SET vValidacao = 0;

  /*Validar se a Linha está ok - Retorno 1*/	
  SELECT IF(ART_Linha.Situacao = 'Normal', 0, 1) INTO vValidacao
  FROM ART_Linha 
  JOIN ART_Horario ON ART_Horario.Linha_Id = ART_Linha.Id
  WHERE ART_Horario.Id = pHorario_Id;
  
  IF vValidacao = 0 THEN
    /*Validar Situação e se Horário esta Confirmado - Retorno 2*/
    SELECT IF((IF(Situacao = 'Normal', 0, 2) + IF(Confirmado = 'Sim', 0, 2)) = 0, 0,2) INTO vValidacao
    FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;
  END IF;

  IF vValidacao = 0 THEN
    SET vDataInicio = pDataInicio;

    SELECT ART_Horario.Horario INTO vHorario FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT ART_Horario.TipoVeiculo_Id INTO vTipoVeiculo_Id FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT ART_Horario.Duracao INTO vDuracao FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT ART_Horario.Descricao INTO vDescricao FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;    

    SELECT (DATEDIFF(pDataFim, pDataInicio)) INTO vQtdDias;
    
    SET vQTD = 0;
    WHILE vQtd <= vQtdDias DO
      
      /*Verifica se o horário tráfega em feriados*/
      SELECT Feriado INTO vHorarioFeriado FROM ART_Horario WHERE Id = pHorario_Id;

      SET vFeriado = 0;
      IF vHorarioFeriado = 0 THEN
        /*Se o horário tráfega em feriados então verifica se a data é feriado*/
        SELECT EXISTS(
        SELECT ART_Feriados.Id FROM ART_Feriados
        WHERE ART_Feriados.DataMovel = DATE_ADD(vDataInicio, INTERVAL vQtd DAY) OR
        (ART_Feriados.Dia = DAY(DATE_ADD(vDataInicio, INTERVAL vQtd DAY)) AND
        ART_Feriados.Mes = MONTH(DATE_ADD(vDataInicio, INTERVAL vQtd DAY)))) INTO vFeriado;
      END IF;

      /*Verificar a frequência do horário*/
      SELECT SUBSTRING(Frequencia FROM 1 FOR 1) INTO vDomingo FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 2 FOR 1) INTO vSegunda FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 3 FOR 1) INTO vTerca FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 4 FOR 1) INTO vQuarta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 5 FOR 1) INTO vQuinta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 6 FOR 1) INTO vSexta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 7 FOR 1) INTO vSabado FROM ART_Horario WHERE Id = pHorario_Id;
            
      SELECT 
        CASE DATE_FORMAT(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), '%w')
          WHEN 0 THEN IF(vDomingo = 1, "1", "0")
          WHEN 1 THEN IF(vSegunda = 1, "1", "0")
          WHEN 2 THEN IF(vTerca = 1, "1", "0")
          WHEN 3 THEN IF(vQuarta = 1, "1", "0")
          WHEN 4 THEN IF(vQuinta = 1, "1", "0")
          WHEN 5 THEN IF(vSexta = 1, "1", "0")
          WHEN 6 THEN IF(vSabado = 1, "1", "0")
      END INTO vFrequencia;
      
      IF (vFeriado = 0) AND (vFrequencia = 1) THEN
        /* Grava na tabela de viagens */
        INSERT INTO ART_Viagem(DataInicio, HoraInicio, Horario_Id, TipoVeiculo_Id, Confirmada, Situacao, Duracao, Descricao)
          VALUES(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), vHorario, pHorario_Id, vTipoVeiculo_Id, "Sim", "Normal", vDuracao, vDescricao);
        
        /* Busca o Id da viagem gravada */
        SET vViagem_Id = (SELECT LAST_INSERT_ID());
          
        /* Grava na tabela de viagem item  */
        OPEN Cur_HorarioItem;
        REPEAT
          SET vEOF = FALSE;
          FETCH Cur_HorarioItem INTO viHorarioItem_Id, viHoraEmbarque, viDiaEmbarque;
          SET vFim = vEOF;
          IF NOT vFim THEN
            INSERT INTO ART_ViagemItem(Viagem_Id, HorarioItem_Id, DataHoraEmbarque, Ativo) 
            VALUES(vViagem_Id, viHorarioItem_Id,
              TIMESTAMP(DATE_ADD(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), INTERVAL viDiaEmbarque DAY), viHoraEmbarque), 1); 
          END IF;
        UNTIL vFim END REPEAT;
        CLOSE Cur_HorarioItem;
      END IF;
      
      SET vQtd = vQtd + 1;
    END WHILE;
    SELECT "Procedimento executado com sucesso." AS Mensagem;
  ELSE
  BEGIN
    IF vValidacao = 1 THEN
      SELECT "A linha deste horário está bloqueada." AS Mensagem;
    ELSE IF vValidacao = 2 THEN
      SELECT "Este Horário está Bloqueado ou não confirmado." AS Mensagem;
    END IF;  
    END IF;  
  END;  
  END IF;  
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure Proc_MarcarPoltrona
-- -----------------------------------------------------

DELIMITER $$
USE `veritasweb06`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `Proc_MarcarPoltrona`(
  pViagem_Id     INTEGER,
  pViagemItem_Id INTEGER,
  pPoltrona      INTEGER)
BEGIN
  DECLARE vPassagem_Id INTEGER;
  DECLARE vSemafaro_Id INTEGER;
  
  /* Verificar */
    /* -> Verificar passagem */
    SELECT
      ART_Passagem.Id INTO vPassagem_Id
    FROM
      ART_Passagem 
    WHERE
      ART_Passagem.Viagem_Id = pViagem_Id AND
      ART_Passagem.ViagemItem_Id_Origem = pViagemItem_Id AND
      ART_Passagem.Poltrona = pPoltrona AND   
      ART_Passagem.Situacao = "Normal" AND
      ((ART_Passagem.Tipo = "P") OR (ART_Passagem.Tipo = "R" AND ART_Passagem.ReservaExpiracao > NOW()));

    /* -> Verificar semáfaro */
    SELECT
      ART_Semafaro.Id INTO vSemafaro_Id
    FROM
      ART_Semafaro
    WHERE
      ART_Semafaro.Viagem_Id = pViagem_Id AND
      ART_Semafaro.ViagemItem_Id_Origem = pViagemItem_Id AND
      ART_Semafaro.Poltrona = pPoltrona;
  
  /* Desmarcar */
  
  /* Marcar */
  IF (vPassagem_Id IS NULL) AND (vSemafaro_Id IS NULL) THEN
    INSERT INTO ART_Semafaro(Viagem_Id, Poltrona, ViagemItem_Id_Origem, DataHora)
    VALUES(pViagem_Id, pPoltrona, pViagemItem_Id, NOW());
  ELSE
    SELECT "Poltrona não disponivel";    
  END IF;  
END$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
