CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_Horario`
--

DROP TABLE IF EXISTS `ART_Horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_Horario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Linha_Id` int(11) NOT NULL,
  `Horario` time NOT NULL COMMENT 'Horário inicial de partida do ponto de referência deste horário',
  `Sentido` enum('Ida','Volta') COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Sentido em que o horário irá trafegar com relação ao cadastrado na linha',
  `Frequencia` char(7) COLLATE utf8_swedish_ci NOT NULL COMMENT 'Frequência da viagem (DSTQQSS) ',
  `TipoVeiculo_Id` int(11) DEFAULT NULL,
  `Feriado` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Feriado - Excluir, Incluir e Ignorar (0,1,2)',
  `Confirmado` enum('Sim','Não') COLLATE utf8_swedish_ci NOT NULL DEFAULT 'Não' COMMENT 'Define se o horário está confirmado para gerar viagem',
  `Situacao` enum('Normal','Bloqueado') COLLATE utf8_swedish_ci NOT NULL DEFAULT 'Normal',
  `Duracao` int(11) NOT NULL COMMENT 'Duração da viagem em dias',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `Descricao` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Horario_Linha_Id` (`Linha_Id`),
  KEY `fk_Horario_TipoVeiculo` (`TipoVeiculo_Id`),
  CONSTRAINT `FK_Horario_Linha_Id` FOREIGN KEY (`Linha_Id`) REFERENCES `ART_Linha` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `fk_Horario_TipoVeiculo` FOREIGN KEY (`TipoVeiculo_Id`) REFERENCES `ART_TipoVeiculo` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=2048;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_Horario`
--

LOCK TABLES `ART_Horario` WRITE;
/*!40000 ALTER TABLE `ART_Horario` DISABLE KEYS */;
INSERT INTO `ART_Horario` VALUES (1,2,'12:00:00',NULL,'1000000',2,2,'Sim','Normal',1,'2014-02-26 17:13:05','2014-03-12 17:11:22',NULL,'Feirinha'),(2,2,'14:00:00',NULL,'0001000',3,0,'Sim','Normal',1,'2014-02-26 17:13:56','2014-03-31 08:58:39',NULL,'Bom Retiro, Feirinha, e Mega Polo'),(5,4,'09:30:00',NULL,'0000010',3,2,'Sim','Normal',2,'2014-02-27 08:30:46','2014-03-12 17:12:43',NULL,'Mega Moda'),(6,2,'14:00:00',NULL,'1000000',4,0,'Sim','Normal',2,'2014-03-12 11:22:59','2014-03-12 11:22:59',NULL,'Mega Polo'),(7,2,'14:00:00',NULL,'0001000',4,0,'Sim','Normal',2,'2014-03-12 11:23:31','2014-07-08 15:02:21',NULL,'Mega Polo/ Piratininga'),(11,6,'12:00:00',NULL,'1000000',5,2,'Sim','Normal',1,'2014-03-13 08:42:34','2014-03-13 08:42:34',NULL,'Minas Gerais'),(12,6,'12:00:00',NULL,'1000000',3,2,'Sim','Normal',2,'2014-03-13 08:47:02','2014-03-13 08:47:02',NULL,'Minas Gerais / São Paulo'),(13,7,'14:00:00',NULL,'0001000',5,2,'Sim','Normal',1,'2014-03-13 08:50:46','2014-03-13 08:50:46',NULL,'Ibitinga');
/*!40000 ALTER TABLE `ART_Horario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:16:03
