CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_Fidelizacao`
--

DROP TABLE IF EXISTS `ART_Fidelizacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_Fidelizacao` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Operacao` enum('Débito','Crédito') COLLATE utf8_swedish_ci DEFAULT NULL,
  `Descricao` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL,
  `DataOperacao` date NOT NULL,
  `ValorOperacao` float(10,2) NOT NULL,
  `Pessoa_Id` int(11) NOT NULL COMMENT 'Cliente',
  `Venda_Id` int(11) DEFAULT NULL COMMENT 'Relacionamento com a venda quando for utilizado o saldo',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Fidelizacao_Pessoa_Id` (`Pessoa_Id`),
  KEY `FK_Fidelizacao_Venda_Id` (`Venda_Id`),
  CONSTRAINT `FK_Fidelizacao_Pessoa_Id` FOREIGN KEY (`Pessoa_Id`) REFERENCES `ART_Pessoa` (`Id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_Fidelizacao_Venda_Id` FOREIGN KEY (`Venda_Id`) REFERENCES `ART_Venda` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=780 COMMENT='Fidelização de clientes (Mega Polo)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_Fidelizacao`
--

LOCK TABLES `ART_Fidelizacao` WRITE;
/*!40000 ALTER TABLE `ART_Fidelizacao` DISABLE KEYS */;
INSERT INTO `ART_Fidelizacao` VALUES (1,'Crédito','Bônus de 1% em compras no Mega Polo','2014-02-28',300.00,3,NULL,'2014-02-28 17:40:34','2014-02-28 17:40:34',NULL),(7,'Crédito','Mega Polo','2014-04-24',33.08,2781,NULL,'2014-06-13 15:18:31','2014-06-13 15:18:31',NULL),(8,'Crédito','Mega Polo','2014-03-31',128.15,2781,NULL,'2014-06-13 15:19:34','2014-06-13 15:19:34',NULL),(9,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-14',0.00,3376,36,'2014-06-14 10:20:13','2014-06-14 10:20:13',NULL),(10,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-24',0.00,269,66,'2014-06-24 10:51:24','2014-06-24 10:51:24',NULL),(11,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-24',0.00,1586,67,'2014-06-24 14:05:59','2014-06-24 14:05:59',NULL),(12,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-24',0.00,3329,68,'2014-06-24 14:18:54','2014-06-24 14:18:54',NULL),(13,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-24',0.00,3329,69,'2014-06-24 14:20:21','2014-06-24 14:20:21',NULL),(14,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-25',0.00,3376,70,'2014-06-25 14:03:31','2014-06-25 14:03:31',NULL),(15,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-25',0.00,3376,71,'2014-06-25 14:12:54','2014-06-25 14:12:54',NULL),(19,'Débito','Compra pelo sistema de retaguarda Artaban','2014-06-26',23.49,1308,72,'2014-06-26 16:32:30','2014-06-26 16:32:30',NULL),(20,'Crédito','MEGA POLO','2014-02-06',27.78,3618,NULL,'2014-06-27 14:09:15','2014-06-27 14:09:15',NULL),(21,'Crédito','MEGA POLO','2014-03-07',17.99,3618,NULL,'2014-06-27 14:09:58','2014-06-27 14:09:58',NULL),(23,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-28',0.00,3375,97,'2014-06-28 08:25:04','2014-06-28 08:25:04',NULL),(24,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-06-28',0.00,3058,98,'2014-06-28 08:25:10','2014-06-28 08:25:10',NULL),(25,'Crédito','mega polo','0014-04-29',8.10,1307,NULL,'2014-06-28 11:00:01','2014-06-28 11:00:01',NULL),(26,'Crédito','mega polo','0014-05-13',15.39,1307,NULL,'2014-06-28 11:00:34','2014-06-28 11:00:34',NULL),(27,'Crédito','Estorno de compra - Sistema de retaguarda Artaban','2014-07-01',0.00,3058,117,'2014-07-01 13:27:36','2014-07-01 13:27:36',NULL),(28,'Débito','Compra pelo sistema de retaguarda Artaban','2014-07-01',45.00,3618,124,'2014-07-01 13:36:43','2014-07-01 13:36:43',NULL),(29,'Crédito','Mega Polo','2014-07-14',58.86,3676,NULL,'2014-08-08 09:41:12','2014-08-11 15:39:57',NULL),(30,'Débito','Compra pelo sistema de retaguarda Artaban','2014-08-12',58.86,3676,569,'2014-08-12 13:57:13','2014-08-12 13:57:13',NULL);
/*!40000 ALTER TABLE `ART_Fidelizacao` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:16:19
