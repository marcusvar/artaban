CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_TransacaoRede`
--

DROP TABLE IF EXISTS `ART_TransacaoRede`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_TransacaoRede` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Venda_Id` int(11) DEFAULT NULL,
  `merchantreference` varchar(20) DEFAULT NULL COMMENT 'Número de referência único para cada transação (PD)',
  `status` smallint(6) DEFAULT NULL COMMENT 'Código numérico de retorno que indica o resultado da transação',
  `gateway_reference` varchar(255) DEFAULT NULL COMMENT 'A referência da transação fornecida pelo e-Rede.',
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datetime do momento da entrada da transação no servidor do e-Rede',
  `extended_response_message` varchar(5) DEFAULT NULL COMMENT 'Contém o código de status obtido do host de autorização da Rede',
  `extended_status` varchar(50) DEFAULT NULL COMMENT 'Contém a descrição do código de status obtido do host Rede',
  `CardTxn_authcode` varchar(20) DEFAULT NULL COMMENT 'Código de autorização para transações realizadas com êxito recebidas do banco',
  `CardTxn_card_scheme` varchar(25) DEFAULT NULL COMMENT 'A bandeira do cartão',
  `CardTxn_country` varchar(25) DEFAULT NULL COMMENT 'País',
  `CardTxn_issuer` varchar(50) DEFAULT NULL COMMENT 'O banco emissor do cartão',
  `acquirer` varchar(30) DEFAULT NULL COMMENT 'Banco adquirente',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Transações e-Rede';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_TransacaoRede`
--

LOCK TABLES `ART_TransacaoRede` WRITE;
/*!40000 ALTER TABLE `ART_TransacaoRede` DISABLE KEYS */;
/*!40000 ALTER TABLE `ART_TransacaoRede` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:16:20
