CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_TipoVeiculo`
--

DROP TABLE IF EXISTS `ART_TipoVeiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_TipoVeiculo` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(40) COLLATE utf8_swedish_ci NOT NULL,
  `Pessoa_Id` int(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_TipoVeiculo_Pessoa_Id` (`Pessoa_Id`),
  CONSTRAINT `FK_TipoVeiculo_Pessoa_Id` FOREIGN KEY (`Pessoa_Id`) REFERENCES `ART_Pessoa` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=2730;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_TipoVeiculo`
--

LOCK TABLES `ART_TipoVeiculo` WRITE;
/*!40000 ALTER TABLE `ART_TipoVeiculo` DISABLE KEYS */;
INSERT INTO `ART_TipoVeiculo` VALUES (2,'Leito Turismo',2,'2014-02-24 20:18:17','2014-02-25 08:27:31',NULL),(3,'Misto',2,'2014-02-25 08:55:05','2014-02-25 08:55:05',NULL),(4,'Leito Cama Double Deck',2,'2014-02-25 09:02:12','2014-02-25 09:41:29',NULL),(5,'Leito Leito',2,'2014-02-25 09:08:41','2014-02-25 09:08:41',NULL),(6,'Leito Leito Double Deck',2,'2014-02-25 09:23:04','2014-02-25 09:23:04',NULL),(7,'Leito Turismo Double Deck',2,'2014-02-25 09:28:33','2014-02-25 09:28:33',NULL);
/*!40000 ALTER TABLE `ART_TipoVeiculo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:15:37
