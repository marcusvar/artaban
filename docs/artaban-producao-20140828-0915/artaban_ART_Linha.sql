CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_Linha`
--

DROP TABLE IF EXISTS `ART_Linha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_Linha` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `PontoReferencia_Id_Destino` int(11) NOT NULL COMMENT 'Destino da linha',
  `PontoReferencia_Id_Via` int(11) NOT NULL COMMENT 'Via da linha',
  `Pessoa_Id` int(11) NOT NULL COMMENT 'Relacionamento com a empresa',
  `Situacao` enum('Normal','Bloqueada') COLLATE utf8_swedish_ci NOT NULL DEFAULT 'Normal',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `F_Linha_Pessoa_Id` (`Pessoa_Id`),
  KEY `FK_Linha_PontoReferencia_Id_Destino` (`PontoReferencia_Id_Destino`),
  KEY `FK_Linha_PontoReferencia_Id_Via` (`PontoReferencia_Id_Via`),
  CONSTRAINT `F_Linha_Pessoa_Id` FOREIGN KEY (`Pessoa_Id`) REFERENCES `ART_Pessoa` (`Id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_Linha_PontoReferencia_Id_Destino` FOREIGN KEY (`PontoReferencia_Id_Destino`) REFERENCES `ART_PontoReferencia` (`Id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_Linha_PontoReferencia_Id_Via` FOREIGN KEY (`PontoReferencia_Id_Via`) REFERENCES `ART_PontoReferencia` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=4096;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_Linha`
--

LOCK TABLES `ART_Linha` WRITE;
/*!40000 ALTER TABLE `ART_Linha` DISABLE KEYS */;
INSERT INTO `ART_Linha` VALUES (2,101,101,2,'Normal','2014-02-26 10:15:24','2014-02-26 16:37:58',NULL),(4,100,100,2,'Normal','2014-02-26 16:08:05','2014-02-26 16:08:05',NULL),(6,109,109,2,'Normal','2014-03-12 17:34:07','2014-03-12 17:34:07',NULL),(7,110,110,2,'Normal','2014-03-12 17:35:33','2014-03-12 17:35:33',NULL);
/*!40000 ALTER TABLE `ART_Linha` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:16:10
