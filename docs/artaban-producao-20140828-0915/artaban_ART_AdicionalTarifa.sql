CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_AdicionalTarifa`
--

DROP TABLE IF EXISTS `ART_AdicionalTarifa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_AdicionalTarifa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Linha_Id` int(11) NOT NULL COMMENT 'O valor adicional será para a linha independente do trecho',
  `TipoAdicionalTarifa_Id` int(11) NOT NULL,
  `DataVigencia` date NOT NULL COMMENT 'Data do início da vigência',
  `Valor` double(15,2) DEFAULT NULL,
  `Ida` tinyint(1) NOT NULL DEFAULT '1',
  `Volta` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_AdicionalTarifa_Linha_Id` (`Linha_Id`),
  KEY `FK_Tarifa_TipoAdicionalTarifa` (`TipoAdicionalTarifa_Id`),
  CONSTRAINT `FK_AdicionalTarifa_Linha_Id` FOREIGN KEY (`Linha_Id`) REFERENCES `ART_Linha` (`ID`) ON UPDATE CASCADE,
  CONSTRAINT `FK_Tarifa_TipoAdicionalTarifa` FOREIGN KEY (`TipoAdicionalTarifa_Id`) REFERENCES `ART_TipoAdicionalTarifa` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=8192;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_AdicionalTarifa`
--

LOCK TABLES `ART_AdicionalTarifa` WRITE;
/*!40000 ALTER TABLE `ART_AdicionalTarifa` DISABLE KEYS */;
INSERT INTO `ART_AdicionalTarifa` VALUES (1,2,1,'2014-05-19',30.00,1,1,'2014-05-19 17:44:51','2014-05-19 17:44:51',NULL),(2,4,1,'2014-05-19',50.00,1,1,'2014-05-19 17:46:14','2014-05-19 17:46:14',NULL);
/*!40000 ALTER TABLE `ART_AdicionalTarifa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:15:28
