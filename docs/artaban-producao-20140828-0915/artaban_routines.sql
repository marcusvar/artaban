CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping events for database 'artaban'
--

--
-- Dumping routines for database 'artaban'
--
/*!50003 DROP PROCEDURE IF EXISTS `Proc_ConsHorarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`artaban`@`%` PROCEDURE `Proc_ConsHorarios`(
  pDataViagem        DATE,
  pOrigem            INTEGER,
  pDestino           INTEGER,
  pInicio            INTEGER,
  pLimite            INTEGER)
BEGIN
  DECLARE vDataValidacao DATE;

  IF pDataViagem IS NULL THEN
    SET vDataValidacao = CURRENT_DATE;
  ELSE  
    SET vDataValidacao = pDataViagem;
  END IF;  

SELECT 
  ART_Viagem.Id,	
  ART_Viagem.DataInicio,
  ART_Viagem.HoraInicio,	
  ART_Viagem.Horario_Id,	
  ART_Horario.Horario AS Horario,
  ART_ViagemItem.DataHoraEmbarque,
  PontoReferenciaDestino.Id AS PontoReferenciaDestino_Id,	
  PontoReferenciaDestino.Descricao AS PontoReferenciaDestino_Descricao,	
  ART_Viagem.TipoVeiculo_Id,	
  ART_TipoVeiculo.Descricao AS TipoVeiculo_Descricao,
  ART_Viagem.Confirmada,	
  ART_Viagem.Situacao,
  ART_Viagem.Duracao,
  ART_Viagem.Descricao,
  ART_Veiculo.Numero
  
/*
ART_Viagem.Id
ART_PontoReferencia.Id
pAgenciaWeb 
Currentedate

  CALL PROC_ConsTarifa(14, 1, 2, "2014-03-05");
*/  
  
FROM
  ART_Viagem
  JOIN ART_ViagemItem ON ART_ViagemItem.Viagem_Id = ART_Viagem.Id 
  JOIN ART_Horario ON ART_Horario.Id = ART_Viagem.Horario_Id
  JOIN ART_HorarioItem ON ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
  JOIN ART_LinhaItem ON ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id
  JOIN ART_TipoVeiculo ON ART_TipoVeiculo.Id = ART_Viagem.TipoVeiculo_Id
  LEFT JOIN ART_Veiculo ON ART_Veiculo.Id = ART_Viagem.Veiculo_Id
  JOIN ART_Linha ON ART_Linha.Id = ART_Horario.Linha_Id  
  JOIN ART_PontoReferencia AS PontoReferenciaDestino ON PontoReferenciaDestino.Id = ART_Linha.PontoReferencia_Id_Destino
  JOIN ART_PontoReferencia ON ART_PontoReferencia.Id = ART_LinhaItem.PontoReferencia_Id AND
    ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id AND 
    ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
WHERE
  ART_Linha.PontoReferencia_Id_Destino = pDestino AND
  ART_PontoReferencia.Id = pOrigem AND
  #(pDataViagem IS NULL OR ART_Viagem.DataInicio = pDataViagem) AND
  ART_Linha.PontoReferencia_Id_Destino <> pOrigem AND
  ART_Viagem.DataInicio >= vDataValidacao
ORDER BY
  ART_ViagemItem.DataHoraEmbarque
LIMIT
  pInicio, pLimite;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Proc_ConsHorariosQTd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`artaban`@`%` PROCEDURE `Proc_ConsHorariosQTd`(
  pDataViagem        DATE,
  pOrigem            INTEGER,
  pDestino           INTEGER)
BEGIN
  DECLARE vDataValidacao DATE;

  IF pDataViagem IS NULL THEN
    SET vDataValidacao = CURRENT_DATE;
  ELSE  
    SET vDataValidacao = pDataViagem;
  END IF;  

SELECT 
  Count(ART_Viagem.Id) AS Total
FROM
  ART_Viagem
  JOIN ART_ViagemItem ON ART_ViagemItem.Viagem_Id = ART_Viagem.Id 
  JOIN ART_Horario ON ART_Horario.Id = ART_Viagem.Horario_Id
  JOIN ART_HorarioItem ON ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
  JOIN ART_LinhaItem ON ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id
  JOIN ART_TipoVeiculo ON ART_TipoVeiculo.Id = ART_Viagem.TipoVeiculo_Id 
  JOIN ART_Linha ON ART_Linha.Id = ART_Horario.Linha_Id  
  JOIN ART_PontoReferencia AS PontoReferenciaDestino ON PontoReferenciaDestino.Id = ART_Linha.PontoReferencia_Id_Destino
  JOIN ART_PontoReferencia ON ART_PontoReferencia.Id = ART_LinhaItem.PontoReferencia_Id AND
    ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id AND 
    ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
WHERE
  ART_Linha.PontoReferencia_Id_Destino = pDestino AND
  ART_PontoReferencia.Id = pOrigem AND
  (pDataViagem IS NULL OR ART_Viagem.DataInicio = pDataViagem) AND
  ART_Linha.PontoReferencia_Id_Destino <> pOrigem AND
  ART_Viagem.DataInicio >= vDataValidacao;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Proc_ConsMapaLugarViagem` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`artaban`@`%` PROCEDURE `Proc_ConsMapaLugarViagem`(
  pViagem_Id integer,
  pWeb boolean)
BEGIN
  SELECT
    Consulta.Numero,
    Consulta.Linha,
    Consulta.Coluna,
    Consulta.Andar,
    IF(Consulta.Passagem_Id IS NULL, "poltrona livre", 
    (SELECT CONCAT( "poltrona ",
    # Verifica se a poltrona tem uma "reserva", "ocupada" ou "misto"
    IF((SELECT COUNT(*) AS Total
          FROM ART_Passagem 
          WHERE 
            ART_Passagem.Viagem_Id = pViagem_Id AND ART_Passagem.Situacao = "Normal"
            AND ART_Passagem.Poltrona = Consulta.Numero
          GROUP BY ART_Passagem.Tipo
          LIMIT 1) = 1 AND
       (SELECT COUNT(*) AS Total
          FROM ART_Passagem 
          WHERE 
            ART_Passagem.Viagem_Id = pViagem_Id AND ART_Passagem.Situacao = "Normal"
            AND ART_Passagem.Poltrona = Consulta.Numero) > 1, IF(pWeb, "ocupada",  "misto"), 
        IF(ART_Passagem.Tipo = "P", "ocupada", IF(pWeb, "ocupada", "reserva"))),
    #Verifica se � "masculino", "feminino" ou "misto"
    IF((SELECT COUNT(*) AS Total
          FROM ART_Passagem 
          JOIN ART_Pessoa ap ON ART_Passagem.Pessoa_Id = ap.Id
          WHERE 
            ART_Passagem.Viagem_Id = pViagem_Id AND ART_Passagem.Situacao = "Normal"
            AND ART_Passagem.Poltrona = Consulta.Numero
          GROUP BY ap.Sexo
          LIMIT 1) > 1 OR
       (SELECT COUNT(*) AS Total
          FROM ART_Passagem
        WHERE 
            ART_Passagem.Viagem_Id = pViagem_Id AND ART_Passagem.Situacao = "Normal"
            AND ART_Passagem.Poltrona = Consulta.Numero) = 1 , 
        IF(ART_Pessoa.Sexo = "M", " masculino", " feminino"), " sexo-misto"),
    # Verifica se � "cheia", "ida" ou "volta"
    IF( pWeb, " cheia",
    IF((SELECT COUNT(ART_Passagem.Id) AS Total
        FROM ART_Passagem 
        WHERE 
        ART_Passagem.Viagem_Id = pViagem_Id AND ART_Passagem.Situacao = "Normal" AND
        ART_Passagem.Poltrona = Consulta.Numero) > 1 OR ART_Passagem.Parcial="C", " cheia", CONCAT(" parcial", IF(ART_Passagem.Parcial = "I", " ida", " volta" )))))
    FROM ART_Passagem 
    JOIN ART_Pessoa ON ART_Pessoa.Id = ART_Passagem.Pessoa_Id 
    WHERE ART_Passagem.Id = Consulta.Passagem_Id)
    ) AS Situacao,    
    "" AS Promocao,
    Consulta.Passagem_Id
  FROM
    (SELECT
      ART_TipoVeiculoMapa.Numero,
      ART_TipoVeiculoMapa.Linha,
      ART_TipoVeiculoMapa.Coluna,
      ART_TipoVeiculoMapa.Andar,
      (SELECT ART_Passagem.Id 
        FROM ART_Passagem 
        WHERE 
        ART_Passagem.Viagem_Id = ART_Viagem.Id AND ART_Passagem.Situacao = "Normal" AND
        ART_Passagem.Poltrona = ART_TipoVeiculoMapa.Numero
        LIMIT 1) AS Passagem_Id
    FROM
      ART_Viagem
      JOIN ART_TipoVeiculo ON ART_TipoVeiculo.Id = ART_Viagem.TipoVeiculo_Id
      JOIN ART_TipoVeiculoMapa ON ART_TipoVeiculoMapa.TipoVeiculo_Id = ART_TipoVeiculo.Id
    WHERE
      ART_Viagem.Id = pViagem_Id) AS Consulta
  ORDER BY
    Consulta.Andar,
    Consulta.Linha,
    Consulta.Coluna;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Proc_ConsTarifa` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`artaban`@`%` PROCEDURE `Proc_ConsTarifa`(
  pViagem_Id  INTEGER,
  pOrigem_Id  INTEGER,
  pAgenciaWeb INTEGER,
pDataCompra DATE)
BEGIN
  SELECT
    PontoReferenciaOrigem_Descricao,
    PontoReferenciaDestino_Descricao,
    Valor AS TarifaPiso1, 
    Valor2 AS TarifaPiso2,
    AdicionalTarifa,
    Desconto,
    ((Valor + AdicionalTarifa) - Desconto) AS TotalTarifaPiso1,
    ((Valor2 + AdicionalTarifa) - Desconto) AS TotalTarifaPiso2
  FROM
    (SELECT
      Origem.Descricao AS PontoReferenciaOrigem_Descricao,
      Destino.Descricao AS PontoReferenciaDestino_Descricao,
      /* Tarifa piso1 e tarifa piso2 */  
      ART_Tarifa.Valor,
      ART_Tarifa.Valor2,
      /* Adicional da tarifa */
      IFNULL((SELECT SUM(Valor) FROM ART_AdicionalTarifa 
      WHERE ART_AdicionalTarifa.Linha_Id = ART_Linha.Id
      AND ART_AdicionalTarifa.DataVigencia >= pDataCompra), 0) AS AdicionalTarifa,  
      /* Desconto */
      IFNULL((SELECT ART_Desconto.Percentagem FROM ART_Desconto 
      WHERE (ART_Desconto.AgenciaWeb = pAgenciaWeb or ART_Desconto.AgenciaWeb = "0")
      AND ART_Desconto.Confirmado), 0) AS Desconto
    FROM
      ART_Viagem
      JOIN ART_ViagemItem ON ART_ViagemItem.Viagem_Id = ART_Viagem.Id
      JOIN ART_HorarioItem ON ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id  
      JOIN ART_LinhaItem ON ART_LinhaItem.Id = ART_HorarioItem.LinhaItem_Id
      JOIN ART_Linha ON ART_Linha.Id = ART_LinhaItem.Linha_Id
      JOIN ART_PontoReferencia AS Origem ON Origem.Id = ART_LinhaItem.PontoReferencia_Id
      JOIN ART_PontoReferencia AS Destino ON Destino.Id = ART_Linha.PontoReferencia_Id_Destino  
      JOIN ART_Tarifa ON ART_Tarifa.LinhaItem_Id = ART_LinhaItem.Id AND
        ART_Tarifa.TipoVeiculo_Id = ART_Viagem.TipoVeiculo_Id
    WHERE
      ART_Viagem.Id = pViagem_Id AND
      Origem.Id = pOrigem_Id) AS Consulta;      
      
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Proc_GerarViagem` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`artaban`@`%` PROCEDURE `Proc_GerarViagem`(
  pDataInicio DATE,
  pDataFim    DATE,
  pHorario_Id  INTEGER)
BEGIN
  DECLARE vValidacao TINYINT;
  DECLARE vFeriado TINYINT;
  DECLARE vHorarioFeriado TINYINT;
  DECLARE vFrequencia TINYINT;

  DECLARE vDomingo TINYINT;
  DECLARE vSegunda TINYINT;
  DECLARE vTerca TINYINT;
  DECLARE vQuarta TINYINT;
  DECLARE vQuinta TINYINT;
  DECLARE vSexta TINYINT;
  DECLARE vSabado TINYINT;

  DECLARE vDataInicio DATE;
  DECLARE vHorario TIME;
  DECLARE vTipoVeiculo_Id INTEGER;
  DECLARE vQtdDias INTEGER;
  DECLARE vQtd INTEGER;
  DECLARE vViagem_Id INTEGER;
  DECLARE vDuracao INTEGER;
  DECLARE vDescricao VARCHAR(50);

  DECLARE viHorarioItem_Id INTEGER;
  DECLARE viHorarioItem_Id_Transbordo INTEGER;
  DECLARE viHoraEmbarque TIME;
  DECLARE viRecolher TINYINT;
  DECLARE viDiaEmbarque TINYINT;
  DECLARE vEOF TINYINT;
  DECLARE vFim TINYINT;

  DECLARE Cur_HorarioItem CURSOR FOR
    SELECT Id, LinhaItem_Id_Transbordo, HoraEmbarque, Recolher, DiaEmbarque
    FROM ART_HorarioItem
    WHERE Horario_Id = pHorario_Id;

  DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET vEOF = TRUE;

  SET vValidacao = 0;

  /*Validar se a Linha está ok - Retorno 1*/	
  SELECT IF(ART_Linha.Situacao = 'Normal', 0, 1) INTO vValidacao
  FROM ART_Linha 
  JOIN ART_Horario ON ART_Horario.Linha_Id = ART_Linha.Id
  WHERE ART_Horario.Id = pHorario_Id;
  
  IF vValidacao = 0 THEN
    /*Validar Situação e se Horário esta Confirmado - Retorno 2*/
    SELECT IF((IF(Situacao = 'Normal', 0, 2) + IF(Confirmado = 'Sim', 0, 2)) = 0, 0,2) INTO vValidacao
    FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;
  END IF;

  IF vValidacao = 0 THEN
    SET vDataInicio = pDataInicio;

    SELECT ART_Horario.Horario INTO vHorario FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT ART_Horario.TipoVeiculo_Id INTO vTipoVeiculo_Id FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT ART_Horario.Duracao INTO vDuracao FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;

    SELECT ART_Horario.Descricao INTO vDescricao FROM ART_Horario WHERE ART_Horario.Id = pHorario_Id;    

    SELECT (DATEDIFF(pDataFim, pDataInicio)) INTO vQtdDias;
    
    SET vQTD = 0;
    WHILE vQtd <= vQtdDias DO
      
      /*Verifica se o horário tráfega em feriados*/
      SELECT Feriado INTO vHorarioFeriado FROM ART_Horario WHERE Id = pHorario_Id;

      SET vFeriado = 0;
      IF vHorarioFeriado = 0 THEN
        /*Se o horário tráfega em feriados então verifica se a data é feriado*/
        SELECT EXISTS(
        SELECT ART_Feriados.Id FROM ART_Feriados
        WHERE ART_Feriados.DataMovel = DATE_ADD(vDataInicio, INTERVAL vQtd DAY) OR
        (ART_Feriados.Dia = DAY(DATE_ADD(vDataInicio, INTERVAL vQtd DAY)) AND
        ART_Feriados.Mes = MONTH(DATE_ADD(vDataInicio, INTERVAL vQtd DAY)))) INTO vFeriado;
      END IF;

      /*Verificar a frequência do horário*/
      SELECT SUBSTRING(Frequencia FROM 1 FOR 1) INTO vDomingo FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 2 FOR 1) INTO vSegunda FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 3 FOR 1) INTO vTerca FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 4 FOR 1) INTO vQuarta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 5 FOR 1) INTO vQuinta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 6 FOR 1) INTO vSexta FROM ART_Horario WHERE Id = pHorario_Id;
      SELECT SUBSTRING(Frequencia FROM 7 FOR 1) INTO vSabado FROM ART_Horario WHERE Id = pHorario_Id;
            
      SELECT 
        CASE DATE_FORMAT(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), '%w')
          WHEN 0 THEN IF(vDomingo = 1, "1", "0")
          WHEN 1 THEN IF(vSegunda = 1, "1", "0")
          WHEN 2 THEN IF(vTerca = 1, "1", "0")
          WHEN 3 THEN IF(vQuarta = 1, "1", "0")
          WHEN 4 THEN IF(vQuinta = 1, "1", "0")
          WHEN 5 THEN IF(vSexta = 1, "1", "0")
          WHEN 6 THEN IF(vSabado = 1, "1", "0")
      END INTO vFrequencia;
      
      IF (vFeriado = 0) AND (vFrequencia = 1) THEN
        /* Grava na tabela de viagens */
        INSERT INTO ART_Viagem(DataInicio, HoraInicio, Horario_Id, TipoVeiculo_Id, Confirmada, Situacao, Duracao, Descricao)
          VALUES(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), vHorario, pHorario_Id, vTipoVeiculo_Id, "Sim", "Normal", vDuracao, vDescricao);
        
        /* Busca o Id da viagem gravada */
        SET vViagem_Id = (SELECT LAST_INSERT_ID());
          
        /* Grava na tabela de viagem item  */
        OPEN Cur_HorarioItem;
        REPEAT
          SET vEOF = FALSE;
          FETCH Cur_HorarioItem INTO viHorarioItem_Id, viHorarioItem_Id_Transbordo, viHoraEmbarque, viRecolher, viDiaEmbarque;
          SET vFim = vEOF;
          IF NOT vFim THEN
            INSERT INTO ART_ViagemItem(Viagem_Id, HorarioItem_Id, HorarioItem_Id_Transbordo, DataHoraEmbarque, Recolher, Ativo) 
            VALUES(vViagem_Id, viHorarioItem_Id, viHorarioItem_Id_Transbordo,
              TIMESTAMP(DATE_ADD(DATE_ADD(vDataInicio, INTERVAL vQtd DAY), INTERVAL viDiaEmbarque DAY), viHoraEmbarque), viRecolher, 1); 
          END IF;
        UNTIL vFim END REPEAT;
        CLOSE Cur_HorarioItem;
      END IF;
      
      SET vQtd = vQtd + 1;
    END WHILE;
    SELECT "Procedimento executado com sucesso." AS Mensagem;
  ELSE
  BEGIN
    IF vValidacao = 1 THEN
      SELECT "A linha deste horário está bloqueada." AS Mensagem;
    ELSE IF vValidacao = 2 THEN
      SELECT "Este Horário está Bloqueado ou não confirmado." AS Mensagem;
    END IF;  
    END IF;  
  END;  
  END IF;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `Proc_MarcarPoltrona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
CREATE DEFINER=`artaban`@`%` PROCEDURE `Proc_MarcarPoltrona`(
  pViagem_Id     INTEGER,
  pViagemItem_Id INTEGER,
  pPoltrona      INTEGER)
BEGIN
  DECLARE vPassagem_Id INTEGER;
  DECLARE vSemafaro_Id INTEGER;
  
  /* Verificar */
    /* -> Verificar passagem */
    SELECT
      ART_Passagem.Id INTO vPassagem_Id
    FROM
      ART_Passagem 
    WHERE
      ART_Passagem.Viagem_Id = pViagem_Id AND
      ART_Passagem.ViagemItem_Id_Origem = pViagemItem_Id AND
      ART_Passagem.Poltrona = pPoltrona AND   
      ART_Passagem.Situacao = "Normal" AND
      ((ART_Passagem.Tipo = "P") OR (ART_Passagem.Tipo = "R" AND ART_Passagem.ReservaExpiracao > NOW()));

    /* -> Verificar sem�faro */
    SELECT
      ART_Semafaro.Id INTO vSemafaro_Id
    FROM
      ART_Semafaro
    WHERE
      ART_Semafaro.Viagem_Id = pViagem_Id AND
      ART_Semafaro.ViagemItem_Id_Origem = pViagemItem_Id AND
      ART_Semafaro.Poltrona = pPoltrona;
  
  /* Desmarcar */
  
  /* Marcar */
  IF (vPassagem_Id IS NULL) AND (vSemafaro_Id IS NULL) THEN
    INSERT INTO ART_Semafaro(Viagem_Id, Poltrona, ViagemItem_Id_Origem, DataHora)
    VALUES(pViagem_Id, pPoltrona, pViagemItem_Id, NOW());
  ELSE
    SELECT "Poltrona n�o disponivel";    
  END IF;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:16:30
