CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_Viagem`
--

DROP TABLE IF EXISTS `ART_Viagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_Viagem` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `DataInicio` date NOT NULL COMMENT 'Data do inicio da viagem',
  `HoraInicio` time NOT NULL COMMENT 'Hora do inicio da viagem',
  `Horario_Id` int(11) NOT NULL,
  `TipoVeiculo_Id` int(11) NOT NULL,
  `Confirmada` enum('Sim','Não') COLLATE utf8_swedish_ci NOT NULL DEFAULT 'Não' COMMENT 'Define se a viagem está confirmada para aparecer na consulta',
  `Situacao` enum('Normal','Bloqueada') COLLATE utf8_swedish_ci NOT NULL DEFAULT 'Normal',
  `Duracao` int(11) NOT NULL COMMENT 'Duração da viagem em dias',
  `Descricao` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL,
  `Veiculo_Id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `Fk_Viagem_TipoVeiculo_Id` (`TipoVeiculo_Id`),
  KEY `FK_Viagem_Veiculo_Id_idx` (`Veiculo_Id`),
  KEY `FK_Viagem_Horario_Id` (`Horario_Id`),
  CONSTRAINT `FK_Viagem_Horario_Id` FOREIGN KEY (`Horario_Id`) REFERENCES `ART_Horario` (`Id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_Viagem_Veiculo_Id` FOREIGN KEY (`Veiculo_Id`) REFERENCES `ART_Veiculo` (`Id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `Fk_Viagem_TipoVeiculo_Id` FOREIGN KEY (`TipoVeiculo_Id`) REFERENCES `ART_TipoVeiculo` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=481;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_Viagem`
--

LOCK TABLES `ART_Viagem` WRITE;
/*!40000 ALTER TABLE `ART_Viagem` DISABLE KEYS */;
INSERT INTO `ART_Viagem` VALUES (25,'2014-05-11','12:00:00',1,2,'Sim','Normal',1,'Feirinha 8700',NULL,NULL,NULL,NULL),(26,'2014-05-21','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',NULL,NULL,NULL,NULL),(27,'2014-05-28','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',NULL,NULL,NULL,NULL),(28,'2014-06-01','12:00:00',1,2,'Sim','Normal',1,'FEIRINHA 8700',NULL,'2014-05-28 10:25:16','2014-05-28 10:25:16',NULL),(29,'2014-06-01','12:00:00',12,3,'Sim','Normal',2,'Minas Gerais / São Paulo',NULL,NULL,'2014-05-29 16:07:40',NULL),(30,'2014-06-08','12:00:00',1,2,'Sim','Normal',1,'Feirinha',NULL,NULL,NULL,NULL),(31,'2014-06-15','12:00:00',1,2,'Sim','Normal',1,'Feirinha',NULL,NULL,NULL,NULL),(32,'2014-06-08','14:00:00',6,4,'Sim','Normal',2,'Mega Polo',NULL,NULL,NULL,NULL),(33,'2014-06-29','12:00:00',1,2,'Sim','Normal',1,'Feirinha',NULL,NULL,NULL,NULL),(34,'2014-06-29','14:00:00',6,4,'Sim','Normal',2,'Mega Polo',NULL,NULL,NULL,NULL),(35,'2014-06-04','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',NULL,NULL,NULL,NULL),(37,'2014-06-25','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',NULL,NULL,NULL,NULL),(38,'2014-06-04','14:00:00',7,2,'Sim','Normal',2,'Mega Polo/ Piratininga',NULL,NULL,NULL,NULL),(39,'2014-06-25','14:00:00',7,2,'Sim','Normal',2,'Mega Polo/ Piratininga',NULL,NULL,NULL,NULL),(43,'2014-06-15','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',NULL,'2014-06-13 15:51:32','2014-06-13 15:51:32',NULL),(44,'2014-07-02','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo-  Carro 7300',NULL,NULL,'2014-06-30 14:39:42',NULL),(45,'2014-07-06','12:00:00',1,2,'Sim','Normal',1,'Feirinha- Carro 8700',NULL,'2014-06-30 14:43:51','2014-06-30 14:46:39',NULL),(57,'2014-07-09','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',NULL,NULL,NULL,NULL),(62,'2014-07-09','14:00:00',13,5,'Sim','Normal',1,'Ibitinga',NULL,NULL,NULL,NULL),(63,'2014-07-06','14:00:00',6,4,'Sim','Normal',2,'Mega Polo Teste',NULL,NULL,'2014-07-05 08:32:02',NULL),(67,'2014-07-09','14:00:00',7,4,'Sim','Normal',2,'Mega Polo/ Piratininga',NULL,NULL,'2014-07-08 15:00:51',NULL),(71,'2014-07-13','12:00:00',1,2,'Sim','Normal',1,'Feirinha- Carro 8180',NULL,'2014-07-10 11:22:59','2014-07-12 10:04:40',NULL),(72,'2014-07-14','14:00:00',6,4,'Sim','Normal',2,'MEGA POLO CARRO 8800',NULL,'2014-07-10 14:21:50','2014-07-10 14:21:50',NULL),(73,'2014-07-16','14:00:00',2,3,'Sim','Normal',1,'CARRO 7100',NULL,'2014-07-15 17:33:53','2014-07-15 17:33:53',NULL),(74,'2014-07-20','12:00:00',1,2,'Sim','Normal',1,'Feirinha- Carro 8700',NULL,'2014-07-17 14:27:29','2014-07-17 14:27:29',NULL),(75,'2014-07-20','14:00:00',6,4,'Sim','Normal',2,'Mega Polo - Carro 8800',NULL,'2014-07-17 14:29:02','2014-07-17 14:29:02',NULL),(76,'2014-07-23','14:00:00',7,4,'Sim','Normal',2,'Carro 8800',NULL,'2014-07-21 14:21:42','2014-07-21 14:21:42',NULL),(77,'2014-07-23','14:00:00',2,3,'Sim','Normal',1,'Carro 7300',NULL,'2014-07-22 09:01:15','2014-07-22 09:03:55',NULL),(78,'2014-07-25','06:45:00',5,3,'Sim','Normal',2,'Carro 7300',NULL,'2014-07-22 09:02:27','2014-07-22 09:04:14',NULL),(79,'2014-07-25','06:45:00',5,3,'Sim','Normal',2,'carro 7100',NULL,'2014-07-22 09:04:36','2014-07-22 09:04:36',NULL),(80,'2014-07-27','12:00:00',1,2,'Sim','Normal',1,'Feirinha- Carro 8700',NULL,'2014-07-23 10:34:39','2014-07-23 10:34:39',NULL),(81,'2014-07-25','14:00:00',6,4,'Sim','Normal',2,'Carro 8800',NULL,'2014-07-25 17:03:56','2014-07-25 17:03:56',NULL),(82,'2014-07-27','14:00:00',6,4,'Sim','Normal',2,'Carro 8800',NULL,'2014-07-25 17:06:30','2014-07-25 17:06:30',NULL),(83,'2014-07-30','14:00:00',2,3,'Sim','Normal',1,'Bom Retiro, Feirinha, Mega polo- 7300',NULL,'2014-07-29 09:29:15','2014-07-29 09:31:22',NULL),(84,'2014-07-30','14:00:00',7,4,'Sim','Normal',2,'Mega Polo/ Piratininga- 8800',NULL,'2014-07-29 09:30:19','2014-07-29 09:31:04',NULL),(85,'2014-08-03','12:00:00',1,2,'Sim','Normal',1,'Feirinha Carro 8180',NULL,'2014-07-29 09:30:49','2014-08-01 14:26:03',NULL),(86,'2014-08-03','14:00:00',6,4,'Sim','Normal',2,'Carro 8800',NULL,'2014-08-01 16:19:58','2014-08-01 16:19:58',NULL),(87,'2014-08-06','14:00:00',2,3,'Sim','Normal',1,'Carro 7300',NULL,'2014-08-05 13:49:33','2014-08-05 13:49:33',NULL),(88,'2014-08-06','14:00:00',7,4,'Sim','Normal',2,'Carro 8800',NULL,'2014-08-05 13:50:14','2014-08-05 13:50:14',NULL),(89,'2014-08-10','12:00:00',1,2,'Sim','Normal',1,'Feirinha- Carro 8180',NULL,'2014-08-05 16:10:17','2014-08-08 10:54:14',NULL),(90,'2014-08-10','14:00:00',6,4,'Sim','Normal',2,'Mega Polo- Carro 8800',NULL,'2014-08-05 16:11:00','2014-08-05 16:11:00',NULL),(91,'2014-08-13','14:00:00',2,3,'Sim','Normal',1,'Carro 7300',NULL,'2014-08-11 13:47:49','2014-08-11 13:47:49',NULL),(92,'2014-08-13','14:00:00',7,4,'Sim','Normal',2,'Carro 8.800',NULL,'2014-08-11 13:48:23','2014-08-11 13:48:23',NULL),(93,'2014-08-15','06:45:00',5,3,'Sim','Normal',2,'Carro 7300',NULL,'2014-08-12 17:06:36','2014-08-12 17:06:36',NULL),(94,'2014-08-15','06:45:00',5,3,'Sim','Normal',2,'Carro 7.100',NULL,'2014-08-12 17:06:58','2014-08-12 17:06:58',NULL),(95,'2014-08-17','12:00:00',1,2,'Sim','Normal',1,'Carro 8.700',NULL,'2014-08-13 11:51:14','2014-08-13 11:51:14',NULL),(96,'2014-08-17','14:00:00',6,4,'Sim','Normal',2,'Carro 8800',NULL,'2014-08-13 11:51:51','2014-08-13 11:51:51',NULL),(97,'2014-08-15','14:00:00',7,4,'Sim','Normal',3,'GIFT- Carro 8300',NULL,'2014-08-13 11:52:32','2014-08-14 14:47:35',NULL),(99,'2014-08-17','14:00:00',6,3,'Sim','Normal',2,'Carro-9.100',NULL,'2014-08-14 14:59:22','2014-08-14 14:59:22',NULL),(100,'2014-08-24','12:00:00',1,2,'Sim','Normal',1,'Feirinha',2,'2014-08-19 08:44:27','2014-08-21 17:04:50',NULL),(101,'2014-08-24','12:00:00',1,5,'Sim','Normal',1,'Feirinha',8,'2014-08-19 08:45:50','2014-08-21 17:39:50',NULL),(102,'2014-08-20','14:00:00',2,3,'Sim','Normal',1,'Carro 7300',NULL,'2014-08-19 08:48:23','2014-08-19 08:48:23',NULL),(103,'2014-08-20','14:00:00',7,4,'Sim','Normal',2,'Carro 8800',NULL,'2014-08-19 08:49:06','2014-08-19 08:49:06',NULL),(104,'2014-08-20','14:00:00',13,5,'Sim','Normal',1,'Carro 8000',NULL,'2014-08-19 10:50:49','2014-08-19 10:50:49',NULL),(105,'2014-08-20','14:00:00',2,3,'Sim','Normal',1,'Carro 9100',NULL,'2014-08-19 17:12:02','2014-08-19 17:12:02',NULL),(107,'2014-08-24','14:00:00',6,4,'Sim','Normal',2,'Mega Polo',6,NULL,'2014-08-21 16:36:25',NULL),(108,'2014-08-24','14:00:00',6,3,'Sim','Normal',2,'Mega Polo',7,'2014-08-21 16:39:41','2014-08-21 16:49:22',NULL),(112,'2014-08-27','14:00:00',7,4,'Sim','Normal',2,'Mega Polo/ Piratininga',6,'2014-08-25 16:53:43','2014-08-25 17:02:49',NULL),(113,'2014-08-27','14:00:00',2,5,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',8,'2014-08-25 16:54:36','2014-08-25 17:02:34',NULL),(114,'2014-08-31','12:00:00',1,2,'Sim','Normal',1,'Feirinha Linha Umuarama',2,'2014-08-25 16:54:39','2014-08-25 16:57:54',NULL),(115,'2014-08-31','12:00:00',1,2,'Sim','Normal',1,'Feirinha Linha Cascavel',9,'2014-08-25 16:55:53','2014-08-25 16:59:03',NULL),(116,'2014-08-27','14:00:00',2,3,'Sim','Normal',1,'',7,'2014-08-25 16:57:51','2014-08-27 17:33:04',NULL),(117,'2014-08-27','14:00:00',2,6,'Sim','Normal',1,'Bom Retiro, Feirinha, e Mega Polo',10,'2014-08-25 16:58:49','2014-08-25 17:03:17',NULL),(118,'2014-08-28','06:45:00',5,3,'Sim','Normal',2,'Goiânia ',4,'2014-08-27 08:26:37','2014-08-27 08:29:04',NULL),(119,'2014-08-28','06:45:00',5,3,'Sim','Normal',2,'Goiânia ',3,'2014-08-27 08:27:33','2014-08-27 08:28:53',NULL);
/*!40000 ALTER TABLE `ART_Viagem` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:16:11
