CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_UF`
--

DROP TABLE IF EXISTS `ART_UF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_UF` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) COLLATE utf8_swedish_ci NOT NULL,
  `Sigla` char(2) COLLATE utf8_swedish_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `Pais_Id` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_UF_Pais_Id` (`Pais_Id`),
  CONSTRAINT `FK_UF_Pais_Id` FOREIGN KEY (`Pais_Id`) REFERENCES `ART_Pais` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=606;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_UF`
--

LOCK TABLES `ART_UF` WRITE;
/*!40000 ALTER TABLE `ART_UF` DISABLE KEYS */;
INSERT INTO `ART_UF` VALUES (1,'Acre','AC',NULL,NULL,NULL,1),(2,'Alagoas','AL',NULL,NULL,NULL,1),(3,'Amazonas','AM',NULL,NULL,NULL,1),(4,'Amapá','AP',NULL,NULL,NULL,1),(5,'Bahia','BA',NULL,NULL,NULL,1),(6,'Ceará','CE',NULL,NULL,NULL,1),(7,'Distrito Federal','DF',NULL,NULL,NULL,1),(8,'Espírito Santo','ES',NULL,NULL,NULL,1),(9,'Goiás','GO',NULL,NULL,NULL,1),(10,'Maranhão','MA',NULL,NULL,NULL,1),(11,'Minas Gerais','MG',NULL,NULL,NULL,1),(12,'Mato Grosso do Sul','MS',NULL,NULL,NULL,1),(13,'Mato Grosso','MT',NULL,NULL,NULL,1),(14,'Pará','PA',NULL,NULL,NULL,1),(15,'Paraíba','PB',NULL,NULL,NULL,1),(16,'Pernambuco','PE',NULL,NULL,NULL,1),(17,'Piauí','PI',NULL,NULL,NULL,1),(18,'Paraná','PR',NULL,NULL,NULL,1),(19,'Rio de Janeiro','RJ',NULL,NULL,NULL,1),(20,'Rio Grande do Norte','RN',NULL,NULL,NULL,1),(21,'Rondônia','RO',NULL,NULL,NULL,1),(22,'Roraima','RR',NULL,NULL,NULL,1),(23,'Rio Grande do Sul','RS',NULL,NULL,NULL,1),(24,'Santa Catarina','SC',NULL,NULL,NULL,1),(25,'Sergipe','SE',NULL,NULL,NULL,1),(26,'São Paulo','SP',NULL,NULL,NULL,1),(27,'Tocantins','TO',NULL,NULL,NULL,1),(28,'Alto Paraguay','AP',NULL,NULL,NULL,183),(29,'Alto Paraná','AL',NULL,NULL,NULL,183),(30,'Amambay','AM',NULL,NULL,NULL,183),(31,'Distrito Capital','DC',NULL,NULL,NULL,183),(32,'Boquerón','BO',NULL,NULL,NULL,183),(33,'Caaguazú ','CA',NULL,NULL,NULL,183),(34,'Caazapá','CZ',NULL,NULL,NULL,183),(35,'Canindeyú','CY',NULL,NULL,NULL,183),(36,'Central','CE',NULL,NULL,NULL,183),(37,'Concepción','CO',NULL,NULL,NULL,183),(38,'Cordillera','CL',NULL,NULL,NULL,183),(39,'Guairá','GU',NULL,NULL,NULL,183),(40,'Itapúa','IT',NULL,NULL,NULL,183),(41,'Misiones','MI',NULL,NULL,NULL,183),(42,'Ñeembucú','NE',NULL,NULL,NULL,183),(43,'Paraguarí','PA',NULL,NULL,NULL,183),(44,'Presidente Hayes','PH',NULL,NULL,NULL,183),(45,'San Pedro','SP',NULL,NULL,NULL,183);
/*!40000 ALTER TABLE `ART_UF` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:15:26
