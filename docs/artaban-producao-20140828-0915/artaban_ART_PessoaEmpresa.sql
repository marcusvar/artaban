CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_PessoaEmpresa`
--

DROP TABLE IF EXISTS `ART_PessoaEmpresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_PessoaEmpresa` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Pessoa_Id` int(11) DEFAULT NULL,
  `Site` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Url do site deste cliente',
  `Logo` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'URL da logomarca deste cliente',
  `Host` varchar(150) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Nome ou IP do servidor de e-mail SMTP',
  `Porta` int(11) DEFAULT NULL,
  `Usuario` varchar(50) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Usuário de acesso ao Host SMTP',
  `Senha` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Senha de acesso ao Host SMTP',
  `Email` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'E-mail para uso para envio SMTP',
  `Descricao` varchar(255) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Assunto da mensagem SMTP',
  `erede_rdcd_pv` int(9) DEFAULT NULL COMMENT 'Número de estabelecimento Rede',
  `erede_password` varchar(20) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Senha de acesso ao e-Rede',
  `Subdominio` varchar(20) COLLATE utf8_swedish_ci DEFAULT NULL COMMENT 'Subdomínio do cliente (cliente.artaban.com.br)',
  PRIMARY KEY (`Id`),
  KEY `FK_ART_PessoaEmpresa_ART_Pessoa_Id` (`Pessoa_Id`),
  CONSTRAINT `FK_ART_PessoaEmpresa_ART_Pessoa_Id` FOREIGN KEY (`Pessoa_Id`) REFERENCES `veritasweb03`.`ART_Pessoa` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=16384 COMMENT='Dados específicos para controle de clientes multiempresa';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_PessoaEmpresa`
--

LOCK TABLES `ART_PessoaEmpresa` WRITE;
/*!40000 ALTER TABLE `ART_PessoaEmpresa` DISABLE KEYS */;
INSERT INTO `ART_PessoaEmpresa` VALUES (1,2,'http://www.guacutur.com.br','http://guacutur.artaban.com.br/img/2/logo.png','ssl://smtp.gmail.com',465,'admin@artaban.com.br','artaban@3214','admin@artaban.com.br','Atendimento Guaçu Tur',44754779,'XAjWdDRq','guacutur');
/*!40000 ALTER TABLE `ART_PessoaEmpresa` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:15:52
