CREATE DATABASE  IF NOT EXISTS `artaban` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `artaban`;
-- MySQL dump 10.13  Distrib 5.6.17, for osx10.6 (i386)
--
-- Host: www.artaban.com.br    Database: artaban
-- ------------------------------------------------------
-- Server version	5.5.38

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ART_Usuario`
--

DROP TABLE IF EXISTS `ART_Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ART_Usuario` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(180) COLLATE utf8_swedish_ci NOT NULL,
  `Email` varchar(80) COLLATE utf8_swedish_ci NOT NULL,
  `Senha` varchar(80) COLLATE utf8_swedish_ci NOT NULL,
  `Pessoa_Id` int(11) NOT NULL,
  `PontoReferencia_Id_Agencia` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`Id`),
  KEY `FK_Usuario_Pessoa_Id` (`Pessoa_Id`),
  KEY `FK_Usuario_PontoReferencia_Id_Agencia` (`PontoReferencia_Id_Agencia`),
  CONSTRAINT `FK_Usuario_Pessoa_Id` FOREIGN KEY (`Pessoa_Id`) REFERENCES `ART_Pessoa` (`Id`) ON UPDATE CASCADE,
  CONSTRAINT `FK_Usuario_PontoReferencia_Id_Agencia` FOREIGN KEY (`PontoReferencia_Id_Agencia`) REFERENCES `ART_PontoReferencia` (`Id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1000000 DEFAULT CHARSET=utf8 COLLATE=utf8_swedish_ci AVG_ROW_LENGTH=5461;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ART_Usuario`
--

LOCK TABLES `ART_Usuario` WRITE;
/*!40000 ALTER TABLE `ART_Usuario` DISABLE KEYS */;
INSERT INTO `ART_Usuario` VALUES (1,'Administrador Guaçu Tur','admin2@guacutur.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.1CqSEK.GbN44L2Sv9YgmKfTLS7nULQy',2,98,NULL,NULL,NULL),(2,'Administrador Artaban','admin@veritasweb.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.1CqSEK.GbN44L2Sv9YgmKfTLS7nULQy',1,98,'0000-00-00 00:00:00',NULL,NULL),(3,'Administrador Guaçu Tur','admin@guacutur.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.1CqSEK.GbN44L2Sv9YgmKfTLS7nULQy',2,98,'0000-00-00 00:00:00',NULL,NULL),(4,'Marcus','marcus@guacutur.com.br','40bd001563085fc35165329ea1ff5c5ecbdbbeef',2,98,NULL,NULL,NULL),(5,'FERNANDA FERNANDES','fernanda@guacutur.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.uvjQGJAQkTaszEEzhyKacBKd8MHvxAm',2,98,NULL,NULL,NULL),(6,'IVETE FERRARI GOBBATO','ivete@guacutur.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.mkzriI4OKzoKNZlLOd9iHFbOKTqi8ji',2,98,NULL,NULL,NULL),(7,'GABRIELA TESSER PELICIOLI','gabriela@guacutur.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.7qDm4xWxAX/OGKaJtvJXUZ.JrM3HiPW',2,98,NULL,NULL,NULL),(8,'ALINE RODECZ','aline@guacutur.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.3K.IIanu0vKU8VLDtz0dL3GW7EUl5XK',2,98,NULL,NULL,NULL),(9,'IONE MARIA DALL BELLO','ione@guacutur.com.br','$2y$08$5etGicY6cBW84EJVbPd8Z.SMpLTcGTiFPmY1PvrJ83QOkTtyGZ.IO',2,98,NULL,NULL,NULL),(999999,'Usuário venda web','','',1,999999,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ART_Usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-08-28  9:15:41
