﻿SELECT ap4.Descricao AS Destino,
  CONCAT( al1.Nome, " - ",ap3.Descricao) AS Embarque, ap.Poltrona, ap2.Nome, ap1.Nome AS "Razão Social", av2.Id
  FROM art_passagem ap
  JOIN art_viagemitem av2 ON ap.ViagemItem_Id_Origem = av2.Id
  JOIN art_horarioitem ah ON av2.HorarioItem_Id = ah.Id
  JOIN art_linhaitem al ON ah.LinhaItem_Id = al.ID
  JOIN art_pontoreferencia ap3 ON al.PontoReferencia_Id = ap3.Id
  JOIN art_localidade al1 ON ap3.Localidade_Id = al1.Id
  JOIN art_pessoa ap2 ON ap.Pessoa_Id = ap2.Id
  JOIN art_vendaitem av ON ap.Id = av.Passagem_Id
  JOIN art_venda av1 ON av.Venda_Id = av1.Id
  JOIN art_pessoa ap1 ON av1.Pessoa_Id_Cliente = ap1.Id
  JOIN art_viagem av3 ON ap.Viagem_Id = av3.Id
  JOIN art_horario ah1 ON av3.Horario_Id = ah1.Id
  JOIN art_linha al2 ON ah1.Linha_Id = al2.ID
  JOIN art_pontoreferencia ap4 ON al2.PontoReferencia_Id_Destino = ap4.Id
  WHERE ap.Viagem_Id = 39
  ORDER BY al.ID
