  SELECT
    DATE_FORMAT(DataOperacao,'%d/%m/%Y') AS Data, 
    Descricao, 
    IF(Operacao = 'Cr�dito', ValorOperacao, 0) AS Credito,
    IF(Operacao = 'D�bito', ValorOperacao, 0) AS Debito,
    ValorOperacao AS Valor,
    IF(Operacao = 'Cr�dito', ValorOperacao/0.01, 0) AS ValorCompra
  FROM
    ART_Fidelizacao
  WHERE
    Pessoa_Id = 3
  ORDER BY DataOperacao ASC;

SELECT
  DATE_FORMAT(DataOperacao,'%d/%m/%Y') AS Data, 
  Descricao, 
  IF(Operacao = 'Cr�dito', ValorOperacao, 0) AS Credito,
  IF(Operacao = 'D�bito', ValorOperacao, 0) AS Debito,
  IF(Operacao = 'Cr�dito', ValorOperacao/0.01, 0) AS ValorCompra
FROM
  ART_Fidelizacao
WHERE
  Pessoa_Id = 3
ORDER BY DataOperacao ASC;