Ext.define('App.myForm', {
   extend: 'Ext.form.FormPanel',
   requires: ['Ux.InputTextMask'],

   initComponent : function() {
      Ext.apply(this, {
         items: this.buildForm()
      });

      App.myForm.superclass.initComponent.call(this);
   },

   buildForm: function() {
      return [
         { fieldLabel: 'Telefone', name: 'telefone', allowBlank: false, xtype: 'textfield', plugins: [new Ux.InputTextMask('(99) 9999-9999')] },
         { fieldLabel: 'Celular', name: 'celular', allowBlank: false, xtype: 'textfield', plugins: [new Ux.InputTextMask('(99) 9999-9999')] }
      ];
   }
});