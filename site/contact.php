<?php
$field_name = $_POST['cf_name'];
$field_email = $_POST['cf_email'];
$field_message = $_POST['cf_message'];

$mail_to = 'marcusvarosa@gmail.com';
$subject = 'Mensagem enviada pelo formulário de contato do site ARTABAN por '.$field_name;

$body_message = 'De: '.$field_name."\n";
$body_message .= 'E-mail: '.$field_email."\n";
$body_message .= 'Mensagem: '.$field_message;

$headers = 'De: '.$field_email."\r\n";
$headers .= 'Responder para: '.$field_email."\r\n";

$mail_status = mail($mail_to, $subject, $body_message, $headers);

if ($mail_status) { ?>
	<script language="javascript" type="text/javascript">
		alert('Muito Obrigado! Sua mensagem foi recebida!');
		window.location = 'index.html';
	</script>
<?php
}
else { ?>
	<script language="javascript" type="text/javascript">
		alert('A mensagem falhou e não foi enviada. Por favor, envie um e-mail para marcusvarosa@gmail.com');
		window.location = 'index.html';
	</script>
<?php
}
?>