<?php

header('Content-type: application/pdf');
setlocale( LC_ALL, 'pt_BR');

require_once('config.db.php'); 
require_once("tcpdf2/tcpdf.php");

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$mes = isset($_GET["mes"]) ? $_GET["mes"] : date('n');

$query = "
SELECT 
    DAY(DataNascimento) AS Dia,
    Nome,
    (SELECT
        GROUP_CONCAT(ART_PessoaTelefone.telefone SEPARATOR '&nbsp;&nbsp;')
     FROM ART_PessoaTelefone
     WHERE ART_PessoaTelefone.Pessoa_Id = ART_Pessoa.Id
     GROUP BY ART_PessoaTelefone.Pessoa_Id
    ) AS Telefones
FROM ART_Pessoa
WHERE MONTH(DataNascimento) = $mes
ORDER BY Dia;";

$result = mysql_query($query);

$pdf = new TCPDF('P','mm','A4', true,'UTF-8', false); // configura o tamanho da página
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Veritas');
$pdf->SetTitle('Relatório em PDF');
$pdf->SetSubject('Gerador de Documento PDF');
$pdf->SetKeywords('PDF, Documento, TCPDF');

$mesExtenso = strtoupper(strftime('%B', strtotime(date("Y")."-".$mes."-01")));

$titulo = 'Aniversariantes - MÊS DE '.$mesExtenso;

$pdf->SetHeaderData('logo-guacutur.png', 80, $titulo); //, $mesExtenso, array(0,0,0), array(255,255,255));
$pdf->SetMargins(15, 22, 10);
$pdf->SetHeaderMargin(5);

$pdf->setPrintFooter(false); // remove o rodapé

$pdf->SetFont('helvetica', '', 12); // define o tamanho e o tipo da fonte no texto. 
$pdf->AddPage(); // adiciona a primeira página.
//A partir daqui, basta incluirmos o texto desejado ao documento.

$pdf->SetFont('helvetica', '', 8);

$html = '
<table border="0" cellpadding="2" cellspacing="0">
    <tr style="font-weight: bold; font-size: 10pt">
        <th width="20" style="border-top: #000 3px double; border-bottom: #000 3px double;">Dia</th>
        <th width="250" style="border-top: #000 3px double; border-bottom: #000 3px double;">Cliente</th>
        <th width="250" style="border-top: #000 3px double; border-bottom: #000 3px double;">Telefones</th>
    </tr>
';

while($row = mysql_fetch_assoc($result)) {
    $row = array_map('utf8_encode', $row);
    $html .= '
    <tr>
        <td>'.$row["Dia"].'</td>
        <td>'.$row["Nome"].'</td>
        <td>'.$row["Telefones"].'</td>
    </tr>';
}

$html .= '
</table>';

mysql_close();

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$pdf->Output('relatorio.pdf', 'I');