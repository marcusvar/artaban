<?php
header('Content-type: application/pdf');

require_once('config.db.php'); 
require_once("tcpdf/tcpdf.php");

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id = isset($_GET["Id"]) ? $_GET["Id"] : 1307;

$result = mysql_query("SELECT * FROM ART_Pessoa WHERE Id=$id");
$cliente = mysql_fetch_assoc($result);
$cliente = $cliente["Nome"];

$query = 
"SELECT
  IFNULL(Fidelizacao.DataOperacao, ART_Viagem.DataInicio) AS DataOperacao,
  DATE_FORMAT(IFNULL(Fidelizacao.DataOperacao, ART_Viagem.DataInicio),'%d/%m/%Y') AS DataCreditoDebito,
  Fidelizacao.Descricao, 
  Fidelizacao.Operacao,
  Fidelizacao.ValorOperacao,
  IF(Fidelizacao.Operacao = 'Crédito', ValorOperacao, 0) AS Credito,
  IF(Operacao = 'Débito', ValorOperacao, 0) AS Debito,
  IF(Operacao = 'Crédito', ValorOperacao/0.01, 0) AS ValorCompra
FROM
  ART_Fidelizacao AS Fidelizacao
LEFT JOIN ART_Pessoa    ON ART_Pessoa.Id          = Fidelizacao.Pessoa_Id
LEFT JOIN ART_Venda     ON ART_Venda.Id           = Fidelizacao.Venda_Id
LEFT JOIN ART_VendaItem ON ART_VendaItem.Venda_Id = ART_Venda.Id
LEFT JOIN ART_Passagem  ON ART_Passagem.Id        = ART_VendaItem.Passagem_Id
LEFT JOIN ART_Viagem    ON ART_Viagem.Id          = ART_Passagem.Viagem_Id
WHERE
  Fidelizacao.Pessoa_Id = $id OR ART_Pessoa.Pessoa_Id_Responsavel = $id
ORDER BY DataOperacao DESC;";

$result = mysql_query($query);

$pdf = new TCPDF('P','mm','A4', true,'UTF-8', false); // configura o tamanho da página
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Veritas');
$pdf->SetTitle('Relatório em PDF');
$pdf->SetSubject('Gerador de Documento PDF');
$pdf->SetKeywords('PDF, Documento, TCPDF');

$titulo = 'Extrato de fidelização - Mega Polo';

$pdf->SetHeaderData('logo-guacutur.png', 80, $titulo, utf8_encode($rowViagem["Descricao"])."\n", array(0,0,0), array(255,255,255));
$pdf->SetMargins(15, 22, 10);
$pdf->SetHeaderMargin(5);

$pdf->setPrintFooter(false); // remove o rodapé

$pdf->SetFont('helvetica', '', 12); // define o tamanho e o tipo da fonte no texto. 
$pdf->AddPage(); // adiciona a primeira página.
//A partir daqui, basta incluirmos o texto desejado ao documento.

$pdf->SetFont('','',9);

$html = '
Cliente: '.$cliente.'<br /><br />
<table border="0" cellpadding="2" cellspacing="0">
    <tr style="font-weight: bold; font-size: 10pt">
        <th width="100" style="border-top: #000 3px double; border-bottom: #000 3px double;">Data Viagem/Crédito</th>
        <th width="200" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: left">Descrição</th>
        <th width="90" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Valor da compra</th>
        <th width="65" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Crédito</th>
        <th width="65" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Débito</th>
    </tr>
';
$saldo = 0;
while($row = mysql_fetch_array($result)) {
    $oper = utf8_encode($row["Operacao"]);
    $html .= '
    <tr>
        <td style="text-align: left">'.$row["DataCreditoDebito"].'</td>
        <td style="text-align: left">'.utf8_encode($row["Descricao"]).'</td>
        <td style="text-align: right">'.number_format($oper==="Crédito"?($row["ValorOperacao"]/0.01):0, 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($oper==="Crédito"?$row["ValorOperacao"]:0, 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($oper==="Débito"?$row["ValorOperacao"]:0, 2, ',', '.').'</td>
    </tr>';
    $saldo += (float) ((utf8_encode($row["Operacao"])==="Crédito"?1:-1)*$row["ValorOperacao"]);        
}
$html .= '
    <tr>
        <td style="text-align: left; border-top: #000 3px double; border-bottom: #000 3px double; font-weight: bold; font-size: 10px;">Saldo</td>
        <td colspan="3" style="text-align: right; border-top: #000 3px double; border-bottom: #000 3px double;">&nbsp;</td>
        <td style="text-align: right; border-top: #000 3px double; border-bottom: #000 3px double; font-weight: bold;">'.number_format($saldo, 2, ',', '.').'</td>
    </tr>
</table>';

mysql_close();

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$pdf->Output('relatorio.pdf', 'I');
exit;