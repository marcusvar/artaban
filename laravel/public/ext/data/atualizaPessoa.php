<?php
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

//ChromePhp::log($_POST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);
 
$data = json_decode(json_encode($_POST), FALSE);

//ChromePhp::log($data);

$query = sprintf("UPDATE ART_Pessoa 
         SET Nome       = '%s', 
             Documento  = '%s' , 
             RGIE       = '%s', 
             Sexo       = '%s', 
             Pessoa_Id  = %s, 
             Tipo       = '%s', 
             created_at = '%s', 
             updated_at = '%s'
         WHERE Id = %s",
    mysql_real_escape_string($data->Nome),
    mysql_real_escape_string($data->Documento),
    mysql_real_escape_string($data->RGIE),
    mysql_real_escape_string($data->Sexo),
    mysql_real_escape_string($data->Pessoa_Id), 
    mysql_real_escape_string($data->TipoPessoa),
    date('Y-m-d'), date('Y-m-d'),
    $data->Id
);

//ChromePhp::log($query);

mysql_query($query);

$telefone = $data->telefone;

//ChromePhp::log($telefone);

$empty = true;
foreach ($telefone as $val) {
    if(!empty($val->Descricao) and !empty($val->Numero) ) {
        $empty = false;
        break;
    }
}

if(!$empty) {
    $query = "DELETE FROM ART_PessoaTelefone WHERE Pessoa_Id = ".$data->Id;
    //ChromePhp::log($query);
    mysql_query( $query );
    foreach ($telefone as $val) {
        if(!empty($val->Descricao) and !empty($val->Numero) ) {
            $query = sprintf("INSERT INTO ART_PessoaTelefone (Descricao, Telefone, Pessoa_Id, created_at, updated_at ) ".
                             "values ('%s', '%s', %s, '%s', '%s')",
                mysql_real_escape_string($val->Descricao),
                mysql_real_escape_string($val->Numero),
                mysql_real_escape_string($data->Id),
                date('Y-m-d'), date('Y-m-d'));
//ChromePhp::log($query);
            mysql_query($query);
        }
    }
}

echo json_encode(array(
    "result"=> 
        array( "success" => mysql_errno(),
               "msgerr" => mysql_error(),
               "retorno" => array(
                    "Id"         => $data->Id,
                    "Nome"       => $data->Nome,
                    "Documento"  => $data->Documento,
                    "RGIE"       => $data->RGIE,
                    "Sexo"       => $data->Sexo,
                    "TipoPessoa" => $data->Tipo,
                    "Pessoa_Id"  => $data->Pessoa_Id
        ))
));

mysql_close();
