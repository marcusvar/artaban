<?php
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id  = $_REQUEST['Id'];
//$id = 3;

$web = $_REQUEST['Web'];
//$web = false;

if(isset($id) and $id !== null and $id !== "") {
    $query = "
SELECT 
  ART_Pessoa.Id, 
  ART_Pessoa.Nome, 
  ART_Pessoa.RGIE, 
  ART_Pessoa.Sexo, 
  ART_Pessoa.Tipo, 
  ART_Pessoa.Documento,
  (SELECT
      COALESCE(
      SUM(
      ART_Fidelizacao.ValorOperacao * (
      CASE WHEN
        ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
      END)
      ), 0) saldo
    FROM ART_Fidelizacao
    WHERE  
      ART_Fidelizacao.Pessoa_Id = ART_Pessoa.Id 
      ".($web ? "AND ART_Pessoa.VisualizarFidelizacao" : "").") AS Fidelizacao
FROM 
  ART_PessoaVinculo
JOIN ART_Pessoa ON ART_PessoaVinculo.Pessoa_Id_Vinculo = ART_Pessoa.Id
WHERE 
  ART_PessoaVinculo.Pessoa_Id = $id"; 
} else {
    $query = "
SELECT *, 
  (SELECT
      COALESCE(
      SUM(
      ART_Fidelizacao.ValorOperacao * (
      CASE WHEN
        ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
      END)
      ), 0) saldo
    FROM ART_Fidelizacao
    WHERE  
      ART_Fidelizacao.Pessoa_Id = ART_Pessoa.Id AND
      ART_Pessoa.VisualizarFidelizacao) AS Fidelizacao
    FROM ART_Pessoa WHERE Tipo LIKE 'PF%'";
}

$query = utf8_decode($query)."\n ORDER BY ART_Pessoa.Nome";

$result = mysql_query($query); 

$retorno["result"] = "";

$rows = 0;
while($row = mysql_fetch_assoc($result)) {    
    $retorno["result"][$rows] = array( "Id" => $row["Id"], 
                                       "Pessoa_Id_Vinculo" => $id,
                                       "Nome" => utf8_encode($row["Nome"]),
                                       "Documento" => $row["Documento"], 
                                       "RGIE" => $row["RGIE"],
                                       "Sexo" => $row["Sexo"],
                                       "TipoPessoa" => $row["Tipo"],
                                       "Fidelizacao" => $row["Fidelizacao"]);
    $queryTel = "SELECT * FROM ART_PessoaTelefone WHERE Pessoa_Id = ".$row["Id"];
    $resultTel = mysql_query($queryTel); 
    while($rowTel = mysql_fetch_assoc($resultTel)) {
        $retorno["result"][$rows]["Telefones"][] = array( "Descricao" => utf8_encode($rowTel["Descricao"]), 
                                                          "Telefone"  => $rowTel["Telefone"]);
        
    }
    $rows++;
}

echo json_encode($retorno);