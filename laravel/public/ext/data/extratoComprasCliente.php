<?php
header('Content-type: application/pdf');

require_once('config.db.php'); 
require_once("tcpdf/tcpdf.php");

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id = isset($_GET["Id"]) ? $_GET["Id"] : 3205;

$result = mysql_query("SELECT * FROM ART_Pessoa WHERE Id=$id");
$cliente = mysql_fetch_assoc($result);
$cliente = $cliente["Nome"];

$query = 
"SELECT 
  ART_VendaItem.Id,
  ART_Viagem.Descricao,  
  DATE_FORMAT(ART_Venda.DataHoraEmissao,'%d/%m/%Y %h:%i') AS Emissao,
  ART_Passagem.Valor, 
  ART_Passagem.Desconto, 
  ART_Passagem.Adicional, 
  ART_TipoAdicionalTarifa.Descricao AS TipoAdicional, 
  ART_Fidelizacao.ValorOperacao AS Fidelizacao,
  (ART_Passagem.Valor - ART_Passagem.Desconto - ifnull(ART_Fidelizacao.ValorOperacao,0) + ART_Passagem.Adicional) AS Total,
  DATE_FORMAT(ART_Viagem.DataInicio,'%d/%m/%Y %h:%i') AS DataViagem,
  ART_Viagem.HoraInicio AS HoraViagem 
FROM
  ART_VendaItem
JOIN ART_Venda ON ART_VendaItem.Venda_Id = ART_Venda.Id
JOIN ART_Passagem ON ART_VendaItem.Passagem_Id = ART_Passagem.Id
JOIN ART_Pessoa ON ART_Passagem.Pessoa_Id = ART_Pessoa.Id
LEFT JOIN ART_Fidelizacao ON ART_Venda.Id = ART_Fidelizacao.Venda_Id
JOIN ART_Viagem ON ART_Passagem.Viagem_Id = ART_Viagem.Id
LEFT JOIN ART_TipoAdicionalTarifa ON ART_TipoAdicionalTarifa.Id = ART_Passagem.TipoAdicionalTarifa_Id 
WHERE 
  ART_Pessoa.Id = $id 
ORDER BY 
  ART_Venda.DataHoraEmissao";

$result = mysql_query($query);

$pdf = new TCPDF('P','mm','A4', true,'UTF-8', false); // configura o tamanho da página
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Veritas');
$pdf->SetTitle('Relatório em PDF');
$pdf->SetSubject('Gerador de Documento PDF');
$pdf->SetKeywords('PDF, Documento, TCPDF');

$titulo = 'Extrato de compras';

$pdf->SetHeaderData('logo-guacutur.png', 80, $titulo, utf8_encode($rowViagem["Descricao"])."\n", array(0,0,0), array(255,255,255));
$pdf->SetMargins(15, 22, 10);
$pdf->SetHeaderMargin(5);

$pdf->setPrintFooter(false); // remove o rodapé

$pdf->SetFont('helvetica', '', 12); // define o tamanho e o tipo da fonte no texto. 
$pdf->AddPage(); // adiciona a primeira página.
//A partir daqui, basta incluirmos o texto desejado ao documento.

$pdf->SetFont('','',9);

$html = '
Cliente: '.$cliente.'<br /><br />
<table border="0" cellpadding="2" cellspacing="0">
    <tr style="font-weight: bold; font-size: 10pt">
        <th width="90" style="border-top: #000 3px double; border-bottom: #000 3px double;">Data da Viagem</th>
        <th width="155" style="border-top: #000 3px double; border-bottom: #000 3px double;">Descrição</th>
        <th width="40" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Valor</th>
        <th width="50" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Desconto</th>
        <th width="60" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Fidelização</th>
        <th width="85" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Adicional</th>
        <th width="50" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Total</th>
    </tr>
';
$total = 0;
while($row = mysql_fetch_assoc($result)) {
    $html .= '
    <tr>
        <td style="text-align: center">'.$row["DataViagem"].'</td>
        <td>'.utf8_encode($row["Descricao"]).'</td>
        <td style="text-align: right">'.number_format($row["Valor"], 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($row["Desconto"], 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($row["Fidelizacao"], 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($row["Adicional"], 2, ',', '.').' - '.$row["TipoAdicional"].'</td>
        <td style="text-align: right">'.number_format($row["Total"], 2, ',', '.').'</td>
    </tr>';
    $total += (float) $row["Total"];        
}
$html .= '
    <tr>
        <td style="text-align: center; border-top: #000 3px double; border-bottom: #000 3px double; font-weight: bold; font-size: 10px;">Total em compras</td>
        <td colspan="5" style="text-align: right; border-top: #000 3px double; border-bottom: #000 3px double;">&nbsp;</td>
        <td style="text-align: right; border-top: #000 3px double; border-bottom: #000 3px double; font-weight: bold;">'.number_format($total, 2, ',', '.').'</td>
    </tr>
</table>';

mysql_close();

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$pdf->Output('Relatorio.pdf', 'I');
exit;