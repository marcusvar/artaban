<?php
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// login e senha enviada do formulário 
$email = isset($_POST['email']) ? $_POST['email'] : "marcusvarosa@gmail.com"; 
$pass = isset($_POST['password']) ? $_POST['password'] : "a94ba4d2135d9dc40ce1f55efddd681a15fd6f36"; 

// para proteger o banco contra MySQL injection
$email = stripslashes($email);
$pass = stripslashes($pass);

$email = mysql_real_escape_string($email);
$pass = mysql_real_escape_string($pass);

$query = "SELECT * FROM ART_Pessoa WHERE Email='$email' AND Senha='$pass'";
$result = mysql_query($query); 
$row = mysql_fetch_assoc($result);

if($row) {
    $success = $row['Ativo'] ? true : false;
    $msg = $row['Ativo'] ? 'Usuário autenticado!' : 'Seu cadastro não está ativo!';
} else {
    $success = false;
    $msg = 'Login ou senha incorretos.';
}

/* encerra a conexão */
mysql_close();

echo json_encode(array(
    "success" => $success,
    "msg"     => $msg,
    "status"   => $row ? true : false,
    "result"  => 
        array(
            "Id"    => $row['Id'],
            "Nome"  => $row['Nome'],
            "Data"  => date('Y-m-d\TH:i:s'),
            "Email" => $row['Email']        
        ))
);