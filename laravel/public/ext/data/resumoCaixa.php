<?php
/*
 */
require_once('config.db.php'); 
require_once("tcpdf/tcpdf.php");

//include '../chromephp/ChromePhp.php';

$viagemId = isset($_REQUEST["viagemId"]) ? $_REQUEST["viagemId"] : 175;

//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = 
"SELECT 
    (SELECT 
     CONCAT(ART_PontoReferencia.Descricao, ' - ', 
         DATE_FORMAT(ART_Viagem.DataInicio, '%d/%m/%Y'), ' - ', 
         ART_Viagem.Duracao, IF(ART_Viagem.Duracao=1,' dia','dia(s)'),' - ', 
         ART_Viagem.Descricao, ' - ',COALESCE(ART_Veiculo.Numero,'')) Descricao  
     FROM  
        ART_Viagem
     JOIN ART_Horario ON ART_Viagem.Horario_Id = ART_Horario.Id
     JOIN ART_Linha ON ART_Horario.Linha_Id = ART_Linha.ID
     JOIN ART_PontoReferencia ON ART_Linha.PontoReferencia_Id_Destino = ART_PontoReferencia.Id
     LEFT JOIN ART_Veiculo ON ART_Viagem.Veiculo_Id = ART_Veiculo.Id
     WHERE
        ART_Viagem.Id = $viagemId) AS Descricao,
    ART_ViagemCaixa.*, 
    ART_Veiculo.Numero AS Carro,
    (SELECT group_concat(Lotacao SEPARATOR ', ')
     FROM 
	(SELECT CONCAT(CONVERT(count(*), char), ' ',
	IF(Parcial = 'C', ' Completa(s) ',
	IF(Parcial = 'I', ' Ida(s)', ' Volta(s)'))) AS Lotacao
	 FROM ART_Passagem
	 WHERE Viagem_Id = $viagemId AND Poltrona <> '' AND Situacao = 'Normal'
	 GROUP BY Parcial) x) AS Lotacao,
    (SELECT 
	SUM(Valor-Desconto+(
            SELECT COALESCE(SUM(apa.Valor),0) 
            FROM ART_PassagemAdicional apa 
            WHERE apa.Passagem_Id=ART_Passagem.Id)) 
	 FROM ART_Passagem 
	 WHERE Viagem_Id = $viagemId AND Poltrona <> '' AND Situacao = 'Normal') Total,
    (SELECT
        GROUP_CONCAT(ART_Pessoa.Nome SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        JOIN ART_Pessoa ON ART_Pessoa.Id = ART_ViagemCaixaEncomendas.Pessoa_Id 
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS Cliente,
    (SELECT
        GROUP_CONCAT(ART_TipoAdicionalTarifa.Descricao SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        JOIN ART_TipoAdicionalTarifa ON ART_TipoAdicionalTarifa.Id = ART_ViagemCaixaEncomendas.TipoAdicionalTarifa_Id
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS TipoAdicionalTarifa,
    (SELECT
        GROUP_CONCAT(ART_ViagemCaixaEncomendas.Quantidade SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS Quantidade,
    (SELECT
        GROUP_CONCAT(ART_ViagemCaixaEncomendas.Valor SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS Valor
FROM ART_ViagemCaixa
LEFT JOIN ART_Viagem ON ART_Viagem.Id = ART_ViagemCaixa.Viagem_Id 
LEFT JOIN ART_Veiculo ON ART_Veiculo.Id = ART_Viagem.Veiculo_Id
WHERE ART_ViagemCaixa.Viagem_Id = $viagemId;";

$ret = mysql_query(utf8_decode($query));

$row = mysql_fetch_assoc($ret);
        
$row = array_map('utf8_encode', $row);

mysql_close();

$pdf = new TCPDF('P','mm','A4', true,'UTF-8', false); // configura o tamanho da página
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Veritas');
$pdf->SetTitle('Relatório em PDF');
$pdf->SetSubject('Gerador de Documento PDF');
$pdf->SetKeywords('PDF, Documento, TCPDF');

$titulo = 'Resumo de Caixa';

$pdf->SetHeaderData('logo-guacutur.png', 80, $titulo, $row["Descricao"]."\n", array(0,0,0), array(255,255,255));
$pdf->SetMargins(15, 22, 10);
$pdf->SetHeaderMargin(5);

$pdf->setPrintFooter(false); // remove o rodapé

$pdf->SetFont('helvetica', '', 12); // define o tamanho e o tipo da fonte no texto. 
$pdf->AddPage(); // adiciona a primeira página.
//A partir daqui, basta incluirmos o texto desejado ao documento.

$html = '<table border="0" cellpadding="0" cellspacing="0"><tr><td height="590">';

//$pdf->SetFont('','',12);
$cliente = explode(';', $row['Cliente']);
$numEncom = $row['Cliente'] === "" ? 0 : count($cliente);
if($numEncom > 0){
    $tipo  = explode(';', $row['TipoAdicionalTarifa']);
    $qtd   = explode(';', $row['Quantidade']);
    $valor = explode(';', $row['Valor']);
    $html .= '
    <span style="font-weight: bold; text-decoration: undeline">Encomendas (Volumes/Excessos)</span><br /><br />

    <table border="0" cellpadding="2" cellspacing="0">
        <tr style="font-weight: bold; font-size: 10pt">
            <th width="230" style="border-top: #000 3px double; border-bottom: #000 3px double;">Cliente</th>
            <th width="110" style="border-top: #000 3px double; border-bottom: #000 3px double;">Tipo</th>
            <th width="60" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Quantidade</th>
            <th width="90" style="border-top: #000 3px double; border-bottom: #000 3px double; text-align: right">Valor</th>
        </tr>
    ';
    $totQtd = $totValor = 0;
    for($i=0; $i<$numEncom; $i++){
        $html .= 
        '<tr>'.    
            '<td>'.$cliente[$i].'</td>'.
            '<td>'.$tipo[$i].'</td>'.
            '<td style="text-align: right">'.$qtd[$i].'</td>'.
            '<td style="text-align: right">R$ '.number_format($valor[$i], 2, ',', '.').'</td>'.
        '</tr>';
        $totQtd   += $qtd[$i];
        $totValor += $valor[$i];
    }
    $html .= '
        <tr>
            <td style="text-align: left; border-top: #000 3px double; border-bottom: #000 3px double; font-weight: bold;">Total</td>
            <td colspan="2" style="text-align: right; border-top: #000 3px double; border-bottom: #000 3px double;">'.$totQtd.'</td>
            <td style="text-align: right; border-top: #000 3px double; border-bottom: #000 3px double; font-weight: bold;">R$ '.number_format($totValor, 2, ',', '.').'</td>
        </tr>
    </table><br /><br />';    
} 

$html .= '
<span style="font-weight: bold; text-decoration: undeline">RESUMO DA VIAGEM</span><br /><br />

<table border="0" cellpadding="2" cellspacing="0">
    <tr><td width="30%" style="font-weight: bold;">Motoristas: </td><td width="70%">'.$row['Motoristas'].'</td></tr>
    <tr><td style="font-weight: bold;">Carro: </td><td>'.$row['Carro'].'</td></tr>
    <tr><td style="font-weight: bold;">Lotação: </td><td>'.$row['Lotacao'].'</td></tr>
    <tr><td style="font-weight: bold;">Recolher Foz: </td><td>'.$row['RecolherFoz'].'</td></tr>
    <tr><td style="font-weight: bold;">Recolher Toledo: </td><td>'.$row['RecolherToledo'].'</td></tr>
    <tr><td style="font-weight: bold;">Devolver Foz: </td><td>'.$row['DevolverFoz'].'</td></tr>
    <tr><td style="font-weight: bold;">Devolver Toledo: </td><td>'.$row['DevolverToledo'].'</td></tr>
    <tr><td style="font-weight: bold;">Total da viagem: </td><td>R$ '.number_format($row['Total'] + $totValor, 2, ',', '.').'</td></tr>
</table>
</td></tr></table>
<table border="0" cellpadding="2" cellspacing="0">
    <tr><td width="20%" style="font-weight: bold;">Dinheiro: </td><td width="80%">R$ '.number_format($row['Dinheiro'], 2, ',', '.').'</td></tr>
    <tr><td style="font-weight: bold;">Cheques: </td><td>R$ '.number_format($row['Cheques'], 2, ',', '.').'</td></tr>
    <tr><td style="font-weight: bold;">Vales: </td><td>R$ '.number_format($row['Vales'], 2, ',', '.').'</td></tr>
    <tr><td style="font-weight: bold;">TOTAL: </td><td>R$ '.number_format($row['Dinheiro'] + $row['Cheques'] + $row['Vales'], 2, ',', '.').'</td></tr>
</table>';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$pdf->Output('relatorio.pdf', 'I');