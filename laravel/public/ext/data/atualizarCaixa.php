<?php
require_once('config.db.php'); 
$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

/*
$_POST = array( "Id" => 34,
                "Motoristas" => "Pailo Henrique e Eder",
                "Hotel" => "Mega Polo",
                "RecolherFoz" => "Roni C600",
                "RecolherToledo" => "Cafu 8180",
                "DevolverFoz" => "-",
                "DevolverToledo" => "-",
                "Cheques" => 756.97,
                "Vales" => 188,
                "Dinheiro" => 5340);
    $_POST['encomenda'][2] = array ( "Cliente" => 3,
                                     "Tipo"    => 3,
                                     "Qtd"     => 2,
                                     "Valor"   => "R$ 300,00"
                                   );
    
    $_POST['encomenda'][1] = array ( "Cliente" => 1693,
                                     "Tipo"    => 4,
                                     "Qtd"     => 3,
                                     "Valor"   => "R$ 450,00"
                                   );
*/

$data = json_decode(json_encode($_POST), FALSE);

$query = sprintf("UPDATE ART_ViagemCaixa 
         SET Motoristas     = '%s', 
             Hotel          = '%s', 
             RecolherFoz    = '%s', 
             RecolherToledo = '%s',
             DevolverFoz    = '%s', 
             DevolverToledo = '%s',
             Cheques        = %s, 
             Vales          = %s,
             Dinheiro       = %s
         WHERE Id = %s",
    mysql_real_escape_string($data->Motoristas),
    mysql_real_escape_string($data->Hotel),
    mysql_real_escape_string($data->RecolherFoz),
    mysql_real_escape_string($data->RecolherToledo),
    mysql_real_escape_string($data->DevolverFoz),
    mysql_real_escape_string($data->DevolverToledo),
    mysql_real_escape_string($data->Cheques),
    mysql_real_escape_string($data->Vales),
    mysql_real_escape_string($data->Dinheiro),
    $data->Id
);

$result = mysql_query($query) or die(mysql_error());

$encomendas = $data->encomenda;

$empty = true;
foreach ($encomendas as $val) {
    if(!empty($val->Valor) ) {
        $empty = false;
        break;
    }
}

// Exclui todos os lançamentos 
$query = "DELETE FROM ART_ViagemCaixaEncomendas WHERE ViagemCaixa_Id = ".$data->Id;
mysql_query( $query );

// Verifica se existe lançamentos de encomendas
if(!$empty) {
    foreach ($encomendas as $val) {
        $valor = str_replace(",", ".", str_replace(".", "", str_replace("R$ ", "", $val->Valor)));
        $query = sprintf("INSERT INTO ART_ViagemCaixaEncomendas (ViagemCaixa_Id, Pessoa_Id, TipoAdicionalTarifa_Id, Quantidade, Valor ) ".
                         "VALUES (%s, %s, %s, %s, %s)",
            mysql_real_escape_string($data->Id),
            mysql_real_escape_string($val->Cliente),
            mysql_real_escape_string($val->Tipo),
            mysql_real_escape_string($val->Qtd),
            mysql_real_escape_string($valor));
        mysql_query($query);
    }
}

echo json_encode(array(
    "result"=> 
        array( "success" => mysql_errno(),
               "msgerr" => mysql_error())
));

mysql_close();