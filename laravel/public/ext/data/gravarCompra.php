<?php
/*
 * Grava uma carrinho de compras (reserva ou compra)
 */
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

$_POST["jsonParam"] = isset($_POST["jsonParam"]) ? $_POST["jsonParam"] :
    '{"venda":{"TipoOperacao":"Atualizar"},"passageiros":{"0":{"Id":"3","Valor":210,"Poltrona":"17","Viagem_Id":"123","ViagemItem_Id_Origem":"4","Tipo":"P","Parcial":"C","PassagemId":"2064","Desconto":22,"DescontoFidelizacao":11,"Adicional":{"0":{"Id":1,"Valor":33.33},"1":{"Id":3,"Valor":44.44}}}}}';

//ChromePhp::log($_POST);

$param = json_decode($_POST["jsonParam"], FALSE);

//$data = json_decode(json_encode($_POST), FALSE);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Insere os dados da venda na tabela ART_Venda
$venda = $param->venda;

$compra = $venda->TipoOperacao === 'Compra';

// Insere dados da venda
$query = sprintf("INSERT INTO ART_Venda (PontoReferencia_Id, Pessoa_Id_Cliente, Pessoa_Id, Usuario_Id, ValorDesconto) ".
                 "values (%s, %s, %s, %s, %s)",
    $venda->PontoReferencia_Id,
    $venda->Pessoa_Id_Cliente,
    $venda->Pessoa_Id,
    $venda->Usuario_Id,
    $venda->ValorDesconto    
); 
mysql_query($query);

// Recupera o ID da venda
$venda_id = mysql_insert_id();

$arr = $param->passageiros;

foreach ( $arr as $key => $value )
{
    // Recupera ID do passageiro
    $id = $value->Id;
    
    // Busca ID do item da viagem
    $query = sprintf("SELECT ART_ViagemItem.Id ".
                     "FROM ART_ViagemItem ".
                     "  LEFT JOIN ART_HorarioItem ON ART_ViagemItem.HorarioItem_Id = ART_HorarioItem.Id ".
                     "  LEFT JOIN ART_LinhaItem ON ART_HorarioItem.LinhaItem_Id = ART_LinhaItem.ID ".
                     "WHERE ART_LinhaItem.ID = %s AND ART_ViagemItem.Viagem_Id = %s", 
             $value->ViagemItem_Id_Origem, $value->Viagem_Id);

    $ret = mysql_query( $query, $link );
    $rows = mysql_fetch_array($ret); 

    // Verifica se é uma confirmação de uma reserva
    if($value->PassagemId === "") {
        // Salva os dados de cada passageiro na tabela ART_Passagem
        $query = sprintf("INSERT INTO ART_Passagem (Valor, Desconto, DescontoFidelizacao, Poltrona, Viagem_Id, " . 
                         "ViagemItem_Id_Origem, Pessoa_Id, Empresa_Id, Tipo, Situacao, Parcial, created_at, updated_at) ".
                         "VALUES (%s, %s, %s, '%s', %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s')",
            $value->Valor,
            $value->Desconto,
            $value->DescontoFidelizacao,
            $value->Poltrona,
            $value->Viagem_Id,
            $rows["Id"], // Id do item da viagem relacionado com o local de embarque
            $id,
            $venda->Pessoa_Id_Cliente === '' ? 'NULL' : $venda->Pessoa_Id_Cliente,
            $value->Tipo, // R = Reserva, P = Passagem
            'Normal',
            $value->Parcial,
            date('Y-m-d H:i:s'), date('Y-m-d H:i:s')
        ); 
        mysql_query($query);    

        // Recupera o ID da passagem
        $passagem_id = mysql_insert_id();
        
        // Salva adicionais da compra da passagem
        $add = $value->Adicional;
        foreach ( $add as $keyAdd => $valueAdd )
        {
            $queryAdd = sprintf("INSERT INTO ART_PassagemAdicional (Passagem_Id, TipoAdicionalTarifa_Id, Valor, created_at, updated_at) ".
                             "values (%s, %s, %s, '%s', '%s')",
                $passagem_id,
                $valueAdd->Id,
                $valueAdd->Valor,
                date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

            mysql_query($queryAdd);
        }
    } else {
        $passagem_id = $value->PassagemId;
        $query = sprintf("UPDATE ART_Passagem 
                          SET Tipo                   = '%s',
                              Desconto               = %s, 
                              DescontoFidelizacao    = %s,
                              updated_at             = '%s'
                          WHERE Id = %s",
            mysql_real_escape_string($value->Tipo),
            $value->Desconto,    
            $value->DescontoFidelizacao,
            date('Y-m-d'),
            $passagem_id
        );        
        mysql_query($query); 
        // Atualiza os valores dos adicionais
        mysql_query("DELETE FROM ART_PassagemAdicional WHERE Passagem_Id=$passagem_id;");
        $add = $value->Adicional;
        foreach ( $add as $keyAdd => $valueAdd )
        {
            $queryAdd = sprintf("INSERT INTO ART_PassagemAdicional (Passagem_Id, TipoAdicionalTarifa_Id, Valor,  created_at, updated_at) ".
                             "values (%s, %s, %s, '%s', '%s')",
                $passagem_id,
                $valueAdd->Id,
                $valueAdd->Valor,
                date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

            mysql_query($queryAdd);
        }
    }
    
    // salva os dados na tabela ART_VendaItem
    $query = sprintf("INSERT INTO ART_VendaItem (Venda_Id, Passagem_Id, created_at, updated_at) ".
                     "values (%s, %s, '%s', '%s')",
        $venda_id,
        $passagem_id,
        date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

    mysql_query($query);
    
    if($value->DescontoFidelizacao > 0) {
        $query = sprintf("INSERT INTO ART_Fidelizacao (Operacao, DataOperacao, Descricao, ".
                         "ValorOperacao, Pessoa_Id, Venda_Id, created_at, updated_at) ".
                         "values ('%s', '%s', '%s', %s, %s, %s, '%s', '%s')",
            utf8_decode('Débito'),
            date('Y-m-d H:i:s'),
            'Compra pelo sistema de retaguarda Artaban',
            $value->DescontoFidelizacao,
            $venda->Pessoa_Id_Cliente,    
            $venda_id,
            date('Y-m-d H:i:s'), date('Y-m-d H:i:s')    
        );
        mysql_query($query); 
    }         
}          

echo json_encode(array(
    "result" => array( 
        "merchantreference" => $venda_id
    ),
    "success" => mysql_errno(),
    "msg"     => mysql_error()
));

mysql_close();
