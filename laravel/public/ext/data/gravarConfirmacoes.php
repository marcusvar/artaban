<?php

require_once('config.db.php'); 

$param = json_decode($_POST["jsonParam"], FALSE);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

foreach ( $param as $key => $value ) {
    if($value->Tipo !== 'W') {
        $query = sprintf("UPDATE ART_Passagem 
                          SET Tipo                   = '%s',
                              updated_at             = '%s'
                          WHERE Id = %s",
            mysql_real_escape_string($value->Tipo),
            date('Y-m-d'),
            $value->Id
        );        
        mysql_query($query);         
    }
}

echo json_encode(array(
    "success" => mysql_errno(),
    "msg"     => mysql_error()
));

mysql_close();