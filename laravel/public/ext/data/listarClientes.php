<?php
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
$nome  = utf8_decode($_REQUEST['query']);

$query = "SELECT * FROM(
SELECT 
  ART_Pessoa.Id,
  ART_Pessoa.Nome,
  NULL AS IdResponsavel,
  NULL AS Responsavel
FROM
  ART_Pessoa
WHERE
  SUBSTR(ART_Pessoa.Tipo,1,2)='PF' AND
  ART_Pessoa.Nome like CONCAT('%$nome%')
UNION
SELECT
  IFNULL(Emp.Id,ART_Pessoa.Id) AS Id,
  IFNULL(Emp.Nome,ART_Pessoa.Nome) Nome,
  IFNULL(Emp.Pessoa_Id_Responsavel,ART_Pessoa.Pessoa_Id_Responsavel) IdResponsavel,
  (SELECT Nome FROM ART_Pessoa ap WHERE ap.Id=ART_Pessoa.Id) AS Responsavel
FROM
  ART_Pessoa
LEFT JOIN ART_Pessoa Emp ON Emp.Pessoa_Id_Responsavel = ART_Pessoa.Id
WHERE
  NOT ISNULL(Emp.Pessoa_Id_Responsavel) AND
  (Emp.Nome like '%$nome%' OR ART_Pessoa.Nome LIKE '%$nome%')
) AS Consulta
WHERE   
  NOT ISNULL(Consulta.Id)
ORDER BY 
  Consulta.Nome ASC      
LIMIT $start,  $limit";
$result = mysql_query($query); 
        
$retorno = array();

while ($row = mysql_fetch_assoc($result)) {
    $retorno[] = array( "Id" => $row["Id"], 
                        "Nome" => utf8_encode($row["Nome"]),
                        "IdResponsavel" => $row["IdResponsavel"],
                        "Responsavel" => utf8_encode($row["Responsavel"]));
}

//consulta total de linhas na tabela
$queryTotal = mysql_query(
        "SELECT count(*) as num ".
        "FROM ART_Pessoa ".
        "LEFT JOIN ART_Pessoa AS Empresa ON Empresa.Pessoa_Id_Responsavel = ART_Pessoa.Id ".
        "WHERE ART_Pessoa.Nome ". 
        "LIKE '%$nome%'");

$row = mysql_fetch_assoc($queryTotal);
$total = $row['num'];

//encoda para formato JSON
echo json_encode(array(
    "total" => $total,
    "result" => $retorno
));

