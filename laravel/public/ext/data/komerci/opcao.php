﻿<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Pagamento</title>
<style type="text/css">
body {
	text-align: center; 
	font-family: Tahoma, Geneva, sans-serif; 
	font-size: 12px;
	padding: 0;
	margin: 0;
}
form {
	margin: 0 auto;
}
#conteudo {
	margin: 0 auto;
	width: 440px;
}
.bandeira {
	float: left;
	width: 96px;
	height: 120px;
	position: relative;
	background-repeat: no-repeat;
	margin: 0 3px 0 3px;
	padding: 0 3px 0 3px;	
	border-radius: 7px;
	border: 1px solid #333;
}
#forma {
	color:#333;
	text-align: left;
	width: 395px;
	margin: 0 auto;
	height: 35px;
	font-size: 20px;
	padding: 5px 0 0 0;
}
#cifrao {
	border: 1px solid #004000;
	background-color:#060;
	color: #FFF;
	width: 26px;
	height: 22px;
	float: left;
	text-align: center;
	border-radius: 13px;
	padding: 0 0 4px 0;
}
#cartoes {
	color:#333;
	text-align: left;
	width: 395px;
	margin: 0 auto;
	height: 35px;
	font-size: 20px;
	padding: 5px 0 0 0;
	font-weight: bold;
}
#esfera-fora {
	border: 1px solid #333333;
	background-color:#666;
	width: 14px;
	height: 14px;
	float: left;
	text-align: center;
	border-radius: 7px;
	margin-top: 4px;
}
#esfera-dentro {
	background-color:#CCC;
	width: 10px;
	height: 10px;
	text-align: center;
	border-radius: 5px;	
	margin: 2px 0 0 2px;
}
</style>
</head>

<body>
<form name="form_card" action="https://ecommerce.redecard.com.br/pos_virtual/form_card.asp" method="post">
    <input type="hidden" name="TOTAL" value="0.01">
    <input type="hidden" name="TRANSACAO" value="04">
    <input type="hidden" name="PARCELAS" value="00">
    <input type="hidden" name="FILIACAO" value="44754779">
    <input type="hidden" name="DISTRIBUIDOR" value="999999999">
    <input type="hidden" name="NUMPEDIDO" value="123456">
    <input type="hidden" name="PAX1" value="XXXXXXX">
    <input type="hidden" name="CODVER" value="XXXXXXXXXXXXXXXX-XXX">
    <input type="hidden" name="URLBACK" value="https://artaban.veritasweb.com.br/response.php">
    <input type="hidden" name="URLCIMA" value="https://artaban.veritasweb.com.br/img/logo-guacutur.png"> 
    <div style="margin: 50px 0 20px 0">
    <img src="logo-guacutur.png" width="408" height="66"  alt=""/>
    </div>
    <div id="forma"><div id="cifrao">$</div>&nbsp;Escolha a opção de pagamento:</div>
    <div id="cartoes"><div id="esfera-fora"><div id="esfera-dentro"></div></div>&nbsp;Cartão de crédito:</div>
    <div id="conteudo">
        <div class="bandeira" style="background-image: url(visa.png);">
            <div style="position: absolute; bottom: 7px; left: 40px;">
                <input type="radio"  name="BANDEIRA" value="VISA" checked><br />
                Visa 
            </div>
        </div>
        <div class="bandeira" style="background-image: url(mastercard.png);">
            <div style="position: absolute; bottom: 7px; left: 21px;">
                <input type="radio"  name="BANDEIRA" value="MASTERCARD"><br />
                Mastercard 
            </div>
        </div>
        <div class="bandeira" style="background-image: url(diners.png);">
            <div style="position: absolute; bottom: 7px; left: 33px;">
                <input type="radio"  name="BANDEIRA" value="DINERS"><br />
                Diners 
            </div>
        </div>
        <div class="bandeira" style="background-image: url(hipercard.png);">
            <div style="position: absolute; bottom: 7px; left: 23px;">
                <input type="radio"  name="BANDEIRA" value="HIPERCARD"><br />
                Hipercard 
            </div>
        </div>    	
    </div>
    <div style="clear: both"></div>
    <div>
    	<p>&nbsp;</p>
    	<p><br />
    	    <input type = "submit" name = "enviar" value = "Prosseguir">
	    </p>    
    </div>
</form>
</body>
</html>