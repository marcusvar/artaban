<?php
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$doc  = isset($_REQUEST['cpfCnpj']) ? $_REQUEST['cpfCnpj'] : "335.324.430-15";

$isCpf = strlen($doc) === 14;

$query = 
    "SELECT 
       ART_Pessoa.*, Responsavel.Nome AS Responsavel
     FROM 
       ART_Pessoa
     LEFT JOIN ART_Pessoa Responsavel ON ART_Pessoa.Pessoa_Id_Responsavel = Responsavel.Id
     WHERE 
       ART_Pessoa.Documento = '$doc'"; 
$result = mysql_query($query); 
$row = mysql_fetch_assoc($result);

$retorno["result"] = array( "Id" => $isCpf ? $row["Id"] : $row["Pessoa_Id_Responsavel"], 
                            "Pessoa_Id" => $row["Pessoa_Id"],
                            "Nome" => $isCpf ? utf8_encode($row["Nome"]) : $row["Responsavel"],
                            "Email" => $row["Email"],
                            "Documento" => $row["Documento"], 
                            "RGIE" => $row["RGIE"],
                            "Sexo" => $row["Sexo"],
                            "Tipo" => $row["Tipo"],
                            "IdResponsavel" => $isCpf ? $row["Pessoa_Id_Responsavel"] : $row["Id"],
                            "Responsavel" => $isCpf ? $row["Responsavel"] : $row["Nome"] );

$query     = "SELECT * FROM ART_PessoaTelefone WHERE Pessoa_Id = ".$row["Id"];
$resultTel = mysql_query($query); 
while($rowTel = mysql_fetch_assoc($resultTel)) {
    $retorno["result"]["Telefones"][] = array( "Descricao" => utf8_encode($rowTel["Descricao"]), 
                                               "Telefone"  => $rowTel["Telefone"]);

}

echo json_encode($retorno);