<?php
header('Content-type: application/pdf');

require_once('config.db.php'); 
require_once("tcpdf2/tcpdf.php");

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$inicial = isset($_GET["inicial"]) ? $_GET["inicial"] : date('Y-m-d');
$periodo = isset($_GET["periodo"]) ? $_GET["periodo"] : 60;
$opcao   = isset($_GET["opcao"])   ? $_GET["opcao"]   : TRUE;

$query = "
select
    ifnull((select ART_Pessoa.Nome 
     from ART_Pessoa
     where ART_Pessoa.Id = Pessoa.Pessoa_Id_Responsavel
     limit 1
    ), Pessoa.Nome) as Cliente,
    Pessoa.NomeUsual,
    (SELECT
     GROUP_CONCAT(ART_PessoaTelefone.Descricao,': ',ART_PessoaTelefone.telefone SEPARATOR '<br /> ')
     FROM ART_PessoaTelefone
     WHERE ART_PessoaTelefone.Pessoa_Id = Pessoa.Id
     GROUP BY ART_PessoaTelefone.Pessoa_Id
    ) AS Telefones,
    Cidade.Nome as Cidade,
    DATE_FORMAT(ART_Viagem.DataInicio, '%d/%m/%Y') as DataViagem
from 
    #ordena a data pela mais recente
    (select * from ART_Viagem order by ART_Viagem.DataInicio desc) as ART_Viagem
join ART_Passagem as Passagem on Passagem.Viagem_Id = ART_Viagem.Id
join ART_VendaItem on ART_VendaItem.Passagem_Id = Passagem.Id
join ART_Venda Venda on Venda.Id = ART_VendaItem.Venda_Id
join ART_Pessoa as Pessoa on Venda.Pessoa_Id_Cliente = Pessoa.Id
join ART_PessoaEndereco as Endereco on Endereco.Pessoa_Id = Pessoa.Id
join ART_PessoaTelefone as Telefones on Telefones.Pessoa_Id = Pessoa.Id
join ART_Localidade as Cidade on Cidade.Id = Endereco.Localidade_Id
where
    IF($opcao,
      (DATEDIFF('$inicial', ART_Viagem.DataInicio) < $periodo), 
      (DATEDIFF('$inicial', ART_Viagem.DataInicio) >= $periodo))
group by Passagem.Pessoa_Id
order by 
    Cliente asc;";

$result = mysql_query($query);

$pdf = new TCPDF('L','mm','A4', true,'UTF-8', false); // configura o tamanho da página
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Veritas');
$pdf->SetTitle('Relatório em PDF');
$pdf->SetSubject('Gerador de Documento PDF');
$pdf->SetKeywords('PDF, Documento, TCPDF');
$pdf->setFontSubsetting(false) ;

$titulo = 'Compras por período';

$pdf->SetHeaderData('logo-guacutur.png', 80, $titulo, utf8_encode($rowViagem["Descricao"])."\n", array(0,0,0), array(255,255,255));
$pdf->SetMargins(15, 22, 10);
$pdf->SetHeaderMargin(5);

$pdf->setPrintFooter(false); // remove o rodapé

$pdf->SetFont('helvetica', '', 12); // define o tamanho e o tipo da fonte no texto. 
$pdf->AddPage(); // adiciona a primeira página.
//A partir daqui, basta incluirmos o texto desejado ao documento.

$pdf->SetFont('','',9);

$html = '
<table border="0" cellpadding="2" cellspacing="0">
    <tr style="font-weight: bold; font-size: 10pt">
        <th width="250" style="border-top: #000 3px double; border-bottom: #000 3px double;">Cliente</th>
        <th width="200" style="border-top: #000 3px double; border-bottom: #000 3px double;">Nome Fantasia</th>
        <th width="120" style="border-top: #000 3px double; border-bottom: #000 3px double;">Telefones</th>
        <th width="130" style="border-top: #000 3px double; border-bottom: #000 3px double;">Cidade</th>
        <th width="50" style="border-top: #000 3px double; border-bottom: #000 3px double;">Última viagem</th>
    </tr>
';
//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->writeHTML($html, true, false, true, false, '');
$html = '';
$i = 0;
while($row = mysql_fetch_assoc($result)) {
    $row = array_map('utf8_encode', $row);
    $html .= '
    <tr>
        <td width="250">'.$row["Cliente"].'</td>
        <td width="200">'.$row["NomeUsual"].'</td>
        <td width="120">'.$row["Telefones"].'</td>
        <td width="130">'.$row["Cidade"].'</td>
        <td width="50" style="text-align: center">'.$row["DataViagem"].'</td>
    </tr>';
    $i++;
    if($i >= 30) {
        $i = 0;
        //$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $html = '';
    }
}

mysql_close();

$html .= '
</table>';

//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->writeHTML($html, true, false, true, false, '');

$pdf->lastPage();

$pdf->Output('relatorio.pdf', 'I');