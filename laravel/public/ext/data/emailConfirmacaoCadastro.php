<?php
require_once("phpmailer/class.phpmailer.php");
require_once("lib/class.cript.php");
require_once("lib/defines.php");
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$id           = $_REQUEST['Id'];

$query = 
    "SELECT 
    ART_Pessoa.Pessoa_Id AS idEmpresa, 
    ART_PessoaEmpresa.Host, 
    ART_PessoaEmpresa.Porta,
    ART_PessoaEmpresa.Usuario,
    ART_PessoaEmpresa.Senha,
    ART_PessoaEmpresa.Email,
    ART_PessoaEmpresa.Descricao,
    ART_PessoaEmpresa.Logo,
    ART_PessoaEmpresa.Site,
    ART_Pessoa.Id,
    ART_Pessoa.Nome,
    ART_Pessoa.NomeUsual,
    ART_PessoaEmpresa.Subdominio
FROM 
    ART_Pessoa 
LEFT JOIN ART_PessoaEmpresa ON ART_PessoaEmpresa.Pessoa_Id = ART_Pessoa.Pessoa_Id
WHERE
    ART_Pessoa.Id = $id";
$result = mysql_query($query); 
$row = mysql_fetch_assoc($result);

$chave = CHAVE_CRIPTO;
$cript = new cript($chave,0);
$chave = $cript->criptData($id);

$url = $_SERVER["HTTP_HOST"];
$url = ($url === $row['Subdominio'].'artaban.com.br' ? 
        'https://'.$url.'/ext/' : 
        'http://'.$url.'/reserva/').'data/ativarCadastro.php?chave='.$chave;

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Envia e-mail para o cliente
$mail = new PHPMailer();
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->IsHTML(true);
$mail->CharSet = 'UTF-8';
$mail->Host = $row['Host']; // Endereço do servidor SMTP
$mail->Port = $row['Porta']; // Porta
$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
$mail->SMTPSecure = 'ssl'; // SSL REQUERIDO pelo GMail
$mail->Username = $row['Usuario']; // Usuário do servidor SMTP
$mail->Password = $row['Senha']; // Senha do servidor SMTP
$mail->From = $row['Email']; // Seu e-mail
$mail->FromName = $row['Descricao']; // Seu nome
$mail->WordWrap = 50; // Definição de quebra de linha
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
$mail->Subject  = "Confirmação de cadastro"; // Assunto da mensagem

$mail->Body = '<font face="Tahoma, Geneva, sans-serif">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
           <td width="50px">&nbsp;</td>
    </tr>
    <tr>
        <td align="center">
            <table width="800" cellspacing="0" cellpadding="0" style="border:solid 1px #999">
                <tr>
                    <td><p><img src="'.$row['Logo'].'" width="300" height="42" alt="'.utf8_encode($row['NomeUsual']).'" style="padding-left: 50px;"/></p>
                    <p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td style="padding: 0 50px 0 50px;">
                        <p>Prezado(a) '.$NomeCliente.'</p>
                        <p>Seja bem vindo a <font face="Tahoma, Geneva, sans-serif">'.utf8_encode($row['NomeUsual']).'</font>, estamos muito felizes com a sua inclus&atilde;o em nosso sistema.<br />
                            <br />
							Para a confirma&ccedil;&atilde;o de seu e-mail e libera&ccedil;&atilde;o do seu cadastro, clique no link abaixo:<br />
                      </p></td>
                </tr>
                <tr>
                    <td style="padding-left:280px"><table width="200" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="32" align="center" valign="middle" style="background-color:#003;border: 2px ridge #030; color:white; font-weight:bold"><font face="Tahoma, Geneva, sans-serif"><a href="'.$url.'" style="color:white;text-decoration:none">ATIVAR CADASTRO</a></font></td>
                        </tr>
                    </table></td>
                </tr>
                <tr>
                    <td style="padding: 0 50px 0 50px;"><p>Caso tenha receio em clicar no link, queira por gentileza informar o endere&ccedil;o abaixo diretamente no seu navegador de Internet:</p>
                        <a href="'.$url.'">'.$url.'</a>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 0px 50px 40px 50px;"><p>&nbsp;</p>
                        <p>Agradecemos a sua compreens&atilde;o e esperamos, sempre, contar com voc&ecirc; como nosso cliente.</p>
<p><br />
    <br />
    Atenciosamente,</p>
                      <p><br />
                          <br />
                          '.utf8_encode($row['NomeUsual']).'<br />
                          <a href="'.$row['Site'].'" onmouseover="this.style.textDecoration=\'underline\';this.style.color=\'black\'" onmouseout="this.style.textDecoration=\'none\';">'.$row['Site'].'</a></p></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</font>

<p><br />
</p>';

$mail->AddAddress($EmailCliente, $NomeCliente);
$enviado = $mail->Send();
$mail->ClearAllRecipients();
$mail->ClearAttachments();

echo '{ "success" : "'.($enviado ? 'true' : 'false').'", "msg" : '.$mail->ErrorInfo.'}';
