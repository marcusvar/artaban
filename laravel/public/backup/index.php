<?php

# Informa qual o conjunto de caracteres será usado.
mb_internal_encoding('UTF-8');
header('Content-type: text/html; charset=UTF-8');

// Carrega a classe PHPMailer
require 'phpmailer/class.phpmailer.php'; 

$url = 'http://guacutur.artaban.com.br/'.($_SERVER['SERVER_NAME'] === "localhost" ? "artaban/" : "")."backup/";

//print_r($_SERVER);

//CONFIGURAÇÕES  

// Define o nome do diretório de backup
define('BACKUP_DIR', './' ) ; 
// Define as credenciais do banco de dados
//define('HOST', '128.0.0.1' ) ; 
define('HOST', 'localhost' ) ; 
//define('USER', 'root' ) ; 
define('USER', 'artaban' ) ; 
//define('PASSWORD', '' ) ; 
define('PASSWORD', 'a7i2ok82i6xh8aov' ) ; 
define('DB_NAME', 'artaban' ) ; 
/*
Define o nome do arquivo sql
*/

$fileName = 'backup-artaban-' . date('Ymd') . '-' . date('His') ; 
// Define limite de tempo de execução
if(function_exists('max_execution_time')) {
    if( ini_get('max_execution_time') > 0 ) { 	
        set_time_limit(0) ;
    }
}

//FIM DAS CONFIGURAÇÕES  

// Verifique se o diretório já está criado e tem as devidas permissões
if (!file_exists(BACKUP_DIR)) mkdir(BACKUP_DIR , 0700) ;
if (!is_writable(BACKUP_DIR)) chmod(BACKUP_DIR , 0700) ; 

/*
// Cria um arquivo ".htaccess" para restringir o acesso direto ao diretório de backup. 
$content = 'deny from all' ; 
$file = new SplFileObject(BACKUP_DIR . '/.htaccess', "w") ;
$written = $file->fwrite($content) ;
// Verify that ".htaccess" is written , if not , die the script
if($written <13) die("Não foi possível criar o arquivo \".htaccess\", execução do backup cancelada!");
*/

$mysqli = new mysqli(HOST , USER , PASSWORD , DB_NAME) ;
if (mysqli_connect_errno())
{
    printf("Conexão falhou: %s", mysqli_connect_error());
    exit();
}

# Aqui está o segredo
$mysqli->query("SET NAMES 'utf8'");
$mysqli->query('SET character_set_connection=utf8');
$mysqli->query('SET character_set_client=utf8');
$mysqli->query('SET character_set_results=utf8');

 // Informações iniciais
$return = "--\n";
$return .= "-- Script gerado pelo Sistema Artaban \n";
$return .= '-- Exportado em: ' . date("d/m/Y") . ' às ' . date("h:i") . "\n\n\n";
$return .= "-- Cliente: Guaçu Tur\n";
$return .= "-- Banco de Dados : " . DB_NAME . "\n";
$return .= "\n";
$return .= "-- \n";
$return .= "-- Desabilita as chaves estrangeiras (foreign keys)\n";
$return .= "-- \n";
$return .= "/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;\n";
$return .= "\n";
$return .= "-- \n";
$return .= "-- Define o conjunto de caracteres que o cliente irá utilizar para enviar instruções SQL ao servidor\n";
$return .= "--\n";
$return .= "SET NAMES 'utf8';\n";
$return .= "\n";
$return .= "-- \n";
$return .= "-- Seleciona banco de dados\n";
$return .= "--\n";
$return .= "USE artaban;\n";
$return .= "\n";

$tables = array() ; 
// Explorando o conteudo  das tabelas deste banco de dados
$result = $mysqli->query('SHOW TABLES' ) ; 
// Percorre "$result" retornando o resultado em um array
while ($row = $result->fetch_row()) 
{
    $tables[] = $row[0] ;
}

$handle = fopen($fileName.'.sql', "w+");
//fwrite($f, pack("CCC",0xef,0xbb,0xbf));
//fwrite($handle, utf8_decode($return));

fwrite($handle, utf8_encode(utf8_decode($return)));

// Percorre cada tabela
foreach($tables as $table) { 
    // Recebe o conteúdo de cada tabela
    $result = $mysqli->query('SELECT * FROM '. $table);   
    // Retorna o número de campos (colunas) de cada tabela
    $num_fields = $mysqli->field_count  ;
    // Adiciona as informações da tabela
    $return = "--\n" ;
    $return .= '-- Estrutura da tabela `' . $table . '`' . "\n" ;
    $return .= "--\n" ;
    $return.= 'DROP TABLE IF EXISTS `'.$table.'`;' . "\n" ; 
    // Obtem o table-schema
    $schema = $mysqli->query('SHOW CREATE TABLE '.$table) ;
    // Extrai table-schema 
    $tableschema = $schema->fetch_row() ; 
    // Adiciona table-shema no código
    $return.= $tableschema[1].";\n\n";
    $return .= "--\n";
    $return .= "-- Dumping dos dados para a tabela `$table`\n" ; 
    $return .= "--\n";

    fwrite($handle, utf8_encode(utf8_decode($return)));
    
    // Percorre cada linha da tabela
    while($rowdata = $result->fetch_row()) { 
    
        // Prepara o código que irá inserir dados na tabela 
        $return = 'INSERT INTO `'.$table .'`  VALUES ( '  ;
        // Extrai os dados de cada linha 
        for($i=0; $i<$num_fields; $i++) {
            $return .= '"'.$rowdata[$i] . "\"," ;
        }
        // remove a última vírgula 
        $return = substr("$return", 0, -1) ; 
        $return .= ");" ."\n" ;
        
        fwrite($handle, utf8_encode(utf8_decode($return)));

     } 
     fwrite($handle, "\n") ; 
}

// Encerra a conexão
$mysqli->close() ;

$return = "\n";
$return .= "--\n"; 
$return .= "-- Habilita as chaves estrangeiras (foreign keys)\n";
$return .= "-- \n";
$return .= "/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;\n";

fwrite($handle, $return);

fclose($handle);

$zip = new ZipArchive() ;
$resOpen = $zip->open(BACKUP_DIR . "/" .$fileName.".zip" , ZIPARCHIVE::CREATE) ;
if( $resOpen ){
    $zip->addFile( BACKUP_DIR . "/" .$fileName.'.sql', $fileName.'.sql' ) ;
}
$zip->close() ;
$fileSize = get_file_size_unit(filesize(BACKUP_DIR . "/". $fileName . '.zip')) ; 
$message = <<<msg
  <h2>BACKUP realizado com sucesso</h2>
 
  <p>o arquivo tem o nome de: <b> $fileName.zip </ b> e o tamanho do arquivo é de: $fileSize.\n\n</p>

  <p>Este arquivo ZIP não poderá ser acessado através de um navegador web por estar armazenado em um diretório protegido\n\n</p>

  <p>Recomenda-se transferir este backup via FTP para um outro sistema de arquivos.\n\n</p>
msg;
echo $message ; 

// Função para adicionar a pororiedade Unit depois tamanho do arquivo. 
function get_file_size_unit($file_size){
    switch (true) {
        case ($file_size/1024 < 1) :
            return intval($file_size ) ." Bytes" ;
            break;
        case ($file_size/1024 >= 1 && $file_size/(1024*1024) < 1)  :
            return intval($file_size/1024) ." KB" ;
            break;
        default:
        return intval($file_size/(1024*1024)) ." MB" ;
    }
}

$email_remetente = "admin@veritasweb.com.br";
$email_conteudo = 
    "Backup realizado com sucesso em ".date("d/m/Y").' às '.date("H\hi\m")."<br /><br />".
    "Cliente: Guaçu Tur<br /><br />".
    "Backup disponível para download em:<br /><br />".
    '<a href="'.$url.$fileName.'.zip">'.$url.$fileName.'.zip</a><br /><br />'.
    'Recomendamos baixar este arquivo e guarda-lo em local seguro (Ex: HD Extrerno)';

$mail = new PHPMailer(true); // the true param means it will throw exceptions on errors, which we need to catch

$mail->IsSMTP(); // telling the class to use SMTP

$mail->Host       = $email_remetente; // SMTP server
//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
$mail->Username   = $email_remetente;  // GMAIL username
$mail->Password   = "admin@3214";            // GMAIL password

// Emails do cliente
//$mail->AddAddress('guacutur@guacutur.com.br', utf8_decode('Guaçu Tur'));
//$mail->AddAddress('fabio@guacutur.com.br', utf8_decode('Fábio Tessari'));
//$mail->AddAddress('compras@guacutur.com.br', utf8_decode('Compras Guaçu Tur'));
$mail->AddAddress('marcusvarosa@gmail.com', utf8_decode('Marcus - Veritas'));

// Com cópia oculta 
//$mail->AddBCC("marcusvarosa@gmail.com", 'Marcus Vinicius');
//$mail->AddBCC('laercio.fazioni@gmail.com', 'Luiz Laercio');
$mail->SetFrom($email_remetente, 'Veritas - Web Design Inteligente');
$mail->Subject = 'Backup dos dados do sistema Artaban';
$mail->MsgHTML($email_conteudo);            

if(!$mail->Send()) {
  $msg = $mail->ErrorInfo;
} else {
  $msg = 
    "E-Mail enviado com sucesso! em ".date("d/m/Y")."<br />\n".
    "Arquivo ".$url.$fileName.".zip<br />\n<hr>\n";
;
}

$file = fopen(BACKUP_DIR . '/log.html', "a+");
fwrite($file, $msg) ;
fclose($file); 
