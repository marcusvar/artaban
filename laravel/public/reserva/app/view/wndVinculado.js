/*
 * File: app/view/wndVinculado.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Artaban.view.wndVinculado', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.form.Panel',
        'Ext.form.field.ComboBox',
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Fill',
        'Ext.button.Button'
    ],

    autoShow: true,
    id: 'wndVinculado',
    width: 520,
    resizable: false,
    closable: false,
    title: 'Escolha a pessoa a vincular',
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'form',
                    id: 'frmBuscaVinculado',
                    bodyPadding: 10,
                    items: [
                        {
                            xtype: 'combobox',
                            anchor: '100%',
                            id: 'cbxVinculados',
                            fieldLabel: 'Escolha a pessoa',
                            labelWidth: 110,
                            msgTarget: 'side',
                            name: 'vinculadoId',
                            allowBlank: false,
                            emptyText: 'selecione uma pessoa...',
                            displayField: 'Nome',
                            forceSelection: true,
                            minChars: 1,
                            queryMode: 'local',
                            store: 'strListarPassageiros',
                            typeAhead: true,
                            valueField: 'Id'
                        }
                    ],
                    dockedItems: [
                        {
                            xtype: 'toolbar',
                            dock: 'bottom',
                            items: [
                                {
                                    xtype: 'tbfill'
                                },
                                {
                                    xtype: 'button',
                                    action: 'cancel',
                                    text: 'Cancelar',
                                    listeners: {
                                        click: {
                                            fn: me.onButtonClick,
                                            scope: me
                                        }
                                    }
                                },
                                {
                                    xtype: 'button',
                                    action: 'select',
                                    itemId: 'selecVinculado',
                                    text: 'Selecionar'
                                }
                            ]
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onButtonClick: function(button, e, eOpts) {
        button.up('window').destroy();
    }

});