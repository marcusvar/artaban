/*
 * File: app/view/wndCaixaResumo.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Artaban.view.wndCaixaResumo', {
    extend: 'Ext.window.Window',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.toolbar.Fill',
        'Ext.button.Button'
    ],

    autoShow: true,
    height: 600,
    width: 800,
    resizable: false,
    layout: 'border',
    closable: false,
    title: 'Resumo de Caixa',
    modal: true,
    y: 15,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    region: 'center',
                    listeners: {
                        afterrender: {
                            fn: me.onContainerAfterRender,
                            scope: me
                        }
                    }
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            handler: function(button, e) {
                                button.up('window').destroy();
                            },
                            text: 'Fechar'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    onContainerAfterRender: function(component, eOpts) {
        component.update('<iframe src="data/resumoCaixa.php?viagemId=' + global.horario.viagemId + '" style="border:0" width="100%" height="100%""></iframe>');

    }

});