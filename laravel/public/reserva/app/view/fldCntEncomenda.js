/*
 * File: app/view/fldCntEncomenda.js
 *
 * This file was generated by Sencha Architect version 3.0.4.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('Artaban.view.fldCntEncomenda', {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.fldcntencomenda',

    requires: [
        'Ext.form.field.ComboBox',
        'Ext.view.BoundList',
        'Ext.XTemplate',
        'Ext.form.field.Number',
        'Ext.Img'
    ],

    clienteId: 0,
    clienteNome: '',
    layout: 'column',
    fieldLabel: 'Label',
    hideLabel: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'combobox',
                    columnWidth: 0.39,
                    itemId: 'comboCliente',
                    fieldLabel: 'Nome',
                    labelWidth: 35,
                    msgTarget: 'side',
                    name: 'cliente',
                    allowBlank: false,
                    emptyText: 'selecione um cliente...',
                    displayField: 'Nome',
                    forceSelection: true,
                    minChars: 3,
                    pageSize: 25,
                    typeAhead: true,
                    valueField: 'Id',
                    listConfig: {
                        xtype: 'boundlist',
                        itemSelector: 'div',
                        itemTpl: Ext.create('Ext.XTemplate', 
                            '<tpl if="this.verificaEmpresa(Responsavel)">',
                            '    Empresa: {Nome}<br />',
                            '    Cliente: {Responsavel}',
                            '<tpl else>    ',
                            '    Cliente: {Nome}',
                            '</tpl>    ',
                            '',
                            {
                                verificaEmpresa: function(nome) {
                                    return nome === '' ? false : true;
                                }
                            }
                        )
                    }
                },
                {
                    xtype: 'combobox',
                    columnWidth: 0.26,
                    itemId: 'tipo',
                    margin: '0 0 0 10',
                    fieldLabel: 'Descrição',
                    labelWidth: 65,
                    name: 'tipoAdicional',
                    emptyText: 'Escolha um adicional...',
                    displayField: 'Descricao',
                    queryMode: 'local',
                    store: [
                        [
                            3,
                            'VOLUMES'
                        ],
                        [
                            4,
                            'ESPECIAL'
                        ]
                    ],
                    valueField: 'Id'
                },
                {
                    xtype: 'numberfield',
                    columnWidth: 0.18,
                    margin: '0 0 0 10',
                    fieldLabel: 'Quantidade',
                    labelWidth: 70,
                    name: 'Qtd'
                },
                me.processValor({
                    xtype: 'textfield',
                    mask: 'R$ #9.999.990,00',
                    money: true,
                    returnWithMask: true,
                    columnWidth: 0.17,
                    itemId: 'valor',
                    margin: '0 0 0 10',
                    fieldLabel: 'Valor',
                    labelWidth: 33,
                    name: 'Valor',
                    listeners: {
                        blur: {
                            fn: me.onTxtFldAdicionalBlur,
                            scope: me
                        }
                    }
                }),
                {
                    xtype: 'image',
                    height: 16,
                    margin: '3 0 0 3',
                    style: 'cursor: pointer;',
                    width: 16,
                    src: 'resources/images/trash.png',
                    listeners: {
                        afterrender: {
                            fn: me.onImageAfterRender,
                            scope: me
                        }
                    }
                }
            ],
            listeners: {
                afterrender: {
                    fn: me.onFieldcontainerAfterRender,
                    scope: me
                }
            }
        });

        me.callParent(arguments);
    },

    processValor: function(config) {
        config.plugins = [new Ext.ux.TextMaskPlugin()];
        return config;
    },

    onTxtFldAdicionalBlur: function(component, e, eOpts) {
        var form = component.up('form').getForm(),
            fieldset = component.up('fieldset'),
            values = form.getValues(),
            valor = 0;

        for(var key in values) {
            if(key.substr(0,9)==='encomenda' && key.indexOf('Valor') !== -1) {
                valor += global.converteMoedaFloat(values[key]);
            }
        }

        form.findField('Total').setValue( global.formatReal(
            global.converteMoedaFloat(form.findField('Dinheiro').getValue()) -
            global.converteMoedaFloat(form.findField('Cheques').getValue()) -
            global.converteMoedaFloat(form.findField('Vales').getValue()) +
            valor
        ));
    },

    onImageAfterRender: function(component, eOpts) {
        component.el.on('click', function () {
            var me = this,
                fieldset = me.up('fieldset'),
                form = component.up('form').getForm(),
                values = form.getValues(),
                valor = 0;

            // Exclui os campos encomendas e atualiza o store
            fieldset.remove(me.up('fldcntencomenda').el.id);

            for(var key in values) {
                if(key.substr(0,9)==='encomenda' && key.indexOf('Valor') !== -1) {
                    valor += global.converteMoedaFloat(values[key]);
                }
            }

            form.findField('Total').setValue( global.formatReal(
                global.converteMoedaFloat(form.findField('Dinheiro').getValue()) -
                global.converteMoedaFloat(form.findField('Cheques').getValue()) -
                global.converteMoedaFloat(form.findField('Vales').getValue()) +
                valor
            ));


        }, component);
    },

    onFieldcontainerAfterRender: function(component, eOpts) {
        var fieldset = component.up('fieldset'),
            valor = component.child('textfield[name=Valor]'),
            tipo = component.child('combo[name=tipoAdicional]'),
            cliente = component.child('combo[name=cliente]'),
            qtd = component.child('numberfield');

        cliente.name   = 'encomenda['+fieldset.numFields+'][Cliente]';
        tipo.name      = 'encomenda['+fieldset.numFields+'][Tipo]';
        qtd.name       = 'encomenda['+fieldset.numFields+'][Qtd]';
        valor.name     = 'encomenda['+fieldset.numFields+'][Valor]';

        fieldset.numFields = fieldset.numFields + 1;
    }

});