# ext-theme-neptune-b79b057d-1c4c-4bb4-95d7-03a774394f94/sass/etc

This folder contains miscellaneous SASS files. Unlike `"ext-theme-neptune-b79b057d-1c4c-4bb4-95d7-03a774394f94/sass/etc"`, these files
need to be used explicitly.
