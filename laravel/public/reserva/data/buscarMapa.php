<?php
require_once('db.php');
function mysqli_call(mysqli $dbLink, $procName, $params="")
{
    if(!$dbLink) {
        throw new Exception("A conexão MySQLi é inválida.");
    }
    else
    {
        // Execute the SQL command.
        // The multy_query method is used here to get the buffered results,
        // so they can be freeded later to avoid the out of sync error.
        $sql = "CALL {$procName}({$params});";
        $sqlSuccess = $dbLink->multi_query($sql);
        if($sqlSuccess)
        {
            if($dbLink->more_results())
            {
                // Get the first buffered result set, the one with our data.
                $result = $dbLink->use_result();
                $output = array();
                // Put the rows into the outpu array
                while($row = $result->fetch_assoc())
                {
                    $output[] = $row;
                }
                // Free the first result set.
                // If you forget this one, you will get the "out of sync" error.
                $result->free();
                // Go through each remaining buffered result and free them as well.
                // This removes all extra result sets returned, clearing the way
                // for the next SQL command.
                while($dbLink->more_results() && $dbLink->next_result())
                {
                    $extraResult = $dbLink->use_result();
                    if($extraResult instanceof mysqli_result){
                        $extraResult->free();
                    }
                }
                return $output;
            }
            else
            {
                return false;
            }
        }
        else
        {
            throw new Exception("A chamada falhou: " . $dbLink->error);
        }
    }
}

// Define data atual caso não seja fornecido uma data
$viagemId = isset($_POST['viagemId']) ? $_POST['viagemId'] : 24;
$web      = isset($_POST['web']) ? $_POST['web'] : 'TRUE';

$mapa = mysqli_call($mysqli, "Proc_ConsMapaLugarViagem", "$viagemId, $web");

for ($i = 0; $i < count($mapa); $i++) {
    $piso   = "Piso".$mapa[$i]["Andar"];
    $linha  = intval($mapa[$i]["Linha"])-1;
    $coluna = "Numero".$mapa[$i]["Coluna"];
    $passagem = "Passagem_Id".$mapa[$i]["Coluna"];
    $promocao = "Promocao".$mapa[$i]["Coluna"];;
    $situacao = "Situacao".$mapa[$i]["Coluna"];;
    
    $result[$piso][$linha][$coluna]   = $mapa[$i]["Numero"];
    $result[$piso][$linha][$passagem] = $mapa[$i]["Passagem_Id"];
    $result[$piso][$linha][$promocao] = $mapa[$i]["Promocao"];
    $result[$piso][$linha][$situacao] = $mapa[$i]["Situacao"];
}

echo json_encode(array(
    "result"  => $result,
    "success" => $mysqli->errno,
    "msg"     => $mysqli->error
));

$mysqli->close();