<?php
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

//ChromePhp::log($_POST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id = isset($_REQUEST["Id"]) ? $_REQUEST["Id"] : 6679 ;

// Busca se este cliente tem empresas cadastradas como responsável
$query = "SELECT Nome FROM ART_Pessoa WHERE Pessoa_Id_Responsavel = $id";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
if($row) {
    $ret[] = $row;
    while ($row = mysql_fetch_assoc($result)) {
        $ret[] = $row;
    }
    echo json_encode(array(
        "mensagem" => "Este cliente possui empresas cadastradas.<br />Primeiro exclua as empresas deste cliente para depois realizar sua exclusão.",
        "result"   => $ret,
        "status" => true 
    ));    

    mysql_close();
    
    exit();
}

// Busca por compras e/ou fidelizacao para este cliente
$query = "
SELECT
   DATE_FORMAT(ART_Viagem.DataInicio, '%d/%m/%Y') AS Data,
   DATE_FORMAT(ART_Viagem.HoraInicio, '%h:%i') AS Hora,
   ART_Viagem.Descricao,
   ART_Passagem.Poltrona
FROM
   ART_Venda
JOIN ART_VendaItem ON ART_VendaItem.Venda_Id = ART_Venda.Id
JOIN ART_Passagem ON ART_Passagem.Id = ART_VendaItem.Passagem_Id AND ART_Passagem.Situacao <> 'Cancelada' 
JOIN ART_Viagem ON ART_Viagem.Id = ART_Passagem.Viagem_Id
WHERE
   ART_Venda.Pessoa_Id_Cliente = $id
ORDER BY ART_Viagem.DataInicio, ART_Viagem.HoraInicio";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
if($row) {
    $ret[] = array_map('utf8_encode', $row);
    while ($row = mysql_fetch_assoc($result)) {
        $ret[] = array_map('utf8_encode', $row);
    }

    $query = "
    SELECT * FROM ART_Fidelizacao WHERE ART_Fidelizacao.Pessoa_Id = $id";
    $result = mysql_query($query);
    $row = mysql_fetch_assoc($result);
    $fidelizacao = $row ? " e saldo Mega Polo" : "";
    echo json_encode(array(
        "mensagem" => "Este cliente possui passagens compradas$fidelizacao em seu nome.<br />Deseja realmente excluir este cliente.",
        "result" => $ret
    ));    

    mysql_close();
    
    exit();
}

echo json_encode(array(
    "mensagem" => "Deseja realmente excluir este cliente?"
));    
