<?php

//error_reporting(E_ALL & ~(E_STRICT|E_NOTICE));
//ini_set('display_errors', 1);

require_once('config.db.php'); 
include("mpdf60/mpdf.php");

//include '../chromephp/ChromePhp.php';

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$cidade = isset($_GET["cidade"]) ? $_GET["cidade"] : "Cascavel";
$id     = isset($_GET["id"]) ? $_GET["id"] : 2853; 

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : 2;

$query = "
SELECT
    CONCAT(Pessoa.Nome, IF(NOT ISNULL(
		(SELECT Pessoa_Id_Responsavel FROM ART_Pessoa 
		WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel)
	), CONCAT('<br />',

    (SELECT
    GROUP_CONCAT(ART_Pessoa.Nome SEPARATOR '<br /> ')
     FROM ART_Pessoa
     WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
     GROUP BY ART_Pessoa.Pessoa_Id_Responsavel
    )),'')) AS Nome,

    CONCAT('', IF(NOT ISNULL(
		(SELECT Pessoa_Id_Responsavel FROM ART_Pessoa 
		WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel)
	), CONCAT('<br />',

    (SELECT
    GROUP_CONCAT(ART_Pessoa.NomeUsual SEPARATOR '<br /> ')
     FROM ART_Pessoa
     WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
     GROUP BY ART_Pessoa.Pessoa_Id_Responsavel
    )),'')) AS Fantasia,
    
    (SELECT
     GROUP_CONCAT(ART_PessoaTelefone.Descricao,': ',ART_PessoaTelefone.telefone SEPARATOR '<br /> ')
     FROM ART_PessoaTelefone
     WHERE ART_PessoaTelefone.Pessoa_Id = Pessoa.Id
     GROUP BY ART_PessoaTelefone.Pessoa_Id
    ) AS Telefones,
    CONCAT(ART_PessoaEndereco.Logradouro, ', N&deg; ', ART_PessoaEndereco.Numero, IF(ART_PessoaEndereco.Complemento = '', '', ' - ' + ART_PessoaEndereco.Complemento)) as Endereco
FROM 
    ART_Pessoa AS Pessoa
LEFT JOIN ART_PessoaEndereco ON ART_PessoaEndereco.Pessoa_Id = Pessoa.Id
WHERE 
	ART_PessoaEndereco.Localidade_Id = $id AND
	Pessoa.Tipo LIKE 'PF%' AND 
        Pessoa.Pessoa_Id = $idClienteVeritas
ORDER BY Pessoa.Nome ASC;";

$result = mysql_query($query);
$html = '
<table>
    <caption>Cidade: '.$cidade.'</caption>
    <tr>
        <th class="header">Cliente</th>
        <th class="header">Nome Fantasia</th>
        <th class="header">Telefones</th>
        <th class="header">Endereço</th>
    </tr>    
';

while($row = mysql_fetch_assoc($result)) {
    $row = array_map('utf8_encode', $row);
    $html .= '
    <tr>
        <td class="separador">'.$row["Nome"].'</td>
        <td class="separador">'.$row["Fantasia"].'</td>
        <td class="separador">'.$row["Telefones"].'</td>
        <td class="separador">'.$row["Endereco"].'</td>
    </tr>';
}

mysql_close();

$html .= '
</table>';

//ChromePhp::log($query);


$mpdf=new mPDF('c','A4-L','8','helvetica',15,15,26,16,9,9); 
$mpdf->debug = true;

$mpdf->SetDisplayMode('fullpage');

$stylesheet = file_get_contents('style.css');
$mpdf->WriteHTML($stylesheet,1);

$mpdf->SetHTMLHeader('
    <table border="0">
        <tr>
            <td width="330">
                <img src="images/'.$idClienteVeritas.'/logo-report.jpg" />
            </td>
            <td style="padding-left: 15px;">
                <div style="font-weight: bold; font-size: 20px; padding-bottom: 6px;">Clientes por Cidade</div>
                <!--div style="font-size: 18px;">Aqui vai o sub-título</div-->
            </td>
        </tr>
    </table>
');   

//ChromePhp::info("Passou aqui!");

$mpdf->WriteHTML($html,2);

//ChromePhp::log($html);

$mpdf->Output('relatorio.pdf','I');