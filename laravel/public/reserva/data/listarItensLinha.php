<?php
function capitalize($str, $e = array())
{
    return join(' ',
	array_map(
            create_function(
		'$str',
		'return (!in_array($str, '
		. var_export($e, true)
		. ')) ? ucfirst($str) : $str;'
            ),
            explode(' ', strtolower($str))
        )
    );
}

require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';
//ChromePhp::log($_POST);

$linhaId = isset($_POST["linhaId"]) ? $_POST["linhaId"] : 4;
$web = $_REQUEST['web'] == 1 ? true : false;

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = 
"SELECT
  ART_LinhaItem.ID AS id, 
  CONCAT(UPPER(ART_Localidade.Nome), ' - ') AS Estado,
  LOWER(ART_PontoReferencia.Descricao) AS Localidade
FROM
  ART_LinhaItem
JOIN ART_PontoReferencia ON ART_LinhaItem.PontoReferencia_Id = ART_PontoReferencia.Id
JOIN ART_Localidade ON ART_PontoReferencia.Localidade_Id = ART_Localidade.Id
JOIN ART_UF ON ART_Localidade.UF_Id = ART_UF.Id
WHERE
  ART_LinhaItem.Linha_Id = $linhaId ".($web ? " AND ART_LinhaItem.Web=1 " : "")."
ORDER BY 
  ART_LinhaItem.Sequencia";

$ret = mysql_query($query);

while ($row = mysql_fetch_assoc($ret)) {
    $rows[] = array(
        "id"        => $row['id'],
        "descricao" => utf8_encode($row['Estado'].capitalize($row['Localidade'], array('e','do','da','dos','das','de')))
    );
}

//$rows = array_map('utf8_encode', mysql_fetch_assoc($ret));

//$rows = array_map('utf8_encode', $rows);

echo json_encode(array(
    "result" => $rows 
));

mysql_close();