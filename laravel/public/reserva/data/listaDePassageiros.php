<?php
//header('Content-type: application/pdf');
function arrayUtf8Enconde(array $array) {
    //instancia um novo array
    $novo = array();
    //entar em um loop para verificar e converter cada indice do array
    foreach ($array as $i => $value) {
        //verifica se o indice é um array
        if (is_array($value)) {
            //aqui chama novamente o próprio método para verificar novamente(recursividade)
            $value = arrayUtf8Enconde($value);
        } elseif (!mb_check_encoding($value, 'UTF-8')) {//se não for array, verifica se o valor está codificado como UTF-8
            //aqui ele codifica
            $value = utf8_encode($value);
        }
        //recoloca o valor no array
        $novo[$i] = $value;
    }
    //retorna o array
    return $novo;
}

//require_once('config.db.php'); 
include("mpdf60/mpdf.php");

//include '../chromephp/ChromePhp.php';
$_GET["c"] = isset($_GET["c"]) ? $_GET["c"] : 10;
//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : $_GET["c"];
//ChromePhp::log($idClienteVeritas);
$id = isset($_GET["Id"]) ? $_GET["Id"] : 524;

$_GET["tipo"] = isset($_GET["tipo"]) ? $_GET["tipo"] : "lista";

$recolher = $_GET['tipo'] === 'recolher';

$query = 
"SELECT 
ART_ViagemItem.HorarioItem_Id,
  CONCAT(IFNULL(LinhaItem.Sequencia, ART_LinhaItem.Sequencia), 
         IFNULL(PontoReferencia.Id, ART_PontoReferencia.Id)) AS Sequencia, 
  CONCAT(ART_LinhaItem.Sequencia, ART_PontoReferencia.Id) AS SeqRecolher,
DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%H:%i') AS DataHoraEmbarque,

(SELECT HoraEmbarque 
   FROM ART_HorarioItem hi
     WHERE 
    hi.LinhaItem_id = ART_ViagemItem.HorarioItem_Id_Transbordo AND
    hi.Horario_Id = ART_HorarioItem.Horario_Id) AS HoraEmbarque,

  UPPER(CONCAT(DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%H:%i'), ' - ',
        ART_Localidade.Nome, ' - ', ART_PontoReferencia.Descricao)) AS Embarque,  
  
  UPPER(CONCAT(
  SUBSTRING(
    (IF(ART_ViagemItem.Recolher, 
      COALESCE( 
      (SELECT HoraEmbarque FROM ART_HorarioItem hi 
        WHERE hi.LinhaItem_id = ART_ViagemItem.HorarioItem_Id_Transbordo AND
          hi.Horario_Id = ART_HorarioItem.Horario_Id), DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%H:%i:%s')),
    DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%H:%i:%s'))  
    ),1,5), ' - ',
      IFNULL(Localidade.Nome,ART_Localidade.Nome), ' - ', 
      IFNULL(PontoReferencia.Descricao, ART_PontoReferencia.Descricao))) AS Baldeacao,
  
  ART_ViagemItem.HorarioItem_Id_Transbordo,            
  ART_Passagem.Poltrona, 
  ART_ViagemItem.Recolher,
  ART_Pessoa.Nome AS Passageiro, 
  ART_ViagemItem.DataHoraEmbarque,
  ART_ViagemItem.Recolher,
  (SELECT 
      ap.Nome
   FROM 
      ART_VendaItem avi
   JOIN ART_Venda av ON avi.Venda_Id = av.Id
   JOIN ART_Pessoa ap ON av.Pessoa_Id_Cliente = ap.Id
   WHERE
     avi.Passagem_Id = ART_Passagem.Id
   LIMIT 1  
   ) AS Empresa,
  Empresa.Nome AS EmpresaReserva,
    (SELECT
      GROUP_CONCAT(ART_PessoaTelefone.telefone SEPARATOR '<br />')
   FROM ART_PessoaTelefone
   WHERE ART_PessoaTelefone.Pessoa_Id = ART_Passagem.Pessoa_Id
   GROUP BY ART_PessoaTelefone.Pessoa_Id
  ) AS Telefone,
  ART_Pessoa.RGIE AS RG,
  Empresa.Documento AS CNPJ,
  IF(ART_Passagem.Parcial='C','',IF(ART_Passagem.Parcial='V', 'Volta','Ida')) AS Forma,
  ART_Passagem.Valor,
  ART_Passagem.DescontoFidelizacao,
  ART_Passagem.Desconto AS DescontoPassagem,
  ART_Passagem.Tipo,
  (SELECT COALESCE(SUM(apa.Valor),0) 
   FROM ART_PassagemAdicional apa 
   WHERE apa.Passagem_Id=ART_Passagem.Id) AS Adicional
FROM
  ART_Passagem
JOIN ART_Pessoa ON ART_Passagem.Pessoa_Id = ART_Pessoa.Id
JOIN ART_ViagemItem ON ART_Passagem.ViagemItem_Id_Origem = ART_ViagemItem.Id
JOIN ART_HorarioItem ON ART_ViagemItem.HorarioItem_Id = ART_HorarioItem.Id
JOIN ART_LinhaItem ON ART_HorarioItem.LinhaItem_Id = ART_LinhaItem.ID
JOIN ART_PontoReferencia ON ART_LinhaItem.PontoReferencia_Id = ART_PontoReferencia.Id
JOIN ART_Localidade ON ART_PontoReferencia.Localidade_Id = ART_Localidade.Id
LEFT JOIN ART_LinhaItem AS LinhaItem ON ART_ViagemItem.HorarioItem_Id_Transbordo = LinhaItem.ID
LEFT JOIN ART_PontoReferencia AS PontoReferencia ON LinhaItem.PontoReferencia_Id = PontoReferencia.Id
LEFT JOIN ART_Localidade AS Localidade ON PontoReferencia.Localidade_Id = Localidade.Id
LEFT JOIN ART_Pessoa AS Empresa ON ART_Passagem.Empresa_Id = Empresa.Id
WHERE
  ART_Passagem.Viagem_Id = $id AND
  ART_Passagem.Situacao <> 'Cancelada' AND
  ART_Passagem.Poltrona <> ''
ORDER BY ".($recolher ? "ART_ViagemItem.DataHoraEmbarque, SeqRecolher" : "Baldeacao").", ART_Passagem.Poltrona;";

//ChromePhp::log($query);

$result = mysql_query($query); 

$sequencia = 0;
$i = 0;

$retorno = array();

while($row = mysql_fetch_assoc($result)) {
    $seq = $recolher ? $row["SeqRecolher"] : $row["Sequencia"];
    if($sequencia !== $seq ) {
        $i++;
        $retorno[$i]["Embarque"] = ((is_null($row["HorarioItem_Id_Transbordo"]) AND !$recolher) OR ($row["Recolher"] === "1" AND $recolher)) ? $row["Embarque"] : $row["Baldeacao"];
        $retorno[$i]["Recolher"] = $row["Recolher"] === "1";
        $sequencia = $seq;
    }    
    $retorno[$i]["Passageiros"][] = $row;
}

$retorno = arrayUtf8Enconde($retorno);

// Gera o título do relatório
$query = 
"SELECT 
  CONCAT(ART_PontoReferencia.Descricao, ' - ', 
         DATE_FORMAT(ART_Viagem.DataInicio, '%d/%m/%Y'), ' - ', 
         ART_Viagem.Duracao, IF(ART_Viagem.Duracao=1,' dia','dia(s)'),' - ', 
         ART_Viagem.Descricao, ' - ',COALESCE(ART_Veiculo.Numero,'')) Descricao  
FROM  
  ART_Viagem
JOIN ART_Horario ON ART_Viagem.Horario_Id = ART_Horario.Id
JOIN ART_Linha ON ART_Horario.Linha_Id = ART_Linha.ID
JOIN ART_PontoReferencia ON ART_Linha.PontoReferencia_Id_Destino = ART_PontoReferencia.Id
LEFT JOIN ART_Veiculo ON ART_Viagem.Veiculo_Id = ART_Veiculo.Id
WHERE
  ART_Viagem.Id = $id";

$result    = mysql_query($query);
$rowViagem = mysql_fetch_assoc($result);

$query = "SELECT SUM(ART_ViagemCaixaEncomendas.Valor) as Valor
FROM ART_ViagemCaixaEncomendas
JOIN ART_ViagemCaixa ON ART_ViagemCaixa.Id = ART_ViagemCaixaEncomendas.ViagemCaixa_id
WHERE ART_ViagemCaixa.Viagem_Id = $id";

$result    = mysql_query($query);
$encomenda = mysql_fetch_assoc($result);

mysql_close();

$titulo = 'Lista de Passageiros';

if($_GET['tipo'] === 'cargas') {
    $titulo = 'Controle de Carga dos Passageiros';
} elseif($_GET['tipo'] === 'mega') {
    $titulo = 'Lista de Passageiros - Mega Polo';
} elseif($_GET['tipo'] === 'recebimento') {
    $titulo = 'Lista de Recebimento';
} elseif($recolher) {
    $titulo = 'RECOLHER UMUARAMA';
}

//<th width="3%"  style="border-top: #000 3px double; border-bottom: #000 3px double;">T</th>

$html = '
<html>
<head>
</head>
<body>';
if($_GET['tipo']==='cargas') {
    $html = 
    '<table>
            <tr>
                <th class="header">T</th>
                <th class="header">Pol</th>
                <th class="header">Nome do passageiro</th>
                <th class="header">Razão Social</th>
            </tr>';
    $passageiros = 0;
    foreach ($retorno as $key => $ret) {
            $html .=
            '       <tr>
                        <td colspan="4"><div class="cidade">'.$ret["Embarque"].'</div></td>
                    </tr>';
        foreach ($ret["Passageiros"] as $chave => $campo) {        
            $html .= 
            '       <tr>
                        <td>'.$campo["Tipo"].'</td>
                        <td>'.$campo["Poltrona"].'</td>
                        <td>'.$campo["Passageiro"].'</td>
                        <td>'.($campo["Tipo"]==="R" ? $campo["EmpresaReserva"] : $campo["Empresa"]).'</td>
                    </tr>';    
            $passageiros++;
        }
        if(count($ret)>0) {
            $html .= "<tr><td colspan='4'>&nbsp;</td></tr>";
        }    
    }        
    $html .= 
    '       <tr>
                <td class="foter" colspan="4">Total de passageiros: '.$passageiros.'</td>
            </tr>';    
}
else if($_GET['tipo']==='mega') {
    $html = 
    '<table>
            <tr>
                <th class="header">T</th>
                <th class="header">Pol</th>
                <th class="header">Nome do passageiro</th>
                <th class="header">Razão Social</th>
                <th class="header">CNPJ</th>
            </tr>';
    $passageiros = 0;
    foreach ($retorno as $key => $ret) {
            $html .=
            '       <tr>
                        <td colspan="5"><div class="cidade">'.$ret["Embarque"].'</div></td>
                    </tr>';
        foreach ($ret["Passageiros"] as $chave => $campo) {        
            $html .= 
            '       <tr>
                        <td>'.$campo["Tipo"].'</td>
                        <td>'.$campo["Poltrona"].'</td>
                        <td>'.$campo["Passageiro"].'</td>
                        <td>'.($campo["Tipo"]==="R" ? $campo["EmpresaReserva"] : $campo["Empresa"]).'</td>
                        <td>'.($campo["CNPJ"]).'</td>    
                    </tr>';    
            $passageiros++;
        }
        if(count($ret)>0) {
            $html .= "<tr><td colspan='4'>&nbsp;</td></tr>";
        }    
    }        
    $html .= 
    '       <tr>
                <td class="foter" colspan="4">Total de passageiros: '.$passageiros.'</td>
            </tr>';
}
else if($_GET['tipo']==='recebimento')
{
    $html = 
    '<table>
            <tr>
                <th class="header">T</th>
                <th class="header">Pol</th>
                <th class="header">Nome do passageiro</th>
                <th class="header">Telefones</th>
                <th class="header">Op.Vg</th>
                <th class="header" style="text-align: right">Valor</th>
                <th class="header" style="text-align: right">Desconto</th>
                <th class="header" style="text-align: right">Final</th>
            </tr>';
    $valor = $desconto = $final = $passageiros = 0;
    foreach ($retorno as $key => $ret) {
            $html .=
            '       <tr>
                        <td colspan="8"><div class="cidade">'.$ret["Embarque"].'</div></td>
                    </tr>';
        foreach ($ret["Passageiros"] as $chave => $campo) {
            $total = $campo["Valor"]-$campo["DescontoPassagem"]+$campo["Adicional"];
            $html .= 
            '       <tr>
                        <td>'.$campo["Tipo"].'</td>
                        <td>'.$campo["Poltrona"].'</td>
                        <td>'.$campo["Passageiro"].'</td>
                        <td>'.$campo["Telefone"].'</td>
                        <td>'.$campo["Forma"].'</td>
                        <td style="text-align: right">'.number_format($total, 2, ',', '.').'</td>
                        <td style="text-align: right">'.number_format($campo["DescontoFidelizacao"], 2, ',', '.').'</td>
                        <td style="text-align: right">'.number_format($total-$campo["DescontoFidelizacao"], 2, ',', '.').'</td>
                    </tr>';    
            $valor += $total;
            $desconto += $campo["DescontoFidelizacao"];
            $passageiros++;
        }
        if(count($ret)>0) {
            $html .= "<tr><td colspan='8'>&nbsp;</td></tr>"; 
        }    
    }
    if($encomenda['Valor']) {
        $final += $encomenda["Valor"];
        $html .=     
        '       <tr style="font-weight: bold; font-size: 10pt;">
                    <td colspan="3">Encomendas/Volumes</td>
                    <td colspan="3" style="text-align: right;">'.number_format($encomenda["Valor"], 2, ',', '.').'</td> 
                    <td colspan="2"></td>
                </tr>
                <tr><td colspan="8"></td>
                </tr>';      
    }
    $html .=     
    '       <tr>
                <td class="foter" colspan="3">TOTAIS</td>
                <td class="foter" style="text-align: right;">'.$passageiros.'</td>
                <td class="foter"></td>
                <td class="foter" style="text-align: right;">'.number_format($valor, 2, ',', '.').'</td>
                <td class="foter" style="text-align: right;">'.number_format($desconto, 2, ',', '.').'</td>
                <td class="foter" style="text-align: right;">'.number_format($valor-$desconto, 2, ',', '.').'</td>
            </tr>';      
    // Total sem o desconto mais a encomenda (volumes)
    $html .=     
    '      <tr><td colspan="8" height=30></td></tr> 
           <tr>
                <td colspan="3" style="font-weight: bold; font-size: 12pt">Total com Encomendas/Volumes</td>
                <td colspan="3" style="text-align: right; font-weight: bold; font-size: 12pt">'.number_format($valor + $encomenda["Valor"], 2, ',', '.').'</td> 
                <td colspan="2"></td>
            </tr>
            <tr><td colspan="8"></td>
            </tr>';      
} else if($recolher)  {
    $html = 
    '<table>
            <tr>
                <th class="header">T</th>
                <th class="header">Pol</th>
                <th class="header">Nome do passageiro</th>
                <th class="header">Telefones</th>
                <th class="header">Op.Vg</th>
                <th class="header">Documento</th>
                <th class="header" style="text-align: right">Valor</th>
            </tr>';
    $passageiros = 0;
    foreach ($retorno as $key => $ret) {
        if(!$ret["Recolher"]) {
            continue;
        }
        $html .=
            '       <tr>
                        <td colspan="7"><div class="cidade">'.$ret["Embarque"].'</div></td>
                    </tr>';
        foreach ($ret["Passageiros"] as $chave => $campo) {
            $html .= 
            '       <tr>
                        <td>'.$campo["Tipo"].'</td>
                        <td>'.$campo["Poltrona"].'</td>
                        <td>'.$campo["Passageiro"].'</td>
                        <td>'.$campo["Telefone"].'</td>
                        <td>'.$campo["Forma"].'</td>
                        <td>'.$campo["RG"].'</td>
                        <td style="text-align: right">'.number_format($campo["Valor"]-$campo["DescontoFidelizacao"]-$campo["DescontoPassagem"]+$campo["Adicional"], 2, ',', '.').'</td>
                    </tr>'; 
            $passageiros++;
        }
        if(count($ret)>0) {
            $html .= "<tr><td colspan='7'>&nbsp;</td></tr>"; 
        }    
    }        
    $html .= 
    '       <tr>
                <td class="foter" colspan="7">Total de passageiros: '.$passageiros.'</td>
            </tr>';
} else {
    //Lista de Passageiros
    $html = 
    '<table>
            <tr>
                <th class="header">T</th>
                <th class="header">Pol</th>
                <th class="header">Nome do passageiro</th>
                <th class="header">Telefones</th>
                <th class="header">Op.Vg</th>
                <th class="header">Documento</th>
            </tr>';
    $passageiros = 0;
    foreach ($retorno as $key => $ret) {
            $html .=
            '       <tr>
                        <td colspan="6"><div class="cidade">'.$ret["Embarque"].'</div></td>
                    </tr>';
        foreach ($ret["Passageiros"] as $chave => $campo) {
            $html .= 
            '       <tr>
                        <td>'.$campo["Tipo"].'</td>
                        <td>'.$campo["Poltrona"].'</td>
                        <td>'.$campo["Passageiro"].'</td>
                        <td>'.$campo["Telefone"].'</td>
                        <td>'.$campo["Forma"].'</td>
                        <td>'.$campo["RG"].'</td>
                    </tr>'; 
            $passageiros++;
        }
        if(count($ret)>0) {
            $html .= "<tr><td colspan='6'>&nbsp;</td></tr>"; 
        }    
    }        
    $html .= 
    '       <tr>
                <td class="foter" colspan="6">Total de passageiros: '.$passageiros.'</td>
            </tr>';
}

$html .= '</table>
          </body>
          </html>';
    
$mpdf=new mPDF('c','A4','9','helvetica',15,15,26,16,9,9); 

$mpdf->SetDisplayMode('fullpage');

$stylesheet = file_get_contents('style.css');
$mpdf->WriteHTML($stylesheet,1);

$mpdf->SetHTMLHeader('
    <table border="0">
        <tr>
            <td width="330">
                <img src="images/'.$idClienteVeritas.'/logo-report.jpg" />
            </td>
            <td style="padding-left: 15px;">
                <div style="font-weight: bold; font-size: 18px; padding-bottom: 6px;">'.$titulo.'</div>
                <div style="font-size: 14px;">'.utf8_encode($rowViagem["Descricao"]).'</div>
            </td>
        </tr>
    </table>
');    
$mpdf->WriteHTML($html,2);

$mpdf->Output('relatorio.pdf','I');
exit;
