<?php

// Função para criptografar e descriptografar uma chave
//copyright www.alexandrub.tk
//ver 1.00
class cript
{
    var $key;
    var $td;
    var $time4keyToChange;
    var $iv;

    function cript($aKey='time',$aTime4keyToChange=3600){
        $this->time4keyToChange=$aTime4keyToChange;
        if($aKey!='time')
                $this->key=$aKey; //."&".intval(time()/$this->time4keyToChange);
        else
                $this->key=intval(time()/$this->time4keyToChange);
        $this->td = MCRYPT_RIJNDAEL_256;
        $this->iv = "qe3jigneqfrgnqw2egfmas4qetjkn5lg";
    }

    function hexFromBin($data){
        return bin2hex($data);
    }

    function binFromHex($data){
        $len = strlen($data);
        return pack("H" . $len, $data);
    }

    function criptData($data){
        return $this->hexFromBin(mcrypt_encrypt($this->td, $this->key, $data, MCRYPT_MODE_CBC, $this->iv));
    }
    
    function decriptData($eData){
        return trim(mcrypt_decrypt($this->td, $this->key, $this->binFromHex($eData), MCRYPT_MODE_CBC, $this->iv));
    }
}

