<?php
/**
 * Classe de conexão usando as classes Mysqli
 * 
 * Esta classe ajuda a padronizar as mensagems de tela e e-mails apresentada para o cliente
 * 
 * @name dbConnection
 * @author Marcus Vinicius Araujo da Rosa (marcusvarosa@gmail.com)
 * @author Michael Claydson (michael@eucatur.com.br)
 */
 
 class dbConnection extends mysqli {

    private $dbHostName          = BD_HOSTNAME;  // nome do servidor
    private $dbUserName          = BD_USERNAME;  // nome do usuário
    private $dbPassword          = BD_PASSWORD;  // senha
    private $dbDatabaseName      = BD_DATABASE;  // nome do banco de dados
    private $progressTransaction = false;        // flag de controle do progresso da transação
    
    /**
    * Construtor da classe connection. 
    *
    * @param boolean $connect  (Opcional) Conecta automaticamente quando o objeto é criado
    * @param string  $database (Opcional) Nome do banco de dados
    * @param string  $server   (Opcional) Endereço do servidor
    * @param string  $username (Opcional) Nome do usuário
    * @param string  $password (Opcional) Senha
    * 
    **/  
    public function __construct($connect = false, $database = null, $server = null,
                                $username = null, $password = null){
        if (strlen($this->dbHostName) > 0 &&
            strlen($this->dbUserName) > 0) {
            if ($connect) $this->openConnection();
        }    
    }
    
    /**
    * Método que realiza a conexão com o banco de dados MySQL. 
    * 
    * @param string  $default  (Opcional) Abre a conexão com os valores das constantes pré-definidas no sistema
    * @param string  $database (Opcional) Nome do banco de dados
    * @param string  $server   (Opcional) Endereço do servidor
    * @param string  $username (Opcional) Nome do usuário
    * @param string  $password (Opcional) Senha
    * 
    **/  
    public function openConnection($default=true, $server = null, $username = null, $password = null, $database = null) {
        try {
            // inicializa com com as constantes padrão
            if($default) {
                $server   = is_null($server)   ? $this->dbHostName     : $server;
                $username = is_null($username) ? $this->dbUserName     : $username;
                $password = is_null($password) ? $this->dbPassword     : $password;
                $database = is_null($database) ? $this->dbDatabaseName : $database;
            }
            
            @$this->connect($server, $username, $password, $database);

            if(mysqli_connect_errno()){
                throw new Exception(mysqli_connect_error());
            }

        } catch (Exception $db_error){
            $msg_error = $this->check_errors($db_error);
            if($msg_error){
                $this->save_logs($msg_error, $db_error);
            }
            exit;
        }        
    }
    
    /**
    * Método para iniciar a transação MySQL. 
    **/  
    public function beginTransaction() {
        $this->autocommit(false);
        $this->progressTransaction = true;
    }
    
    /**
    * Método para salvar a transação atual. 
    **/  
    public function commitTransaction() {
        $this->commit();
    }
    
    /**
    * Método para reverter transação corrente. 
    **/  
    public function rollbackTransaction() {
        $this->rollback();
        $this->endTransaction();
    }

    /**
    * Método que executa uma ou múltiplas consultas que são concatenadas com ponto e vírgula. 
    * @param string $query - Query para consulta no banco.
    **/  
    public function executeQuery($query){
        try {            
            if ($this->multi_query($query)) {
                do {
                    if ($result = $this->store_result()) {
                        return $result; 
                    }
                } while ($this->next_result());
            } else {
                throw new Exception($this->errno);
            }
        } catch (Exception $e) {
            $msg_error = $this->check_errors($e, $query);
            if($msg_error){
                $this->save_logs($msg_error, $e);
            }
            $this->rollbackTransaction();
            $this->closeConnection();
            exit;
        }
    }
    
    /**
    * Método que finaliza a transação MySQL. 
    **/  
    public function endTransaction() {
        $this->progressTransaction = false;
        $this->autocommit(true);
    }

    /**
    * Método que finaliza a transação MySQL. 
    **/  
    public function closeConnection() {
            if (!mysqli_connect_errno())
            $this->close();
    }    
    
    /**
    * Função que traduz algumas mensagens de erro do MySQL. 
    * @param integer $error - Código de erro do MySQL.
    * @param string $query - Query para consulta no banco.
    **/  
    private function check_errors($error, $query = null){
        
        $errorCode = ($error->getMessage() + 0) == 0 ? mysqli_connect_errno() : $error->getMessage(); 
        if(isset($errorCode)){
            if($errorCode == 2003){
                return $errorCode.": Não foi possível conectar no servidor de banco de dados <font color=\"red\">" . BD_HOSTNAME . "</font>!";
            }                                                                        
            if($errorCode == 1049){
                return $errorCode.": O banco de dados [ " . BD_DATABASE . " ] não foi encontrado!";
            }
            if($errorCode == 1045){
                return $errorCode.": Acesso negado para o usuário. Por favor verifique o usuário e senha de acesso!";
            }
            if($errorCode == 1146){
                $msg_error = $this->error;
                $array = explode("'", $msg_error);
                $table = explode(".", $array[1]);
                $msg_error = "A tabela [" . $table[1] . "] não foi encontrada!<br />";
                $query = preg_replace(sql_regcase("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/"),
                    "<font color=\"blue\">\\1</font>", $query);
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";           
            }
            if($errorCode == 1054){
                $msg_error = $errorCode.": Existe algum campo na instrução SQL desconhecido!<br />";
                $query = preg_replace(sql_regcase("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/"),
                    "<font color=\"blue\">\\1</font>", $query);
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";           }
            if($errorCode == 1064){
                $msg_error = $errorCode.": Exite um erro de sintaxe na instrução SQL:<br />".$this->error."<br />";
                $query = preg_replace(sql_regcase("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/"),
                    "<font color=\"blue\">\\1</font>", $query);
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";
            }
            else
            {
                $msg_error = $errorCode.": ".$this->error."<br />";
                $query = preg_replace(sql_regcase("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/"),
                    "<font color=\"blue\">\\1</font>", $query);
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";
            }
        }
    }
    
    /**
    * Função que grava logs dos erros gerados pelo MySQL. 
    * @param string $msg_error - Mensagem traduzida do erro do MySQL.
    * @param string $msg_exception - Mensagem de excessão.
    **/      
    private function save_logs($msg_error, $msg_exception){
        $date = date ("d/m/Y H:i:s");
        $server= $_SERVER['SERVER_NAME'];

        if (!file_exists(LOGS_PATH)) mkdir(LOGS_PATH);

        $msg_log = "<font color=\"red\">ERRO: </font>".$server ." em ".$date."<br />".$msg_error."<br />Excessão: <br />".
                   nl2br($msg_exception->getTraceAsString());
        
        echo json_encode(array(
            "result" => array( 
                "error" => $msg_log
            )
        ));
        
        $msg_log .= "<br /><hr />\r\n\r\n";

        error_log($msg_log, 3, LOGS_PATH . "index.html");
        //** baca, usar o header do php e ver porque gera o erro. **//
        //printf("<script type=\"text/javascript\">window.location='error.html';</script>");
    }

    public function __destruct() {
    }
}
