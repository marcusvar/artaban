<?php 
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = "
    SELECT 
        ART_Localidade.Id,
        ART_Localidade.Nome
    FROM ART_PontoReferencia
    JOIN ART_Localidade ON ART_Localidade.Id = ART_PontoReferencia.Localidade_Id 
    GROUP BY ART_PontoReferencia.Localidade_Id
    ORDER BY ART_Localidade.Nome
" ;
$result = mysql_query($query) or die(mysql_error());
$retorno = array('result' => array());
while($row = mysql_fetch_assoc($result)) {
    $retorno["result"][] = array( "id" => $row["Id"], 
                                  "descricao" => utf8_encode($row["Nome"]));
}

echo json_encode($retorno);

mysql_close();

