<?php
function search($array, $key, $value) 
{ 
    $results = array(); 

    if (is_array($array)) 
    { 
        if (isset($array[$key]) && $array[$key] == $value) 
            $results[] = $array; 

        foreach ($array as $subarray) 
            $results = array_merge($results, search($subarray, $key, $value)); 
    } 

    return $results; 
} 

require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id  = $_REQUEST['Id']; 
//$id = 1580;
//$id = 3;

$web = $_REQUEST['Web'] == 1 ? true : false;
//$web = true;

if(isset($id) and $id !== null and $id !== "") {
    $query = "
SELECT 
  ART_Pessoa.Id, 
  ART_Pessoa.Nome, 
  ART_Pessoa.RGIE, 
  ART_Pessoa.Sexo, 
  ART_Pessoa.Tipo, 
  ART_Pessoa.Documento
FROM 
  ART_PessoaVinculo
JOIN ART_Pessoa ON ART_PessoaVinculo.Pessoa_Id_Vinculo = ART_Pessoa.Id
WHERE 
  ART_PessoaVinculo.Pessoa_Id = $id"; 
} else {
    $query = "SELECT * FROM ART_Pessoa WHERE Tipo LIKE 'PF%'";
}

//ChromePhp::warn('$query');
//ChromePhp::log($query);

$query = utf8_decode($query)."\n ORDER BY ART_Pessoa.Nome";

$result = mysql_query($query); 

$retorno["result"] = "";

$rows = 0;
while($row = mysql_fetch_assoc($result)) {  
    $retorno["result"][$rows] = array( "Id" => $row["Id"], 
                                       "Pessoa_Id_Vinculo" => $id,
                                       "Nome" => utf8_encode($row["Nome"]),
                                       "Documento" => utf8_encode($row["Documento"]), 
                                       "RGIE" => utf8_encode($row["RGIE"]),
                                       "Sexo" => $row["Sexo"],
                                       "TipoPessoa" => $row["Tipo"]);
    $queryTel = "SELECT * FROM ART_PessoaTelefone WHERE Pessoa_Id = ".$row["Id"];
    $resultTel = mysql_query($queryTel); 
    while($rowTel = mysql_fetch_assoc($resultTel)) {
        $retorno["result"][$rows]["Telefones"][] = array( "Descricao" => utf8_encode($rowTel["Descricao"]), 
                                                          "Telefone"  => $rowTel["Telefone"]);
        
    }
    $rows++;
}

$encontrouCliente = count(search($retorno["result"], 'Id', $id)) > 0;

if(($rows === 0 OR !$encontrouCliente)) {
    $query   = "
SELECT 
  ART_Pessoa.RGIE, 
  ART_Pessoa.Sexo, 
  ART_Pessoa.Tipo, 
  ART_Pessoa.Documento,
  IFNULL(ART_Pessoa.Pessoa_Id_Responsavel, ART_Pessoa.Id) AS Id, 
  IF(ISNULL(ART_Pessoa.Pessoa_Id_Responsavel), ART_Pessoa.Nome, Responsavel.Nome) AS Nome, 
  IF(NOT ISNULL(ART_Pessoa.Pessoa_Id_Responsavel), ART_Pessoa.Id, ART_Pessoa.Pessoa_Id_Responsavel) AS IdResponsavel,
  ART_Pessoa.Email
 FROM 
   ART_Pessoa 
 LEFT JOIN ART_Pessoa Responsavel ON ART_Pessoa.Pessoa_Id_Responsavel = Responsavel.Id
 WHERE ART_Pessoa.Id = $id"; 
    
    $result  = mysql_query($query); 
    $row     = mysql_fetch_assoc($result);
    $retorno["result"][$rows] = array( "Id" => $row["Id"], 
                                       "Pessoa_Id_Vinculo" => $id,
                                       "Nome" => utf8_encode($row["Nome"]),
                                       "Documento" => utf8_encode($row["Documento"]), 
                                       "RGIE" => utf8_encode($row["RGIE"]),
                                       "Sexo" => $row["Sexo"],
                                       "TipoPessoa" => $row["Tipo"]);
}

echo json_encode($retorno);