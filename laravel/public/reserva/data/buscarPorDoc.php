<?php
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$doc  = isset($_REQUEST['cpfCnpj']) ? $_REQUEST['cpfCnpj'] : "335.324.430-15";

$isCpf = strlen($doc) === 14;

$query = 
    "SELECT 
       ART_Pessoa.*, 
       Responsavel.Nome AS Responsavel, 
       ART_PessoaEndereco.Logradouro,
       ART_PessoaEndereco.Numero,
       ART_PessoaEndereco.Bairro,
       ART_PessoaEndereco.Complemento,
       ART_PessoaEndereco.Cep,
       ART_PessoaEndereco.Localidade_Id,
       ART_Localidade.Nome AS LocaliodadeNome,
       ART_UF.Id AS UFId, 
       ART_UF.Nome AS UFNome,
       ART_Pais.Id AS PaisId,
       ART_Pais.Nome AS PaisNome,
       (SELECT
         COALESCE(
         SUM(
         ART_Fidelizacao.ValorOperacao * (
         CASE WHEN
           ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
         END)
         ), 0) saldo
         FROM ART_Fidelizacao
         WHERE ART_Fidelizacao.Pessoa_Id = IFNULL(ART_Pessoa.Pessoa_Id_Responsavel, ART_Pessoa.Id)) AS Fidelizacao
     FROM 
       ART_Pessoa
     LEFT JOIN ART_Pessoa Responsavel ON ART_Pessoa.Pessoa_Id_Responsavel = Responsavel.Id
     JOIN ART_PessoaEndereco ON ART_PessoaEndereco.Pessoa_Id = ART_Pessoa.Id
     JOIN ART_Localidade ON ART_Localidade.Id = ART_PessoaEndereco.Localidade_Id
     JOIN ART_UF ON ART_UF.Id = ART_Localidade.UF_Id
     JOIN ART_Pais ON ART_Pais.Id = ART_UF.Pais_Id
     WHERE 
       ART_Pessoa.Documento = '$doc'"; 
$result = mysql_query(utf8_decode($query)); 
$row = mysql_fetch_assoc($result);

$retorno["result"] = array_map('utf8_encode', $row);

$query     = "SELECT * FROM ART_PessoaTelefone WHERE Pessoa_Id = ".$row["Id"];
$resultTel = mysql_query($query); 
while($rowTel = mysql_fetch_assoc($resultTel)) {
    $retorno["result"]["Telefones"][] = array( "Descricao" => utf8_encode($rowTel["Descricao"]), 
                                               "Telefone"  => $rowTel["Telefone"]);
}

echo json_encode($retorno);