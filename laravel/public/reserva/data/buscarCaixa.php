<?php
/*
 */
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

$viagemId = isset($_REQUEST["viagemId"]) ? $_REQUEST["viagemId"] : 174;

//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = "SELECT * FROM ART_ViagemCaixa WHERE Viagem_Id=$viagemId";
$verifica = mysql_query($query);
$rowCaixa = mysql_fetch_assoc($verifica);

if(!$rowCaixa) {
    $query = "INSERT INTO ART_ViagemCaixa (Viagem_Id) VALUES ($viagemId);";
    $ret = mysql_query($query);
}

$query = 
"SELECT 
    ART_ViagemCaixa.*, 
    ART_Veiculo.Numero AS Carro,
    (SELECT group_concat(Lotacao SEPARATOR ', ')
     FROM 
	(SELECT CONCAT(CONVERT(count(*), char), ' ',
	IF(Parcial = 'C', ' Completa(s) ',
	IF(Parcial = 'I', ' Ida(s)', ' Volta(s)'))) AS Lotacao
	 FROM ART_Passagem
	 WHERE Viagem_Id = $viagemId AND Poltrona <> '' AND Situacao = 'Normal'
	 GROUP BY Parcial) x) AS Lotacao,
    (SELECT 
	SUM(Valor-Desconto+(
            SELECT COALESCE(SUM(apa.Valor),0) 
            FROM ART_PassagemAdicional apa 
            WHERE apa.Passagem_Id=ART_Passagem.Id)) 
	 FROM ART_Passagem 
	 WHERE Viagem_Id = $viagemId AND Poltrona <> '' AND Situacao = 'Normal') Total,

    (SELECT
        GROUP_CONCAT(ART_ViagemCaixaEncomendas.Pessoa_Id SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS Pessoa_Id,
    (SELECT
        GROUP_CONCAT(ART_Pessoa.Nome SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        JOIN ART_Pessoa ON ART_Pessoa.Id = ART_ViagemCaixaEncomendas.Pessoa_Id 
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS Cliente,
    (SELECT
        GROUP_CONCAT(ART_ViagemCaixaEncomendas.TipoAdicionalTarifa_Id SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS TipoAdicionalTarifa_Id,
    (SELECT
        GROUP_CONCAT(ART_ViagemCaixaEncomendas.Quantidade SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS Quantidade,
    (SELECT
        GROUP_CONCAT(ART_ViagemCaixaEncomendas.Valor SEPARATOR ';')
        FROM ART_ViagemCaixaEncomendas
        WHERE ART_ViagemCaixaEncomendas.ViagemCaixa_Id = ART_ViagemCaixa.Id
        GROUP BY ART_ViagemCaixaEncomendas.ViagemCaixa_Id) AS Valor
FROM ART_ViagemCaixa
LEFT JOIN ART_Viagem ON ART_Viagem.Id = ART_ViagemCaixa.Viagem_Id 
LEFT JOIN ART_Veiculo ON ART_Veiculo.Id = ART_Viagem.Veiculo_Id
WHERE ART_ViagemCaixa.Viagem_Id = $viagemId;";

$ret = mysql_query(utf8_decode($query));

while ($row = mysql_fetch_assoc($ret)) {
    $rows[] = array_map('utf8_encode', $row);
}

echo json_encode(array(
    "result" => $rows 
));

mysql_close();
