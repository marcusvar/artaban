<?php
header('Content-type: application/pdf');

require_once('config.db.php'); 
include("mpdf60/mpdf.php");

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : 2;

$id = isset($_GET["Id"]) ? $_GET["Id"] : 3538;

$result = mysql_query("SELECT * FROM ART_Pessoa WHERE Id=$id");
$cliente = mysql_fetch_assoc($result);
$cliente = $cliente["Nome"];

$query = 
"SELECT 
  ART_VendaItem.Id,
  ART_Viagem.Descricao,  
  DATE_FORMAT(ART_Venda.DataHoraEmissao,'%d/%m/%Y %h:%i') AS Emissao,
  ART_Passagem.Valor, 
  ART_Passagem.Desconto, 
  ART_Passagem.Adicional, 
  ART_TipoAdicionalTarifa.Descricao AS TipoAdicional, 
  ART_Fidelizacao.ValorOperacao AS Fidelizacao,
  (ART_Passagem.Valor - ART_Passagem.Desconto - ifnull(ART_Fidelizacao.ValorOperacao,0) + ART_Passagem.Adicional) AS Total,
  DATE_FORMAT(ART_Viagem.DataInicio,'%d/%m/%Y %h:%i') AS DataViagem,
  ART_Viagem.HoraInicio AS HoraViagem 
FROM
  ART_VendaItem
JOIN ART_Venda ON ART_VendaItem.Venda_Id = ART_Venda.Id
JOIN ART_Passagem ON ART_VendaItem.Passagem_Id = ART_Passagem.Id
JOIN ART_Pessoa ON ART_Passagem.Pessoa_Id = ART_Pessoa.Id
LEFT JOIN ART_Fidelizacao ON ART_Venda.Id = ART_Fidelizacao.Venda_Id
JOIN ART_Viagem ON ART_Passagem.Viagem_Id = ART_Viagem.Id
LEFT JOIN ART_TipoAdicionalTarifa ON ART_TipoAdicionalTarifa.Id = ART_Passagem.TipoAdicionalTarifa_Id 
WHERE 
  ART_Pessoa.Id = $id AND
  ART_Passagem.Situacao <> 'Cancelada' AND
  ART_Passagem.Poltrona <> ''
GROUP BY ART_Passagem.Id  
ORDER BY 
  ART_Venda.DataHoraEmissao";

$result = mysql_query($query);

$html = ' 
<html>
<head>
</head>
<body>
<table>
    <caption>Cliente: '.utf8_encode($cliente).'</caption>
    <tr>
        <th class="header">Data da Viagem</th>
        <th class="header">Descrição</th>
        <th class="header">Valor</th>
        <th class="header">Desconto</th>
        <th class="header">Fidelização</th>
        <th class="header">Adicional</th>
        <th class="header">Total</th>
    </tr>
';
$total = 0;
while($row = mysql_fetch_assoc($result)) {
    $html .= '
    <tr>
        <td style="text-align: center">'.$row["DataViagem"].'</td>
        <td>'.utf8_encode($row["Descricao"]).'</td>
        <td style="text-align: right">'.number_format($row["Valor"], 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($row["Desconto"], 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($row["Fidelizacao"], 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($row["Adicional"], 2, ',', '.').' - '.$row["TipoAdicional"].'</td>
        <td style="text-align: right">'.number_format($row["Total"], 2, ',', '.').'</td>
    </tr>';
    $total += (float) $row["Total"];        
}
$html .= '
    <tr>
        <td class="foter">Total em compras</td>
        <td colspan="5" class="foter">&nbsp;</td>
        <td class="foter">'.number_format($total, 2, ',', '.').'</td>
    </tr>
</table>
</body>
</html>
';

mysql_close();

$mpdf=new mPDF('c','A4','9','helvetica',15,15,26,16,9,9); 

$mpdf->SetDisplayMode('fullpage');

$stylesheet = file_get_contents('style.css');
$mpdf->WriteHTML($stylesheet,1);

$titulo = 'Extrato de compras';

$mpdf->SetHTMLHeader('
    <table border="0">
        <tr>
            <td width="330">
                <img src="images/'.$idClienteVeritas.'/logo-report.jpg" />
            </td>
            <td style="padding-left: 15px;">
                <div style="font-weight: bold; font-size: 20px; padding-bottom: 6px;">'.$titulo.'</div>
                <!--div style="font-size: 18px;">Aqui vai o sub-título</div-->
            </td>
        </tr>
    </table>
');    
$mpdf->WriteHTML($html,2);

$mpdf->Output('relatorio.pdf','I');