<?php
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
$nome  = utf8_decode($_REQUEST['query']);

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : (isset($_REQUEST["clienteVeritasId"]) ? $_REQUEST["clienteVeritasId"] : 2);

$query = "
SELECT * FROM(
    SELECT 
        ART_Pessoa.Id,
        ART_Pessoa.Pessoa_Id,
        ART_Pessoa.Nome,
        NULL AS IdResponsavel,
        NULL AS Responsavel,
        NULL AS NomeUsual 
    FROM
        ART_Pessoa
    WHERE
        SUBSTR(ART_Pessoa.Tipo,1,2)='PF' AND
        ART_Pessoa.Nome like '%$nome%'
    UNION
    SELECT
        IFNULL(Emp.Id,ART_Pessoa.Id) AS Id,
        ART_Pessoa.Pessoa_Id,
        IFNULL(Emp.Nome,ART_Pessoa.Nome) Nome,
        IFNULL(Emp.Pessoa_Id_Responsavel,ART_Pessoa.Pessoa_Id_Responsavel) IdResponsavel,
        (SELECT Nome FROM ART_Pessoa ap WHERE ap.Id=ART_Pessoa.Id) AS Responsavel,
        (SELECT NomeUsual FROM ART_Pessoa ap WHERE ap.Id=Emp.Id) AS NomeUsual
    FROM
        ART_Pessoa
    LEFT JOIN ART_Pessoa Emp ON Emp.Pessoa_Id_Responsavel = ART_Pessoa.Id
    WHERE
        NOT ISNULL(Emp.Pessoa_Id_Responsavel) AND
        (Emp.Nome like '%$nome%' OR ART_Pessoa.Nome LIKE '%$nome%' OR Emp.NomeUsual LIKE '%$nome%')
) AS Consulta
WHERE   
  NOT ISNULL(Consulta.Id) AND Consulta.Pessoa_Id = $idClienteVeritas
ORDER BY 
  Consulta.Nome ASC      
LIMIT $start,  $limit";

//ChromePhp::log($query);

$result = mysql_query($query); 
        
$retorno = array();

while ($row = mysql_fetch_assoc($result)) {
    $retorno[] = array( "Id" => $row["Id"], 
                        "Nome" => utf8_encode($row["Nome"]),
                        "IdResponsavel" => $row["IdResponsavel"],
                        "Responsavel" => utf8_encode($row["Responsavel"]),
                        "NomeUsual" => utf8_encode($row["NomeUsual"]));
}

//consulta total de linhas na tabela
$queryTotal = 
        "SELECT count(*) as num 
         FROM ART_Pessoa 
         LEFT JOIN ART_Pessoa AS Empresa ON Empresa.Pessoa_Id_Responsavel = ART_Pessoa.Id 
         WHERE ART_Pessoa.Nome LIKE '%$nome%' AND ART_Pessoa.Pessoa_Id = $idClienteVeritas";

$result = mysql_query($queryTotal);

//ChromePhp::log($queryTotal);

$row = mysql_fetch_assoc($result);
$total = $row['num'];

//encoda para formato JSON
echo json_encode(array(
    "total" => $total,
    "result" => $retorno
));

