<?php

require_once('db.php');
function in_array_column($text, $column, $array)
{
    if (!empty($array) && is_array($array))
    {
        for ($i=0; $i < count($array); $i++)
        {
            if ($array[$i][$column]==$text || strcmp($array[$i][$column],$text)==0) return true;
        }
    }
    return false;
}
function mysqli_call(mysqli $dbLink, $procName, $params="")
{
    if(!$dbLink) {
        throw new Exception("A conexão MySQLi é inválida.");
    }
    else
    {
        // Execute the SQL command.
        // The multy_query method is used here to get the buffered results,
        // so they can be freeded later to avoid the out of sync error.
        $sql = "CALL {$procName}({$params});";
        //ChromePhp::log($sql);
        $sqlSuccess = $dbLink->multi_query($sql);
        if($sqlSuccess)
        {
            if($dbLink->more_results())
            {
                // Get the first buffered result set, the one with our data.
                $result = $dbLink->use_result();
                $output = array();
                // Put the rows into the outpu array
                while($row = $result->fetch_assoc())
                {
                    $output[] = $row;
                }
                // Free the first result set.
                // If you forget this one, you will get the "out of sync" error.
                $result->free();
                // Go through each remaining buffered result and free them as well.
                // This removes all extra result sets returned, clearing the way
                // for the next SQL command.
                while($dbLink->more_results() && $dbLink->next_result())
                {
                    $extraResult = $dbLink->use_result();
                    if($extraResult instanceof mysqli_result){
                        $extraResult->free();
                    }
                }
                return $output;
            }
            else
            {
                return false;
            }
        }
        else
        {
            throw new Exception("A chamada falhou: " . $dbLink->error);
        }
    }
}

require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);


$_REQUEST['viagemId'] = isset($_REQUEST['viagemId']) ? $_REQUEST['viagemId'] : 341;
$_REQUEST['poltrona'] = isset($_REQUEST['poltrona']) ? $_REQUEST['poltrona'] : '27';
$_REQUEST['embarqueId'] = isset($_REQUEST['embarqueId']) ? $_REQUEST['embarqueId'] : 153;
$_REQUEST['acao'] = isset($_REQUEST['acao']) ? $_REQUEST['acao'] : 'marcar';


$operacao = $_REQUEST['acao'] === 'marcar' ? 'TRUE' : 'FALSE';

$query = sprintf("SELECT ART_ViagemItem.Id ".
                 "FROM ART_ViagemItem ".
                 "  LEFT JOIN ART_HorarioItem ON ART_ViagemItem.HorarioItem_Id = ART_HorarioItem.Id ".
                 "  LEFT JOIN ART_LinhaItem ON ART_HorarioItem.LinhaItem_Id = ART_LinhaItem.ID ".
                 "WHERE ART_LinhaItem.ID = %s AND ART_ViagemItem.Viagem_Id = %s", 
         $_REQUEST['embarqueId'], $_REQUEST['viagemId']);

$ret = mysql_query( $query, $link );
$viagemItemId = mysql_fetch_array($ret); 

$semaforoId = mysqli_call($mysqli, "Proc_MarcarPoltrona", "'{$_REQUEST['viagemId']}', 
                                                         '{$viagemItemId['Id']}',
                                                         '{$_REQUEST['poltrona']}', $operacao");
echo '{"result":{"semaforoId":'.$semaforoId[0]['_returnVAL'].'}}';
