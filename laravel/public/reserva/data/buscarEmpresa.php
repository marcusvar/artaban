<?php
require_once('config.db.php'); 

$id = isset($_REQUEST["Id"]) ? $_REQUEST["Id"] : 2;

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = 
    "SELECT 
        ART_PessoaEmpresa.Email,
        ART_PessoaEmpresa.Logo,
        ART_PessoaEmpresa.Site,
        ART_Pessoa.Nome,
        ART_Pessoa.NomeUsual
    FROM 
        ART_Pessoa 
    LEFT JOIN ART_PessoaEmpresa ON ART_PessoaEmpresa.Pessoa_Id = ART_Pessoa.Id
    WHERE 
        ART_Pessoa.Id = $id"; 
$result = mysql_query($query); 
$row = mysql_fetch_assoc($result);

$row['Data'] = date('Y-m-d\TH:i:s');

$row = array_map('utf8_encode', $row);
echo json_encode(array(
    "result" => $row 
));