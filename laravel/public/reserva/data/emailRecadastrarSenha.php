<?php
require_once("phpmailer/class.phpmailer.php");
require_once("lib/class.cript.php");
require_once("lib/defines.php");
require_once('config.db.php'); 

include '../chromephp/ChromePhp.php';

//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$EmailCliente = isset($_REQUEST['Email']) ? $_REQUEST['Email'] : 'marcusvarosa@live.com';

$query = 
    "SELECT 
    ART_Pessoa.Pessoa_Id AS idEmpresa, 
    ART_PessoaEmpresa.Host, 
    ART_PessoaEmpresa.Porta,
    ART_PessoaEmpresa.Usuario,
    ART_PessoaEmpresa.Senha,
    ART_PessoaEmpresa.Email,
    ART_PessoaEmpresa.Descricao,
    ART_PessoaEmpresa.Logo,
    ART_PessoaEmpresa.Site,
    ART_PessoaEmpresa.Fone_Suporte,
    ART_PessoaEmpresa.Email_Suporte,
    ART_Pessoa.Id,
    ART_Pessoa.Nome,
    ART_Pessoa.NomeUsual,
    ART_PessoaEmpresa.Subdominio
FROM 
    ART_Pessoa 
LEFT JOIN ART_PessoaEmpresa ON ART_PessoaEmpresa.Pessoa_Id = ART_Pessoa.Pessoa_Id
WHERE
    ART_Pessoa.Email = '$EmailCliente'"; 
$result = mysql_query($query); 
$row = mysql_fetch_assoc($result);

$idEmpresa    = $row['idEmpresa'];
$NomeCliente  = utf8_encode($row['Nome']);
$id           = $row['Id'];

$chave = CHAVE_CRIPTO;
$cript = new cript($chave,0);
$chave = $cript->criptData($id);

$url = $_SERVER["HTTP_HOST"];

$url = ($url === $row['Subdominio'].'.artaban.com.br' ? 
        'https://'.$url.'/ext/' : 
        'http://'.$url.'/').'?id='.$idEmpresa.'&c='.$chave;

ChromePhp::log($row['Subdominio']);

// Envia e-mail para o cliente
$mail = new PHPMailer();
$mail->IsSMTP(); // Define que a mensagem será SMTP
$mail->IsHTML(true);
$mail->CharSet = 'UTF-8';
$mail->Host = $row['Host']; // Endereço do servidor SMTP
$mail->Port = $row['Porta']; // Porta
$mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
$mail->SMTPSecure = 'ssl'; // SSL REQUERIDO pelo GMail
$mail->Username = $row['Usuario']; // Usuário do servidor SMTP
$mail->Password = $row['Senha']; // Senha do servidor SMTP
$mail->From = $row['Email']; // Seu e-mail
$mail->FromName = utf8_encode($row['Descricao']); // Seu nome
$mail->WordWrap = 50; // Definição de quebra de linha
$mail->IsHTML(true); // Define que o e-mail será enviado como HTML
$mail->Subject  = "Confirmação de cadastro"; // Assunto da mensagem

$mail->Body = '<font face="Tahoma, Geneva, sans-serif">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
           <td width="50px">&nbsp;</td>
    </tr>
    <tr>
        <td align="center">
            <table width="800" cellspacing="0" cellpadding="0" style="border:solid 1px #999">
                <tr>
                    <td><p><img src="'.$row['Logo'].'" width="300" height="42" alt="'.utf8_encode($row['NomeUsual']).'" style="padding-left: 50px;"/></p>
                    <p>&nbsp;</p></td>
                </tr>
                <tr>
                    <td style="padding: 0 50px 0 50px;">
                        <p>Prezado(a) '.$NomeCliente.'</p>
                        <p>Recebemos seu pedido para recadastrar sua senha em nosso sistema.<br />
                        <br />
                    Por favor, clique no link abaixo para acessar nosso sistema e recadastrar sua nova senha:</p></td>
                </tr>
                <tr>
                    <td style="padding-left:280px"><table width="200" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="32" align="center" valign="middle" style="background-color:#003;border: 2px ridge #030; color:white; font-weight:bold"><font face="Tahoma, Geneva, sans-serif"><a href="'.$url.'" style="color:white;text-decoration:none">RECADASTRAR</a></font></td>
                        </tr>
                    </table></td>
                </tr>
                <tr>
                    <td style="padding: 0 50px 0 50px;"><p>Caso tenha receio em clicar no link, queira por gentileza informar o endere&ccedil;o abaixo diretamente no seu navegador de Internet:</p>
                        <a href="'.$url.'">'.$url.'</a>
                    </td>
                </tr>

                <tr>
                    <td style="padding: 0px 50px 40px 50px;"><p>&nbsp;</p>
                        <p>&nbsp;</p>
                        <p>Se n&atilde;o foi voc&ecirc; que solicitou o recadastramento de sua senha, desconsidere este e-mail.</p>
                        <p>&nbsp;</p>

                        <br />
                        Atenciosamente,</p>
                        <p><br />
                          <br />
                          '.utf8_encode($row['NomeUsual']).'<br />
                          <a href="'.$row['Site'].'" onmouseover="this.style.textDecoration=\'underline\';this.style.color=\'black\'" onmouseout="this.style.textDecoration=\'none\';">'.$row['Site'].'</a></p></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</font>

<p><br />
</p>';

$mail->AddAddress($EmailCliente, $NomeCliente);
$enviado = $mail->Send();
$mail->ClearAllRecipients();
$mail->ClearAttachments();

if($enviado) {
    echo '{"Resultado":"OK",'.
          '"Mensagem":"<div style=\'text-align: center;\'>Você receberá um e-mail em sua conta <strong>'.$_POST["email"].'</strong><br />'.
          'com um link para o recadastramento de sua senha.<br />'.
          'Caso você não consiga mais acessar sua conta de e-mail<br />',
          'entre em contato com o nosso suporte: <br />'.
          'e-mail: <strong>'.$row['Email_Suporte'].'</strong> ou telefone <strong>'.$row['Fone_Suporte'].'</strong>.<br /><br />'.
          '<font color=\'#FF0000\'><strong>Atenção</strong></font> Lembre-se de autorizar o email <strong>admin@artaban.com.br</strong><br />'.
          'em seu filtro anti-spam para que não tenha problemas em receber nossos emails.</div>"}';
} else {
    echo '{"Resultado":"ERRO", "Mensagem":"Por algum motivo não foi poss&iacute;vel o envio do e-mail<br />Por favor, tente novamente mais tarde."}';
}
