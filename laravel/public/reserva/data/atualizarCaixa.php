<?php
require_once('config.db.php'); 
$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

//include '../chromephp/ChromePhp.php';

/*
$_POST = array( "Id" => 87,
                "Motoristas" => "SILVIO E CARLINHOS",
                "Hotel" => "FEIRINHA",
                "RecolherFoz" => "CLAUDIO VAN Ã",
                "RecolherToledo" => "N",
                "DevolverFoz" => "RONE 7100",
                "DevolverToledo" => "N",
                "Cheques" => 3290,
                "Vales" => 510,
                "Dinheiro" => 5090);
    $_POST['encomenda'][2] = array ( "Cliente" => 3,
                                     "Tipo"    => 3,
                                     "Qtd"     => 2,
                                     "Valor"   => "R$ 300,00"
                                   );
    
    $_POST['encomenda'][1] = array ( "Cliente" => NULL,
                                     "Tipo"    => NULL,
                                     "Qtd"     => NULL,
                                     "Valor"   => "R$ 0,00"
                                   );
*/
$data = json_decode(json_encode($_POST), FALSE);

$query = sprintf("UPDATE ART_ViagemCaixa 
         SET Motoristas     = '%s', 
             Hotel          = '%s', 
             RecolherFoz    = '%s', 
             RecolherToledo = '%s',
             DevolverFoz    = '%s', 
             DevolverToledo = '%s',
             Cheques        = %s, 
             Vales          = %s,
             Dinheiro       = %s
         WHERE Id = %s",
    mysql_real_escape_string(utf8_decode($data->Motoristas)),
    mysql_real_escape_string(utf8_decode($data->Hotel)),
    mysql_real_escape_string(utf8_decode($data->RecolherFoz)),
    mysql_real_escape_string(utf8_decode($data->RecolherToledo)),
    mysql_real_escape_string(utf8_decode($data->DevolverFoz)),
    mysql_real_escape_string(utf8_decode($data->DevolverToledo)),
    mysql_real_escape_string($data->Cheques),
    mysql_real_escape_string($data->Vales),
    mysql_real_escape_string($data->Dinheiro),
    $data->Id
);
//        ChromePhp::warn('$query');
//        ChromePhp::log($query);

$result = mysql_query($query) or die(mysql_error());

$encomendas = $data->encomenda;

$empty = true;
foreach ($encomendas as $val) {
    if(!is_null($val->Cliente)) {
        $empty = false;
        break;
    }
}

// Exclui todos os lançamentos 
$query = "DELETE FROM ART_ViagemCaixaEncomendas WHERE ViagemCaixa_Id = ".$data->Id;
mysql_query( $query );

// Verifica se existe lançamentos de encomendas
if(!$empty) {
    foreach ($encomendas as $val) {
        $valor = str_replace(",", ".", str_replace(".", "", str_replace("R$ ", "", $val->Valor)));
        $query = sprintf("INSERT INTO ART_ViagemCaixaEncomendas (ViagemCaixa_Id, Pessoa_Id, TipoAdicionalTarifa_Id, Quantidade, Valor ) ".
                         "VALUES (%s, %s, %s, %s, %s)",
            mysql_real_escape_string($data->Id),
            mysql_real_escape_string($val->Cliente),
            mysql_real_escape_string($val->Tipo),
            mysql_real_escape_string($val->Qtd),
            mysql_real_escape_string($valor));

        mysql_query($query);
    }
}

echo json_encode(array(
    "result"=> 
        array( "success" => mysql_errno(),
               "msgerr" => mysql_error())
));

mysql_close();