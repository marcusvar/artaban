<?php
class connection extends mysqli {

    /**
    * Construtor da classe connection, que realiza a conexãoo com o banco de dados MySQL. 
    **/  
    public function __construct(){
        try {
            @$this->connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD, BD_DATABASE);

            if(mysqli_connect_errno()){
                throw new Exception(mysqli_connect_error());
            }

        } catch (Exception $db_error){
            $msg_error = $this->check_errors($db_error);
            if($msg_error){
                $this->save_logs($msg_error, $db_error);
                //$this->send_mail($msg_error);
            }
            exit;
        }
    }

    /**
    * Função que executa uma ou múltiplas consultas que são concatenadas com ponto e vírgula. 
    * @param string $query - Query para consulta no banco.
    **/  
    public function executeQuery($query){
        try {
            if ($this->multi_query($query)) {
                do {
                    if ($result = $this->store_result()) {
                        return $result; 
                    }
                } while ($this->next_result());
            } else {
                throw new Exception($this->errno);
            }
        } catch (Exception $e) {
            $msg_error = $this->check_errors($e, $query);
            if($msg_error){
                $this->save_logs($msg_error, $e);
                //$this->send_mail($msg_error);
            }
            exit;
        }
    }
    
    /**
    * Função que traduz algumas mensagens de erro do MySQL. 
    * @param integer $error - Código de erro do MySQL.
    * @param string $query - Query para consulta no banco.
    **/  
    private function check_errors($error, $query = null){
        
        $errorCode = ($error->getMessage() + 0) == 0 ? mysqli_connect_errno() : $error->getMessage(); 
        if(isset($errorCode)){
            if($errorCode == 2003){
                return $errorCode.": Não foi possível conectar no servidor de banco de dados <font color=\"red\">" . BD_HOSTNAME . "</font>!";
            }                                                                        
            if($errorCode == 1049){
                return $errorCode.": O banco de dados [ " . BD_DATABASE . " ] não foi encontrado!";
            }
            if($errorCode == 1045){
                return $errorCode.": Acesso negado para o usuário. Por favor verifique o usuário e senha de acesso!";
            }
            if($errorCode == 1146){
                $msg_error = $this->error;
                $array = explode("'", $msg_error);
                $table = explode(".", $array[1]);
                $msg_error = "A tabela [" . $table[1] . "] não foi encontrada!<br />";
                $query = preg_replace(sql_regcase("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/"),
                    "<font color=\"blue\">\\1</font>", $query);
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";           
            }
            if($errorCode == 1054){
                $msg_error = $errorCode.": Existe algum campo na instrução SQL desconhecido!<br />";
                $query = preg_replace(sql_regcase("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/"),
                    "<font color=\"blue\">\\1</font>", $query);
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";           }
            if($errorCode == 1064){
                $msg_error = $errorCode.": Exite um erro de sintaxe na instrução SQL:<br />".$this->error."<br />";
                $query = preg_replace(sql_regcase("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/"),
                    "<font color=\"blue\">\\1</font>", $query);
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";
            }
            else
            {
                $msg_error = $errorCode.": ".$this->error."<br />";
                $query = preg_replace("
                    /(select|insert|delete|drop table|alter table|from|where|limit|\*|order by|group by)/",
                    "<font color=\"blue\">\\1</font>", $query);
                $query = trim($query);//limpa espaços vazio
                $query = strip_tags($query);//tira tags html e php
                $query = addslashes($query);//Adiciona barras invertidas a uma string 
                return $msg_error . "[ <font color=\"red\">" . $query . "</font> ]";
            }
        }
    }
    
    /**
    * Função que grava logs dos erros gerados pelo MySQL. 
    * @param string $msg_error - Mensagem traduzida do erro do MySQL.
    * @param string $msg_exception - Mensagem de excessão.
    **/      
    private function save_logs($msg_error, $msg_exception){
        $date = date ("d/m/Y H:i:s");
        $server= $_SERVER['SERVER_NAME'];

        if (!file_exists(LOGS_PATH)) mkdir(LOGS_PATH);

        $msg_log = "<font color=\"red\">ERRO: </font>".$server ." em ".$date."<br />".$msg_error."<br />Excess�o: <br />"
          .nl2br($msg_exception->getTraceAsString()). "<br /><hr />\r\n\r\n";

        error_log($msg_log, 3, LOGS_PATH . "log_erros.html");

        //** baca, usar o header do php e ver porque gera o erro. **//
        //printf("<script type=\"text/javascript\">window.location='error.html';</script>");
    }

    /**
    * Função que realiza o envio de e-mail. 
    * @param string $msg_error - Mensagem traduzida do erro do MySQL.
    **/  
    private function send_mail($msg_error){
        $email = new PHPMailer();
        $email->IsSMTP(); // send via SMTP
        $email->Host = "smtp.gmail.com"; //seu servidor SMTP
        $email->SMTPAuth = true; // 'true' para autenticação
        $email->Username = "eucatur.alertas"; // usuário de SMTP
        $email->Password = "eucatur@2012"; // senha de SMTP
        $email->From = "alertas.eucatur@gmail.com";
        $email->FromName = "Portal Web Eucatur";
        $email->AddAddress("marcusvarosa@gmail.com","Administrador");
        $email->WordWrap = 50; // Definição de quebra de linha
        $email->IsHTML(true); // envio como HTML se 'true'
        $email->Subject = "Erro no site da Eucatur";
        $email->Body = "Existe algum problema com o site da Eucatur,
          favor verificar a mensagem de erro em anexo e consultar os logs gerados. Mensagem: " . $msg_error;
        $email->Send();
        //if(!$email->Send()){
        //    echo "Mensagem não enviada";
        //    echo "Mailer Error: " . $wmail->ErrorInfo;
        //} else {
        //    echo "Mensagem enviada";
        //}
    }

    public function __destruct() {
        if (!mysqli_connect_errno())
            $this->close();
    }

}
?>
