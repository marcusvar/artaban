<?php

require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';
//ChromePhp::log($_POST);

$linhaId = isset($_POST["linhaId"]) ? $_POST["linhaId"] : 2;
// Id do cliente Veritas - Multiempresa
//$id      = isset($_POST["Id"]) ? $_POST["Id"] : 2;


// Busca o Id do cliente Veritas definido no virtualhost do apache, caso não exista recebe parâmetro
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : $_REQUEST['Id'];

//ChromePhp::log($idClienteVeritas);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = 
"SELECT
  ART_Linha.ID AS id, 
  CONCAT(UPPER(ART_PontoReferencia.Descricao), '-', ART_UF.Sigla) AS descricao,
  IFNULL(ART_AdicionalTarifa.Valor,0) AS escolta
FROM
  ART_Linha
JOIN ART_PontoReferencia ON ART_Linha.PontoReferencia_Id_Destino = ART_PontoReferencia.Id
JOIN ART_Localidade ON ART_PontoReferencia.Localidade_Id = ART_Localidade.Id
JOIN ART_UF ON ART_Localidade.UF_Id = ART_UF.Id
LEFT JOIN ART_AdicionalTarifa ON ART_Linha.ID = ART_AdicionalTarifa.Linha_Id
WHERE ART_Linha.Pessoa_Id = $idClienteVeritas
ORDER BY id";

$ret = mysql_query($query);

while ($row = mysql_fetch_assoc($ret)) {
    $rows[] = array_map('utf8_encode', $row);
}

//$rows = array_map('utf8_encode', mysql_fetch_assoc($ret));

//$rows = array_map('utf8_encode', $rows);

echo json_encode(array(
    "result" => $rows 
));

mysql_close();