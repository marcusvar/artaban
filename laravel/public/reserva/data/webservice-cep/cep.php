<?php 
include('phpQuery-onefile.php');
require_once('../config.db.php'); 

function simple_curl($url,$post=array(),$get=array()){
	$url = explode('?',$url,2);
	if(count($url)===2){
		$temp_get = array();
		parse_str($url[1],$temp_get);
		$get = array_merge($get,$temp_get);
	}

	$ch = curl_init($url[0]."?".http_build_query($get));
	curl_setopt ($ch, CURLOPT_POST, 1);
	curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($post));
	curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	return curl_exec ($ch);
}

$cep = isset($_REQUEST['cep']) ? $_REQUEST['cep'] : "76900280";

$html = simple_curl('http://m.correios.com.br/movel/buscaCepConfirma.do',array(
    'cepEntrada'=>$cep,
    'tipoCep'=>'',
    'cepTemp'=>'',
    'metodo'=>'buscarCep'
));

phpQuery::newDocumentHTML($html, $charset = 'utf-8');

$dados = 
array(
	'logradouro'=> trim(pq('.caixacampobranco .resposta:contains("Logradouro: ") + .respostadestaque:eq(0)')->html()),
        'endereco'=> trim(pq('.caixacampobranco .resposta:contains("Endereço: ") + .respostadestaque:eq(0)')->html()),
	'bairro'=> trim(pq('.caixacampobranco .resposta:contains("Bairro: ") + .respostadestaque:eq(0)')->html()),
	'cidade / uf'=> trim(pq('.caixacampobranco .resposta:contains("Localidade / UF: ") + .respostadestaque:eq(0)')->html()),
        'cidade/uf'=> trim(pq('.caixacampobranco .resposta:contains("Localidade/UF: ") + .respostadestaque:eq(0)')->html()),
	'cep'=> trim(pq('.caixacampobranco .resposta:contains("CEP: ") + .respostadestaque:eq(0)')->html())
);

$dados['logradouro'] = empty($dados['logradouro']) ? $dados['endereco'] : $dados['logradouro'];
$dados['logradouro'] = explode('- de',$dados['logradouro']);
$dados['logradouro'] = $dados['logradouro'][0];
$dados['logradouro'] = explode('- até',$dados['logradouro']);
$dados['logradouro'] = $dados['logradouro'][0];
$dados['logradouro'] = explode(', ',$dados['logradouro']);
$dados['logradouro'] = $dados['logradouro'][0];

$dados['cidade/uf'] = empty($dados['cidade / uf']) ? $dados['cidade/uf'] : $dados['cidade / uf'];
$dados['cidade/uf'] = explode('/',$dados['cidade/uf']);
$dados['cidade'] = trim($dados['cidade/uf'][0]);
$dados['uf'] = trim($dados['cidade/uf'][1]);
unset($dados['cidade/uf']);
unset($dados['cidade / uf']);
unset($dados['endereco']);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = "
SELECT 
    ART_Localidade.Id AS Localidade_Id, 
    ART_Localidade.UF_Id
FROM 
    ART_Localidade 
JOIN ART_UF ON ART_UF.Id = ART_Localidade.UF_Id
WHERE 
    ART_Localidade.Nome = '".utf8_decode($dados['cidade'])."' AND
    ART_UF.Sigla = '".utf8_decode($dados['uf'])."'"; 

$result = mysql_query($query); 
$row = mysql_fetch_assoc($result);

$dados['Localidade_Id'] = $row['Localidade_Id'];
$dados['UF_Id'] = $row['UF_Id']; 
        
die(json_encode($dados));