<?php
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : 2;

$query = "SELECT
  ART_Pessoa.Id,
  ART_Pessoa.Nome
FROM
  ART_Pessoa
WHERE
  ART_Pessoa.Tipo LIKE 'PF%' AND ART_Pessoa.Pessoa_Id = $idClienteVeritas
ORDER BY ART_Pessoa.Nome ASC";
$result = mysql_query($query); 
        
$retorno = array();

while ($row = mysql_fetch_assoc($result)) {
    $retorno[] = array( "Id" => $row["Id"], 
                        "Nome" => utf8_encode($row["Nome"]),
                        "Empresa" => $row["Cliente"]);
}

//encoda para formato JSON
echo json_encode(array(
    "result" => $retorno
));
