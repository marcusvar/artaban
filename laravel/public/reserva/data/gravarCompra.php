<?php
/*
 * Grava uma carrinho de compras (reserva ou compra)
 */
require_once('config.db.php'); 
require_once("lib/connection.class.php"); 

//include '../chromephp/ChromePhp.php';

$_POST["jsonParam"] = isset($_POST["jsonParam"]) ? $_POST["jsonParam"] :
    '{"venda":{"PontoReferencia_Id":98,"Pessoa_Id_Cliente":"4209","Pessoa_Id":2,"Usuario_Id":3,"ValorDesconto":0,"Pessoa_Id_Fidelizacao":"4209","DescontoFidelizacao":0,"TipoOperacao":"Compra"},"passageiros":{"0":{"Id":"4209","Valor":400,"Poltrona":"01","Viagem_Id":"607","ViagemItem_Id_Origem":"230","Tipo":"P","Parcial":"C","PassagemId":"12007","Desconto":10,"Adicional":{},"Fidelizacao":0}}}';

//ChromePhp::log($_POST);

$param = json_decode($_POST["jsonParam"], FALSE);

//$data = json_decode(json_encode($_POST), FALSE);

// Abre a conexão com o banco de dados
$db = new dbConnection();
$db->openConnection();
$db->beginTransaction();

// Insere os dados da venda na tabela ART_Venda
$venda = $param->venda;

$compra = $venda->TipoOperacao === 'Compra';

// Insere dados da venda
$query = sprintf("INSERT INTO ART_Venda (PontoReferencia_Id, Pessoa_Id_Cliente, Pessoa_Id, Usuario_Id, ValorDesconto) ".
                 "values (%s, %s, %s, %s, %s); SELECT LAST_INSERT_ID() AS Id;",
    $venda->PontoReferencia_Id,
    $venda->Pessoa_Id_Cliente,
    $venda->Pessoa_Id,
    $venda->Usuario_Id,
    $venda->ValorDesconto    
); 
$result = $db->executeQuery($query);
$rows   = $result->fetch_array(MYSQLI_ASSOC);

// Recupera o ID da venda
$venda_id = $rows["Id"];

if($venda->DescontoFidelizacao > 0) {
    $query = sprintf("INSERT INTO ART_Fidelizacao (Operacao, DataOperacao, Descricao, ".
                     "ValorOperacao, Pessoa_Id, Venda_Id, created_at, updated_at) ".
                     "values ('%s', '%s', '%s', %s, %s, %s, '%s', '%s')",
        utf8_decode('Débito'),
        date('Y-m-d H:i:s'),
        'Compra pelo sistema de retaguarda Artaban',
        $venda->DescontoFidelizacao,
        $venda->Pessoa_Id_Fidelizacao, // Insere sempre o ID do responsável    
        $venda_id,
        date('Y-m-d H:i:s'), date('Y-m-d H:i:s')    
    );
    $db->executeQuery($query);
}   


$arr = $param->passageiros;

foreach ( $arr as $key => $value )
{
    // Recupera ID do passageiro
    $id = $value->Id;
    
    // Busca ID do item da viagem
    $query = sprintf("SELECT ART_ViagemItem.Id ".
                     "FROM ART_ViagemItem ".
                     "  LEFT JOIN ART_HorarioItem ON ART_ViagemItem.HorarioItem_Id = ART_HorarioItem.Id ".
                     "  LEFT JOIN ART_LinhaItem ON ART_HorarioItem.LinhaItem_Id = ART_LinhaItem.ID ".
                     "WHERE ART_LinhaItem.ID = %s AND ART_ViagemItem.Viagem_Id = %s", 
             $value->ViagemItem_Id_Origem, $value->Viagem_Id);

    $result = $db->executeQuery($query);
    $rows   = $result->fetch_array(MYSQLI_ASSOC);

    // Verifica se é uma confirmação de uma reserva
    if($value->PassagemId === "") {
        // Salva os dados de cada passageiro na tabela ART_Passagem
        $query = sprintf("INSERT INTO ART_Passagem (Valor, Desconto, DescontoFidelizacao, Poltrona, Viagem_Id, " . 
                         "ViagemItem_Id_Origem, Pessoa_Id, Empresa_Id, Tipo, Situacao, Parcial, created_at, updated_at) ".
                         "VALUES (%s, %s, %s, '%s', %s, %s, %s, %s, '%s', '%s', '%s', '%s', '%s'); SELECT LAST_INSERT_ID() AS Id;",
            $value->Valor,
            $value->Desconto,
            $value->Fidelizacao,
            $value->Poltrona,
            $value->Viagem_Id,
            $rows["Id"], // Id do item da viagem relacionado com o local de embarque
            $id,
            $venda->Pessoa_Id_Cliente,
            $value->Tipo, // R = Reserva, P = Passagem
            'Normal',
            $value->Parcial,
            date('Y-m-d H:i:s'), date('Y-m-d H:i:s')
        ); 
        $result = $db->executeQuery($query);    
        $rows   = $result->fetch_array(MYSQLI_ASSOC);
        
        // Recupera o ID da passagem
        $passagem_id = $rows["Id"];
        
        // Salva adicionais da compra da passagem

        foreach ( $value->Adicional as $keyAdd => $valueAdd )
        {
            if(!is_null($valueAdd->Valor)) {
                $queryAdd = sprintf("INSERT INTO ART_PassagemAdicional (Passagem_Id, TipoAdicionalTarifa_Id, Valor, created_at, updated_at) ".
                                 "values (%s, %s, %s, '%s', '%s')",
                    $passagem_id,
                    $valueAdd->Id,
                    $valueAdd->Valor,
                    date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

                $db->executeQuery($queryAdd);                
            }
        }
    } else {
        $passagem_id = $value->PassagemId;
        $query = sprintf("UPDATE ART_Passagem 
                          SET Tipo                   = '%s',
                              Parcial                = '%s',
                              Desconto               = %s, 
                              DescontoFidelizacao    = %s,
                              updated_at             = '%s'
                          WHERE Id = %s",
            $value->Tipo,
            $value->Parcial,
            $value->Desconto,    
            $value->Fidelizacao,
            date('Y-m-d'),
            $passagem_id
        );        
        $db->executeQuery($query); 
        // Atualiza os valores dos adicionais
        $db->executeQuery("DELETE FROM ART_PassagemAdicional WHERE Passagem_Id=$passagem_id;");
        $add = $value->Adicional;
        foreach ( $add as $keyAdd => $valueAdd )
        {
            $queryAdd = sprintf("INSERT INTO ART_PassagemAdicional (Passagem_Id, TipoAdicionalTarifa_Id, Valor,  created_at, updated_at) ".
                             "values (%s, %s, %s, '%s', '%s')",
                $passagem_id,
                $valueAdd->Id,
                $valueAdd->Valor,
                date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

            $db->executeQuery($queryAdd);
        }
    }
    
    // salva os dados na tabela ART_VendaItem
    $query = sprintf("INSERT INTO ART_VendaItem (Venda_Id, Passagem_Id, created_at, updated_at) ".
                     "values (%s, %s, '%s', '%s')",
        $venda_id,
        $passagem_id,
        date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));

    $db->executeQuery($query);      
}          

echo json_encode(array(
    "result" => array( 
        "error"             => "",
        "merchantreference" => $venda_id
    )
));

$db->commitTransaction();
$db->endTransaction();
$db->closeConnection();

