<?php

//include '../chromephp/ChromePhp.php';

require_once('config.db.php'); 
require_once("lib/connection.class.php"); 
require_once("phpmailer/class.phpmailer.php");
require_once("lib/class.cript.php");
require_once("lib/defines.php");

//ChromePhp::log($_POST);

// Abre a conexão com o banco de dados
$db = new dbConnection();
$db->openConnection();
$db->beginTransaction();

if(!isset($_POST["Tipo"])) {
    $_POST = array( "Tipo" => "PF Brasileiro",
                    "RGIE" => "8327465827634",
                    "Documento" => "444.999.703-42",
                    "Nome" => "ujsddhfgjhsadgf jsahdgfasugdf",
                    //"NomeUsual" => "LOJA jdshgfjshdgf",
                    "RGIE" => "276354726534",
                    "Sexo" => "M",
                    //"Pessoa_Id_Responsavel" => "3",
                    //"DataNascimento" => "1961-04-20",
                    "Email" => "SARTORILOJAS@YAHOO.COM.BR",
                    //"Vincular" => 'on',
                    //"Senha" => "2e1a97c9ea8f3b2492486020f31ec93bf1490d7d",
                    //"IdVincular" => 3,
                    "Pessoa_Id" => 2,
                    "Id"=> 3,
                    "VisualizarFidelizacao" => 1,
                    "EmPotencial" => 0,
                    "Negativado" => 0,
                    "Observacao" => ""
    //                "Web" => true
    );

    
    $_POST['telefone'][3] = array ( "Descricao" => "Comercial",
                                    "Numero" => "(45) 32861196"  );
    $_POST['telefone'][2] = array ( "Descricao" => "Celular",
                                    "Numero" => "(45) 91343893"  );
     
     
/*
    $_POST['endereco'] = array( "Bairro" => "",
                                "Cep" => "",
                                "Complemento" => "",
                                "Localidade_Id" => "",
                                "Logradouro" => "",
                                "Numero" => "",
                                "uf" => "") ;
 * 
 */

    
    $_POST['endereco'] = array( "Bairro" => "Urupá",
                                "Cep" => "85780-000",
                                "Complemento" => "uggjhgjghjhgjgjhg",
                                "Localidade_Id" => "2823",
                                "Logradouro" => "AVENIDA CICERO B. SOBRINHO",
                                "Numero" => "620",
                                "uf" => "18") ;
     
     


}

//if(!isset($_POST["jsonParam"])) {
//    $_POST["jsonParam"] = '{"Tipo":"PF Brasileiro","Documento":"835.332.633-71","Nome":"teste -----","RGIE":"287354872534","Email":"teste555@teste.com","Senha":"40bd001563085fc35165329ea1ff5c5ecbdbbeef","ConfirmaSenha":null,"Pessoa_Id_Responsavel":"","Sexo":"M","DataNascimento":"1975-10-10","textfield-1108-inputEl":"Personalizar","telefone[0][Descricao]":"Comercial","telefone[0][Numero]":"(76) 57657657","textfield-1114-inputEl":"Personalizar","telefone[1][Descricao]":"Celular","telefone[1][Numero]":"(76) 576576567","endereco[Cep]":"76900-280","endereco[Logradouro]":"Rua Velho Teotônio","endereco[Numero]":"123","endereco[Complemento]":"jhsgdfjhsdg","endereco[Bairro]":"Urupá","endereco[uf]":"21","endereco[Localidade_Id]":"4369","Pessoa_Id":"2","Web":true}';
//}
//$data = json_decode($_POST["jsonParam"], FALSE);

$data = json_decode(json_encode($_POST), FALSE);

$data->Pessoa_Id_Responsavel = (isset($data->Pessoa_Id_Responsavel) AND !empty($data->Pessoa_Id_Responsavel)) ? 
                               $data->Pessoa_Id_Responsavel : 'NULL'; 
$data->DataNascimento = (isset($data->DataNascimento) AND !empty($data->DataNascimento)) ? 
                        "'".$data->DataNascimento."'" : 'NULL'; 
$data->Sexo = empty($data->Sexo) ? 'NULL' : "'".$data->Sexo."'";

$clienteId = (isset($data->Id) AND !empty($data->Id)) ? $data->Id : 0;

//ChromePhp::log($clienteId);

if($clienteId === 0) {
    $query = sprintf("INSERT INTO ART_Pessoa (".
                     "Nome, NomeUsual, Documento, RGIE, Email, Senha, Sexo, DataNascimento, ".
                     "Pessoa_Id, Tipo, Pessoa_Id_Responsavel, EmPotencial, VisualizarFidelizacao, ".
                     "Negativado, Observacao, created_at, updated_at) ".
                     "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %s, %s, %s, '%s', %s, %s, %s, %s, '%s', '%s', '%s'); ".
                     "SELECT LAST_INSERT_ID() AS Id;",
        $data->Nome,
        $data->NomeUsual,
        $data->Documento,
        $data->RGIE,
        $data->Email,
        $data->Senha,
        $data->Sexo,
        $data->DataNascimento,
        $data->Pessoa_Id, // Código do cliente Veritas
        $data->Tipo,
        $data->Pessoa_Id_Responsavel,
        $data->EmPotencial,
        $data->VisualizarFidelizacao,
        $data->Negativado,    
        $data->Observacao,
        date('Y-m-d'), date('Y-m-d')
    );
      
} else {
    $query = sprintf("UPDATE ART_Pessoa 
             SET Nome                  = '%s', 
                 NomeUsual             = '%s',
                 Documento             = '%s', 
                 RGIE                  = '%s',
                 Email                 = '%s',
                 Senha                 = '%s',
                 Sexo                  = %s,
                 DataNascimento        = %s,
                 Pessoa_Id             = %s, 
                 Tipo                  = '%s', 
                 Pessoa_Id_Responsavel = %s, 
                 EmPotencial           = %s, 
                 VisualizarFidelizacao = %s,
                 Negativado            = %s,
                 Observacao            = '%s',
                 updated_at            = '%s'
             WHERE Id = %s",
        $data->Nome,
        $data->NomeUsual,
        $data->Documento,
        $data->RGIE,
        $data->Email,
        $data->Senha,
        $data->Sexo,
        $data->DataNascimento,
        $data->Pessoa_Id, 
        $data->Tipo,
        $data->Pessoa_Id_Responsavel,
        $data->EmPotencial,
        $data->VisualizarFidelizacao,
        $data->Negativado,
        $data->Observacao,
        date('Y-m-d'),
        $data->Id
    );    
}

//ChromePhp::log($query);

$result = $db->executeQuery(utf8_decode($query));
if($result) {
    $rows   = $result->fetch_array(MYSQLI_ASSOC);
}
//ChromePhp::log(mysql_error());

// Verifica se é altera'
$id = ($clienteId === 0 ? $rows["Id"] : $clienteId);
//ChromePhp::log(mysql_error());

$data->Id = $id;

$empty = true;
foreach ($data->endereco as $val) {
    if($val !== "") {
        $empty = false;
        break;
    }
}

$query = "SELECT * FROM ART_PessoaEndereco WHERE ART_PessoaEndereco.Pessoa_Id = $id";
$result = $db->executeQuery($query);
$row    = $result->fetch_array(MYSQLI_ASSOC);

//ChromePhp::log($query);


if(!$empty) {
    if(!$row) {
        $query = sprintf("INSERT INTO ART_PessoaEndereco (Logradouro, Numero, Bairro, ".
                                                         "Complemento, Cep, Localidade_Id, ".
                                                         "Pessoa_Id, created_at, updated_at ) ".
                         "VALUES ('%s', '%s', '%s', '%s', '%s', '%s', %s, '%s', '%s')",
        $data->endereco->Logradouro,
        $data->endereco->Numero,
        $data->endereco->Bairro,
        $data->endereco->Complemento,
        $data->endereco->Cep,
        $data->endereco->Localidade_Id,
        $id,
        date('Y-m-d'), date('Y-m-d'));
    } else {
        $query = sprintf("UPDATE ART_PessoaEndereco
                 SET Logradouro    = '%s', 
                     Numero        = '%s',
                     Bairro        = '%s' , 
                     Complemento   = '%s',
                     Cep           = '%s',
                     Localidade_Id = '%s',
                     Pessoa_Id     = %s,
                     updated_at    = '%s'
             WHERE Pessoa_Id = %s",
        $data->endereco->Logradouro,
        $data->endereco->Numero,
        $data->endereco->Bairro,
        $data->endereco->Complemento,
        $data->endereco->Cep,
        $data->endereco->Localidade_Id,
        $clienteId,
        date('Y-m-d'),
        $data->Id);
    }    
    
//    ChromePhp::log($query);
    
    $db->executeQuery(utf8_decode($query));
    
//    ChromePhp::log(mysql_error());
}

$telefone = $data->telefone;

//ChromePhp::log($telefone);


$empty = true;
foreach ($telefone as $val) {
    if(!empty($val->Descricao) and !empty($val->Numero) ) {
        $empty = false;
        break;
    }
}

if(!$empty) {
    if($clienteId > 0) {
        $query = "DELETE FROM ART_PessoaTelefone WHERE Pessoa_Id = ".$data->Id;
        $db->executeQuery($query);    
    }
    foreach ($telefone as $val) {
        if(!empty($val->Descricao) and !empty($val->Numero) ) {
            $query = sprintf("INSERT INTO ART_PessoaTelefone (Descricao, Telefone, Pessoa_Id, created_at, updated_at ) ".
                             "VALUES ('%s', '%s', %s, '%s', '%s')",
                $val->Descricao,
                $val->Numero,
                ($clienteId > 0 ? $clienteId : $id),
                date('Y-m-d'), date('Y-m-d'));

            $db->executeQuery(utf8_decode($query));
            
//            ChromePhp::log(utf8_decode($query));
            
            //ChromePhp::log($query);
            //ChromePhp::log(mysql_error());
        }
    }
}

if($data->Vincular === 'on') {
    $query = sprintf("INSERT INTO ART_PessoaVinculo (Pessoa_Id, Pessoa_Id_Vinculo, created_at, updated_at ) ".
                     "VALUES (%s, %s, '%s', '%s')",
        $data->IdVincular,
        $id,
        date('Y-m-d'), date('Y-m-d'));

    $db->executeQuery(utf8_decode($query)); 
    
//    ChromePhp::log($query);
    //ChromePhp::log(mysql_error());
}

$pj = substr($data->Tipo, 0, 2) === "PJ";
if($pj) {
    $query = "
        SELECT 
            ART_Pessoa.Nome,
            (SELECT
             COALESCE(
             SUM(
             ART_Fidelizacao.ValorOperacao * (
             CASE WHEN
               ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
             END)
             ), 0) saldo
             FROM ART_Fidelizacao
             WHERE ART_Fidelizacao.Pessoa_Id = ART_Pessoa.Id
            ) AS Fidelizacao
        FROM ART_Pessoa 
        WHERE ART_Pessoa.Id = ".($clienteId > 0 ? $clienteId : $id);

    $result = $db->executeQuery(utf8_decode($query));
    $row = array_map('utf8_encode', $result->fetch_array(MYSQLI_ASSOC));
    $data->Fidelizacao   = $row["Fidelizacao"];
    $data->Nome          = $row["Nome"];
    $data->IdResponsavel = $data->Pessoa_Id_Responsavel; 
}


if(isset($data->Web)) {

    $query = 
        "SELECT 
        ART_Pessoa.Pessoa_Id AS idEmpresa, 
        ART_PessoaEmpresa.Host, 
        ART_PessoaEmpresa.Porta,
        ART_PessoaEmpresa.Usuario,
        ART_PessoaEmpresa.Senha,
        ART_PessoaEmpresa.Email,
        ART_PessoaEmpresa.Descricao,
        ART_PessoaEmpresa.Logo,
        ART_PessoaEmpresa.Site,
        ART_Pessoa.Id,
        ART_Pessoa.Nome,
        ART_Pessoa.NomeUsual,
        ART_Pessoa.Email AS EmailCliente,
        ART_PessoaEmpresa.Subdominio,
        (SELECT NomeUsual FROM ART_Pessoa WHERE Id = ART_PessoaEmpresa.Pessoa_Id) AS NomeEmpresa
    FROM 
        ART_Pessoa 
    LEFT JOIN ART_PessoaEmpresa ON ART_PessoaEmpresa.Pessoa_Id = ART_Pessoa.Pessoa_Id 
    WHERE
        ART_Pessoa.Id = $id";
    $result = $db->executeQuery($query); 
    $row = $result->fetch_array(MYSQLI_ASSOC);    
    
//    ChromePhp::log(json_encode($row));
    
    $NomeCliente  = utf8_encode($row['Nome']);
    $EmailCliente = $row['EmailCliente'];

    $chave = CHAVE_CRIPTO;
    $cript = new cript($chave,0);
    $chave = $cript->criptData($id);

    $url = $_SERVER["HTTP_HOST"];
    $url = ($url === $row['Subdominio'].'.artaban.com.br' ? 
            'https://'.$url.'/ext/' : 
            'http://'.$url.'/').'data/ativarCadastro.php?chave='.$chave;

    // Envia e-mail para o cliente
    $mail = new PHPMailer();
    $mail->IsSMTP(); // Define que a mensagem será SMTP
    $mail->IsHTML(true);
    $mail->CharSet = 'UTF-8';
    $mail->Host = $row['Host']; // Endereço do servidor SMTP
    $mail->Port = $row['Porta']; // Porta
    $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
    $mail->SMTPSecure = 'ssl'; // SSL REQUERIDO pelo GMail
    $mail->Username = $row['Usuario']; // Usuário do servidor SMTP
    $mail->Password = $row['Senha']; // Senha do servidor SMTP
    $mail->From = $row['Email']; // Seu e-mail
    $mail->FromName = utf8_encode($row['Descricao']); // Seu nome
    $mail->WordWrap = 50; // Definição de quebra de linha
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
    $mail->Subject  = "Confirmação de cadastro"; // Assunto da mensagem

    $mail->Body = '
    <font face="Tahoma, Geneva, sans-serif">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
           <tr>
               <td width="50px">&nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <table width="800" cellspacing="0" cellpadding="0" style="border:solid 1px #999">
                    <tr>
                        <td><p><img src="'.$row['Logo'].'" width="300" height="42" alt="'.utf8_encode($row['NomeUsual']).'" style="padding-left: 50px;"/></p>
                        <p>&nbsp;</p></td>
                    </tr>
                    <tr>
                        <td style="padding: 0 50px 0 50px;">
                            <p>Prezado(a) '.$NomeCliente.'</p>
                            <p>Seja bem vindo a <font face="Tahoma, Geneva, sans-serif">'.utf8_encode($row['NomeEmpresa']).'</font>, estamos muito felizes com a sua inclus&atilde;o em nosso sistema.<br />
                                <br />
                                                            Para a confirma&ccedil;&atilde;o de seu e-mail e libera&ccedil;&atilde;o do seu cadastro, clique no link abaixo:<br />
                          </p></td>
                    </tr>
                    <tr>
                        <td style="padding-left:280px"><table width="200" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="32" align="center" valign="middle" style="background-color:#003;border: 2px ridge #030; color:white; font-weight:bold"><font face="Tahoma, Geneva, sans-serif"><a href="'.$url.'" style="color:white;text-decoration:none">ATIVAR CADASTRO</a></font></td>
                            </tr>
                        </table></td>
                    </tr>
                    <tr>
                        <td style="padding: 0 50px 0 50px;"><p>Caso tenha receio em clicar no link, queira por gentileza informar o endere&ccedil;o abaixo diretamente no seu navegador de Internet:</p>
                            <a href="'.$url.'">'.$url.'</a>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding: 0px 50px 40px 50px;"><p>&nbsp;</p>
                            <p>Agradecemos a sua compreens&atilde;o e esperamos, sempre, contar com voc&ecirc; como nosso cliente.</p>
    <p><br />
        <br />
        Atenciosamente,</p>
                          <p><br />
                              <br />
                              '.utf8_encode($row['NomeUsual']).'<br />
                              <a href="'.$row['Site'].'" onmouseover="this.style.textDecoration=\'underline\';this.style.color=\'black\'" onmouseout="this.style.textDecoration=\'none\';">'.$row['Site'].'</a></p></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    </font>

    <p><br />
    </p>';

    $mail->AddAddress($EmailCliente, $NomeCliente);
    $enviado = $mail->Send();
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
    
}
echo json_encode(array(
    "result"=> 
        array( "error"    => "",
               "emailErr" => $mail->ErrorInfo,
               "retorno"  => (array) $data
        )
));

$db->commitTransaction();
$db->endTransaction();
$db->closeConnection();
