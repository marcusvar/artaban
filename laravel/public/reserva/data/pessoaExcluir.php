<?php
require_once('config.db.php');

function executeQuery($mysqli, $query) {
    $mysqli->query($query);
    $err = $mysqli->error;
    if(!empty($err)) {
        $mysqli->rollback();
        echo json_encode(array(
            "msg"     => $err
        ));
        exit();
    }    
}

$mysqli = new mysqli(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD, BD_DATABASE);

$mysqli->autocommit(false);

$id = isset($_REQUEST["Id"]) ? $_REQUEST["Id"] : 6935 ;

// Seleciona todas as passagens deste cliente
$query = 
"SELECT ART_Passagem.*
FROM ART_Passagem
JOIN ART_VendaItem ON ART_VendaItem.Passagem_Id = ART_Passagem.Id
JOIN ART_Venda ON ART_Venda.Id = ART_VendaItem.Venda_Id
WHERE ART_Venda.Pessoa_Id_Cliente = $id OR (ART_Passagem.Pessoa_Id = $id AND ART_Passagem.Situacao = 'Cancelada')";
$result = $mysqli->query($query);
while($row = $result->fetch_assoc()) {
    $passagens[] = array( "Id" => $row["Id"]);
    $err = $mysqli->error;
    if(!empty($err)) {
        $mysqli->rollback();
        echo json_encode(array(
            "msg"     => $err
        ));
        exit();
        break;
    }
}


// Exclui todas os itens de venda relacionados a este cliente
executeQuery($mysqli, 
"DELETE vi
  FROM ART_VendaItem AS vi
 JOIN ART_Venda AS v ON v.Id=vi.Venda_Id
 JOIN ART_Passagem AS p ON p.Id =vi.Passagem_Id
 WHERE v.Pessoa_Id_Cliente = $id OR (p.Pessoa_Id = $id AND p.Situacao = 'Cancelada')");

// Exclui todas as passagens relacionados a este cliente
foreach ($passagens as $key => $value) {
    executeQuery($mysqli, "DELETE FROM ART_Passagem WHERE Id = ".$value['Id']);
}

// Exclui todas os lançamentos de fidelização relacionados a este cliente
executeQuery($mysqli, 
"DELETE f
 FROM ART_Fidelizacao AS f
 INNER JOIN ART_Venda AS v ON v.Id=f.Venda_Id
 WHERE v.Pessoa_Id_Cliente = $id");

// Exclui todas as vendas relacionados a este cliente
executeQuery($mysqli, "DELETE FROM ART_Venda WHERE Pessoa_Id_Cliente = $id");

// Exclui todas as pessoas vinculadas relacionados a este cliente
executeQuery($mysqli, "DELETE FROM ART_PessoaVinculo WHERE Pessoa_Id_Vinculo = $id");

// Exclui todos os telefones desta pessoa
executeQuery($mysqli, "DELETE FROM ART_PessoaTelefone WHERE Pessoa_Id = $id");

// Exclui a pessoa
executeQuery($mysqli, "DELETE FROM ART_Pessoa WHERE Id = $id");

$mysqli->commit();
echo json_encode(array(
    "success" => $mysqli->errno,
    "msg"     => "Exclusão de cliente bem sucedida!"
));

$mysqli->close();
