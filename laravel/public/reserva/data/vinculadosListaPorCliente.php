<?php
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id  = isset($_REQUEST['clienteId']) ? $_REQUEST['clienteId'] : "77";

$query = "
SELECT * FROM (    
    SELECT 
        Vinculado.Id AS Id,
        ART_Pessoa.Id AS IdPrincipal,
        ART_Pessoa.Nome AS Principal, 
        ART_Pessoa.Nome AS Cliente, 
        Vinculado.Nome AS Vinculado,
        Vinculado.Tipo 
    FROM ART_Pessoa
    LEFT JOIN ART_PessoaVinculo ON ART_Pessoa.Id = ART_PessoaVinculo.Pessoa_Id
    LEFT JOIN ART_Pessoa AS Vinculado ON Vinculado.Id = ART_PessoaVinculo.Pessoa_Id_Vinculo
    WHERE ART_Pessoa.Id = $id
    UNION
    SELECT
        Vinculado.Id AS Id,
        ART_Pessoa.Id AS IdPrincipal,
        Responsavel.Nome AS Principal, 
        ART_Pessoa.Nome AS Cliente, 
        Vinculado.Nome AS Vinculado,
        ART_Pessoa.Tipo 
    FROM ART_Pessoa
    LEFT JOIN ART_Pessoa AS Responsavel ON ART_Pessoa.Pessoa_Id_Responsavel = Responsavel.Id
    LEFT JOIN ART_PessoaVinculo ON ART_Pessoa.Id = ART_PessoaVinculo.Pessoa_Id
    LEFT JOIN ART_Pessoa AS Vinculado ON Vinculado.Id = ART_PessoaVinculo.Pessoa_Id_Vinculo
    WHERE ART_Pessoa.Pessoa_Id_Responsavel = $id
) AS Consulta";

$result = mysql_query($query); 

while ($row = mysql_fetch_assoc($result)) {
    $retorno["result"][] = array_map('utf8_encode', $row);
}

echo json_encode($retorno);

mysql_close();
