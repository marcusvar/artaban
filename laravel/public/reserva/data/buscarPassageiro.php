<?php

require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

$viagemId = isset($_POST["viagemId"]) ? $_POST["viagemId"] : 115;
$poltrona = isset($_POST["poltrona"]) ? $_POST["poltrona"] : '43';

//ChromePhp::log($_POST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = 
"SELECT
  ap2.Id,  
  ap.Empresa_Id AS ClienteId,
  ap.Id AS passagemId,
  ap1.Descricao AS destino,
  ap2.documento AS Documento,
  CONCAT(al2.nome, ' - ', ap3.Descricao) AS embarque,
  ap3.Id AS embarqueId,
  ap2.Nome,
  ap2.Tipo AS TipoPessoa,
  (SELECT
      GROUP_CONCAT(ART_PessoaTelefone.Descricao,': ',ART_PessoaTelefone.telefone SEPARATOR ' ')
    FROM ART_PessoaTelefone
    WHERE ART_PessoaTelefone.Pessoa_Id = ap2.Id
    GROUP BY ART_PessoaTelefone.Pessoa_Id) AS telefones,
  ap.valor AS valor,
  ap.Empresa_Id AS ClienteId,
  ap.Desconto,
    (SELECT
      COALESCE(
      SUM(
      ART_Fidelizacao.ValorOperacao * (
      CASE WHEN
        ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
      END)
      ), 0) saldo
    FROM ART_Fidelizacao
    WHERE ART_Fidelizacao.Pessoa_Id = ap2.Id) AS Fidelizacao,
  ap.DescontoFidelizacao,
  IF(ap.Tipo='P', 'V', 'R') as tipo,
  ap.Parcial as parcial,
  ap.Poltrona as poltrona,
  ap2.RGIE,
  ap2.Sexo,
  (SELECT
      GROUP_CONCAT(ART_PassagemAdicional.Valor SEPARATOR ';')
    FROM ART_PassagemAdicional
    WHERE ART_PassagemAdicional.Passagem_Id = ap.Id
    GROUP BY ART_PassagemAdicional.Passagem_Id) AS Adicional,
  (SELECT
      GROUP_CONCAT(ART_PassagemAdicional.TipoAdicionalTarifa_Id SEPARATOR ';')
    FROM ART_PassagemAdicional
    WHERE ART_PassagemAdicional.Passagem_Id = ap.Id
    GROUP BY ART_PassagemAdicional.Passagem_Id) AS TipoAdicionalTarifa_Id,
  atat.Descricao AS AdicionalDescricao,
  ap.Viagem_Id AS viagemId
FROM ART_Passagem ap
  JOIN ART_Viagem av                ON ap.Viagem_Id = av.Id
  JOIN ART_Horario ah               ON av.Horario_Id = ah.Id
  JOIN ART_Linha al                 ON ah.Linha_Id = al.Id
  JOIN ART_PontoReferencia ap1      ON al.PontoReferencia_Id_Destino = ap1.Id
  JOIN ART_Pessoa ap2               ON ap.Pessoa_Id = ap2.Id
  JOIN ART_ViagemItem av1           ON ap.ViagemItem_Id_Origem = av1.Id
  JOIN ART_HorarioItem ah1          ON av1.HorarioItem_Id = ah1.Id
  JOIN ART_LinhaItem al1            ON ah1.LinhaItem_Id = al1.Id
  JOIN ART_PontoReferencia ap3      ON al1.PontoReferencia_Id = ap3.Id
  JOIN ART_Localidade al2           ON ap3.Localidade_Id = al2.Id
  LEFT JOIN ART_TipoAdicionalTarifa atat ON ap.TipoAdicionalTarifa_Id = atat.Id
WHERE ap.Viagem_Id = $viagemId AND ap.Poltrona = $poltrona";

$ret = mysql_query(utf8_decode($query));

while ($row = mysql_fetch_assoc($ret)) {
    $rows[] = array_map('utf8_encode', $row);
}

//$rows = array_map('utf8_encode', mysql_fetch_assoc($ret));

//$rows = array_map('utf8_encode', $rows);

echo json_encode(array(
    "result" => $rows 
));

mysql_close();



