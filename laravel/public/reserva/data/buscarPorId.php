<?php
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id  = $_REQUEST['clienteId'];
$emp = isset($_REQUEST['emp']);

$query = 
    "SELECT 
".
    ($emp ? "ART_Pessoa.Id, ART_Pessoa.Nome, ART_Pessoa.Id AS Empresas, " : "     
       IFNULL(ART_Pessoa.Pessoa_Id_Responsavel, ART_Pessoa.Id) AS Id, 
       IF(ISNULL(ART_Pessoa.Pessoa_Id_Responsavel), ART_Pessoa.Nome, Responsavel.Nome) AS Nome,   
       IF(NOT ISNULL(ART_Pessoa.Pessoa_Id_Responsavel), ART_Pessoa.Id, ART_Pessoa.Pessoa_Id_Responsavel) AS IdResponsavel,")."
       (SELECT
         COALESCE(
         SUM(
         ART_Fidelizacao.ValorOperacao * (
         CASE WHEN
           ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
         END)
         ), 0) saldo
         FROM ART_Fidelizacao
         WHERE ART_Fidelizacao.Pessoa_Id = IFNULL(ART_Pessoa.Pessoa_Id_Responsavel, ART_Pessoa.Id)) AS Fidelizacao,
       ART_Pessoa.Documento,
       ART_Pessoa.Email,
       ART_Pessoa.DataNascimento,
       ART_Pessoa.EmPotencial,
       ART_Pessoa.VisualizarFidelizacao,
       ART_Pessoa.Negativado,
       ART_Pessoa.NomeUsual,
       ART_Pessoa.RGIE,
       ART_Pessoa.Sexo,
       ART_Pessoa.Tipo,
       ART_PessoaEndereco.Bairro AS 'endereco[Bairro]',
       ART_PessoaEndereco.Cep AS 'endereco[Cep]',
       ART_PessoaEndereco.Complemento AS 'endereco[Complemento]',
       ART_PessoaEndereco.Localidade_Id,
       ART_PessoaEndereco.Logradouro AS 'endereco[Logradouro]',
       ART_PessoaEndereco.Numero AS 'endereco[Numero]',
       ART_UF.Id AS UFId, 
       ART_UF.Nome AS UFNome,
       ART_Pais.Id AS PaisId,
       ART_Pais.Nome AS PaisNome,
       DATE_FORMAT(ART_Pessoa.created_at, '%d/%m/%Y') AS Cadastro,
       ART_Pessoa.Observacao
     FROM 
       ART_Pessoa 
     LEFT JOIN ART_Pessoa Responsavel ON ART_Pessoa.Pessoa_Id_Responsavel = Responsavel.Id
     LEFT JOIN ART_PessoaEndereco ON ART_PessoaEndereco.Pessoa_Id = ART_Pessoa.Id 
     LEFT JOIN ART_Localidade ON ART_Localidade.Id = ART_PessoaEndereco.Localidade_Id
     LEFT JOIN ART_UF ON ART_UF.Id = ART_Localidade.UF_Id
     LEFT JOIN ART_Pais ON ART_Pais.Id = ART_UF.Pais_Id
     WHERE ART_Pessoa.Id = $id"; 

$result = mysql_query(utf8_decode($query)); 

$row = mysql_fetch_assoc($result);

echo json_encode(array(
    "result" => array_map('utf8_encode', $row)
));

mysql_close();