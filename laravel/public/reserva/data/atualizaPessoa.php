<?php
require_once('config.db.php'); 
require_once("lib/connection.class.php"); 
//include '../chromephp/ChromePhp.php';

//ChromePhp::log($_POST);

// Abre a conexão com o banco de dados
$db = new dbConnection();
$db->openConnection();
$db->beginTransaction();
 
$data = json_decode(json_encode($_POST), FALSE);

//ChromePhp::log($data);

$query = sprintf("UPDATE ART_Pessoa 
         SET Nome       = '%s', 
             Documento  = '%s' , 
             RGIE       = '%s', 
             Sexo       = '%s', 
             Pessoa_Id  = %s, 
             Tipo       = '%s', 
             created_at = '%s', 
             updated_at = '%s'
         WHERE Id = %s",
    $data->Nome,
    $data->Documento,
    $data->RGIE,
    $data->Sexo,
    $data->Pessoa_Id, 
    $data->Tipo,
    date('Y-m-d'), date('Y-m-d'),
    $data->Id
);

//ChromePhp::log($query);

$db->executeQuery(utf8_decode($query));

$telefone = $data->telefone;

//ChromePhp::log($telefone);

$empty = true;
foreach ($telefone as $val) {
    if(!empty($val->Descricao) and !empty($val->Numero) ) {
        $empty = false;
        break;
    }
}

if(!$empty) {
    $query = "DELETE FROM ART_PessoaTelefone WHERE Pessoa_Id = ".$data->Id;
    //ChromePhp::log($query);
    $db->executeQuery($query);
    foreach ($telefone as $val) {
        if(!empty($val->Descricao) and !empty($val->Numero) ) {
            $query = sprintf("INSERT INTO ART_PessoaTelefone (Descricao, Telefone, Pessoa_Id, created_at, updated_at ) ".
                             "values ('%s', '%s', %s, '%s', '%s')",
                $val->Descricao,
                $val->Numero,
                $data->Id,
                date('Y-m-d'), date('Y-m-d'));
//ChromePhp::log($query);
            $db->executeQuery(utf8_decode($query));
        }
    }
}

echo json_encode(array(
    "result"=> 
        array( "error" => "",
               "retorno" => array(
                    "Id"         => $data->Id,
                    "Nome"       => $data->Nome,
                    "Documento"  => $data->Documento,
                    "RGIE"       => $data->RGIE,
                    "Sexo"       => $data->Sexo,
                    "TipoPessoa" => $data->Tipo,
                    "Pessoa_Id"  => $data->Pessoa_Id
        ))
));

$db->commitTransaction();
$db->endTransaction();
$db->closeConnection();
