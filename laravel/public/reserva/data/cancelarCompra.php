<?php
/*
 * Desmarca uma poltrona
 */

require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

$_POST["vendaId"] = isset($_POST["vendaId"]) ? $_POST["vendaId"] : 6429;

$vendaId = $_POST["vendaId"];

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = "SELECT ART_VendaItem.Passagem_Id FROM ART_VendaItem WHERE ART_VendaItem.Venda_Id = $vendaId";
$result = mysql_query($query);
while($row = mysql_fetch_assoc($result)) {
    mysql_query("UPDATE ART_Passagem SET Poltrona='', Situacao='Cancelada', updated_at=NOW() WHERE Id=".$row['Passagem_Id']);
}

$ret = mysql_query(
'SELECT 
   IFNULL(ART_Pessoa.Pessoa_Id_Responsavel, ART_Pessoa.Id) AS Pessoa_Id, 
   ART_Fidelizacao.ValorOperacao AS DescontoFidelizacao
 FROM
   ART_Venda
 JOIN ART_Fidelizacao ON ART_Fidelizacao.Venda_Id = ART_Venda.Id  
 JOIN ART_Pessoa ON ART_Pessoa.Id = ART_Venda.Pessoa_Id_Cliente
 WHERE
   ART_Venda.Id = '.$vendaId);

$row = mysql_fetch_array($ret);

if($row['DescontoFidelizacao'] > 0) {
    // Estorna o valor da fidelização
    $query = sprintf("INSERT INTO ART_Fidelizacao (Operacao, DataOperacao, Descricao, ".
                     "ValorOperacao, Pessoa_Id, Venda_Id, created_at, updated_at) ".
                     "values ('%s', '%s', '%s', %s, %s, %s, '%s', '%s')",
        utf8_decode('Crédito'),
        date('Y-m-d H:i:s'),
        'Estorno de compra - Sistema de retaguarda Artaban',
        $row['DescontoFidelizacao'],    
        $row['Pessoa_Id'],
        $vendaId,
        date('Y-m-d H:i:s'), date('Y-m-d H:i:s')    
    );
    mysql_query($query); 
}
            
echo json_encode(array(
    "result" => array( "success" => mysql_errno())
));

mysql_close();

