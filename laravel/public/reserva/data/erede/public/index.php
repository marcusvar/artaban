<?php

include '../../../chromephp/ChromePhp.php';
ChromePhp::log($_REQUEST);
//exit();

//$_REQUEST = '{"number":"5448280000000007","cv2":"132","mes":"10","ano":"2015","merchantreference":"PD00006654","amount":"210","id":"6195"}';

require_once('../../config.db.php'); 

use Rede\Erede\API;
use Slice\Http\Client;

require_once __DIR__.'/../vendor/autoload.php';

//$teste = new ART\Teste();  // teste de criação de uma classe usando php com composer

// Instancia o Slim Framework
$app = new \Slim\Slim();

// Instancia o PHPMailer 
$mail = new \PHPMailer();

// Recupera os parâmetros passados via REQUEST
$request = \Slim\Slim::getInstance()->request();

$compra = json_decode($request->getBody());

$compra = isset($compra) ? $compra : 
          json_decode('{"number":"5448280000000007","cv2":"132","mes":"10","ano":"2015","merchantreference":"PD00006659","amount":"210","id":"6195"}');

ChromePhp::log($compra);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);
// busca dados da empresa e do cliente comprador
$query = 
"
SELECT 
    ART_Pessoa.Pessoa_Id AS idEmpresa, 
    ART_PessoaEmpresa.Host, 
    ART_PessoaEmpresa.Porta,
    ART_PessoaEmpresa.Usuario,
    ART_PessoaEmpresa.Senha,
    ART_PessoaEmpresa.Email,
    ART_PessoaEmpresa.Descricao,
    ART_PessoaEmpresa.Logo,
    ART_PessoaEmpresa.Site,
    ART_Pessoa.Id,
    ART_Pessoa.Nome,
    ART_Pessoa.Documento,
    ART_Pessoa.Email AS EmailCliente,
    ART_PessoaEmpresa.Subdominio,
    ART_PessoaEmpresa.erede_rdcd_pv,
    ART_PessoaEmpresa.erede_password,
    ClienteVeritas.NomeUsual
FROM 
    ART_Pessoa 
LEFT JOIN ART_PessoaEmpresa ON ART_PessoaEmpresa.Pessoa_Id = ART_Pessoa.Pessoa_Id
LEFT JOIN ART_Pessoa AS ClienteVeritas ON ClienteVeritas.Id = ART_Pessoa.Pessoa_Id 
WHERE
    ART_Pessoa.Id = ".$compra->id;

$result = mysql_query($query); 
$row = mysql_fetch_assoc($result);

/*
$app->get('/', function() {
    echo 'Alô Mundo!';
});

$app->run();

*/
if($compra->amount > 0) {
    //$sessao = API::factory('production');
    $sessao = API::factory('sandbox');
    $auth = array(
        "password"     => $row['erede_password'],
        "acquirerCode" => array(
            "rdcd_pv"  => str_pad($row['erede_rdcd_pv'],9,"0",STR_PAD_LEFT)
        )
    );

    $sessao->setAuthentication(\Rede\Erede\Authentication\Authentication::factory($auth));
    // Cria um array com dados do cartão do cliente para validação junto a e-Rede
    $validaTransacao = 
        array( 
            "card_txn" => 
                array( 
                    // dados do cartão
                    "card" => 
                        array(
                            "number"            => $compra->number,
                            "expirydate"        => $compra->mes."/".substr($compra->ano,-2),
                            "card_account_type" => "credit",
                            "cv2avs" => array(
                                "cv2" => $compra->cv2
                            ),
                        ),
                    "method" => "auth"
            ),        
            "txn_details" => 
                array (
                    "merchantreference" => $compra->merchantreference,
                    "amount"            => $compra->amount,
                    "capturemethod"     => "ecomm",
                    "currency"          => "BRL"
                )
        );

    $sessao->setTransaction(Rede\Erede\Transaction\Transaction::factory($validaTransacao));

    $response = $sessao->send();
}
ChromePhp::log($response);

//$response->extended_response_message = 'Sucesso';

/*
if($response->extended_response_message === "Sucesso") {
    $sessao = API::factory('sandbox');
    $sessao->setAuthentication(\Rede\Erede\Authentication\Authentication::factory($auth));
    $consultaTransacao = 
        array( 
            "historic_txn" => 
                array (
                    "reference" => $response->gateway_reference,
                    "method"    => "query"
                )
        );
    $sessao->setTransaction(Rede\Erede\Transaction\Transaction::factory($consultaTransacao));

    $comprovante = $sessao->send();
}
 * 
 */

//ChromePhp::log();
$ret = 'Erro';
$msg = ($compra->amount === 0) ? "Sucesso" : 
       (is_null($response->extended_response_message) ? $response->reason : $response->extended_response_message) ;
//$msg = "Sucesso";
if($msg === "Sucesso") {
    $ret = 'Sucesso';
    $msg = "<div style=\'text-align: center;\'>Você receberá um e-mail em sua conta <strong>{$row['EmailCliente']}</strong><br />
            com o voucher contendo os dados de sua compra.<br />
            Caso você não consiga mais acessar sua conta de e-mail<br />
            entre em contato com o nosso suporte.<br /><br />
            <font color='#FF0000'><strong>Atenção:</strong></font> Lembre-se de autorizar o email <strong>admin@artaban.com.br</strong><br />
            em seu filtro anti-spam para que não tenha problemas em receber nossos emails.</div>";
    $NomeCliente  = utf8_encode($row['Nome']);
    $EmailCliente = $row['EmailCliente'];
    $valor = number_format($compra->amount, 2, ',', '.' );
    $nomeUsual = strtoupper(preg_replace("/&([a-z])[a-z]+;/i", "$1", htmlentities(utf8_encode($row['NomeUsual']))));
    $dataHora = gmdate("d/m/Y H:i:s", $response->time);
    $cartao = substr_replace($compra->number, '******', 6, -4);
    $viagemId = (int) substr($compra->merchantreference, 2);
    // Cria cupom comprovante da cpmpra
    $cupom = 
"REDE<br /><br />
HIPERCARD<br /><br />
COMPROVANTE: $response->auth_host_reference VALOR: $valor<br />
ESTAB: 044754779 $nomeUsual<br />
$dataHora<br />
CARTAO: $cartao<br />
AUTORIZACAO: {$response->CardTxn[authcode]}<br /><br />
RECONHECO E PAGAREI A DIVIDA<br />
AQUI REPRESENTADA<br /><br />
    ";

if($compra->amount > 0) {
    // Salva as isformações de retorno da transação Rede
    $query = "
    INSERT INTO ART_TransacaoRede 
        (Venda_Id, estado, reason, gateway_reference, time_transaction, mode_transaction, extended_response_message, 
         auth_host_reference, card_account_type, CardTxn_authcode, CardTxn_card_scheme, CardTxn_country, CardTxn_issuer, acquirer, 
         merchantreference, mid, cupom, ip  ) 
    VALUES ($viagemId, 
            $response->status,
            '$response->reason',
            '$response->gateway_reference', 
            $response->time,
            '$response->mode',
            '$response->extended_response_message',
            '$response->auth_host_reference',
            '{$response->Card[card_account_type]}',
            '{$response->CardTxn[authcode]}',
            '{$response->CardTxn[card_scheme]}',
            '{$response->CardTxn[country]}',
            '{$response->CardTxn[issuer]}',
            '$response->acquirer',
            '$response->merchantreference',
            $response->mid,
            '$cupom',
            '".getenv("REMOTE_ADDR")."');";
    $result = mysql_query($query);
    
}

//ChromePhp::log($query);
    
    //Busca dados dos passageiros
$query = 
    "
    SELECT 
        CONCAT(ART_PontoReferencia.Descricao, ' - ', ART_Viagem.Descricao)  AS Destino,
        DATE_FORMAT(ART_ViagemItem.DataHoraEmbarque, '%d/%m/%Y %H:%i \h\s') AS DataHoraEmbarque,
        CONCAT(UPPER(ART_Localidade.Nome), ' - ', Origem.Descricao) AS Origem,
        REPLACE(REPLACE(REPLACE(FORMAT((ART_Passagem.Valor - IFNULL(ART_Fidelizacao.ValorOperacao,0)), 2), '.', '@'), ',', '.'), '@', ',' ) AS Valor,
        ART_Pessoa.Nome AS Passageiro,
        IF(ART_Pessoa.Documento='', ART_Pessoa.RGIE, ART_Pessoa.Documento) Documento,
        ART_Passagem.Poltrona,
        REPLACE(REPLACE(REPLACE(FORMAT((IFNULL(ART_AdicionalTarifa.Valor,0)), 2), '.', '@'), ',', '.'), '@', ',' ) AS Escolta
    FROM 
        ART_VendaItem
    JOIN ART_Passagem                  ON ART_Passagem.Id = ART_VendaItem.Passagem_Id
    JOIN ART_ViagemItem                ON ART_ViagemItem.Id = ART_Passagem.ViagemItem_Id_Origem
    JOIN ART_HorarioItem               ON ART_HorarioItem.Id = ART_ViagemItem.HorarioItem_Id
    JOIN ART_LinhaItem                 ON ART_LinhaItem.ID = ART_HorarioItem.LinhaItem_Id
    JOIN ART_PontoReferencia AS Origem ON Origem.Id = ART_LinhaItem.PontoReferencia_Id 
    JOIN ART_Localidade                ON ART_Localidade.Id = Origem.Localidade_Id
    JOIN ART_Viagem                    ON ART_Viagem.Id = ART_Passagem.Viagem_Id
    JOIN ART_Horario                   ON ART_Horario.Id = ART_Viagem.Horario_Id
    JOIN ART_Linha                     ON ART_Linha.Id = ART_Horario.Linha_Id
    JOIN ART_PontoReferencia           ON ART_PontoReferencia.Id = ART_Linha.PontoReferencia_Id_Destino
    LEFT JOIN ART_Fidelizacao          ON ART_Fidelizacao.Venda_Id = ART_VendaItem.Venda_Id 
    JOIN ART_Pessoa                    ON ART_Pessoa.Id = ART_Passagem.Pessoa_Id
    LEFT JOIN ART_AdicionalTarifa      ON ART_Linha.Id = ART_AdicionalTarifa.Linha_Id
    WHERE
        ART_VendaItem.Venda_Id = ".$viagemId;

    $result = mysql_query($query); 

    // Envia e-mail para o cliente

    $mail->IsSMTP(); // Define que a mensagem será SMTP
    $mail->IsHTML(true);
    $mail->CharSet = 'UTF-8';
    $mail->Host = $row['Host']; // Endereço do servidor SMTP
    $mail->Port = $row['Porta']; // Porta
    $mail->SMTPAuth = true; // Usa autenticação SMTP? (opcional)
    $mail->SMTPSecure = 'ssl'; // SSL REQUERIDO pelo GMail
    $mail->Username = $row['Usuario']; // Usuário do servidor SMTP
    $mail->Password = $row['Senha']; // Senha do servidor SMTP
    $mail->From = $row['Email']; // Seu e-mail
    $mail->FromName = utf8_encode($row['Descricao']); // Seu nome
    $mail->WordWrap = 50; // Definição de quebra de linha
    $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
    $mail->Subject  = "Voucher de aquisição de viagem"; // Assunto da mensagem

    // Monta o voucher com os comprovantes da compra e informações da viagem
    $mail->Body = '
    <font face="Tahoma, Geneva, sans-serif">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="50px">&nbsp;</td>
      </tr>
      <tr>
        <td align="center"><table width="800" cellspacing="0" cellpadding="0" style="border:solid 1px #999; padding-top: 15px">
            <tr>
              <td><p><img src="'.$row['Logo'].'" width="300" height="42" alt="'.utf8_encode($row['NomeUsual']).'" style="padding-left: 50px;"/></p>
                <p>&nbsp;</p></td>
            </tr>
            <tr>
              <td style="padding: 0 50px 0 50px;"><p>Prezado(a) '.$NomeCliente.'</p>
                <p>Sua compra foi <font face="Tahoma, Geneva, sans-serif">realizada</font> com sucesso, segue abaixo as informa&ccedil;&otilde;es detalhadas: </p></td>
            </tr>
            <tr>
              <td style="padding: 20px 20px 0 50px;"><table width="100%" border="0" cellspacing="2" cellpadding="1">
                  <tr>
                    <td width="51%" style="border: solid 1px #CCC; padding: 20px; vertical-align: top;"><h4 style="margin: 0 0 8px 0;">Compra</h4>
                      <p style="font-size: 16px; font-weight: bold; border-bottom: solid 2px #CCC;
                                        border-top: solid 2px #CCC; color: green; text-align: center;
                                        margin-bottom: 8px; padding: 4px;">C&oacute;digo do Voucher: '.$compra->merchantreference.'</p>
                      <p style="margin: 5px 0 0 0; font-size: 13px;"><b>Cliente:</b> '.$NomeCliente.'</p>
                      <p style="margin: 5px 0 0 0; font-size: 13px;"><b>Documento:</b> '.$row['Documento'].'</p>
                    </td>
                    <td width="2%" style="margin: 0 15px 0 0">&nbsp;</td>
                    <td width="47%" style="border: solid 1px #CCC; padding: 20px; vertical-align: top;"><h4 style="margin: 0 0 8px 0;">Cupom</h4>
                      <p style="font-family: Courier New, Arial; font-size: 14px; text-align: center;"> 
                      '.($compra->amount === 0 ? "" : $cupom).'
                      </p></td>
                  </tr>
                </table></td>
            </tr>
            <tr>
              <td style="padding: 0px 50px 40px 50px;"><br />
                <p><strong>Viagem para:</strong>'.$passageiros['Destino'].'</p>
                <h4 style="margin-top: 30px;">Dados da(s) passagem(ns)</h4>
                <table cellspacing="0" cellpadding="0" style="font-size: 12px; text-align: left; width: 99%; margin-top:15px; padding: 0;">
                  <tr>
                    <th style="text-align: left; border-top: solid 1px #CCC; border-bottom: solid 1px #CCC; padding: 4px;">Partida</th>
                    <th style="text-align: left; border-top: solid 1px #CCC; border-bottom: solid 1px #CCC; padding: 4px;">Origem</th>
                    <th style="text-align: left; border-top: solid 1px #CCC; border-bottom: solid 1px #CCC; padding: 4px;">Valor</th>
                    <th style="text-align: left; border-top: solid 1px #CCC; border-bottom: solid 1px #CCC; padding: 4px;">Passageiro</th>
                    <th style="text-align: left; border-top: solid 1px #CCC; border-bottom: solid 1px #CCC; padding: 4px;">Documento</th>
                    <th style="text-align: left; border-top: solid 1px #CCC; border-bottom: solid 1px #CCC; padding: 4px;">Poltrona</th>
                  </tr>';
    
    while ($passageiros = array_map('utf8_encode', mysql_fetch_assoc($result))) {
        $mail->Body .= '
                  <tr>
                    <td style="padding: 4px; border-bottom: solid 1px #CCC;">'.$passageiros['DataHoraEmbarque'].'</td>
                    <td style="padding: 4px; border-bottom: solid 1px #CCC;">'.$passageiros['Origem'].'</td> 
                    <td style="padding: 4px; border-bottom: solid 1px #CCC;">'.$passageiros['Valor'].'</td>
                    <td style="padding: 4px; border-bottom: solid 1px #CCC;">'.$passageiros['Passageiro'].'</td>
                    <td style="padding: 4px; border-bottom: solid 1px #CCC;">'.$passageiros['Documento'].'</td>
                    <td style="padding: 4px; border-bottom: solid 1px #CCC; text-align: center">'.$passageiros['Poltrona'].'</td>
                  </tr>';
        $escolta = $passageiros['Escolta'];
    }
    $mail->Body .= '
                </table>
                <br />
                <span style="color: red;"><strong>ATENÇÃO</strong></span><br />
                <br>
                <strong>No momento do embarque será cobrado um adicional de R$ '.$escolta.' referente a ESCOLTA</strong><br />
                <br />
                Valores referente a passagem de ida e volta na data selecionada.
                <br />
                <br />
                Agradecemos a sua confiança e preferência por escolher os nossos serviços e esperamos, sempre, contar com você como nosso cliente.
                <br />
                <br />
                <p>Atenciosamente,</p>
                <br />
                <br />
                <p> '.utf8_encode($row['NomeUsual']).'<br />
                  <a href="'.$row['Site'].'" onmouseover="this.style.textDecoration=\'underline\';this.style.color=\'black\'" onmouseout="this.style.textDecoration=\'none\';">'.$row['Site'].'</a> </p></td>
            </tr>
          </table></td>
      </tr>
    </table>
    </font>
    ';

    $mail->AddAddress($EmailCliente, $NomeCliente);
    $enviado = $mail->Send();
    $mail->ClearAllRecipients();
    $mail->ClearAttachments();
}

mysql_close();

echo json_encode(array(
    "result" => array( 
        "ret"   => $ret,
        "msg"   => $msg
    )
));

