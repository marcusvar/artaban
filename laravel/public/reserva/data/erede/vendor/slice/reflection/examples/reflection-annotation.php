<?php

/**
 * Slice Framework (http://sliceframework.com)
 * Copyright (C) 2013 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * Slice Framework - Example File
 * 
 * Using ReflectionAnnotation API
 */

use Slice\Reflection\ReflectionAnnotation;

# Example class and functions
require_once 'FooBar.php';

# Taking class Annotations
$reflectionClass = new ReflectionClass('Foo');
$reflector = $reflectionClass;

$annotation = new ReflectionAnnotation($reflector);
print_r($annotation->getAnnotations());

# Taking method Annotations
$reflector = $reflectionClass->getMethod('bar');

$annotation = new ReflectionAnnotation($reflector);
print_r($annotation->getAnnotations());

# Taking property Annotations
$reflector = $reflectionClass->getProperty('bar');

$annotation = new ReflectionAnnotation($reflector);
print_r($annotation);

# Taking object Annotation
$foo = new Foo();
$reflector = new ReflectionObject($foo);

$annotation = new ReflectionAnnotation($reflector);
print_r($annotation);

# Taking function annotation
$reflector = new ReflectionFunction('fooBar');

$annotation = new ReflectionAnnotation($reflector);
print_r($annotation->getAnnotations());