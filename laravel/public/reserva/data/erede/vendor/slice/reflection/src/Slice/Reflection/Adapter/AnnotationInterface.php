<?php

/**
 * Slice Framework (http://sliceframework.com)
 * Copyright (C) 2013 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Reflection\Adapter;

/**
 * Slice Framework
 *
 * A extension of native Reflection API.
 *
 * A Reflection Annotation Adapter Interface for propose the polymorphism between any adapter.
 *
 * @package Slice
 * @subpackage Reflection
 * @namespace \Slice\Reflection\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) devSDMF Software Development Inc.
 *
 */

interface AnnotationInterface
{
	/**
	 * The Constructor
	 * 
	 * @param \Reflector $reflector
	 */
	public function __construct(&$reflector = null);
	
	/**
	 * Set the Reflection object in a adapter
	 * 
	 * @param \Reflector $reflector
	 */
	public function setReflector(\Reflector &$reflector);
	
	/**
	 * Get docblock from object
	 * 
	 * @return string
	 */
	public function getComments();
}