<?php

/**
 * Slice Framework (http://sliceframework.com)
 * Copyright (C) 2013 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Reflection;

/**
 * Slice Framework
 * 
 * A extension of native Reflection API.
 * 
 * This is a ReflectionAnnotation class for get the annotations of a class, methods,
 * functions, propertys and any object that have annotations in a docblock.
 * 
 * @package Slice
 * @subpackage Reflection
 * @namespace \Slice\Reflection
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) devSDMF Software Development Inc.
 * 
 */

class ReflectionAnnotation extends \Reflection implements \Reflector
{
	/**
	 * A instance of an a Reflection object
	 * @var \Reflector
	 */
	protected $reflector = null;
	
	/**
	 * A instance of a Annotation adapter
	 * @var \Slice\Reflection\Adapter\AnnotationInterface
	 */
	protected $adapter = null;
	
	/**
	 * A storage of annotations taked from adapter
	 * @var array
	 */
	protected $annotations = array();
	
	/**
	 * The Constructor
	 * 
	 * @param \Reflector $reflector
	 * @param string $adapter
	 * @throws \ReflectionException
	 * @return \Slice\Reflection\ReflectionAnnotation
	 */
	public function __construct(\Reflector &$reflector, $adapter = null)
	{
		$type = $this->getReflectorType($reflector);
		
		if (is_string($type)) {
			$this->reflector = $reflector;
			
			if ($adapter) {
				$type = $adapter;
			}
			$adapter = 'Slice\Reflection\Adapter\\' . ucfirst($type) . 'Annotation';
			$this->adapter = new $adapter($reflector);
			
			$this->loadAnnotations();
		} else {
			throw new \ReflectionException('Invalid Reflector setted, expected a instance of an Reflection object, got ' . gettype($reflector));
		}
		
		return $this;
	}
	
	/**
	 * Function to verify and validate the Reflector type for use to call a correct adapter.
	 * 
	 * @param \Reflector $reflector
	 * @return Ambigous <NULL, string>
	 */
	private function getReflectorType(&$reflector = null)
	{
		$type = null;
		
		if (is_null($reflector)) {
			$reflector = $this->reflector;
		}
		
		if ($reflector instanceof \ReflectionClass) {
			$type = 'Class';
		} elseif ($reflector instanceof \ReflectionFunction) {
			$type = 'FunctionAbstract';
		} elseif ($reflector instanceof \ReflectionMethod) {
			$type = 'FunctionAbstract';
		} elseif ($reflector instanceof \ReflectionObject) {
			$type = 'Class';
		} elseif ($reflector instanceof \ReflectionProperty) {
			$type = 'Property';
		}
		
		return $type;
	}
	
	/**
	 * Load all annotations of a Reflector object in a AnnotationReflection class
	 * 
	 * @return void
	 */
	private function loadAnnotations()
	{
		// load all annotations in a ReflectionAnnotation::annotations array() using the adapter
		$list = array();
		
		preg_match_all('#@(.*?)\n#s', $this->adapter->getComments(), $annotations);
		$annotations = $annotations[0];
		
		foreach ($annotations as $annotation) {
			$annotation = trim($annotation);
			$annotation = explode(' ', $annotation);
			
			$name = str_replace('@','',$annotation[0]);
			unset($annotation[0]);
			$value = implode(' ', $annotation);
			
			$list[$name] = $value;
		}
		
		$this->annotations = $list;
	}
	
	/**
	 * Get all annotations of Reflector
	 * 
	 * @return array
	 */
	public function getAnnotations()
	{
		return $this->annotations;
	}
	
	/**
	 * Get a specific annotation of Reflector
	 * 
	 * @param string $name
	 * @return string
	 */
	public function getAnnotation($name)
	{
		return $this->annotations[$name];
	}
	
	/**
	 * Implementation required by Reflector interface
	 * 
	 * @see Reflector::export()
	 */
	public static function export() {}
	
	/**
	 * Implementation required by Reflector interface
	 * 
	 * @see Reflector::__toString()
	 */
	public function __toString() {}
}