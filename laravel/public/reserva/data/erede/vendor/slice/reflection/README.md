Slice\Reflection
==========

The Slice Reflection API is a extension of native Reflection API of PHP that provides some new functions for help the management of Reflection Objects.

Example
-------

Get annotations of a Class:

```php
use Slice\Reflection\ReflectionAnnotation;

# Example class
require_once 'FooBar.php';

# Taking class Annotations
$reflectionClass = new ReflectionClass('Foo');
$reflector = $reflectionClass;

$annotation = new ReflectionAnnotation($reflector);
print_r($annotation->getAnnotations());
```

Output:

```
Array
(
    [example] => 
    [category] => API
    [package] => Slice
    [subpackage] => Reflection
    [author] => Lucas Mendes de Freitas <devsdmf>
    [copyright] => Slice Framework (c) devSDMF Software Development Inc.
)
```