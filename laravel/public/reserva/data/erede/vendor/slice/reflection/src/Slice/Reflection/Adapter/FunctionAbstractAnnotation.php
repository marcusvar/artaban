<?php

/**
 * Slice Framework (http://sliceframework.com)
 * Copyright (C) 2013 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Reflection\Adapter;

use Slice\Reflection\Adapter\AnnotationInterface;

/**
 * Slice Framework
 *
 * A extension of native Reflection API.
 *
 * This is a FunctionAbstractAnnotation Adapter for take docblock from ReflectionFunction and ReflectionMethod.
 *
 * @package Slice
 * @subpackage Reflection
 * @namespace \Slice\Reflection\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) devSDMF Software Development Inc.
 *
 */

class FunctionAbstractAnnotation implements AnnotationInterface
{
	/**
	 * The Reflector
	 * 
	 * @var \ReflectionFunctionAbstract
	 */
	protected $reflector = null;
	
	/**
	 * The Constructor
	 *
	 * @param \Reflector $reflector
	 * @return \Slice\Reflection\Adapter\ClassAnnotation
	 */
	public function __construct(&$reflector = null)
	{
		if (!is_null($reflector)) {
			$this->setReflector($reflector);
		}
		
		return $this;
	}
	
	/**
	 * Set the Reflection object in a adapter
	 *
	 * @param \Reflector $reflector
	 * @return \Slice\Reflection\Adapter\FunctionAbstractAnnotation
	 * @see \Slice\Reflection\Adapter\AnnotationInterface::setReflector()
	 */
	public function setReflector(\Reflector &$reflector)
	{
		if ($reflector instanceof \ReflectionFunctionAbstract) {
			$this->reflector = $reflector;
		} else {
			throw new \ReflectionException('Invalid instance of reflector, expected instance of ReflectionProperty, got ' . gettype($reflector));
		}
		
		return $this;
	}
	
	/**
	 * Get docblock from object
	 *
	 * @return string
	 * @see \Slice\Reflection\Adapter\AnnotationInterface::getComments()
	 */
	public function getComments()
	{
		return $this->reflector->getDocComment();
	}
}