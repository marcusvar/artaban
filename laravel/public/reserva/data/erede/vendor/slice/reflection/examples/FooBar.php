<?php

/**
 * Slice Framework (http://sliceframework.com)
 * Copyright (C) 2013 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * Example Class for Slice Reflection examples. 
 * 
 * This is a test class for use in examples of Slice Reflection API, this is a demo description
 * for no leave this documentation empty. 
 * 
 * @example
 * @category API
 * @package Slice
 * @subpackage Reflection
 * @author Lucas Mendes de Freitas <devsdmf>
 * @copyright Slice Framework (c) devSDMF Software Development Inc.
 *
 */
class Foo 
{
	/**
	 * Bar var
	 * 
	 * @access protected
	 * @var mixed
	 */
	protected $bar;
	
	/**
	 * The Constructor
	 * 
	 * @access public
	 * @method
	 * @return null
	 */
	public function __construct() {}
	
	/**
	 * Function bar
	 * 
	 * @access public
	 * @method
	 * @return null
	 */
	public function bar() {}
}

/**
 * This is a test function for use in examples of Slice Reflection API.
 * 
 * @example
 * @return null
 */
function fooBar() {}