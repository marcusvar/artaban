<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml;

use Slice\Xml\Serializer;
use Slice\Xml\Adapter\AdapterArray;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * Unit Test class for test Serializer class.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class SerializerTest extends \PHPUnit_Framework_TestCase
{
	
	/**
	 * Method for perform a test to create a instance of Serializer without arguments in constructor
	 * 
	 * @return \Slice\Xml\Serializer
	 */
	public function testInstanceCreationWithoutArguments()
	{
		$serializer = new Serializer();
		$this->assertInstanceOf('Slice\Xml\Serializer', $serializer);
		
		return $serializer;
	}
	
	/**
	 * Method for perform a test to create a instance of Serializer setting a adapter instance in constructor
	 */
	public function testInstanceCreationWithAdapter()
	{
		$adapter = new AdapterArray();
		$this->assertInstanceOf('Slice\Xml\Serializer', new Serializer($adapter));
	}
	
	/**
	 * Method for perform a test to create a instance of Serializer setting an array of options in constructor
	 */
	public function testInstanceCreationWithOptions()
	{
		$options = array(
			Serializer::SERIALIZE_OPTION_ROOT_NODE=>'UnitTest'
		);
		$this->assertInstanceOf('Slice\Xml\Serializer', new Serializer(null,$options));
	}
	
	/**
	 * Method for perform a test to create a instance of Serializer setting all arguments in constructor
	 */
	public function testInstanceCreationWithAllArguments()
	{
		$adapter = new AdapterArray();
		$options = array(
			Serializer::SERIALIZE_OPTION_ROOT_NODE=>'UnitTest'
		);
		$this->assertInstanceOf('Slice\Xml\Serializer', new Serializer($adapter,$options));
	}
	
	/**
	 * Method for perform a test of setAdapter method with a valid adapter instance.
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Serializer $serializer
	 */
	public function testSettingInstanceOfAdapter($serializer)
	{
		$adapter = new AdapterArray();
		$this->assertTrue($serializer->setAdapter($adapter));
	}
	
	/**
	 * Method for perform a test of setAdapter method with a valid adapter name.
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Serializer $serializer
	 */
	public function testSettingAdapterByString($serializer)
	{
		$this->assertTrue($serializer->setAdapter('array'));
	}
	
	/**
	 * Method for perform a test of setAdapter method with a invalid adapter instance.
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 101
	 * @param Serializer $serializer
	 */
	public function testSettingInvalidInstanceOfAdapter($serializer)
	{
		$invalidAdapter = new \stdClass();
		$serializer->setAdapter($invalidAdapter);
	}
	
	/**
	 * Method for perform a test of setAdapter method with a invalid adapter name.
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 100
	 * @param Serializer $serializer
	 */
	public function testSettingInvalidAdapterByString($serializer)
	{
		$serializer->setAdapter('invalid');
	}
	
	/**
	 * Method for perform a test of setOption with an valid option using name and value arguments
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Serializer $serializer
	 */
	public function testSettingOneOption($serializer)
	{
		$this->assertNull($serializer->setOption(Serializer::SERIALIZE_OPTION_ROOT_NODE,'UnitTest'));
	}
	
	/**
	 * Method for perform a test of setOption with a valid array of options
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Serializer $serializer
	 */
	public function testSettingArrayOptions($serializer)
	{
		$options = array(
			Serializer::SERIALIZE_OPTION_ROOT_NODE=>'UnitTest',
			Serializer::SERIALIZE_OPTION_XML_DECLARATION=>true
		);
		$this->assertNull($serializer->setOption($options));
	}
	
	/**
	 * Method for perform a test of setOption with an invalid option
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @expectedException Slice\Xml\Exception\SerializeException 
	 * @expectedExceptionCode 100
	 * @param Serializer $serializer
	 */
	public function testSettingInvalidOption($serializer)
	{
		$serializer->setOption('invalid','option');
	}
	
	/**
	 * Method for perform a test of serialize process with a valid parameters
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Serializer $serializer
	 */
	public function testSerialize($serializer)
	{
		$data = array(
			'node1'=> array(
				'child1' => array(
					'child1Node1'=>'value1',
					'child1Node2'=>'value2',
				),
				'child2' => array(
					'child2Node1' => 'value3',
					'child2Node2' => 'value4',
				),
				'child3' => array(
					'child3Node1' => 'value5',
					'child3Node2' => 'value6',
				)
			),
			'node2' => array(
				'child4' => array(
					'child4Node1' => 'value7',
					'child4Node2' => 'value8',
				),
				'child5' => array(
					'child5Node1' => 'value9',
					'child5Node2' => 'value10',
				),
				'child6' => array(
					'child6Node1' => 'value11',
					'child6Node2' => 'value12',
				),
			),
			'replaceTag'=>'valueRepalce',
		);
		
		$adapter = new AdapterArray();
		
		$serializer->setAdapter($adapter);
		
		$serializer->setOption(Serializer::SERIALIZE_OPTION_INDENT,"  ");
		$serializer->setOption(Serializer::SERIALIZE_OPTION_EOL,PHP_EOL);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ROOT_NODE,'rootNode');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ROOT_NOTE_ATTRIBUTES,array('attrRoot'=>'valueRoot'));
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_DECLARATION,true);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_ENCODING,'UTF-8');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_VERSION,'1.1');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY,true);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE,'originalKey');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_TAG_MAP,array('replaceTag'=>'replacedTag'));
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION,function($var){
			return $var .= '';
		});
		$serializer->setOption(Serializer::SERIALIZE_OPTION_CONVERT_BOOLEAN_TO_STRING,true);
		
		$this->assertTrue($serializer->serialize($data));
	}
	
	/**
	 * Method for perform a test to set a invalid original key attribute option in adapter
	 *
	 * @expectedException Slice\Xml\Exception\SerializeException
	 * @expectedExceptionCode 101
	 */
	public function testSerializeWithInvalidSetOfOriginalKeyAttribute()
	{
		# Initializing new instance of Serializer
		$serializer = new Serializer();
		
		$data = array('test'=>'value');
		
		$adapter = new AdapterArray();
		
		$serializer->setAdapter($adapter);
		
		$serializer->setOption(Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY,true);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE,null);
		$serializer->serialize($data);
	}
	
	/**
	 * Method for perform a test to set a invalid encode function
	 * 
	 * @expectedException Slice\Xml\Exception\SerializeException
	 * @expectedExceptionCode 102
	 */
	public function testSerializeWithInvalidEncodeFunction()
	{
		# Initializing new isntance of Serializer
		$serializer = new Serializer();
		
		$data = array('test'=>'value');
		
		$adapter = new AdapterArray();
		
		$serializer->setAdapter($adapter);
		
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION,'test');
		$serializer->serialize($data);
	}
	
	/**
	 * Method for perform a test to get a instance of adapter using an array
	 */
	public function testGetAdapterFromAnArray()
	{
		$adapter = Serializer::getAdapter(array('test'=>'value'));
		$this->assertInstanceOf('Slice\Xml\Adapter\AdapterArray', $adapter);
	}
	
	/**
	 * Method for perform a test to get a instance of adapter using an json string
	 */
	public function testGetAdapterFromAnJson()
	{
		$adapter = Serializer::getAdapter(json_encode(array('test'=>'value')));
		$this->assertInstanceOf('Slice\Xml\Adapter\AdapterJson', $adapter);
	}
	
	/**
	 * Method for test a exception triggered when a data that has not a specific adapter
	 * was set in getAdapter method
	 * 
	 * @expectedException Slice\Xml\Exception\SerializeException
	 * @expectedExceptionCode 104
	 */
	public function testGetAdapterWithInvalidType()
	{
		$adapter = Serializer::getAdapter(new \stdClass());
	}
}