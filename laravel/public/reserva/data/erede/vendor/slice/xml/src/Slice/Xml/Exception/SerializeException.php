<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Exception;

use Slice\Xml\Exception\Exception;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * A Exception class for handle and manage errors occoured in runtime of Serializer class.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Exception
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class SerializeException extends Exception
{
	
	/**
	 * Constants with error code of an specific message
	 * @var integer
	 */
	const INVALID_OPTION 			     = 100;
	const INVALID_ORIGINAL_KEY_ATTRIBUTE = 101;
	const INVALID_ENCODE_FUNCTION		 = 102;
	const ADAPTER_NOT_SET				 = 103;
	const ADAPTER_NOT_MATCH				 = 104;
	
	/**
	 * Array with predefined messages
	 * @var array
	 */
	protected $_messages = array(
		self::INVALID_OPTION 				 => 'The option {name} is not valid, use class constants for the correct use.',
		self::INVALID_ORIGINAL_KEY_ATTRIBUTE => 'The original key attribute setted is not valid.',
		self::INVALID_ENCODE_FUNCTION		 => 'The encode function setted is not callable.',
		self::ADAPTER_NOT_SET				 => 'You must be set an adapter before perform this action.',
		self::ADAPTER_NOT_MATCH				 => 'No adapter found to {type} type of data.',
	);
}