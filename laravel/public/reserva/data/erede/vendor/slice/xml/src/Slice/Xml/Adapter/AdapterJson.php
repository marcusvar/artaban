<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Adapter;

use Slice\Xml\Exception\AdapterException;
use Slice\Xml\Exception\SerializeException;
use Slice\Xml\Adapter\AdapterInterface;
use Slice\Xml\Adapter\AdapterArray;
use Slice\Xml\Serializer;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * This is a adapter to serialize and unserialize json strings.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class AdapterJson extends AdapterArray implements AdapterInterface
{
	
	/**
	 * Set option array in adapter
	 * 
	 * @param array $options The options to be set
	 * @throws AdapterException
	 * @see \Slice\Xml\Adapter\AdapterArray::setOptions()
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
	}
	
	/**
	 * Set data to serialize/unserialize
	 * 
	 * @param string  $data	   The data to be serialized or unserialized
	 * @param boolean $reverse Control var for validate if is serialize or unserialize use
	 * @throws AdapterException
	 * @see \Slice\Xml\Adapter\AdapterInterface::setData()
	 */
	public function setData($data, $reverse = false)
	{
		if (is_string($data)) {
			$this->_data = $data;
			$this->_reverse = $reverse;			
		} else {
			throw new AdapterException(array('adapterName'=>'AdapterJson','type'=>'string'),AdapterException::ADAPTER_INVALID_DATA);
		}
	}
	
	/**
	 * Validate adapter state (Not implemented yet.)
	 * 
	 * @see \Slice\Xml\Adapter\AdapterInterface::validate()
	 */
	public function validate()
	{
		# Validate Unserialize data
		if ($this->_reverse) {
			# Getting data
			$data = $this->_data;
			
			if (simplexml_load_string($data)) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 * Serialize data
	 * 
	 * @return string
	 * @see \Slice\Xml\Adapter\AdapterInterface::serialize()
	 */
	public function serialize()
	{
		# Verifying if reverse is true
		if ($this->_reverse) {
			throw new AdapterException(null,AdapterException::ADAPTER_REVERSE_SET);
		}
		
		$dataInJson = $this->_data;
		$this->_data = json_decode($this->_data,true);
		
		$xml = parent::serialize();
		
		$this->_data = $dataInJson;
		
		return $xml;
	}
	
	/**
	 * Unserialize a XML string into a json format
	 * 
	 * @return string
	 * @see \Slice\Xml\Adapter\AdapterInterface::unserialize()
	 */
	public function unserialize()
	{
		# Verifying if reverse is true
		if (!$this->_reverse) {
			throw new AdapterException(null,AdapterException::ADAPTER_REVERSE_UNSET);
		}
		
		# Validate XML
		if (!$this->validate()) {
			throw new AdapterException(array('adapterName'=>'AdapterJson','type'=>'xml string'),AdapterException::ADAPTER_INVALID_DATA);
		}
		
		# Initializing XML parser component
		$simplexml = simplexml_load_string($this->_data);
		
		# Parse XML
		$data = parent::unserialize();
		
		# Converting array to json
		$data = json_encode($data);
		
		return $data;
	}
}