<?php

/**
 * Slice Framework (http://sliceframework.com)
 * Copyright (C) 2013 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * Slice Framework - Example File
 *
 * Unserializing into a array
 */

# Importing library
use Slice\Xml\Unserializer;
use Slice\Xml\Adapter\AdapterArray;

# Example xml content
$data = '<?xml version="1.0" encoding="UTF-8"?><rootNode attrRoot="valueRoot"><node1 originalKey="node1"><child1 originalKey="child1"><child1Node1 originalKey="child1Node1">value1</child1Node1><child1Node2 originalKey="child1Node2">value2</child1Node2></child1><child2 originalKey="child2"><child2Node1 originalKey="child2Node1">value3</child2Node1><child2Node2 originalKey="child2Node2">value4</child2Node2></child2><child3 originalKey="child3"><child3Node1 originalKey="child3Node1">value5</child3Node1><child3Node2 originalKey="child3Node2">value6</child3Node2></child3></node1><node2 originalKey="node2"><child4 originalKey="child4"><child4Node1 originalKey="child4Node1">value7</child4Node1><child4Node2 originalKey="child4Node2">value8</child4Node2></child4><child5 originalKey="child5"><child5Node1 originalKey="child5Node1">value9</child5Node1><child5Node2 originalKey="child5Node2">value10</child5Node2></child5><child6 originalKey="child6"><child6Node1 originalKey="child6Node1">value11</child6Node1><child6Node2 originalKey="child6Node2">value12</child6Node2></child6></node2><replaceTag originalKey="replaceTag">valueRepalce</replaceTag></rootNode>';

# Initializing adapter
$adapter = new AdapterArray();

# Initializing Unserializer
$unserializer = new Unserializer($adapter);

# Unserialize!
if ($unserializer->unserialize($data)) {
	# Printing result
	print_r($unserializer->getUnserializedData());
} else {
	echo "An error ocourred!";
}