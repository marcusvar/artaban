<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml;

use Slice\Xml\Exception\Exception;
use Slice\Xml\Exception\AdapterException;
use Slice\Xml\Exception\SerializeException;
use Slice\Xml\Adapter\AdapterInterface;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * Serializer is a class for parse and generate XML contents based in adapters available
 * of Xml package of Slice Framework.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class Serializer
{
	
	/**
	 * Serializer options constants
	 */
	const SERIALIZE_OPTION_INDENT 					 = 'indent';
	const SERIALIZE_OPTION_EOL 						 = 'eol';
	const SERIALIZE_OPTION_ROOT_NODE 				 = 'rootNode';
	const SERIALIZE_OPTION_ROOT_NOTE_ATTRIBUTES 	 = 'rootNodeAttributes';
	const SERIALIZE_OPTION_XML_DECLARATION 			 = 'xmlDeclaration';
	const SERIALIZE_OPTION_XML_VERSION				 = 'xmlVersion';
	const SERIALIZE_OPTION_XML_ENCODING 			 = 'xmlEncoding';
	const SERIALIZE_OPTION_STORE_ORIGINAL_KEY 		 = 'originalKey';
	const SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE 	 = 'originalKeyAttribute';
	const SERIALIZE_OPTION_TAG_MAP 					 = 'tagMap';
	const SERIALIZE_OPTION_ENCODE_FUNCTION 			 = 'encodeFunction';
	const SERIALIZE_OPTION_CONVERT_BOOLEAN_TO_STRING = 'convertBoolean';
	
	/**
	 * The know options of Serializer class
	 * @var array
	 */
	private $_knowOptions = array(
		self::SERIALIZE_OPTION_INDENT,
		self::SERIALIZE_OPTION_EOL,
		self::SERIALIZE_OPTION_ROOT_NODE,
		self::SERIALIZE_OPTION_ROOT_NOTE_ATTRIBUTES,
		self::SERIALIZE_OPTION_XML_DECLARATION,
		self::SERIALIZE_OPTION_XML_VERSION,
		self::SERIALIZE_OPTION_XML_ENCODING,
		self::SERIALIZE_OPTION_STORE_ORIGINAL_KEY,
		self::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE,
		self::SERIALIZE_OPTION_TAG_MAP,
		self::SERIALIZE_OPTION_ENCODE_FUNCTION,
		self::SERIALIZE_OPTION_CONVERT_BOOLEAN_TO_STRING
	);
	
	/**
	 * The default value for options
	 * @var array
	 */
	private $_defaultOptions = array(
		self::SERIALIZE_OPTION_INDENT 					 => "",
		self::SERIALIZE_OPTION_EOL 						 => "\n",
		self::SERIALIZE_OPTION_ROOT_NODE 				 => null,
		self::SERIALIZE_OPTION_ROOT_NOTE_ATTRIBUTES 	 => array(),
		self::SERIALIZE_OPTION_XML_DECLARATION 			 => false,
		self::SERIALIZE_OPTION_XML_VERSION				 => '1.0',
		self::SERIALIZE_OPTION_XML_ENCODING 			 => false,
		self::SERIALIZE_OPTION_STORE_ORIGINAL_KEY 		 => false,
		self::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE 	 => null,
		self::SERIALIZE_OPTION_TAG_MAP 					 => array(),
		self::SERIALIZE_OPTION_ENCODE_FUNCTION 			 => null,
		self::SERIALIZE_OPTION_CONVERT_BOOLEAN_TO_STRING => false,
	);
	
	/**
	 * The configurable options for serialize
	 * @var array
	 */
	private $_options = array();
	
	/**
	 * The instance of adapter used in serialize
	 * @var AdapterInterface
	 */
	protected $adapter = null;
	
	/**
	 * The data to serialize
	 * @var mixed
	 */
	protected $data = null;
	
	/**
	 * The result of serialize function
	 * @var string
	 */
	protected $serializedData = null;
	
	/**
	 * The Constructor
	 * 
	 * @param multitype:AdapterInterface|string $adapter A instance or name of adapter to be used in serializer class
	 * @param array								$options An array with options to configure serializer
	 * @return Serializer
	 */
	public function __construct($adapter = null, $options = null)
	{
		# Verifying if adapter was set.
		if (!is_null($adapter)) {
			$this->setAdapter($adapter);
		}
		
		# Setting default options in options array
		$this->_options = $this->_defaultOptions;
		
		if (is_array($options)) {
			$this->setOption($options);
		}
		
		return $this;
	}
	
	/**
	 * Set the adapter in Serializer
	 * 
	 * @param multitype:AdapterInterface|string $adapter The name or instance of an valid adapter
	 * @throws AdapterException
	 * @return boolean
	 */
	public function setAdapter($adapter)
	{
		if ($adapter instanceof AdapterInterface) {
			# Instance of an adapter
			$this->adapter = $adapter;
			return true;
		} elseif (is_string($adapter)) {
			# Adapter name for validate and trying to initialize
			try {
				$adapterWithNamespace = "Slice\Xml\Adapter\Adapter" . ucfirst($adapter);
				if (class_exists($adapterWithNamespace)) {
					$this->adapter = new $adapterWithNamespace();
					return true;
				} else {
					throw new Exception();
				}
			} catch (Exception $e)
			{
				throw new AdapterException(null,AdapterException::ADAPTER_NOT_FOUND);
				return false;
			}
		} else {
			throw new AdapterException(null,AdapterException::ADAPTER_INVALID);
			return false;
		}
	}
	
	/**
	 * Configure options to serialize
	 * 
	 * @param multitype:string|array $name  The name of option (use class constants for reference)
	 * @param string 				 $value The value of option
	 * @throws SerializeException
	 * @return null
	 */
	public function setOption($name, $value = null)
	{
		if (is_array($name)) {
			foreach ($name as $key => $value) {
				$this->setOption($key,$value);
			}
		} else {
			if (in_array($name, $this->_knowOptions)) {
				$this->_options[$name] = $value;
			} else {
				throw new SerializeException(array('name'=>$name),SerializeException::INVALID_OPTION);
			}
		}
	}
	
	/**
	 * Get value of an option
	 * 
	 * @param string $name The name of option to get the value
	 * @return multitype:string|array|boolean|null
	 */
	public function getOption($name = null)
	{
		if (is_null($name)) {
			return $this->_options;
		} else {
			if (isset($this->_options[$name])) {
				return $this->_options[$name];
			} else {
				return null;
			}
		}
	}
	
	/**
	 * Alias for getOption method
	 * 
	 * @see getOption()
	 * @return array
	 */
	public function getOptions()
	{
		return $this->getOption(null);
	}
	
	/**
	 * Serialize the data
	 * 
	 * @param mixed $data The data to be serialized
	 * @throws SerializeException
	 * @return boolean
	 */
	public function serialize($data)
	{
		# Getting data
		$this->data = $data;
		
		# Validating and getting adapter
		if (is_null($this->adapter) || !($this->adapter instanceof AdapterInterface)) {
			throw new SerializeException(null,SerializeException::ADAPTER_NOT_SET);
		} else {
			$adapter = &$this->adapter;
		}
		
		# Setting data and options in adapter
		$adapter->setData($data,false);
		$adapter->setOptions($this->_options);
		
		# Performing serialization
		$result = $adapter->serialize();
		
		# Verifying and returning the result
		if (is_string($result)) {
			$this->serializedData = $result;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Get the serialized XML
	 * 
	 * @return string
	 */
	public function getSerializedData()
	{
		return $this->serializedData;
	}
	
	/**
	 * Reset Serializer to initial state
	 */
	public function reset()
	{
		$this->_options		  = $this->_defaultOptions;
		$this->adapter		  = null;
		$this->data 		  = null;
		$this->serializedData = null;
	}
	
	/**
	 * A static method for perform all process in serialization
	 * 
	 * @param mixed 							$data	 The data to be serialized
	 * @param multitype:AdapterInterface|string $adapter The instance or name of adapter, if not specified the function will try to get a instance of adapter
	 * @param array 							$options Array with options to serialize
	 * @return string|boolean
	 */
	public static function direct($data, $adapter = null, $options = null)
	{
		# Verifying if adapter was specified
		if (is_null($adapter)) {
			$adapter = self::getAdapter($data);
		}
		
		# Getting a instance of Serializer
		$serializer = new self($adapter,$options);
		
		if ($serializer->serialize($data)) {
			return $serializer->getSerializedData();
		} else {
			return false;
		}
	}
	
	/**
	 * Get a instance of a adapter based in type of data
	 * 
	 * @param mixed $data The data to determine the specific adapter
	 * @throws SerializeException
	 * @return multitype:AdapterInterface|null
	 */
	public static function getAdapter($data)
	{
		# Verifying know types of adapters
		if (is_array($data)) {
			# If data is an array
			$adapter = new Adapter\AdapterArray();
		} elseif (is_string($data) && (count(json_decode($data)) > 0) && !json_last_error()) {
			# If data is a string and converting to array using json_encode has one or more elements 
			# and none error occoured (assuming this is a Json content)
			$adapter = new Adapter\AdapterJson();
		} else {
			$type = gettype($data);
			$adapterWithNamespace = "Slice\Xml\Adapter\Adapter" . ucfirst($type);
			if (class_exists($adapterWithNamespace)) {
				$adapter = new $adapterWithNamespace();
			} else {
				$adapter = null;
				throw new SerializeException(array('type'=>$type),SerializeException::ADAPTER_NOT_MATCH);
			}
		}
		
		return $adapter;
	}
}