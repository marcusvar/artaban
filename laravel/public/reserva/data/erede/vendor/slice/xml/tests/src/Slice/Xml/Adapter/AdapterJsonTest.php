<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Adapter;

use Slice\Xml\Adapter\AdapterJson;
use Slice\Xml\Serializer;
use Slice\Xml\Exception\SerializeException;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * Unit Test class for test AdapterJson class.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class AdapterJsonTest extends \PHPUnit_Framework_TestCase
{
	
	/**
	 * Method for perform a test to create a instance of AdapterJson
	 * 
	 * @return \Slice\Xml\Adapter\AdapterJson
	 */
	public function testInstanceCreation()
	{
		$adapter = new AdapterJson();
		$this->assertInstanceOf('Slice\Xml\Adapter\AdapterJson', $adapter);
		
		return $adapter;
	}
	
	/**
	 * Method for perform a test to set an array of options in adapter
	 * 
	 * @depends testInstanceCreation
	 * @param AdapterJson $adapter
	 */
	public function testSetOptionsArray($adapter)
	{
		$this->assertNull($adapter->setOptions(array()));
	}
	
	/**
	 * Method for perform a test to set a invalid set of options in adapter
	 * 
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 103
	 * @param AdapterJson $adapter
	 */
	public function testInvalidSetOptionsArray($adapter)
	{
		$adapter->setOptions(null);
	}
	
	/**
	 * Method for perform a test to set a valid data content in adapter
	 * 
	 * @depends testInstanceCreation
	 * @param AdapterJson $adapter
	 */
	public function testSetData($adapter)
	{
		$data = json_encode(array('test1'=>'value1','test2'=>'value2'));
		
		$this->assertNull($adapter->setData($data,false));
	}
	
	/**
	 * Method for perform a test to set a invalid data content in adapter
	 * 
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 104
	 * @param AdapterJson $adapter
	 */
	public function testInvalidSetData($adapter)
	{
		$adapter->setData(null,false);
	}
	
	/**
	 * Method to perform a test of a valid data content serialize
	 * 
	 * @depends testInstanceCreation
	 * @param AdapterJson $adapter
	 * @return string
	 */
	public function testSerialize($adapter)
	{
		# Initializing Serializer for test with all options
		$serializer = new Serializer();
		$serializer->setOption(Serializer::SERIALIZE_OPTION_INDENT,"  ");
		$serializer->setOption(Serializer::SERIALIZE_OPTION_EOL,PHP_EOL);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ROOT_NODE,'rootNode');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ROOT_NOTE_ATTRIBUTES,array('attrRoot'=>'valueRoot'));
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_DECLARATION,true);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_ENCODING,'UTF-8');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_VERSION,'1.0');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY,true);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE,'originalKey');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_TAG_MAP,array('replaceTag'=>'replacedTag'));
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION,function($var){
			return utf8_encode($var);
		});
		$serializer->setOption(Serializer::SERIALIZE_OPTION_CONVERT_BOOLEAN_TO_STRING,true);
		
		$data = array(
			'rootNode' => array(
				'node1'=> array(
					'child1' => array(
						'child1Node1'=>'value1',
						'child1Node2'=>'value2',
					),
					'child2' => array(
						'child2Node1' => 'value3',
						'child2Node2' => 'value4',
					),
					'child3' => array(
						'child3Node1' => 'value5',
						'child3Node2' => 'value6',
					)
				),
				'node2' => array(
					'child4' => array(
						'child4Node1' => 'value7',
						'child4Node2' => 'value8',
					),
					'child5' => array(
						'child5Node1' => 'value9',
						'child5Node2' => 'value10',
					),
					'child6' => array(
						'child6Node1' => 'value11',
						'child6Node2' => 'value12',
					),
				),
			),
		);
		
		$data = json_encode($data);
		
		$adapter->setOptions($serializer->getOptions());
		$adapter->setData($data,false);
		$result = $adapter->serialize();
		
		return $result;
	}
	
	/**
	 * Method for perform a test to set a invalid original key attribute option in adapter
	 * 
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\SerializeException
	 * @expectedExceptionCode 101
	 * @param AdapterJson $adapter
	 */
	public function testSerializeWithInvalidSetOfOriginalKeyAttribute($adapter)
	{
		# Initializing Serializer for test with all options
		$serializer = new Serializer();
		$serializer->setOption(Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY,true);
		
		$data = json_encode(array('test'=>'value'));
		
		$adapter->setOptions($serializer->getOptions());
		$adapter->setData($data, false);
		$adapter->serialize();
	}
	
	/**
	 * Method for perform a test to set a invalid encode function
	 *
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\SerializeException
	 * @expectedExceptionCode 102
	 * @param AdapterJson $adapter
	 */
	public function testSerializeWithInvalidEncodeFunction($adapter)
	{
		# Initializing Serializer for test with all options
		$serializer = new Serializer();
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION,'test');
		
		$data = json_encode(array('test'=>'value'));
		
		$adapter->setOptions($serializer->getOptions());
		$adapter->setData($data, false);
		$adapter->serialize();
	}
	
	/**
	 * Method for perform a test to set a valid data content to unserialize in adapter
	 *
	 * @depends testSerialize
	 * @param string $xml
	 */
	public function testSetDataUnserialize($xml)
	{
		$adapter = new AdapterJson();
		$this->assertNull($adapter->setData($xml,true));
	}
	
	/**
	 * Method for perform a test to set a invalid data content to unserialize in adapter
	 *
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 104
	 */
	public function testInvalidSetDataUnserialize()
	{
		$adapter = new AdapterJson();
		$adapter->setData(null,true);
	}
	
	/**
	 * Method for perform a test to unserialize a xml content
	 *
	 * @depends testSerialize
	 * @param string $xml
	 */
	public function testUnserialize($xml)
	{
		$adapter = new AdapterJson();
		$adapter->setData($xml,true);
		$this->assertJson($adapter->unserialize());
	}
}