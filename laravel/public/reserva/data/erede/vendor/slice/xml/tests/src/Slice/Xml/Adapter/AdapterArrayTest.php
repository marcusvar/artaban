<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Adapter;

use Slice\Xml\Adapter\AdapterArray;
use Slice\Xml\Serializer;
use Slice\Xml\Exception\SerializeException;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * Unit Test class for test AdapterArray class.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class AdapterArrayTest extends \PHPUnit_Framework_TestCase
{
	
	/**
	 * Method for perform a test to create a instance of AdapterArray
	 * 
	 * @return \Slice\Xml\Adapter\AdapterArray
	 */
	public function testInstanceCreation()
	{
		$adapter = new AdapterArray();
		$this->assertInstanceOf('Slice\Xml\Adapter\AdapterArray', $adapter);
		
		return $adapter;
	}
	
	/**
	 * Method for perform a test to set an array of options in adapter
	 * 
	 * @depends testInstanceCreation
	 * @param ArrayAdapter $adapter
	 */
	public function testSetOptionsArray($adapter)
	{
		$this->assertNull($adapter->setOptions(array()));
	}
	
	/**
	 * Method for perform a test to set a invalid set of options in adapter
	 * 
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 103
	 * @param ArrayAdapter $adapter
	 */
	public function testInvalidSetOptionsArray($adapter)
	{
		$adapter->setOptions(null);
	}
	
	/**
	 * Method for perform a test to set a valid data content to serialize in adapter
	 * 
	 * @depends testInstanceCreation
	 * @param AdapterArray $adapter
	 */
	public function testSetDataSerialize($adapter)
	{
		$data = array(
			'test1'=>'value1',
			'test2'=>'value2'
		);
		
		$this->assertNull($adapter->setData($data,false));
	}
	
	/**
	 * Method for perform a test to set a invalid data to serialize in adapter
	 * 
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 104
	 * @param AdapterArray $adapter
	 */
	public function testInvalidSetDataSerialize($adapter)
	{
		$adapter->setData(null,false);
	}
	
	/**
	 * Method to perform a test of a valid data content serialize
	 * 
	 * @depends testInstanceCreation
	 * @param Serializer $adapter
	 * @return string
	 */
	public function testSerialize($adapter)
	{
		# Initializing Serializer for test with all options
		$serializer = new Serializer();
		$serializer->setOption(Serializer::SERIALIZE_OPTION_INDENT,"  ");
		$serializer->setOption(Serializer::SERIALIZE_OPTION_EOL,PHP_EOL);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ROOT_NODE,'rootNode');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ROOT_NOTE_ATTRIBUTES,array('attrRoot'=>'valueRoot'));
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_DECLARATION,true);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_ENCODING,'UTF-8');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_XML_VERSION,'1.0');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY,true);
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE,'originalKey');
		$serializer->setOption(Serializer::SERIALIZE_OPTION_TAG_MAP,array('replaceTag'=>'replacedTag'));
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION,function($var){
			return utf8_encode($var);
		});
		$serializer->setOption(Serializer::SERIALIZE_OPTION_CONVERT_BOOLEAN_TO_STRING,true);
		
		$data = array(
			'rootNode' => array(
				'node1'=> array(
					'child1' => array(
						'child1Node1'=>'value1',
						'child1Node2'=>'value2',
					),
					'child2' => array(
						'child2Node1' => 'value3',
						'child2Node2' => 'value4',
					),
					'child3' => array(
						'child3Node1' => 'value5',
						'child3Node2' => 'value6',
					)
				),
				'node2' => array(
					'child4' => array(
						'child4Node1' => 'value7',
						'child4Node2' => 'value8',
					),
					'child5' => array(
						'child5Node1' => 'value9',
						'child5Node2' => 'value10',
					),
					'child6' => array(
						'child6Node1' => 'value11',
						'child6Node2' => 'value12',
					),
				),
			),
		);
		
		$adapter->setOptions($serializer->getOptions());
		$adapter->setData($data,false);
		$result = $adapter->serialize();
		
		return $result;
	}
	
	/**
	 * Method for perform a test to set a invalid original key attribute option in adapter
	 * 
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\SerializeException
	 * @expectedExceptionCode 101
	 * @param AdapterArray $adapter
	 */
	public function testSerializeWithInvalidSetOfOriginalKeyAttribute($adapter)
	{
		# Initializing Serializer for test with all options
		$serializer = new Serializer();
		$serializer->setOption(Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY,true);
		
		$data = array('test'=>'value');
		
		$adapter->setOptions($serializer->getOptions());
		$adapter->setData($data, false);
		$adapter->serialize();
	}
	
	/**
	 * Method for perform a test to set a invalid encode function
	 *
	 * @depends testInstanceCreation
	 * @expectedException Slice\Xml\Exception\SerializeException
	 * @expectedExceptionCode 102
	 * @param AdapterArray $adapter
	 */
	public function testSerializeWithInvalidEncodeFunction($adapter)
	{
		# Initializing Serializer for test with all options
		$serializer = new Serializer();
		$serializer->setOption(Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION,'test');
		
		$data = array('test'=>'value');
		
		$adapter->setOptions($serializer->getOptions());
		$adapter->setData($data, false);
		$adapter->serialize();
	}
	
	/**
	 * Method for perform a test to set a valid data content to unserialize in adapter
	 * 
	 * @depends testSerialize
	 * @param string $xml
	 */
	public function testSetDataUnserialize($xml)
	{
		$adapter = new AdapterArray();
		$this->assertNull($adapter->setData($xml,true));
	}
	
	/**
	 * Method for perform a test to set a invalid data content to unserialize in adapter
	 * 
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 104
	 */
	public function testInvalidSetDataUnserialize()
	{
		$adapter = new AdapterArray();
		$adapter->setData(null,true);
	}
	
	/**
	 * Method for perform a test to unserialize a xml content
	 * 
	 * @depends testSerialize
	 * @param string $xml
	 */
	public function testUnserialize($xml)
	{
		$adapter = new AdapterArray();
		$adapter->setData($xml,true);
		$this->assertInternalType('array',$adapter->unserialize());
	}
}