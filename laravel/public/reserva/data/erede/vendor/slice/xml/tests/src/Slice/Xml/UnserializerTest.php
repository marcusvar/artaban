<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml;

use Slice\Xml\Unserializer;
use Slice\Xml\Adapter\AdapterArray;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * Unit Test class for test Unserializer class.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class UnserializerTest extends \PHPUnit_Framework_TestCase
{
	
	/**
	 * Method for perform a test to create a instance of Unserializer without arguments in constructor
	 * 
	 * @return \Slice\Xml\Unserializer
	 */
	public function testInstanceCreationWithoutArguments()
	{
		$unserializer = new Unserializer();
		$this->assertInstanceOf('Slice\Xml\Unserializer', $unserializer);
		
		return $unserializer;
	}
	
	/**
	 * Method for perform a test to create a instance of Unserializer setting a adapter instance in constructor
	 */
	public function testInstanceCreationWithAdapter()
	{
		$adapter = new AdapterArray();
		$this->assertInstanceOf('Slice\Xml\Unserializer', new Unserializer($adapter));
	}
	
	/**
	 * Method for perform a test of setAdapter method with a valid adapter instance
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Unserializer $unserializer
	 */
	public function testSettingInstanceOfAdapter($unserializer)
	{
		$adapter = new AdapterArray();
		$this->assertTrue($unserializer->setAdapter($adapter));
	}
	
	/**
	 * Method for perform a test of setAdapter method with a valid adapter name
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Unserializer $unserializer
	 */
	public function testSettingAdapterByString($unserializer)
	{
		$this->assertTrue($unserializer->setAdapter('array'));
	}
	
	/**
	 * Method for perform a failure test of setAdapter method setting a invalid adapter instance
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 101
	 * @param Unserializer $unserializer
	 */
	public function testSettingInvalidInstanceOfAdapter($unserializer)
	{
		$unserializer->setAdapter(new \stdClass());
	}
	
	/**
	 * Method for perform a failure test of setAdapter method setting a invalid adapter name
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @expectedException Slice\Xml\Exception\AdapterException
	 * @expectedExceptionCode 100
	 * @param unknown $unserializer
	 */
	public function testSettingInvalidAdapterByString($unserializer)
	{
		$unserializer->setAdapter('invalid');
	}
	
	/**
	 * Method for perform a test of unserialize method 
	 * 
	 * @depends testInstanceCreationWithoutArguments
	 * @param Unserializer $unserializer
	 */
	public function testUnserialize($unserializer)
	{
		# XML content to be serialized
		$data = '<?xml version="1.0" encoding="UTF-8"?><rootNode attrRoot="valueRoot"><node1 originalKey="node1"><child1 originalKey="child1"><child1Node1 originalKey="child1Node1">value1</child1Node1><child1Node2 originalKey="child1Node2">value2</child1Node2></child1><child2 originalKey="child2"><child2Node1 originalKey="child2Node1">value3</child2Node1><child2Node2 originalKey="child2Node2">value4</child2Node2></child2><child3 originalKey="child3"><child3Node1 originalKey="child3Node1">value5</child3Node1><child3Node2 originalKey="child3Node2">value6</child3Node2></child3></node1><node2 originalKey="node2"><child4 originalKey="child4"><child4Node1 originalKey="child4Node1">value7</child4Node1><child4Node2 originalKey="child4Node2">value8</child4Node2></child4><child5 originalKey="child5"><child5Node1 originalKey="child5Node1">value9</child5Node1><child5Node2 originalKey="child5Node2">value10</child5Node2></child5><child6 originalKey="child6"><child6Node1 originalKey="child6Node1">value11</child6Node1><child6Node2 originalKey="child6Node2">value12</child6Node2></child6></node2><replaceTag originalKey="replaceTag">valueRepalce</replaceTag></rootNode>';
		
		$adapter = new AdapterArray();
		
		$unserializer->setAdapter($adapter);
		
		$result = $unserializer->unserialize($data);
		
		$this->assertTrue($result);
	}
}