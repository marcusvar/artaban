<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Adapter;

use Slice\Xml\Exception\AdapterException;
use Slice\Xml\Exception\SerializeException;
use Slice\Xml\Adapter\AdapterInterface;
use Slice\Xml\Adapter\AbstractAdapter;
use Slice\Xml\Serializer;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * This is a adapter to serialize and unserialize arrays.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class AdapterArray extends AbstractAdapter implements AdapterInterface
{
	
	/**
	 * Set option array in adapter
	 * 
	 * @param array $options The options to be set
	 * @throws AdapterException
	 * @see \Slice\Xml\Adapter\AbstractAdapter::setOptions()
	 */
	public function setOptions($options)
	{
		parent::setOptions($options);
	}
	
	/**
	 * Set data to serialize/unserialize
	 * 
	 * @param multitype:array|string $data	  The data to be serialized or unserialized
	 * @param boolean				 $reverse Control var for validate when is serialize or unserialize use
	 * @throws AdapterException
	 * @see \Slice\Xml\Adapter\AdapterInterface::setData()
	 */
	public function setData($data, $reverse = false)
	{
		if (is_array($data) && $reverse == false) {
			$this->_data = $data;
			$this->_reverse = $reverse;			
		} elseif (is_string($data) && $reverse == true) {
			$this->_data = $data;
			$this->_reverse = $reverse;
		} else {
			throw new AdapterException(array('adapterName'=>'AdapterArray','type'=>'array or string'),AdapterException::ADAPTER_INVALID_DATA);
		}
	}
	
	/**
	 * Validate adapter state (Not implemented yet.)
	 * 
	 * @see \Slice\Xml\Adapter\AdapterInterface::validate()
	 */
	public function validate(){}
	
	/**
	 * Serialize data
	 * 
	 * @return string
	 * @see \Slice\Xml\Adapter\AdapterInterface::serialize()
	 */
	public function serialize()
	{
		# Verifying if reverse is true
		if ($this->_reverse) {
			throw new AdapterException(null,AdapterException::ADAPTER_REVERSE_SET);
		}
		
		# Getting data and options
		$options = &$this->_options;
		$data = &$this->_data;
		$eol = $options[Serializer::SERIALIZE_OPTION_EOL];
		$startingIndent = 0;
		
		# Verifying and setting root node
		if (!is_null($rootNode = $options[Serializer::SERIALIZE_OPTION_ROOT_NODE])) {
			$templateRoot = '<' . $rootNode;
			foreach ($options[Serializer::SERIALIZE_OPTION_ROOT_NOTE_ATTRIBUTES] as $param => $value) {
				$templateRoot .= ' ' . $param . '="' . $value . '"';
			}
			$templateRoot .= '>' . $eol . '{xmlContentInRootNode}</' . $rootNode . '>';
			
			$startingIndent = 1;
		}
		
		# Verifying if is to store the original key and the original key attr was set
		if ($options[Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY] 
			&& is_null($options[Serializer::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE])) {
			throw new SerializeException(null,SerializeException::INVALID_ORIGINAL_KEY_ATTRIBUTE);
		}
		
		# Verifying if was specified a function to encode 
		if (!is_null($options[Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION]) 
			&& is_callable($options[Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION])) {
			$encode = true;
		} elseif (!is_null($options[Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION]) 
				  && !is_callable($options[Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION])) {
			throw new SerializeException(null,SerializeException::INVALID_ENCODE_FUNCTION);
		} else {
			$encode = false;
		}
		
		$xml = $this->parseSerialize($data,$options[Serializer::SERIALIZE_OPTION_STORE_ORIGINAL_KEY],$encode,$startingIndent);
		
		# Mergin root node if was set
		if (!is_null($rootNode)) {
			$xml = str_replace('{xmlContentInRootNode}', $xml, $templateRoot);
		}
		
		# Verifying if is to write XML declaration
		if ($options[Serializer::SERIALIZE_OPTION_XML_DECLARATION]) {
			$declaration = '<?xml';
			
			# Verifying if was setted XML version
			if (!is_null($options[Serializer::SERIALIZE_OPTION_XML_VERSION])) {
				$declaration .= ' version="' . $options[Serializer::SERIALIZE_OPTION_XML_VERSION] . '"';
			}
			
			# Verifying if was setted XML encoding
			if (!is_null($options[Serializer::SERIALIZE_OPTION_XML_ENCODING])) {
				$declaration .= ' encoding="' . $options[Serializer::SERIALIZE_OPTION_XML_ENCODING] . '"';
			}
			
			$declaration .= '?>' . $eol;
			$xml = $declaration . $xml;
		}
		
		return $xml;
	}
	
	/**
	 * Unserialize a XML string into a array format
	 * 
	 * @return array
	 * @see \Slice\Xml\Adapter\AdapterInterface::unserialize()
	 */
	public function unserialize()
	{
		# Verifying if reverse is true
		if (!$this->_reverse) {
			throw new AdapterException(null,AdapterException::ADAPTER_REVERSE_UNSET);
		}
		
		# Initializing XML parser component
		$simplexml = simplexml_load_string($this->_data);
		
		# Parse XML
		$data = $this->parseUnserialize($simplexml);
		
		return $data;
	}
	
	/**
	 * Parse array into a xml string
	 * 
	 * @param array   $data		   The data to be parsed
	 * @param boolean $originalKey Control var for store the original array key
	 * @param boolean $encode	   Control var for use encode function setted in options
	 * @param integer $indentSize  The size of identation to be generated
	 * @return string
	 */
	private function parseSerialize(array $data, $originalKey = false, $encode = false, $indentSize = 0)
	{
		# Initializing vars
		$str = '';
		$eol = $this->_options[Serializer::SERIALIZE_OPTION_EOL];
		$indent = '';
		
		for($i=0;$i<$indentSize;$i++)
			$indent .= $this->_options[Serializer::SERIALIZE_OPTION_INDENT];
		
		foreach ($data as $key => $value) {
			# Loop control var
			$loop = 0;
			
			# Verifying if value is an array and applying recursivity
			if (is_array($value)) {
				$childIndentSize = $indentSize + 1;
				$value = $this->parseSerialize($value,$originalKey,$encode,$childIndentSize);
				$loop = 1;
			}
			
			# Verifying if is to store the original key
			if ($originalKey) {
				$original = ' ' . $this->_options[Serializer::SERIALIZE_OPTION_ORIGINAL_KEY_ATTRIBUTE] . '="' .  $key . '"';
			} else {
				$original = '';
			}
			
			# Verifying if has a tag map for this key
			if (in_array($key,$this->_options[Serializer::SERIALIZE_OPTION_TAG_MAP])) {
				$key = $this->_options[Serializer::SERIALIZE_OPTION_TAG_MAP][$key];
			}
			
			# Verifying if is to convert boolean to string and convert value
			if ($this->_options[Serializer::SERIALIZE_OPTION_CONVERT_BOOLEAN_TO_STRING] && is_bool($value)) {
				$value = ($value) ? 'true' : 'false';
			}
			
			# Verifying if was specified a encode function
			if ($encode) {
				$key = $this->_options[Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION]($key);
				$original = $this->_options[Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION]($original);
				$value = $this->_options[Serializer::SERIALIZE_OPTION_ENCODE_FUNCTION]($value);
			}
			
			# A POG implementation for correct break line.
			if ($loop == 1) {
				$str .= $indent . '<' . $key . $original . '>' . $eol . $value . $indent . '</'.$key.'>' . $eol;
			} else {
				$str .= $indent . '<' . $key . $original . '>' . $value . '</'.$key.'>' . $eol;
			}
		}
		
		return $str;
	}
	
	/**
	 * Parse a SimpleXMLElement into a array
	 * 
	 * @param \SimpleXMLElement $xml
	 * @return array
	 */
	private function parseUnserialize(\SimpleXMLElement $xml)
	{
		# Initializing storage component
		$data = array();
		
		foreach ($xml as $key => $value) {
			# Verifying if value contains child elements
			if ($value->count() > 0) {
				# Recursive walk to get child elements
				$data[$key] = $this->parseUnserialize($value);
			} else {
				# Getting value from node and setting in array
				$data[$key] = $value->__toString();
			}
		}
		
		return $data;
	}
}