<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Exception;

use Slice\Xml\Exception\Exception;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * A Exception class for handle and manage errors occoured in runtime of Adapter instances.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Exception
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class AdapterException extends Exception
{
	
	/**
	 * Constants with error code of an specific message
	 * @var integer
	 */
	const ADAPTER_NOT_FOUND 	  = 100;
	const ADAPTER_INVALID		  = 101;
	const ADAPTER_NOT_IMPLEMENTED = 102;
	const ADAPTER_INVALID_OPTION  = 103;
	const ADAPTER_INVALID_DATA 	  = 104;
	const ADAPTER_REVERSE_SET	  = 105;
	const ADAPTER_REVERSE_UNSET	  = 106;
	
	/**
	 * Array with predefined messages
	 * @var array
	 */
	protected $_messages = array(
		self::ADAPTER_NOT_FOUND 	  => 'Adapter specified not found',
		self::ADAPTER_NOT_IMPLEMENTED => 'The adapter must be a implementation of an AdapterInterface',
		self::ADAPTER_INVALID		  => 'Invalid adapter specified, the adapter must be a implementation of an AdapterInterface or a valid adapter name',
		self::ADAPTER_INVALID_OPTION  => 'Invalid option array specified',
		self::ADAPTER_INVALID_DATA	  => 'Invalid data setted, the {adapterName} requires a {type} data type',
		self::ADAPTER_REVERSE_SET	  => 'Serialize method is not allowed when reverse parameter is true',
		self::ADAPTER_REVERSE_UNSET	  => 'Unserialize method is not allowed when reverse parameter is false',
	);
}