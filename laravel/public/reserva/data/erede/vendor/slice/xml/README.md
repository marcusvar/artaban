Slice\Xml
==========

Slice Xml is a package with set of tools and libraries for handle and manage XML contents and files, see example files for more information about use of this package and your features.

Features
--------
- Adapters
- Serializer
- Unserializer

Installation and Configuration
------------------------------

Slice\Xml package is available over Composer or you can clone this repository and switch to the version that you wish use.

Examples
--------

### Serialize an array
```php
<?php

# Importing library
use Slice\Xml\Serializer;
use Slice\Xml\Adapter\AdapterArray;

# Example array
$data = array(
	'node1'=> array(
		'child1' => array(
			'child1Node1'=>'value1',
			'child1Node2'=>'value2',
		),
		'child2' => array(
			'child2Node1' => 'value3',
			'child2Node2' => 'value4',
		),
		'child3' => array(
			'child3Node1' => 'value5',
			'child3Node2' => 'value6',
		)
	),
	'node2' => array(
		'child4' => array(
			'child4Node1' => 'value7',
			'child4Node2' => 'value8',
		),
		'child5' => array(
			'child5Node1' => 'value9',
			'child5Node2' => 'value10',
		),
		'child6' => array(
			'child6Node1' => 'value11',
			'child6Node2' => 'value12',
		),
	),
	'replaceTag'=>'valueReplace',
);

# Initializing adapter
$adapter = new AdapterArray();

# Initializing Serializer
$serializer = new Serializer($adapter);

# Array with serialize options
$options = array(
	Serializer::SERIALIZE_OPTION_EOL 			 => PHP_EOL,
	Serializer::SERIALIZE_OPTION_INDENT 		 => '  ',
	Serializer::SERIALIZE_OPTION_ROOT_NODE 		 => 'rootNode',
	Serializer::SERIALIZE_OPTION_XML_DECLARATION => true,
);

# Setting options in serializer
$serializer->setOption($options);

# Serialize!!
if ($serializer->serialize($data)) {
	# Printing result
	echo $serializer->getSerializedData();
} else {
	echo "An error ocourred in serialization!";
}
```

For more examples details see examples folder.

Unit Tests
----------

```
PHPUnit 4.0.11 by Sebastian Bergmann.

..............................................

Slice\Xml\Adapter\AdapterArray
 [x] Instance creation
 [x] Set options array
 [x] Invalid set options array
 [x] Set data serialize
 [x] Invalid set data serialize
 [x] Serialize
 [x] Serialize with invalid set of original key attribute
 [x] Serialize with invalid encode function
 [x] Set data unserialize
 [x] Invalid set data unserialize
 [x] Unserialize

Slice\Xml\Adapter\AdapterJson
 [x] Instance creation
 [x] Set options array
 [x] Invalid set options array
 [x] Set data
 [x] Invalid set data
 [x] Serialize
 [x] Serialize with invalid set of original key attribute
 [x] Serialize with invalid encode function
 [x] Set data unserialize
 [x] Invalid set data unserialize
 [x] Unserialize

Slice\Xml\Serializer
 [x] Instance creation without arguments
 [x] Instance creation with adapter
 [x] Instance creation with options
 [x] Instance creation with all arguments
 [x] Setting instance of adapter
 [x] Setting adapter by string
 [x] Setting invalid instance of adapter
 [x] Setting invalid adapter by string
 [x] Setting one option
 [x] Setting array options
 [x] Setting invalid option
 [x] Serialize
 [x] Serialize with invalid set of original key attribute
 [x] Serialize with invalid encode function
 [x] Get adapter from an array
 [x] Get adapter from an json
 [x] Get adapter with invalid type

Slice\Xml\Unserializer
 [x] Instance creation without arguments
 [x] Instance creation with adapter
 [x] Setting instance of adapter
 [x] Setting adapter by string
 [x] Setting invalid instance of adapter
 [x] Setting invalid adapter by string
 [x] Unserialize

Time: 1.17 seconds, Memory: 4.00Mb

OK (46 tests, 62 assertions)
```