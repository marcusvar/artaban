<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Adapter;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * The Adapter Interface provides a list of methods that must be implemented in all 
 * adapters that will be used in Serializer class.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

interface AdapterInterface
{
	
	/**
	 * Set option array in adapter
	 * 
	 * @param array $options The options to be set
	 */
	public function setOptions($options);
	
	/**
	 * Set data to serialize/unserialize
	 * 
	 * @param mixed   $data	   The data to be serialized or unserialized
	 * @param boolean $reverse Contro var for validate if is serialize or unserialize use
	 */
	public function setData($data, $reverse = false);
	
	/**
	 * Validate adapter state
	 */
	public function validate();
	
	/**
	 * Serialize data
	 * 
	 * @return string
	 */
	public function serialize();
	
	/**
	 * Unserialize data
	 */
	public function unserialize();
}