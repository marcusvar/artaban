<?php

/**
 * Slice Framework (http://sliceframework.com)
 * Copyright (C) 2013 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

/**
 * Slice Framework - Example File
 *
 * Serializing a array
 */

# Importing library
use Slice\Xml\Serializer;
use Slice\Xml\Adapter\AdapterArray;

# Example array
$data = array(
	'node1'=> array(
		'child1' => array(
			'child1Node1'=>'value1',
			'child1Node2'=>'value2',
		),
		'child2' => array(
			'child2Node1' => 'value3',
			'child2Node2' => 'value4',
		),
		'child3' => array(
			'child3Node1' => 'value5',
			'child3Node2' => 'value6',
		)
	),
	'node2' => array(
		'child4' => array(
			'child4Node1' => 'value7',
			'child4Node2' => 'value8',
		),
		'child5' => array(
			'child5Node1' => 'value9',
			'child5Node2' => 'value10',
		),
		'child6' => array(
			'child6Node1' => 'value11',
			'child6Node2' => 'value12',
		),
	),
	'replaceTag'=>'valueReplace',
);

# Initializing adapter
$adapter = new AdapterArray();

# Initializing Serializer
$serializer = new Serializer($adapter);

# Array with serialize options
$options = array(
	Serializer::SERIALIZE_OPTION_EOL 			 => PHP_EOL,
	Serializer::SERIALIZE_OPTION_INDENT 		 => '  ',
	Serializer::SERIALIZE_OPTION_ROOT_NODE 		 => 'rootNode',
	Serializer::SERIALIZE_OPTION_XML_DECLARATION => true,
);

# Setting options in serializer
$serializer->setOption($options);

# Serialize!!
if ($serializer->serialize($data)) {
	# Printing result
	echo $serializer->getSerializedData();
} else {
	echo "An error ocourred in serialization!";
}