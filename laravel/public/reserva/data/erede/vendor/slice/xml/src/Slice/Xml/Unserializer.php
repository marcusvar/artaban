<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml;

use Slice\Xml\Exception\Exception;
use Slice\Xml\Exception\AdapterException;
use Slice\Xml\Exception\UnserializeException;
use Slice\Xml\Adapter\AdapterInterface;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * Unserializer is a class for convert XML contents to other formats based in adapters available
 * of Xml package of Slice Framework.
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

class Unserializer
{
	
	/**
	 * The know options of Unserializer class
	 * @var array
	 */
	private $_knowOptions = array();
	
	/**
	 * The default value for options
	 * @var array
	 */
	private $_defaultOptions = array();
	
	/**
	 * The configurable options for unserialize
	 * @var array
	 */
	private $_options = array();
	
	/**
	 * The instance of adapter used in unserialize
	 * @var AdapterInterface
	 */
	protected $adapter = null;
	
	/**
	 * The data to unserialize
	 * @var mixed
	 */
	protected $data = null;
	
	/**
	 * The result of unserialize function
	 * @var mixed
	 */
	protected $unserializedData = null;
	
	/**
	 * The Constructor
	 * 
	 * @param multitype:AdapterInterface|string $adapter The name or instance of an adapter to be used in Unserializer 
	 * @param array 							$options An array with options to configure unserializer
	 * @return Unserializer
	 */
	public function __construct($adapter = null, $options = null)
	{
		# Verifying if adapter was set
		if (!is_null($adapter)) {
			$this->setAdapter($adapter);
		}
		
		# Setting default options in options array
		$this->_options = $this->_defaultOptions;
		
		# Verifying if options array was set
		if (is_array($options)) {
			$this->setOption($options);
		}
		
		return $this;
	}
	
	/**
	 * Set the adapter in Unserializer
	 * 
	 * @param multitype:AdapterInterface|string $adapter
	 * @throws AdapterException
	 * @return boolean
	 */
	public function setAdapter($adapter)
	{
		if ($adapter instanceof AdapterInterface) {
			# Instance of an adapter
			$this->adapter = $adapter;
			return true;
		} elseif (is_string($adapter)) {
			# Adapter name for validate and trying to initialize
			try {
				$adapterWithNamespace = "Slice\Xml\Adapter\Adapter" . ucfirst($adapter);
				if (class_exists($adapterWithNamespace)) {
					$this->adapter = new $adapterWithNamespace();
					return true;
				} else {
					throw new Exception();
				}
			} catch (Exception $e)
			{
				throw new AdapterException(null,AdapterException::ADAPTER_NOT_FOUND);
				return false;
			}
		} else {
			throw new AdapterException(null,AdapterException::ADAPTER_INVALID);
			return false;
		}
	}
	
	/**
	 * Configure options to unserialize
	 * 
	 * @param multitype:string|array $name  An array with multiple options or the name of option
	 * @param string 				 $value The value of option if $name is a string
	 * @throws UnserializeException
	 * @return null
	 */
	public function setOption($name, $value = null)
	{
		if (is_array($name)) {
			foreach ($name as $key => $value) {
				$this->setOption($key,$value);
			}
		} else {
			if (in_array($name, $this->_knowOptions)) {
				$this->_options[$name] = $value;
			} else {
				throw new UnserializeException(array('name'=>$name),UnserializeException::INVALID_OPTION);
			}
		}
	}
	
	/**
	 * Get value of an option
	 * 
	 * @param string $name
	 * @return multitype:string|array|boolean|null
	 */
	public function getOption($name = null)
	{
		if (is_null($name)) {
			return $this->_options;
		} else {
			if (isset($this->_options[$name])) {
				return $this->_options[$name];
			} else {
				return null;
			}
		}
	}
	
	/**
	 * Alias for getOption method
	 * 
	 * @see getOption()
	 * @return array
	 */
	public function getOptions()
	{
		return $this->getOption(null);
	}
	
	/**
	 * Unserialize content
	 * 
	 * @param string $data
	 * @return boolean
	 */
	public function unserialize($data)
	{
		# Getting data
		$this->data = $data;
		
		#Validating and getting adapter
		if (is_null($this->adapter) || !($this->adapter instanceof AdapterInterface)) {
			throw new UnserializeException(null,UnserializeException::ADAPTER_NOT_SET);
		} else {
			$adapter = &$this->adapter;
		}
		
		# Setting data and options in array
		$adapter->setData($data,true);
		$adapter->setOptions($this->_options);
		
		# Performing unserialization
		$result = $adapter->unserialize();
		
		# Verifying and returning result
		if (!is_null($result)) {
			$this->unserializedData = $result;
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Get the unserialized data
	 * 
	 * @return mixed
	 */
	public function getUnserializedData()
	{
		return $this->unserializedData;
	}
	
	/**
	 * Reset Unserializer to initial state
	 */
	public function reset()
	{
		$this->_options 		= $this->_defaultOptions;
		$this->adapter 			= null;
		$this->data 			= null;
		$this->unserializedData = null;
	}
}