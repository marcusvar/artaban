<?php

/**
 * Slice Framework (http://slice.devsdmf.net)
 * Copyright (c) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Slice\Xml\Adapter;

use Slice\Xml\Exception\AdapterException;

/**
 * Slice Framework
 *
 * A package with set of tools and libraries for handle and manage XML contents and files.
 *
 * This is a abstract class to extend in adapters, this class provides prepared variables and
 * "templates" of code of some functions that must be implemented in child classes. 
 *
 * @package Slice
 * @subpackage Xml
 * @namespace Slice\Xml\Adapter
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright Slice Framework (c) 2010~2014 devSDMF Software Development Inc.
 *
 */

abstract class AbstractAdapter
{
	
	/**
	 * The options to be used in adapter serialize
	 * @var array
	 */
	protected $_options = array();
	
	/**
	 * The data will be serialized
	 * @var mixed
	 */
	protected $_data = null;
	
	/**
	 * Control var for setData method to validate the type of content, diff from serialize and unserialize patterns
	 * @var boolean
	 */
	protected $_reverse = false;
	
	/**
	 * Set option array in adapter
	 * 
	 * @param array $options The options to be set
	 * @throws AdapterException
	 */
	public function setOptions($options)
	{
		if (is_array($options)) {
			$this->_options = $options;
		} else {
			throw new AdapterException(null,AdapterException::ADAPTER_INVALID_OPTION);
		}
	}
}