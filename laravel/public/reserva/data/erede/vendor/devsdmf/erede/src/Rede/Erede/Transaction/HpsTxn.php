<?php

/**
 * eRede Payment Gateway SDK for PHP Applications
 * Copyright (C) 2010~2014 devSDMF Software Development Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 *
 */

namespace Rede\Erede\Transaction;

use Rede\Erede\AbstractComponent;
use Rede\Erede\InterfaceComponent;
use Rede\Erede\Transaction\AdditionalInfo;

/**
 * eRede Payment Gateway SDK for PHP Applications
 *
 * This is a SDK (Software Development Kit) of eRede Payment Gateway
 * for integrate PHP applications providing payment services.
 *
 * This is a TxnDetails component of library. This is a component with
 * details about transaction.
 *
 * @package Rede
 * @subpackage Erede
 * @namespace \Rede\Erede\Transaction
 * @author Lucas Mendes de Freitas (devsdmf)
 * @copyright 2010~2014 (c) devSDMF Software Development Inc.
 *
 */

class HpsTxn extends AbstractComponent implements InterfaceComponent
{
	
	/**
	 * A ação que está sendo solicitada 
     * configuração de sessão da Interface Personalizada
	 * 
	 * @var string
	 * @persistent false
	 */
	protected $method = 'setup';
	
	/**
	 * A ação que está sendo solicitada
	 * Deve ter o valor "setup"
     * 
	 * @var string
	 */
	protected $page_set_id = null;
	
	/**
	 * Um número de ID que significa o "page set" que deve ser utilizado
     * ao apresentar a tela de captura de dados ao titular do cartão.
	 * 
	 * @var integer - O valor inteiro de um page set que já tenha sido configurado
	 * @persistent false
	 */
	protected $return_url = null;
	
	/**
	 * The method of capture (ecomm/cont_auth)
	 * 
	 * @var string
	 * @persistent false
	 */
	protected $expiry_url = null;
	
	/**
	 * The Constructor
	 */
	public function __construct(){}
	
	/**
	 * Parse component as XML
	 * @see \Rede\Erede\InterfaceComponent::asXML()
	 */
	public function asXML()
	{
		return $this->parseXML();
	}
	
	/**
	 * Factory component using a configurable array
	 * 
	 * @param array $data
	 * @return \Rede\Erede\Transaction\HpsTxn
	 */
	public static function factory(array $data)
	{
		$instance = new self();
		
		$instance->method      = (isset($data['method']))      ? $data['method'] : 'setup';
		$instance->page_set_id = (isset($data['page_set_id'])) ? $data['page_set_id'] : 1;
		$instance->return_url  = (isset($data['return_url']))  ? $data['return_url'] : null;
		$instance->expiry_url  = (isset($data['expiry_url']))  ? $data['expiry_url'] : null;
        
		return $instance;
	}
}