<?php
header('Content-type: application/pdf');

require_once('config.db.php'); 
include("mpdf60/mpdf.php");

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : 2;

$id = isset($_GET["Id"]) ? $_GET["Id"] : 1307;

$result = mysql_query("SELECT * FROM ART_Pessoa WHERE Id=$id");
$cliente = mysql_fetch_assoc($result);
$cliente = $cliente["Nome"];

$query = 
"SELECT
  IFNULL(Fidelizacao.DataOperacao, ART_Viagem.DataInicio) AS DataOperacao,
  DATE_FORMAT(IFNULL(Fidelizacao.DataOperacao, ART_Viagem.DataInicio),'%d/%m/%Y') AS DataCreditoDebito,
  Fidelizacao.Descricao, 
  Fidelizacao.Operacao,
  Fidelizacao.ValorOperacao,
  IF(Fidelizacao.Operacao = 'Crédito', ValorOperacao, 0) AS Credito,
  IF(Operacao = 'Débito', ValorOperacao, 0) AS Debito,
  IF(Operacao = 'Crédito', ValorOperacao/0.01, 0) AS ValorCompra
FROM
  ART_Fidelizacao AS Fidelizacao
LEFT JOIN ART_Pessoa    ON ART_Pessoa.Id          = Fidelizacao.Pessoa_Id
LEFT JOIN ART_Venda     ON ART_Venda.Id           = Fidelizacao.Venda_Id
LEFT JOIN ART_VendaItem ON ART_VendaItem.Venda_Id = ART_Venda.Id
LEFT JOIN ART_Passagem  ON ART_Passagem.Id        = ART_VendaItem.Passagem_Id
LEFT JOIN ART_Viagem    ON ART_Viagem.Id          = ART_Passagem.Viagem_Id
WHERE
  Fidelizacao.Pessoa_Id = $id OR ART_Pessoa.Pessoa_Id_Responsavel = $id
ORDER BY DataOperacao DESC;";

$result = mysql_query($query);

$html = '
<html>
<head>
</head>
<body>
<table>
    <caption>Cliente: '.utf8_encode($cliente).'</caption>
    <tr>
        <th class="header">Data Viagem/Crédito</th>
        <th class="header">Descrição</th>
        <th class="header" style="text-align: right">Valor da compra</th>
        <th class="header" style="text-align: right">Crédito</th>
        <th class="header" style="text-align: right">Débito</th>
    </tr>
';
$saldo = 0;
while($row = mysql_fetch_array($result)) {
    $oper = utf8_encode($row["Operacao"]);
    $html .= '
    <tr>
        <td>'.$row["DataCreditoDebito"].'</td>
        <td>'.utf8_encode($row["Descricao"]).'</td>
        <td style="text-align: right">'.number_format($oper==="Crédito"?($row["ValorOperacao"]/0.01):0, 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($oper==="Crédito"?$row["ValorOperacao"]:0, 2, ',', '.').'</td>
        <td style="text-align: right">'.number_format($oper==="Débito"?$row["ValorOperacao"]:0, 2, ',', '.').'</td>
    </tr>';
    $saldo += (float) ((utf8_encode($row["Operacao"])==="Crédito"?1:-1)*$row["ValorOperacao"]);        
}
$html .= '
    <tr>
        <td class="foter">Saldo</td>
        <td colspan="3" class="foter">&nbsp;</td>
        <td class="foter">'.number_format($saldo, 2, ',', '.').'</td>
    </tr>
</table>
</body>
</html>';

mysql_close();

$mpdf=new mPDF('c','A4','9','helvetica',15,15,26,16,9,9); 

$mpdf->SetDisplayMode('fullpage');

$stylesheet = file_get_contents('style.css');
$mpdf->WriteHTML($stylesheet,1);

$titulo = 'Extrato de fidelização - Mega Polo';

$mpdf->SetHTMLHeader('
    <table border="0">
        <tr>
            <td width="330">
                <img src="images/'.$idClienteVeritas.'/logo-report.jpg" />
            </td>
            <td style="padding-left: 15px;">
                <div style="font-weight: bold; font-size: 20px; padding-bottom: 6px;">'.$titulo.'</div>
                <!--div style="font-size: 18px;">Aqui vai o sub-título</div-->
            </td>
        </tr>
    </table>
');    
$mpdf->WriteHTML($html,2);

$mpdf->Output('relatorio.pdf','I');
