<?php

header('Content-type: application/pdf');
setlocale( LC_ALL, 'pt_BR');

require_once('config.db.php'); 
include("mpdf60/mpdf.php");

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : 2;

$mes = isset($_GET["mes"]) ? $_GET["mes"] : date('n');

$query = "
SELECT 
    DAY(DataNascimento) AS Dia,
    Nome,
    (SELECT
        GROUP_CONCAT(ART_PessoaTelefone.telefone SEPARATOR '&nbsp;&nbsp;')
     FROM ART_PessoaTelefone
     WHERE ART_PessoaTelefone.Pessoa_Id = ART_Pessoa.Id
     GROUP BY ART_PessoaTelefone.Pessoa_Id
    ) AS Telefones
FROM ART_Pessoa
WHERE MONTH(DataNascimento) = $mes  AND ART_Pessoa.Pessoa_Id = $idClienteVeritas
ORDER BY Dia;";

$result = mysql_query($query);

$html = '
<html>
<head>
</head>
<body>
<table>
    <tr>
        <th class="header">Dia</th>
        <th class="header">Cliente</th>
        <th class="header">Telefones</th>
    </tr>
';

while($row = mysql_fetch_assoc($result)) {
    $row = array_map('utf8_encode', $row);
    $html .= '
    <tr>
        <td>'.$row["Dia"].'</td>
        <td>'.$row["Nome"].'</td>
        <td>'.$row["Telefones"].'</td>
    </tr>';
}

$html .= '
</table>
</body>
</html>';

mysql_close();

$mpdf=new mPDF('c','A4','9','helvetica',15,15,26,16,9,9); 

$mpdf->SetDisplayMode('fullpage');

$stylesheet = file_get_contents('style.css');
$mpdf->WriteHTML($stylesheet,1);

$mesExtenso = strtoupper(strftime('%B', strtotime(date("Y")."-".$mes."-01")));

$titulo = 'Aniversariantes';

$mpdf->SetHTMLHeader('
    <table border="0">
        <tr>
            <td width="330">
                <img src="images/'.$idClienteVeritas.'/logo-report.jpg" />
            </td>
            <td style="padding-left: 15px;">
                <div style="font-weight: bold; font-size: 20px; padding-bottom: 6px;">'.$titulo.'</div>
                <div style="font-size: 18px;">MÊS DE '.$mesExtenso.'</div>
            </td>
        </tr>
    </table>
');    
$mpdf->WriteHTML($html,2);

$mpdf->Output('relatorio.pdf','I');