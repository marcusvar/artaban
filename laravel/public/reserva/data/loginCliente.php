<?php
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// login e senha enviada do formulário 
$email = $_REQUEST['email']; 
//$email = "marcusvarosa@bol.com.br"; 

$pass = $_REQUEST['password'];  
//$pass = "40bd001563085fc35165329ea1ff5c5ecbdbbeef";

// para proteger o banco contra MySQL injection
$email = stripslashes($email);
$pass = stripslashes($pass);

$email = mysql_real_escape_string($email);
$pass = mysql_real_escape_string($pass);

$query = 
      "SELECT 
       ART_Pessoa.Id, 
       ART_Pessoa.Nome, 
       IF(ISNULL(ART_Pessoa.Pessoa_Id_Responsavel), ART_Pessoa.Id, ART_Pessoa.Pessoa_Id_Responsavel) AS IdResponsavel,
       ART_Pessoa.Email,
       (SELECT
         COALESCE(
         SUM(
         ART_Fidelizacao.ValorOperacao * (
         CASE WHEN
           ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
         END)
         ), 0) saldo
       FROM ART_Fidelizacao
       WHERE ART_Fidelizacao.Pessoa_Id = IFNULL(ART_Pessoa.Pessoa_Id_Responsavel, ART_Pessoa.Id)) AS Fidelizacao,
       ART_Pessoa.Ativo
     FROM 
       ART_Pessoa 
     LEFT JOIN ART_Pessoa Responsavel ON ART_Pessoa.Pessoa_Id_Responsavel = Responsavel.Id
     WHERE ART_Pessoa.Email='$email' AND ART_Pessoa.Senha='$pass'";
$result = mysql_query(utf8_decode($query)); 
$row = mysql_fetch_assoc($result);

if($row) {
    $success = $row['Ativo'] ? true : false;
    $msg = $row['Ativo'] ? 'Usuário autenticado!' : 'Seu cadastro não está ativo!';
} else {
    $success = false;
    $msg = 'Login ou senha incorretos.';
}

$ret = array(
    "success" => $success,
    "msg"     => $msg,
    "status"   => $row ? true : false,
    "result"  => 
        array(
            "Id"            => $row['Id'],
            "Nome"          => utf8_encode($row['Nome']),
            "Data"          => date('Y-m-d\TH:i:s'),
            "Email"         => $row['Email'],
            "Fidelizacao"   => $row['Fidelizacao'],
            "IdResponsavel" => $row['IdResponsavel']
        ));

$query = "
    SELECT 
       ART_Pessoa.Id, 
       ART_Pessoa.Nome, 
       IF(ISNULL(ART_Pessoa.Pessoa_Id_Responsavel), ART_Pessoa.Id, ART_Pessoa.Pessoa_Id_Responsavel) AS IdResponsavel,
       ART_Pessoa.Email,
       (SELECT
         COALESCE(
         SUM(
         ART_Fidelizacao.ValorOperacao * (
         CASE WHEN
           ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
         END)
         ), 0) saldo
       FROM ART_Fidelizacao
       WHERE ART_Fidelizacao.Pessoa_Id = IFNULL(ART_Pessoa.Pessoa_Id_Responsavel, ART_Pessoa.Id)) AS Fidelizacao,
       ART_Pessoa.Ativo
     FROM 
       ART_Pessoa 
     LEFT JOIN ART_Pessoa Responsavel ON ART_Pessoa.Pessoa_Id_Responsavel = Responsavel.Id
     WHERE 
       ART_Pessoa.Pessoa_Id_Responsavel = ".$row['Id'];
$result = mysql_query(utf8_decode($query)); 

$cliente = array(
    "Id"            => $row["Id"], 
    "Nome"          => utf8_encode($row['Nome']),
    "Email"         => $row['Email'],
    "Fidelizacao"   => $row['Fidelizacao'],
    "IdResponsavel" => $row['IdResponsavel']
);
$cli = false;

while ($row = mysql_fetch_assoc($result)) {
    $cli = true;
    $ret["empresas"][] = array_map('utf8_encode', $row);
}

if($cli) {
    array_unshift($ret["empresas"], $cliente);
}
/* encerra a conexão */
mysql_close();

echo json_encode($ret);
