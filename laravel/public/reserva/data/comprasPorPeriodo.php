<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once('config.db.php'); 
include("mpdf60/mpdf.php");

//include '../chromephp/ChromePhp.php';
// Busca o Id do cliente Veritas definido no virtualhost do apache
$idCliente = getenv('CLIENTE')       ? getenv('CLIENTE')     : 2;

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Busca o Id do cliente Veritas definido no virtualhost do apache
$idClienteVeritas = getenv('CLIENTE') ? getenv('CLIENTE') : 2;

$inicial = isset($_GET["inicial"]) ? $_GET["inicial"] : date('Y-m-d');
$periodo = isset($_GET["periodo"]) ? $_GET["periodo"] : 60;
$opcao   = isset($_GET["opcao"])   ? $_GET["opcao"]   : 'FALSE';

$query = "
SELECT
    ART_Localidade.Nome AS Cidade,

    CONCAT(Pessoa.Nome, IF(NOT ISNULL(
        (SELECT Pessoa_Id_Responsavel FROM ART_Pessoa 
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel)
    ), CONCAT('<br />',

    (SELECT
    GROUP_CONCAT(ART_Pessoa.Nome SEPARATOR '<br /> ')
        FROM ART_Pessoa
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel
    )),'')) AS Cliente,

    CONCAT('', IF(NOT ISNULL(
        (SELECT Pessoa_Id_Responsavel FROM ART_Pessoa 
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel)
    ), CONCAT('<br />',

    (SELECT
    GROUP_CONCAT(ART_Pessoa.NomeUsual SEPARATOR '<br /> ')
        FROM ART_Pessoa
        WHERE ART_Pessoa.Pessoa_Id_Responsavel = Pessoa.Id
        GROUP BY ART_Pessoa.Pessoa_Id_Responsavel
    )),'')) AS NomeUsual,

    (SELECT
        GROUP_CONCAT(ART_PessoaTelefone.Descricao,': ',ART_PessoaTelefone.telefone SEPARATOR '<br /> ')
        FROM ART_PessoaTelefone
        WHERE ART_PessoaTelefone.Pessoa_Id = Pessoa.Id
        GROUP BY ART_PessoaTelefone.Pessoa_Id
    ) AS Telefones,

    DATE_FORMAT(MAX(ART_Passagem.DataHoraEmissao), '%Y-%m-%d') AS UltimaCompra,

    DATE_FORMAT(MAX(ART_Viagem.DataInicio), '%d/%m/%Y') AS DataViagem,

    DATEDIFF('$inicial', MAX(ART_Viagem.DataInicio)) AS Diferenca

FROM 
    ART_Pessoa AS Pessoa
LEFT JOIN ART_PessoaVinculo ON Pessoa.Id = ART_PessoaVinculo.Pessoa_Id
LEFT JOIN ART_Pessoa AS Vinculo ON ART_PessoaVinculo.Pessoa_Id_Vinculo = Vinculo.Id
LEFT JOIN ART_PessoaEndereco ON ART_PessoaEndereco.Pessoa_Id = Pessoa.Id
LEFT JOIN ART_Localidade on ART_Localidade.Id = ART_PessoaEndereco.Localidade_Id
JOIN ART_Passagem ON ( Pessoa.Id = ART_Passagem.Pessoa_Id OR Vinculo.Id = ART_Passagem.Pessoa_Id ) AND ART_Passagem.Situacao <> 'Cancelada'
LEFT JOIN ART_Viagem ON ART_Viagem.Id = ART_Passagem.Viagem_Id
#LEFT JOIN ART_Pessoa AS Passageiro ON ART_Passagem.Pessoa_Id = Passageiro.Id
WHERE 
    Pessoa.Pessoa_Id = $idCliente 
GROUP BY Pessoa.Id
HAVING IF($opcao, Diferenca <= $periodo, Diferenca > $periodo)
ORDER BY ART_Localidade.Nome, Pessoa.Nome ASC;";

$result = mysql_query($query);

$mpdf=new mPDF('c','A4-L','9','helvetica',15,15,26,16,9,9); 

$mpdf->SetDisplayMode('fullpage');

$stylesheet = file_get_contents('style.css');
$mpdf->WriteHTML($stylesheet,1);

$titulo = 'Compras por período';

$mpdf->SetHTMLHeader('
    <table border="0">
        <tr>
            <td width="330">
                <img src="images/'.$idClienteVeritas.'/logo-report.jpg" />
            </td>
            <td style="padding-left: 15px;">
                <div style="font-weight: bold; font-size: 20px; padding-bottom: 6px;">'.$titulo.'</div>
                <!--div style="font-size: 18px;">Aqui vai o sub-título</div-->
            </td>
        </tr>
    </table>
');    

$html = ' 
<table>
    <tr>
        <th class="header">Cliente</th>
        <th class="header">Nome Fantasia</th>
        <th class="header">Telefones</th>
        <th class="header">Última viagem</th>
    </tr>';

$mpdf->WriteHTML($html,2,true,false);
$html = "";

$localidade = ".";
$cont = 0;
while($row = mysql_fetch_assoc($result)) {
    $row = array_map('utf8_encode', $row);
    if($localidade !== $row["Cidade"]) {
        $localidade = $row["Cidade"];
        
        $html .= '
        <tr>
            <td class="titulo"><span class="cidade2">'.($row["Cidade"] === "" ? "Sem cidade cadastrada" : $row["Cidade"]).'</span></td><td colspan="3"></td>
        </tr>';
         
    }

    $html .= '
    <tr>
        <td class="separador">'.$row["Cliente"].'</td>
        <td class="separador">'.$row["NomeUsual"].'</td>
        <td class="separador">'.$row["Telefones"].'</td>
        <td class="separador" style="text-align: center">'.$row["DataViagem"].'</td>
    </tr>';
    
    if($cont > 60) {
        $cont = 0;
        $mpdf->WriteHTML($html,2,false,false);
        $html = "";
    } else {
        $cont++;
    }
}

mysql_close();

$html .= '
</table>
';

$mpdf->WriteHTML($html,2,false,TRUE);

//ChromePhp::log($query);
//ChromePhp::log($html);


/**
$mpdf->WriteHTML('<p>This is the beginning...<br>', 2, true, false);
$mpdf->WriteHTML('...this is the middle 1...<br>', 2, false, false);
$mpdf->WriteHTML('...this is the middle 2...<br>', 2, false, false);
$mpdf->WriteHTML('...this is the middle 3...<br>', 2, false, false);
$mpdf->WriteHTML('...and this is the end</p>', 2, false, true);
 * 
 */


$mpdf->Output('relatorio.pdf','I');
