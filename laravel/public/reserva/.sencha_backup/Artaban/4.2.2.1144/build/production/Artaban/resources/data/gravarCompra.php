<?php
/*
 * Grava uma carrinho de compras (reserva ou compra)
 */
require_once('config.db.php'); 

include '../chromephp/ChromePhp.php';

$_POST["jsonParam"] = isset($_POST["jsonParam"]) ? $_POST["jsonParam"] :
    '{"venda":{"PontoReferencia_Id":98,"Pessoa_Id_Cliente":"3","Pessoa_Id":2,"Usuario_Id":3,"ValorDesconto":20},"fidelizacao":{"ValorOperacao":30},"passageiros":{"0":{"Telefone":"(28) 763487234 (28) 376482348 (28) 735481276 (98) 364921834 ","Valor":100,"Poltrona":"01","Viagem_Id":3,"ViagemItem_Id_Origem":1,"ReservaExpiracao":null,"Tipo":"P"}}}';

//ChromePhp::log($_POST);

$param = json_decode($_POST["jsonParam"], FALSE);

//$data = json_decode(json_encode($_POST), FALSE);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

// Insere os dados da venda na tabela ART_Venda
$venda = $param->venda;

$query = sprintf("INSERT INTO ART_Venda (PontoReferencia_Id, Pessoa_Id_Cliente, Pessoa_Id, Usuario_Id, ValorDesconto) ".
                 "values (%s, %s, %s, %s, %s)",
    $venda->PontoReferencia_Id,
    $venda->Pessoa_Id_Cliente,
    $venda->Pessoa_Id,
    $venda->Usuario_Id,
    $venda->ValorDesconto    
); 
mysql_query($query);

// Recupera o ID da venda
$venda_id = mysql_insert_id();

// Verifica se tem valor utilizado de fidelização e faz o lançamento do debite na tabela ART_Fidelizacao 
$fidelizacao = $param->fidelizacao;

if($fidelizacao->ValorOperacao > 0) {
    $query = sprintf("INSERT INTO ART_Fidelizacao (Operacao, DataOperacao, Descricao, ".
                     "ValorOperacao, Pessoa_Id, Venda_Id, created_at, updated_at) ".
                     "values ('%s', '%s', '%s', %s, %s, %s, '%s', '%s')",
        utf8_decode('Débito'),
        date('Y-m-d H:i:s'),
        'Compra pelo sistema de retaguarda Artaban',
        $fidelizacao->ValorOperacao,
        $venda->Pessoa_Id_Cliente,    
        $venda_id,
        date('Y-m-d H:i:s'), date('Y-m-d H:i:s')    
    );
    mysql_query($query); 
}

$arr = $param->passageiros;

foreach ( $arr as $key => $value )
{
    // Recupera ID do passageiro
    $id = $value->Id;
    
    // Busca ID do item da viagem
    $query = sprintf("SELECT ART_ViagemItem.Id ".
                     "FROM ART_ViagemItem ".
                     "  LEFT JOIN ART_HorarioItem ON ART_ViagemItem.HorarioItem_Id = ART_HorarioItem.Id ".
                     "  LEFT JOIN ART_LinhaItem ON ART_HorarioItem.LinhaItem_Id = ART_LinhaItem.ID ".
                     "WHERE ART_LinhaItem.ID = %s AND ART_ViagemItem.Viagem_Id = %s", 
             $value->ViagemItem_Id_Origem, $value->Viagem_Id);
    
    $ret = mysql_query( $query, $link );
    $rows = mysql_fetch_array($ret); 
    
    // Salva os dados de cada passageiro na tabela ART_Passagem
    $query = sprintf("INSERT INTO ART_Passagem (Valor, Poltrona, Viagem_Id," . 
                     "ViagemItem_Id_Origem, Pessoa_Id, Tipo, Situacao, ReservaExpiracao, created_at, updated_at) ".
                     "values (%s, '%s', %s, %s, %s, '%s', '%s', '%s', '%s', '%s')",
        $value->Valor,
        $value->Poltrona,
        $value->Viagem_Id,
        $rows["Id"], // Id do item da viagem relacionado com o local de embarque
        $id,
        $value->Tipo,
        'Normal',
        $value->ReservaExpiracao,
        date('Y-m-d H:i:s'), date('Y-m-d H:i:s')
    ); 
    mysql_query($query);    
    
    // Recupera o ID da passagem
    $passagem_id = mysql_insert_id();

    // salva os dados na tabela ART_VendaItem
    $query = sprintf("INSERT INTO ART_VendaItem (Venda_Id, Passagem_Id, created_at, updated_at) ".
                     "values (%s, %s, '%s', '%s')",
        $venda_id,
        $passagem_id,
        date('Y-m-d H:i:s'), date('Y-m-d H:i:s'));
    
    mysql_query($query);      
}

echo json_encode(array(
    "result" => array( 
        "success" => mysql_errno(),
        "msg"     => mysql_error()
        )
));

mysql_close();

?>
