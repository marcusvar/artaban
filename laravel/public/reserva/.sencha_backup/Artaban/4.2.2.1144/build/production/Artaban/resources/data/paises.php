<?php 
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = "SELECT ART_Pais.Id, CONCAT( ART_Pais.Nome, '-', ART_Pais.Sigla) AS Nome ".
         "FROM ART_Pais ".
         "WHERE NOT ART_Pais.Nome = 'Brasil' ".
         "ORDER BY ART_Pais.Nome ASC";
$result = mysql_query($query); 
        
$retorno = array();
while ($row = mysql_fetch_assoc($result)) {
    $retorno["result"][] = array( "Id" => $row["Id"], 
                                  "Nome" => utf8_encode($row["Nome"]));
}
echo json_encode($retorno);
?>
