var emEdicao = false;

function requestError(resp){
    var title = 'Erro';
	if (resp.status === 408) {
		Ext.Msg.alert('Aviso', 'Sessão expirada');
	} else {
		if (resp.status === 404) {
			Ext.Msg.alert(title, Ext.util.Format.htmlEncode('Servidor não está pronto'));
		} else {
			if (resp.status !== undefined) {
				Ext.Msg.alert(title, Ext.util.Format.htmlEncode('Servidor não encontrado (') + resp.status + ')');
			} else {
				Ext.Msg.alert(title, 'Servidor não encontrado');
			}
		}
	}    
}
