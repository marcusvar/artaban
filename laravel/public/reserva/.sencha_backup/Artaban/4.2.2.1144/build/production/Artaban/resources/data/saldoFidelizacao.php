<?php
require_once('config.db.php');

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$id  = $_REQUEST['Id'];
$id = 3;

$query = utf8_decode(
"SELECT
  COALESCE(SUM(CASE WHEN ART_Fidelizacao.Operacao = 'Crédito' THEN ART_Fidelizacao.ValorOperacao ELSE 0
  END), 0) credito,
  COALESCE(SUM(CASE WHEN ART_Fidelizacao.Operacao = 'Débito' THEN ART_Fidelizacao.ValorOperacao ELSE 0
  END), 0) debito,
  (SELECT
      COALESCE(
      SUM(
      ART_Fidelizacao.ValorOperacao * (
      CASE WHEN
        ART_Fidelizacao.Operacao = 'Crédito' THEN 1 ELSE -1
      END)
      ), 0) saldo
    FROM ART_Fidelizacao
    WHERE ART_Fidelizacao.Pessoa_Id = $id) AS saldo
FROM ART_Fidelizacao
WHERE ART_Fidelizacao.Pessoa_Id = $id");

$result = mysql_query($query); 
$row    = mysql_fetch_assoc($result);

echo json_encode(array(
    "result"=> 
        array( "success" => mysql_errno(),
               "msg"     => mysql_error(),
               "valor"   => $row['saldo'])
));

mysql_close();
