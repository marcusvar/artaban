{
    "classAlias": "widget.cpffield",
    "className": "Ext.ux.CpfField",
    "inherits": "Ext.form.field.Text",
    "autoName": "MyCpfField",
    "helpText": "TextField CPF - Mesmo TextField com máscara e validaço para CPF",

    "toolbox": {
        "name": "CPF Text Field",
        "category": "Form Fields",
        "groups": ["Forms",'Views']
    },
    configs: [{
        name: 'autocomplete',
        type: 'string',
        hidden: false,
        defaultValue: 'off'
    },{
        name: 'soNumero',
        type: 'boolean',
        hidden: false,
        defaultValue: false
    },{
        name: 'maxLength',
        type: 'number',
        hidden: false,
        defaultValue: 14
    },{
        name: 'emptyText',
        type: 'string',
        hidden: false,
        defaultValue: 'Digite aqui seu CPF...'
    }]
}   