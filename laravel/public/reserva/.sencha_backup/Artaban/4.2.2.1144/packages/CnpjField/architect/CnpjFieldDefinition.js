{
    "classAlias": "widget.cnpjfield",
    "className": "Ext.ux.CnpjField",
    "inherits": "Ext.form.field.Text",
    "autoName": "MyCnpjField",
    "helpText": "TextField CNPJ - Mesmo TextField com máscara e validação para CNPJ",

    "toolbox": {
        "name": "CNPJ Text Field",
        "category": "Form Fields",
        "groups": ["Forms",'Views']
    },
    configs: [{
        name: 'autocomplete',
        type: 'string',
        hidden: false,
        defaultValue: true
    },{
        name: 'soNumero',
        type: 'boolean',
        hidden: false,
        defaultValue: 'off'
    },{
        name: 'maxLength',
        type: 'number',
        hidden: false,
        defaultValue: 14
    },{
        name: 'emptyText',
        type: 'string',
        hidden: false,
        defaultValue: 'Digite aqui o CNPJ...'
    }]
}   