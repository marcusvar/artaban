<?php

require_once('db.php');
function in_array_column($text, $column, $array)
{
    if (!empty($array) && is_array($array))
    {
        for ($i=0; $i < count($array); $i++)
        {
            if ($array[$i][$column]==$text || strcmp($array[$i][$column],$text)==0) return true;
        }
    }
    return false;
}
function mysqli_call(mysqli $dbLink, $procName, $params="")
{
    if(!$dbLink) {
        throw new Exception("A conexão MySQLi é inválida.");
    }
    else
    {
        // Execute the SQL command.
        // The multy_query method is used here to get the buffered results,
        // so they can be freeded later to avoid the out of sync error.
        $sql = "CALL {$procName}({$params});";
        $sqlSuccess = $dbLink->multi_query($sql);
        if($sqlSuccess)
        {
            if($dbLink->more_results())
            {
                // Get the first buffered result set, the one with our data.
                $result = $dbLink->use_result();
                $output = array();
                // Put the rows into the outpu array
                while($row = $result->fetch_assoc())
                {
                    $output[] = $row;
                }
                // Free the first result set.
                // If you forget this one, you will get the "out of sync" error.
                $result->free();
                // Go through each remaining buffered result and free them as well.
                // This removes all extra result sets returned, clearing the way
                // for the next SQL command.
                while($dbLink->more_results() && $dbLink->next_result())
                {
                    $extraResult = $dbLink->use_result();
                    if($extraResult instanceof mysqli_result){
                        $extraResult->free();
                    }
                }
                return $output;
            }
            else
            {
                return false;
            }
        }
        else
        {
            throw new Exception("A chamada falhou: " . $dbLink->error);
        }
    }
}

//include '../chromephp/ChromePhp.php';

//ChromePhp::warn('$_REQUEST');
//ChromePhp::log($_REQUEST);

$start = $_REQUEST['start']; 
$start = isset($_REQUEST['start'])     ? $_REQUEST['start']     : 0;
$limit = $_REQUEST['limit']; 
$limit = isset($_REQUEST['limit'])     ? $_REQUEST['limit']     : 10;

$_REQUEST['linhaItemId'] = 3;
$_REQUEST['linhaId'] = 2;

$horarios = mysqli_call($mysqli, "Proc_ConsHorarios", "NULL, (SELECT
    PontoReferencia_Id
  FROM ART_LinhaItem 
  WHERE Id = {$_REQUEST['linhaItemId']}), (SELECT
    PontoReferencia_Id_Destino
  FROM ART_Linha
  WHERE Id = {$_REQUEST['linhaId']}), $start, $limit");
  
for ($i = 0; $i < count($horarios); $i++) {
    $tarifa = mysqli_call($mysqli, "Proc_ConsTarifa", $horarios[$i]['Id'].", 
    (SELECT
      PontoReferencia_Id
      FROM ART_LinhaItem 
      WHERE Id = {$_REQUEST['linhaItemId']}), '1', '".date('Y-m-d')."'");

    $mapa = mysqli_call($mysqli, "Proc_ConsMapaLugarViagem", $horarios[$i]['Id']);
    // Busca pelo segundo piso
    $piso = in_array_column('2', 'Andar', $mapa);
    
    //$dataRetorno = implode("/",array_reverse(explode("-",substr($dataRetorno,0,10)))).substr($dataRetorno,10); 
    $dataRetorno = date('d/m/Y', strtotime("+".$horarios[$i]['Duracao']." days",strtotime($horarios[$i]['DataHoraEmbarque'])));
    $valor2 = $tarifa[0]['TarifaPiso2'];
    $retorno[] =  
           array( "viagemId"  => $horarios[$i]['Id'],
                  "partida"   => date('d/m/Y H:i', strtotime( $horarios[$i]['DataHoraEmbarque'] )),
                  "retorno"   => $dataRetorno,
                  "servico"   => $horarios[$i]['TipoVeiculo_Descricao'],
                  "valor"     => $tarifa[0]['TarifaPiso1'],
                  "valor2"    => $valor2 === '0.00' ? $valor2 : '0.00',
                  "descricao" => $horarios[$i]['Descricao'],
                  "duracao"   => $horarios[$i]['Duracao']. " dia(s)",
                  "valores"   => ($piso ? "Piso superior: " : "Leito: ").'R$ '.number_format($tarifa[0]['TarifaPiso1'], 2, ',', '.')."<br />".
                                 ($piso ? "Piso inferior: " : "Leito cama: ").'R$ '.number_format($tarifa[0]['TarifaPiso2'], 2, ',', '.')
           );
}

$total = mysqli_call($mysqli, 
    "Proc_ConsHorariosQTd", "NULL, 
        (SELECT PontoReferencia_Id FROM ART_LinhaItem WHERE Id = {$_REQUEST['linhaItemId']}), 
        (SELECT PontoReferencia_Id_Destino FROM ART_Linha WHERE Id = {$_REQUEST['linhaId']})");

echo json_encode(array(
    "result"  => $retorno,
    "total"   => $total[0]['Total'],
    "success" => $mysqli->errno,
    "msg"     => $mysqli->error
));

$mysqli->close();
