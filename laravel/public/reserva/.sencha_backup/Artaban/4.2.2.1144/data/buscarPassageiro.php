<?php
/*
 * Grava uma carrinho de compras (reserva ou compra)
 */
require_once('config.db.php'); 

//include '../chromephp/ChromePhp.php';

$id = isset($_POST["passagemId"]) ? $_POST["passagemId"] : 1;

//ChromePhp::log($_POST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$query = 
"SELECT
  ap.Id AS passagemId,
  ap1.Descricao AS destino,
  ap2.documento AS documento,
  CONCAT(al2.nome, ' - ', ap3.Descricao) AS embarque,
  ap2.nome AS nome,
  (SELECT
      GROUP_CONCAT(ART_PessoaTelefone.Descricao,': ',ART_PessoaTelefone.telefone SEPARATOR ' ')
    FROM ART_PessoaTelefone
    WHERE ART_PessoaTelefone.Pessoa_Id = ap2.Id
    GROUP BY ART_PessoaTelefone.Pessoa_Id) AS telefone,
  ap.valor AS valor,
  ap.Tipo as tipo
FROM ART_Passagem ap
  JOIN ART_Viagem av           ON ap.Viagem_Id = av.Id
  JOIN ART_Horario ah          ON av.Horario_Id = ah.Id
  JOIN ART_Linha al            ON ah.Linha_Id = al.Id
  JOIN ART_PontoReferencia ap1 ON al.PontoReferencia_Id_Destino = ap1.Id
  JOIN ART_Pessoa ap2          ON ap.Pessoa_Id = ap2.Id
  JOIN ART_ViagemItem av1      ON ap.ViagemItem_Id_Origem = av1.Id
  JOIN ART_HorarioItem ah1     ON av1.HorarioItem_Id = ah1.Id
  JOIN ART_LinhaItem al1       ON ah1.LinhaItem_Id = al1.Id
  JOIN ART_PontoReferencia ap3 ON al1.PontoReferencia_Id = ap3.Id
  JOIN ART_Localidade al2      ON ap3.Localidade_Id = al2.Id
WHERE ap.Id = $id;";

$ret = mysql_query($query);

$rows = array_map('utf8_encode', mysql_fetch_assoc($ret));

//$rows = array_map('utf8_encode', $rows);

echo json_encode(array(
    "result" => $rows 
));

mysql_close();

?>

