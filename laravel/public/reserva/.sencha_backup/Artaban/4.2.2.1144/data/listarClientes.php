<?php
require_once('config.db.php'); 

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);

$start = isset($_REQUEST['start']) ? $_REQUEST['start'] : 0;
$limit = isset($_REQUEST['limit']) ? $_REQUEST['limit'] : 10;
$nome  = $_REQUEST['query'];

$query = "SELECT
  IFNULL(Empresa.Id,ART_Pessoa.Id) AS Id,
  ART_Pessoa.Nome,
  ART_Pessoa.Telefone,
  Empresa.Nome AS Cliente
FROM
  ART_Pessoa
  LEFT JOIN ART_Pessoa AS Empresa ON Empresa.Pessoa_Id_Responsavel = ART_Pessoa.Id 
WHERE
  ART_Pessoa.Nome LIKE '%$nome%'
ORDER BY ART_Pessoa.Nome ASC      
LIMIT $start,  $limit";
$result = mysql_query($query); 
        
$retorno = array();

while ($row = mysql_fetch_assoc($result)) {
    $retorno[] = array( "Id" => $row["Id"], 
                        "Nome" => utf8_encode($row["Nome"]),
                        "Empresa" => $row["Cliente"]);
}

//consulta total de linhas na tabela
$queryTotal = mysql_query(
        "SELECT count(*) as num ".
        "FROM ART_Pessoa ".
        "LEFT JOIN ART_Pessoa AS Empresa ON Empresa.Pessoa_Id_Responsavel = ART_Pessoa.Id ".
        "WHERE ART_Pessoa.Nome ". 
        "LIKE '%$nome%'");

$row = mysql_fetch_assoc($queryTotal);
$total = $row['num'];

//encoda para formato JSON
echo json_encode(array(
    "total" => $total,
    "result" => $retorno
));

?>

