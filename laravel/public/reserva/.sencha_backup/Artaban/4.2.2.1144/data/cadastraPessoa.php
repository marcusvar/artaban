<?php
require_once('config.db.php'); 

/*
if(!isset($_POST["Tipo"])) {
    $_POST = array( "Tipo" => "PF Brasileiro",
                    "RGIE" => "28736481238471",
                    "Documento" => "182.425.921-28",
                    "Nome" => "ksjdfjksahdfkja skjdgfkjsagdf",
                    "Sexo" => "M",
                    "Vincular" => "on",
                    "Pessoa_Id" => 2);
                          
    $_POST['telefone'][0] = array ( "Descricao" => "Comercial",
                                    "Numero" => "92138649186"  );
    $_POST['telefone'][1] = array ( "Descricao" => "Celular",
                                    "Numero" => "81743821763"  );
    $_POST['telefone'][3] = array ( "Descricao" => "Residencial",
                                    "Numero" => "18723548127"  );
    $_POST['telefone'][5] = array ( "Descricao" => "Claro",
                                    "Numero" => "98654862354"  );

 
}    
   
    $_POST = array( "Nome" => "Cliente PF Brasileira de Teste",
                    //"NomeUsual" => "Empresa de Teste",
                    "Documento" => "803.954.544-76",
                    "RGIE" => "87654321",
                    "Email" => "",
                    "Sexo" => "M",
                    "Telefone" => "(69) 3422-3456",
                    "Celular" => "(82) 736487263",
                    "Tipo" => "PF Estrangeiro",
                    //"Pessoa_Id_Responsavel" => 1475,
                    "Pessoa_Id" => 2);
    
    $_POST['endereco'] = array( "Bairro" => "dkjfghksjdfg",
                                "Cep" => "82736-482",
                                "Complemento" => "ksjdhksjhf",
                                "Localidade_Id" => "2853",
                                "Logradouro" => "kajgsdfkjagsd",
                                "Numero" => "876",
                                "uf" => "18") ;
    $_POST['telefone'][0] = array ( "Descricao" => "Comercial",
                                    "Numero" => "92138649186"  );
    $_POST['telefone'][1] = array ( "Descricao" => "Celular",
                                    "Numero" => "81743821763"  );
    $_POST['telefone'][3] = array ( "Descricao" => "Residencial",
                                    "Numero" => "18723548127"  );
    $_POST['telefone'][6] = array ( "Descricao" => "Claro",
                                    "Numero" => "98654862354"  );
}
*/ 

//include '../chromephp/ChromePhp.php';

//ChromePhp::log($_POST);

$link = mysql_connect(BD_HOSTNAME, BD_USERNAME, BD_PASSWORD);
mysql_select_db(BD_DATABASE,$link);
 
$data = json_decode(json_encode($_POST), FALSE);

$data->Pessoa_Id_Responsavel = isset($_POST["Pessoa_Id_Responsavel"]) ?
                               $_POST["Pessoa_Id_Responsavel"] :
                               'NULL';
//ChromePhp::log($data);

$query = sprintf("INSERT INTO ART_Pessoa (".
                 "Nome, NomeUsual, Documento, RGIE, Email, Sexo, DataNascimento, ".
                 "Telefone, Celular, Pessoa_Id, Tipo, Pessoa_Id_Responsavel, ".
                 "created_at, updated_at) ".
                 "values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', ".
                         "'%s', %s, '%s', '%s')",
    mysql_real_escape_string($data->Nome),
    mysql_real_escape_string($data->NomeUsual),
    mysql_real_escape_string($data->Documento),
    mysql_real_escape_string($data->RGIE),
    mysql_real_escape_string($data->Email),
    mysql_real_escape_string($data->Sexo),
    mysql_real_escape_string($data->DataNascimento),
    mysql_real_escape_string($data->Telefone),
    mysql_real_escape_string($data->Celular),
    mysql_real_escape_string($data->Pessoa_Id), 
    mysql_real_escape_string($data->Tipo),
    $data->Pessoa_Id_Responsavel, 
    date('Y-m-d'), date('Y-m-d')
);

mysql_query($query);

$id = mysql_insert_id();

$empty = true;
foreach ($data->endereco as $val) {
    if($val !== "") {
        $empty = false;
        break;
    }
}

if(!$empty) {
    $query = sprintf("INSERT INTO ART_PessoaEndereco (Logradouro, Numero, Bairro, ".
                                                     "Complemento, Cep, Localidade_Id, ".
                                                     "Pessoa_Id, created_at, updated_at ) ".
                     "values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
        mysql_real_escape_string($data->endereco->Logradouro),
        mysql_real_escape_string($data->endereco->Numero),
        mysql_real_escape_string($data->endereco->Bairro),
        mysql_real_escape_string($data->endereco->Complemento),
        mysql_real_escape_string($data->endereco->Cep),
        mysql_real_escape_string($data->endereco->Localidade_Id),
        mysql_real_escape_string($id),
        date('Y-m-d'), date('Y-m-d'));
        
    mysql_query($query);
}

$telefone = $data->telefone;

//ChromePhp::log($telefone);


$empty = true;
foreach ($telefone as $val) {
    if(!empty($val->Descricao) and !empty($val->Numero) ) {
        $empty = false;
        break;
    }
}

if(!$empty) {
    foreach ($telefone as $val) {
        if(!empty($val->Descricao) and !empty($val->Numero) ) {
            $query = sprintf("INSERT INTO ART_PessoaTelefone (Descricao, Telefone, Pessoa_Id, created_at, updated_at ) ".
                             "values ('%s', '%s', %s, '%s', '%s')",
                mysql_real_escape_string($val->Descricao),
                mysql_real_escape_string($val->Numero),
                mysql_real_escape_string($id),
                date('Y-m-d'), date('Y-m-d'));

            mysql_query($query);
        }
    }
}

if($data->Vincular === 'on') {
    $query = sprintf("INSERT INTO ART_PessoaVinculo (Pessoa_Id, Pessoa_Id_Vinculo, created_at, updated_at ) ".
                     "values (%s, %s, '%s', '%s')",
        mysql_real_escape_string($data->Pessoa_Id),
        mysql_real_escape_string($id),
        date('Y-m-d'), date('Y-m-d'));

    mysql_query($query);    
}

echo json_encode(array(
    "result"=> 
        array( "success" => mysql_errno(),
               "msg"     => mysql_error(),
               "retorno" => array(
                    "id" => $id,
                    "nome" => $data->Nome,
                    "telefone" => $data->Telefone,
                    "sexo" => $data->Sexo,
                    "email" => $data->Email
        ))
));

mysql_close();
