function ModalPessoaTelefones(pessoaId) {
    this.pessoaId = pessoaId;
    
    this.btnAdicionar = $('.modal-btn-pessoa-adicionar-telefone');
    this.btnSalvar = $('.modal-btn-pessoa-salvar-telefones');
    this.form = $('form#modal-form-pessoa-telefones');
    this.mensagem = $('div#modal-form-mensagem-pessoa-telefones');
};


ModalPessoaTelefones.prototype.modal = function(eventoAoFechar) {
    this.btnAdicionar.hide();
    this.form.html('').hide();
    
    var box = $('#box-modal-pessoa-telefones');
    
    box.modal();
    
    if (typeof eventoAoFechar !== 'undefined') {
        box.on('hidden.bs.modal', eventoAoFechar);        
    }
    
    this._buscarTelefones();    
        
    this.form.fadeIn('slow');
    this.btnAdicionar.show();
    
    this._adicionarEventos();
};


ModalPessoaTelefones.prototype._adicionarEventos = function() {
    var obj = this;
    
    this.btnAdicionar.off('click').on('click', function() {
        obj._adicionarInput("", "");
    });
    
    this.form.off('click').on('click', '.modal-btn-pessoa-remover-telefone',function() {
        obj._removerInput($(this).parent().parent());
    });
    
    this.btnSalvar.off('click').on('click', function() {
        obj._salvarTelefones();
    });
    
    this.form.off('focus').on('focus', '.telefone-mask',function() {
        $(this).inputmask("mask", {
            "mask": "(99) [9]99999999999",
            greedy: false,
        });       
    });    
}


ModalPessoaTelefones.prototype._adicionarInput = function(descricao, telefone) {
    this.form.append(
        ' <div class="row">'
      
      + '   <div class="col-xs-6">'
      + '     <div class="form-group">'
      + '       <input class="form-control" list="lista-telefones-tipos" name="Descricao[]" type="text"'
      + '              value="' + descricao + '" placeholder="Descrição (Principal, Residencial, Celular, etc)">'
      + '     </div>'
      + '   </div>'
          
      + '   <div class="col-xs-3">'
      + '     <div class="form-group">'
      + '       <input class="form-control telefone-mask" name="Telefone[]" type="text"'
      + '              value="' + telefone +'" placeholder="(12) 12345678">'
      + '     </div>'
      + '   </div>'
          
      + '   <div class="col-xs-1">'
      + '     <a href="#" class="btn btn-danger modal-btn-pessoa-remover-telefone">'
      + '       <span class="glyphicon glyphicon-trash"></span>'
      + '     </a>' 
      + '   </div>'
      
      + ' </div>'
    );    
};


ModalPessoaTelefones.prototype._removerInput = function(divRow) {
    divRow.fadeOut("normal", function() { 
        $(this).remove(); 
    });
};


ModalPessoaTelefones.prototype._buscarTelefones = function() {
    var pessoaId = this.pessoaId;
    var mensagem = this.mensagem;
    var form = this.form;
    var obj = this;
        
    $.ajax({
        url: '/api/cadastro/pessoas/telefones/' + pessoaId,
        type: 'GET',
        dataType: 'json',
	
        beforeSend: function () {
            mensagem.html('<p>Aguarde ...</p>').fadeIn();
        },
            
        complete: function () {
            mensagem.html('').fadeOut();
        },
	
        success: function (data, textStatus, xhr) {
	        form.append('<input name="PessoaId" type="hidden" value="' + pessoaId + '">');
                
	        $.each(data, function(i, telefone){
                obj._adicionarInput(telefone.Descricao, telefone.Telefone);         
            });
	    }
    });    
};


ModalPessoaTelefones.prototype._salvarTelefones = function() {
    var mensagem = this.mensagem;
    var form = this.form;
    var obj = this;
    
    var formInputsButtons = $("#box-modal-pessoa-telefones :input, #box-modal-pessoa-telefones a.btn"); 
    var data = this.form.serialize();
    
    $.ajax({
        url: '/api/cadastro/pessoas/telefones/salvar',
        type: 'POST',
        data: data,
        dataType: 'json',
            
        beforeSend: function() {
            formInputsButtons.attr("disabled", "disabled");                
            mensagem.html('<p>Aguarde, salvando as alterações ...</p>').fadeIn();
        },
            
        complete: function() {
            formInputsButtons.removeAttr("disabled");                
        },
	
        success: function (data, textStatus, xhr) {
	        mensagem.html('<p>Alterações salvas com sucesso!</p>').fadeIn();
	    },
            
        error: function (data, textStatus, xhr) {
	        mensagem.html('<p>Não foi possível salvar as alterações. Tente novamente em alguns minutos!</p>').fadeIn();
	    }
    });    
};


function inputMask() {
    $(".money-mask").maskMoney({symbol:'R$ ', thousands:'.', decimal:',', symbolStay: true});
    
    $(".placa-veiculo-mask").inputmask("mask", {"mask": "aaa-9999"});
        
    $(".horario-mask").inputmask("mask", {"mask": "99:99:99"});
        
    $(".cep-mask").inputmask("mask", {"mask": "99999-999"});
    
    $(".cpf-mask").inputmask("mask", {"mask": "999.999.999-99"});
      
    $(".cnpj-mask").inputmask("mask", {"mask": "99.999.999/9999-99"});
            
    $(".telefone-mask").inputmask("mask", {
        "mask": "(99) [9]99999999999",
        greedy: false,
    });
      
    $(".cpf-cnpj-mask").inputmask("mask", {
        "mask": "99999999999[999]",
        greedy: false,
    });
}


$(document).ready(function () {
 
    inputMask();    

    
    $(".chosen-select").chosen({
        allow_single_deselect: true,
        no_results_text: "Nenhum resultado encontrado!",
        width: "100%"
    });
  
    
    $('.datepicker').datepicker({
        format: 'dd/mm/yyyy',
        language: "pt-BR"
    }).on('changeDate', function(ev) {
        $('.datepicker').datepicker('hide');
    });
    
    
    $("select.estado-select").change(function() {
        var childrenSelectId = '#' + $(this).attr('data-children');
      
        $(childrenSelectId).empty();
      
        $.ajax({
            url: '/cadastro/localidades?UF_Id=' + this.value,
            type: 'GET',
            dataType: 'json',
	
            beforeSend: function () {
                $(childrenSelectId).prop("disabled", true).trigger("chosen:updated");
            },
	
            success: function (data, textStatus, xhr) {
	            $(childrenSelectId).append('<option value=""><option>');
            
	            $.each(data, function(i, dataOption){
                    $(childrenSelectId).append('<option value="' + dataOption.Id + '">' + dataOption.Nome +'<option>');
                });
	  
	            $(childrenSelectId).prop("disabled", false).trigger("chosen:updated");
	        }
        });
    });
    
          
    $("select.pais-select").change(function() {
        var estadoSelectId = '#' + $(this).data('estado-id');
        var cidadeSelectId = '#' + $(this).data('cidade-id');
      
        $(estadoSelectId).empty().trigger("chosen:updated");
        $(cidadeSelectId).empty().trigger("chosen:updated");
      
        $.ajax({
            url: '/api/cadastro/estados?Pais_Id=' + this.value,
            type: 'GET',
            dataType: 'json',
	
            beforeSend: function () {
                $(estadoSelectId).prop("disabled", true).trigger("chosen:updated");
            },
	
            success: function (data, textStatus, xhr) {
	            $(estadoSelectId).append('<option value=""><option>');
            
	            $.each(data, function(i, dataOption){
                    $(estadoSelectId).append('<option value="' + dataOption.Id + '">' + dataOption.Nome +'<option>');
                });
	  
	            $(estadoSelectId).prop("disabled", false).trigger("chosen:updated");
	        }
        });
    });    
 
  
    $("input#pessoa-documento").blur(function() {
        var input = $(this);
        var documento = input.val();
      
        $("div#pessoa-documento-erro").html('');
      
        if (documento != '') {
            $.ajax({
                url: '/api/cadastro/pessoas/validarDocumento',
                type: 'POST',
                dataType: 'json',
	            data: {
                    Documento: documento,
                    Tipo: $('input#Tipo').val()
                },
    
                beforeSend: function () {
                    $("div#pessoa-documento-erro").html('Validando o documento ...');
                },
        
                success: function (data, textStatus, xhr) {
	                if (data.mensagem == 'CREATE') {
	                    $("div#pessoa-documento-erro").html('');
	
	                } else if (data.mensagem == 'EDIT') {
	                    window.location = data.url;  	    
                    }	       
                },
            
                error: function (data, textStatus, xhr) {
                    $("div#pessoa-documento-erro").html(data.responseJSON.mensagem);
	                input.focus();
                }
            });
        }
      
        return false;
    });
  
  
    $(".btn-voltar-pagina").click(function() {
        window.history.back();
        return false;
    });
  
    
    $('a.submit-form-delete').click(function() {
        if (confirm('Deseja realmente excluir esse registro?')) {
            $(this).parent().submit();    
        }    
    });

  
    $('.search-button').click(function() {
        $('div#form-search').toggle();
    });
    
    
    $('.btn-pessoa-telefones').click(function() {
        var eventoAoFechar = null;
        
        var pessoaId = $(this).data('pessoa-id');
        var recarregar = $(this).data('recarregar-pagina');
        
        if (recarregar) {
            eventoAoFechar = function() {
                window.location.reload();
            };
        } 
        
        var modalPessoaTelefones = new ModalPessoaTelefones(pessoaId);
        modalPessoaTelefones.modal(eventoAoFechar);
    });
    
  
    $("select.linha-select").change(function() {
        var childrenSelectId = '#' + $(this).attr('data-children');
      
        $(childrenSelectId).empty();
      
        $.ajax({
            url: '/api/cadastro/itensLinha?Linha_Id=' + this.value,
            type: 'GET',
            dataType: 'json',
	
            beforeSend: function () {
                $(childrenSelectId).prop("disabled", true).trigger("chosen:updated");
            },
	
            success: function (data, textStatus, xhr) {
	            $(childrenSelectId).append('<option value=""><option>');
            
	            $.each(data, function(i, dataOption){
                    $(childrenSelectId).append('<option value="' + dataOption.ID + '">' + dataOption.Descricao +'<option>');
                });
	  
	            $(childrenSelectId).prop("disabled", false).trigger("chosen:updated");
	        }    
        });
    });

  
    $("select.horario-select").change(function() {
        var childrenSelectId = '#' + $(this).attr('data-children');
      
        $(childrenSelectId).empty();
      
        $.ajax({
            url: '/api/cadastro/pontosReferencia?Horario_Id=' + this.value,
            type: 'GET',
            dataType: 'json',
	
            beforeSend: function () {
                $(childrenSelectId).prop("disabled", true).trigger("chosen:updated");
            },
	
            success: function (data, textStatus, xhr) {
	            $(childrenSelectId).append('<option value=""><option>');
            
	            $.each(data, function(i, dataOption){
                     $(childrenSelectId).append('<option value="' + dataOption.Id + '">' + dataOption.Descricao +'<option>');
                });
	  
	            $(childrenSelectId).prop("disabled", false).trigger("chosen:updated");
	        }
        });
    });
    
    $('input#tipo-veiculo-tarifa-unica').change(function() {
        $('div#box-tipo-veiculo-tarifa-unica label').html('Valor da tarifa:');
        
        if (this.checked) {
            $('div#box-tipo-veiculo-tarifa-mista').hide();    
        }
    });
    
    
    $('input#tipo-veiculo-tarifa-mista').change(function() {
        $('div#box-tipo-veiculo-tarifa-unica label').html('Valor da tarifa (completo/superior/esquerdo):');
        
        if (this.checked) {
            $('div#box-tipo-veiculo-tarifa-mista').fadeIn();    
        }
    });
    
    $('table.mapa-veiculo tbody tr td a').editable({
        type: 'text',
        title: '',
        emptytext: '',
    
        url: '/api/cadastro/mapa',
        mode: 'inline',
        send: 'always',
    
        params: function(params) {
            params.TipoVeiculo_Id = $('#tipoVeiculoId').val();
            params.Andar  = $(this).parent().parent().parent().parent().attr('data-andar');
            params.Coluna = parseInt($(this).parent().prop('cellIndex')) + 1;
            params.Linha  = $(this).parent().parent().prop('rowIndex');
            params.Numero = params.value;
	
            return params;
        },
    
        success: function(response, newValue) {
            if (!response.success) return response.msg;
            return true;
        }
    });
    
});