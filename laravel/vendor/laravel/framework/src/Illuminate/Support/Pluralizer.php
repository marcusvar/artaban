<?php namespace Illuminate\Support;

class Pluralizer {

	/**
	 * Plural word form rules.
	 *
	 * @var array
	 */
	protected static $plural = array(
		'/^(paí)s$/i'     => '\1ses',
        '/(z|r)$/i'       => '\1es',
        '/al$/i'          =>  'ais',
        '/el$/i'          =>  'eis',
        '/ol$/i'          =>  'ois',
        '/ul$/i'          =>  'uis',
        '/([^aeou])il$/i' =>  '\1is',
        '/m$/i'           =>   'ns',
        '/^(japon|escoc|ingl|dinamarqu|fregu|portugu)ês$/i' =>  '\1eses',
        '/^(|g)ás$/i'       =>  '\1ases',
        '/ão$/i'            =>  'ões',
        '/^(irm|m)ão$/i'    => '\1ãos',
        '/^(alem|c|p)ão$/i' =>  '\1ães',
        '/ao$/i'            =>  'oes',
        '/^(irm|m)ao$/i'    => '\1aos',
        '/^(alem|c|p)ao$/i' =>  '\1aes',
        '/(s)$/i'     =>  '\1',
        '/$/'         =>  's',
	);

	/**
	 * Singular word form rules.
	 *
	 * @var array
	 */
	protected static $singular = array(
        '/^(á|gá|paí)s$/i' =>  '\1s',
        '/(r|z)es$/i'      =>  '\1',
        '/([^p])ais$/i'    =>  '\1al',
        '/eis$/i'          =>  'el',
        '/ois$/i'          =>  'ol',
        '/uis$/i'          =>  'ul',
        '/(r|t|f|v)is$/i'  =>  '\1il',
        '/ns$/i'           =>  'm',
        '/sses$/i'         =>  'sse',
        '/^(.*[^s]s)es$/i' =>  '\1',
        '/ães$/i'          =>  'ão',
        '/aes$/i'          =>  'ao',
        '/ãos$/i'          =>  'ão',
        '/aos$/i'          =>  'ao',
        '/ões$/i'          =>  'ão',
        '/oes$/i'          =>  'ao',
        '/(japon|escoc|ingl|dinamarqu|fregu|portugu)eses$/i' => '\1ês',
        '/^(g|)ases$/i' => '\1ás',
        '/([^ê])s$/i'   =>  '\1',
	);

	/**
	 * Irregular word forms.
	 *
	 * @var array
	 */
	protected static $irregular = array(
	
	);

	/**
	 * Uncountable word forms.
	 *
	 * @var array
	 */
	protected static $uncountable = array(
		'tórax',
        'tênis',
        'ônibus',
        'lápis',
        'fênix'
	);

	/**
	 * The cached copies of the plural inflections.
	 *
	 * @var array
	 */
	protected static $pluralCache = array();

	/**
	 * The cached copies of the singular inflections.
	 *
	 * @var array
	 */
	protected static $singularCache = array();

	/**
	 * Get the singular form of the given word.
	 *
	 * @param  string  $value
	 * @return string
	 */
	public static function singular($value)
	{
		if (isset(static::$singularCache[$value]))
		{
			return static::$singularCache[$value];
		}

		$result = static::inflect($value, static::$singular, static::$irregular);

		return static::$singularCache[$value] = $result ?: $value;
	}

	/**
	 * Get the plural form of the given word.
	 *
	 * @param  string  $value
	 * @param  int     $count
	 * @return string
	 */
	public static function plural($value, $count = 2)
	{
		if ($count == 1) return $value;

		// First we'll check the cache of inflected values. We cache each word that
		// is inflected so we don't have to spin through the regular expressions
		// on each subsequent method calls for this word by the app developer.
		if (isset(static::$pluralCache[$value]))
		{
			return static::$pluralCache[$value];
		}

		$irregular = array_flip(static::$irregular);

		// When doing the singular to plural transformation, we'll flip the irregular
		// array since we need to swap sides on the keys and values. After we have
		// the transformed value we will cache it in memory for faster look-ups.
		$plural = static::$plural;

		$result = static::inflect($value, $plural, $irregular);

		return static::$pluralCache[$value] = $result;
	}

	/**
	 * Perform auto inflection on an English word.
	 *
	 * @param  string  $value
	 * @param  array   $source
	 * @param  array   $irregular
	 * @return string
	 */
	protected static function inflect($value, $source, $irregular)
	{
		if (static::uncountable($value)) return $value;

		// Next, we will check the "irregular" patterns which contain words that are
		// not easily summarized in regular expression rules, like "children" and
		// "teeth", both of which cannot get inflected using our typical rules.
		foreach ($irregular as $irregular => $pattern)
		{
			if (preg_match($pattern = '/'.$pattern.'$/i', $value))
			{
				$irregular = static::matchCase($irregular, $value);
				
				return preg_replace($pattern, $irregular, $value);
			}
		}

		// Finally, we'll spin through the array of regular expressions and look for
		// matches for the word. If we find a match, we will cache and return the
		// transformed value so we will quickly look it up on subsequent calls.
		foreach ($source as $pattern => $inflected)
		{
			if (preg_match($pattern, $value))
			{
				$inflected = preg_replace($pattern, $inflected, $value);

				return static::matchCase($inflected, $value);
			}
		}
	}

	/**
	 * Determine if the given value is uncountable.
	 *
	 * @param  string  $value
	 * @return bool
	 */
	protected static function uncountable($value)
	{
		return in_array(strtolower($value), static::$uncountable);
	}

	/**
	 * Attempt to match the case on two strings.
	 *
	 * @param  string  $value
	 * @param  string  $comparison
	 * @return string
	 */
	protected static function matchCase($value, $comparison)
	{
		$functions = array('mb_strtolower', 'mb_strtoupper', 'ucfirst', 'ucwords');

		foreach ($functions as $function)
		{
			if (call_user_func($function, $comparison) === $comparison)
			{
				return call_user_func($function, $value);
			}
		}

		return $value;
	}

}
