<?php

namespace Way\Generators\Generators;

class ModelGenerator extends Generator {

    /**
     * Fetch the compiled template for a model
     *
     * @param  string $template Path to template
     * @param  string $className
     * @return string Compiled template
     */
    protected function getTemplate($template, $className)
    {
        $this->template = $this->file->get($template);

        if ($this->needsScaffolding($template))
        {
            $this->template = $this->getScaffoldedModel($className);
        }

        return str_replace('{{className}}', $className, $this->template);
    }

    /**
     * Get template for a scaffold
     *
     * @param  string $template Path to template
     * @param  string $name
     * @return string
     */
    protected function getScaffoldedModel($className) {
        
        if (! $fields = $this->cache->getFields()) {
            return str_replace('{{rules}}', '', $this->template);
        }

        $rules = array_map(function($field) {
            return "'$field' => 'required'";
        }, array_keys($fields));
        
        
        $relations = "";
        foreach($fields as $field => $type) {
            if (preg_match('@_id$@', $field)) {
                $relModel = preg_replace('@(_id)$@', '', $field);
                $relModels = explode("_", $relModel);
                        
                $relModels = array_map(function($_model) {
                    return ucwords($_model);                                        
                }, $relModels);
                        
                $relModel = implode("", $relModels);
                $function = lcfirst($relModel);
                                 
                $relations .= "public function $function() { \n "
                           .  "\t    return \$this->belongsTo('$relModel'); \n" 
                           .  "\t} \n";        
            }
        }
        
        $template = str_replace('{{relations}}', $relations, $this->template);
        
        return str_replace('{{rules}}', PHP_EOL."\t\t".implode(','.PHP_EOL."\t\t", $rules) . PHP_EOL."\t", $template);
    }

}
