@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  {{models}}
@stop


@section('submenu')
  <a href="{{ route('cadastro.{{models}}.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.{{models}}.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if (${{models}}->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.{{models}}.index')) }}
     
       {{formElements}}
         
       {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  {{headings}}
	</tr>
      </thead>

      <tbody>
        @foreach (${{models}} as ${{model}})
            <tr>
	        {{fields}}
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo ${{models}}->appends(Input::except('page'))->links(); ?>
@else
    Não há {{models}}
@endif

@stop