<?php
namespace Way\Generators\Generators;

use Illuminate\Support\Pluralizer;

class ViewGenerator extends Generator {

    /**
     * Fetch the compiled template for a view
     *
     * @param  string $template Path to template
     * @param  string $name
     * @return string Compiled template
     */
    protected function getTemplate($template, $name)
    {
        $this->template = $this->file->get($template);

        if ($this->needsScaffolding($template))
        {
            return $this->getScaffoldedTemplate($name);
        }

        // Otherwise, just set the file
        // contents to the file name
        return $name;
    }

    /**
     * Get the scaffolded template for a view
     *
     * @param  string $name
     * @return string Compiled template
     */
    protected function getScaffoldedTemplate($name)
    {
        $model = $this->cache->getModelName();  // post
        $models = Pluralizer::plural($model);   // posts
        $Models = ucwords($models);             // Posts
        $Model = Pluralizer::singular($Models); // Post

        // Create and Edit views require form elements
       // if ($name === 'create.blade' or $name === 'edit.blade')
       if (in_array($name, ['_form.blade', 'index.blade']) ) {
            $searchForm = $name === 'index.blade'; 
            $formElements = $this->makeFormElements($searchForm);
            $this->template = str_replace('{{formElements}}', $formElements, $this->template);
       }
       
        // Replace template vars in view
        foreach(array('model', 'models', 'Models', 'Model') as $var) {
            $this->template = str_replace('{{'.$var.'}}', $$var, $this->template);
        }

        if ($name === 'show.blade') {
            $this->template = str_replace('{{details}}', $this->makeDetailsView($model), $this->template);
        } else {
            // And finally create the table rows
            list($headings, $fields, $editAndDeleteLinks) = $this->makeTableRows($model);
            $this->template = str_replace('{{headings}}', implode(PHP_EOL."\t\t\t\t", $headings), $this->template);
            $this->template = str_replace('{{fields}}', implode(PHP_EOL."\t\t\t\t\t", $fields) . PHP_EOL . $editAndDeleteLinks, $this->template);
        }
        
        return $this->template;
    }
    
    
    /**
     * Create the details view
     *
     * @param  string $model
     * @return Array
     */
    protected function makeDetailsView($model) {
        $models = Pluralizer::plural($model); // posts
        $fields = $this->cache->getFields();
        
        $result = "";
        foreach($fields as $field => $type) {
            if (preg_match('@_Id$@', $field)) {
                $relModel = preg_replace('@(_Id)$@', '', $field);
                $h3 = "<h2 class=\"detalhes\">" . ucwords($relModel) . "</h2>";
                $p = "<p class=\"detalhes\">{{{ \${$model}->{$relModel}->nome }}}</p>";
            } else {
                $h3 = "<h2 class=\"detalhes\">" . ucwords($field) . "</h2>";
                $p = "<p class=\"detalhes\">{{{ \${$model}->{$field} }}}</p>";   
            }
            
            $result .= $h3 . "\n";
            $result .= $p  . "\n\n";            
        }
                
        return $result;
    }


    /**
     * Create the table rows
     *
     * @param  string $model
     * @return Array
     */
    protected function makeTableRows($model)
    {
        $models = Pluralizer::plural($model); // posts

        $fields = $this->cache->getFields();

        // First, we build the table headings
        $headings = array_map(function($field) {
            if (preg_match('@_Id$@', $field)) {
                $field = preg_replace('@(_Id)$@', '', $field);
            }            
            
            return '<th>' . ucwords($field) . '</th>';            
        }, array_keys($fields));
                
        $headings[] = '<th>Ações</th>'; 

        
        // And then the rows, themselves
        //$fields = array_map(function($field) use ($model) {
        //    return "<td>{{{ \$$model->$field }}}</td>";
        //}, array_keys($fields));

        
        $fieldsArr = [];
        foreach($fields as $field => $type) {
            if (preg_match('@_Id$@', $field)) {
                $relModel = preg_replace('@(_Id)$@', '', $field);
                $relModels = explode("_", $relModel);
                        
                $relModels = array_map(function($_model) {
                    return ucwords($_model);                                        
                }, $relModels);
                        
                $relModel = lcfirst(implode("", $relModels));
                                
                $fieldsArr[] = "<td>{{{ \${$model}->{$relModel}->nome }}}</td>";        
            } else {
                $fieldsArr[] = "<td>{{{ \$$model->$field }}}</td>";    
            }
        }
        
        
        // Now, we'll add the edit and delete buttons.
        $editAndDelete = <<<EOT
                    <td>{{ link_to_route('cadastro.{$models}.show',
                                         'Detalhes',
                                         array(\${$model}->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
                        {{ link_to_route('cadastro.{$models}.edit',
                                         'Editar',
                                         array(\${$model}->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
                        <div class="pull-left">
                          {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.{$models}.destroy', \${$model}->Id))) }}
                            <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                          {{ Form::close() }}
                        </div>
                    </td>
EOT;

        return array($headings, $fieldsArr, $editAndDelete);
    }

    /**
     * Add Laravel methods, as string,
     * for the fields
     *
     * @return string
     */
    public function makeFormElements($searchForm = false) {
        $formMethods = array();

        foreach($this->cache->getFields() as $name => $type) {
            $formalName = ucwords($name);

            // TODO: add remaining types
            switch($type) {
                case 'integer':
                    if (preg_match('@_Id$@', $name)) {
                        $relModel = preg_replace('@(_Id)$@', '', $name);
                        $relModels = explode("_", $relModel);
                        
                        $relModels = array_map(function($_model) {
                            return ucwords($_model);                                        
                        }, $relModels);
                        
                        $relModel = implode("", $relModels);
                        $formalName = $relModel;
                        
                        try {
                            $optionDescription = 'Descricao';
                            \DB::getDoctrineColumn($relModel, $optionDescription);                            
                        } catch (\Exception $e) {
                            $optionDescription = 'Nome';
                        }
                        
                        $options = "$relModel::lists('$optionDescription', 'Id')";
                        if ($searchForm) {
                            $options = "array('' => '') + " . $options;    
                        } 
                                                
                        $element = "{{ Form::select('$name',  $options, null, array('class' => 'form-control')) }}";    
                    } else {
                        $element = "{{ Form::input('number', '$name') }}";
                    }
                    
                    break;

                case 'text':
                    $element = "{{ Form::textarea('$name') }}";
                    break;

                case 'boolean':
                    $element = "{{ Form::checkbox('$name') }}";
                    break;

                default:
                    $element = "{{ Form::text('$name', null, array('class' => 'form-control')) }}";
                  //  $element .= '<input type="text" class="form-control" name="' . $name . '" id="' . $name . '">';
                    break;
            }

            // Now that we have the correct $element,
            // We can build up the HTML fragment
            $frag = <<<EOT
  <div class="form-group">
    <label for="$name">$formalName:</label>
    $element
  </div>
  
EOT;

            $formMethods[] = $frag;
        }

        return implode(PHP_EOL, $formMethods);
    }

}