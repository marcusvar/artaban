<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| such as the size rules. Feel free to tweak each of these messages.
	|
	*/

	"accepted"       => "O campo :attribute deve ser aceito.",
	"active_url"     => "A URL :attribute não é válida.",
	"after"          => "A data :attribute deve ser posterior a :date.",
	"alpha"          => "O campo :attribute deve conter somente letras.",
	"alpha_dash"     => "O campo :attribute deve conter somente letras, números e travessões.",
	"alpha_num"      => "O campo :attribute deve conter somente letras e números.",
	"before"         => "A data :attribute deve ser anterior à :date.",
	"between"        => array(
		"numeric" => "O campo :attribute deve ficar entre :min - :max.",
		"file"    => "O arquivo :attribute deve estar entre :min - :max kilobytes.",
		"string"  => "O campo :attribute deve ser entre :min - :max caracteres.",
	),
	"confirmed"      => "O campo :attribute de confirmação não equivale.",
	"different"      => "O campo :attribute e :other devem ser diferentes.",
	"email"          => "O campo :attribute não é um email válido.",
	"exists"         => "O campo selecionado :attribute e inválido.",
	"image"          => "O campo :attribute deve ser uma imagem.",
	"in"             => "O campo selecionado :attribute e inválido.",
	"integer"        => "O campo :attribute deve ser um número inteiro.",
	"ip"             => "O campo :attribute deve ser um endereço IP válido.",
	"match"          => "O formato do campo :attribute é inválido.",
	"max"            => array(
		"numeric" => "O campo :attribute deve ser menor que :max.",
		"file"    => "O arquivo :attribute deve ser menos que :max kilobytes.",
		"string"  => "O campo :attribute deve ser menos que :max caracteres.",
	),
	"mimes"          => "O campo :attribute deve ser um arquivo dos tipos: :values.",
	"min"            => array(
		"numeric" => "O campo :attribute deve ser pelo menos :min.",
		"file"    => "O arquivo :attribute deve ser mais que :min kilobytes.",
		"string"  => "O campo :attribute deve ter pelo menos :min caracteres.",
	),
	"not_in"         => "O campo selecionado :attribute é inválido.",
	"numeric"        => "O campo :attribute deve ser um número.",
	"required"       => "O campo :attribute é obrigatório.",
	"same"           => "O campo :attribute e :other devem ser iguais.",
	"size"           => array(
		"numeric" => "O campo :attribute deve ter tamanho :size.",
		"file"    => "O arquivo :attribute deve ter :size kilobyte.",
		"string"  => "O campo :attribute deve ter :size caracteres.",
	),
	"unique"         => "O campo :attribute já foi escolhido.",
	"url"            => "A URL :attribute possui um formato inválido.",
    
    "cpf"   => "Esse número de CPF é inválido.",
    "cnpj"  => "Esse número de CNPJ é inválido.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(),
    
    'form' => array(
        'invalid' => 'Por favor, corrija os erros abaixo antes de continuar:'                 
    ),
    
    
    'record' => array(
        'dont_delete' => 'Esse registro não pode ser excluido porque é necessário para o sistema.'                 
    ),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(
            'Descricao'              => 'Descrição',
            'TipoAdicionalTarifa_Id' => 'Tipo de adicional',
            'DataVigencia'           => 'Data da vigência',
            'Linha_Id'               => 'Linha',
            'TipoVeiculo_Id'         => 'Tipo de veículo',
            'Operacao'               => 'Operação',
            'DataOperacao'           => 'Data da operação',
            'ValorOperacao'          => 'Valor da operação',
            'Localidade_Id'          => 'Localidade',
            'Duracao'                => 'Duração',
            'Sequencia'              => 'Sequência',
            'DataInicio'             => 'Data inicial',
            'HoraInicio'             => 'Hora inicial'
        ),

);
