<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

 
# -----------------------------------------------------
# Login
# -----------------------------------------------------
Route::get('/login', array('as' => 'login', 'before' => 'guest', 'uses' => 'AutorizacaoController@loginForm'));
    	
Route::post('/login', array('uses' => 'AutorizacaoController@login'));


# -----------------------------------------------------
# Auth
# -----------------------------------------------------
Route::group(array('before' => 'auth'), function() {
    
    Route::get('logout', array('as' => 'logout', 'uses' => 'AutorizacaoController@logout'));    
    
    Route::get('/', array('as' => 'index', 'uses' => 'HomeController@index'));
    
    Route::resource('usuarios', 'UsuariosController');
    
    Route::resource('grupos', 'GruposController');
    
    Route::get('usuarios/{usuario_id}/permissoes', array('as' => 'permissoes.index', 'uses' => 'PermissoesController@index'));
    
    Route::get('grupos/{grupo_id}/permissoes', array('as' => 'permissoes.grupo', 'uses' => 'PermissoesController@grupo'));
    
    Route::resource('cadastro/estados', 'Cadastro\EstadosController');

    Route::resource('cadastro/localidades', 'Cadastro\LocalidadesController');
   
    Route::resource('cadastro/pessoas', 'Cadastro\PessoasController');
    
    Route::resource('cadastro/pessoas.fidelizacoes', 'Cadastro\FidelizacoesController'); 
    
    Route::resource('cadastro/pessoas.vinculados',
                    'Cadastro\VinculadosController',
                    array('only' => array('index', 'create', 'store', 'destroy')));
            
    Route::resource('cadastro/tiposVeiculo', 'Cadastro\TiposVeiculoController');
    
    Route::resource('cadastro/tiposAdicionaisTarifas', 'Cadastro\TiposAdicionaisTarifasController');
    
    Route::get('cadastro/tiposVeiculo/{tipoVeiculoId}/mapa',
               array(
	               'as' => 'cadastro.tiposVeiculo.mapa.index',
	               'uses' => 'Cadastro\MapasController@index'
               )
    );
    
    Route::resource('cadastro/veiculos', 'Cadastro\VeiculosController');
       
    Route::resource('cadastro/pontosReferencia', 'Cadastro\PontosReferenciaController');
    
    Route::resource('cadastro/linhas', 'Cadastro\LinhasController');
 
    Route::resource('cadastro/linhas.adicionaisTarifa', 'Cadastro\AdicionaisTarifaController');
 
    Route::resource('cadastro/linhas.itensLinha', 'Cadastro\ItensLinhaController');
   
    Route::resource('cadastro/linhas.itensLinha.tarifas', 'Cadastro\TarifasController');
    
    Route::resource('cadastro/horarios', 'Cadastro\HorariosController');
      
    Route::resource('cadastro/horarios.itensHorario', 'Cadastro\ItensHorarioController');
    
    Route::resource('cadastro/viagens', 'Cadastro\ViagensController');

    Route::resource('cadastro/viagens.itensViagem', 'Cadastro\ItensViagemController'); 
   
    Route::resource('cadastro/parametros', 'Cadastro\ParametrosController');  
    
    Route::resource('cadastro/descontosGerais', 'Cadastro\DescontosGeraisController');
    
    
    # -----------------------------------------------------
    # Ext
    # -----------------------------------------------------
    Route::group(array('prefix' => 'ext'), function() {
        Route::get('/reservarPoltrona', array('uses' => 'ExtController@reservarPoltrona'));
        Route::get('/listas', array('uses' => 'ExtController@listas'));
    });
    
    
    # -----------------------------------------------------
    # Gerenciamento
    # -----------------------------------------------------
    Route::group(array('prefix' => 'gerenciamento'), function() {
        Route::get('/gerarViagens', array('as' => 'gerenciamento.formGerarViagens', 'uses' => 'Gerenciamento\ViagensController@formGerar'));
        
        Route::post('/gerarViagens', array('as' => 'gerenciamento.gerarViagens', 'uses' => 'Gerenciamento\ViagensController@gerar'));
    });
   

    # -----------------------------------------------------
    # Api
    # -----------------------------------------------------
    Route::group(array('prefix' => 'api'), function() {
        
        # -----------------------------------------------------
        # Venda
        # -----------------------------------------------------
        Route::group(array('prefix' => 'venda'), function() {
            
            Route::get('/usuario', array('uses' => 'API\Venda\UsuariosController@autenticado'));
            
            Route::get('/estados', array('uses' => 'API\Venda\EstadosController@index'));
            
            Route::post('/cidades/porEstado', array('uses' => 'API\Venda\CidadesController@porEstado'));
                                
            Route::get('/linhas', array('uses' => 'API\Venda\LinhasController@index'));
            
            Route::post('/itensLinha', array('uses' => 'API\Venda\LinhasController@itens'));
            
            Route::post('/horarios', array('uses' => 'API\Venda\HorariosController@index'));
            
            Route::get('/pessoas', array('uses' => 'API\Venda\PessoasController@index'));
            
            Route::post('/pessoas/buscarPorDocumento', array('uses' => 'API\Venda\PessoasController@buscarPorDocumento'));
            
            Route::post('/pessoas/buscarPorId', array('uses' => 'API\Venda\PessoasController@buscarPorId'));
                        
            Route::post('/pessoas/fidelizacao', array('uses' => 'API\Venda\PessoasController@fidelizacao'));
                        
            Route::post('/passagens/alterar', array('uses' => 'API\Venda\PassagensController@alterar'));
            
            Route::post('/passagens/cancelar', array('uses' => 'API\Venda\PassagensController@cancelar'));
            
            Route::post('/passagens/buscarPassageiro', array('uses' => 'API\Venda\PassagensController@buscarPassageiro'));
            
            Route::post('/viagens/mapa', array('uses' => 'API\Venda\ViagensController@mapa'));
            
        });
        
        
        # -----------------------------------------------------
        # Cadastro
        # -----------------------------------------------------
	    Route::group(array('prefix' => 'cadastro'), function() {

            Route::get('/pessoas/form/{documento}',
		               array(
		                   'as' => 'api.cadastro.pessoas.form',
		                   'uses' => '\Cadastro\PessoasController@form'
		               )
	        );
            
            
            Route::post('/pessoas/validarDocumento',
		               array(
		                   'as' => 'api.cadastro.pessoas.validarDocumento',
		                   'uses' => 'API\Cadastro\PessoasController@validarDocumento'
		               )
	        );
            
            
            Route::get('/pessoas/telefones/{pessoaId}',
		               array(
		                   'as' => 'api.cadastro.pessoas.telefones',
		                   'uses' => 'API\Cadastro\PessoasTelefonesController@index'
		               )
	        );
            
            
            Route::post('/pessoas/telefones/salvar',
		               array(
		                   'as' => 'api.cadastro.pessoas.telefones.salvar',
		                   'uses' => 'API\Cadastro\PessoasTelefonesController@salvar'
		               )
	        );
            
            
            Route::get('/estados',
		               array(
		                  // 'as' => 'api.cadastro.pessoas.form',
		                   'uses' => 'API\Cadastro\EstadosController@porPais'
		               )
	        );
        
            Route::get('/itensLinha',
		               array(
		                   'as' => 'api.cadastro.itensLinha.index',
		                   'uses' => 'API\Cadastro\ItensLinhaController@index'
		               )
	        );
	    
	        Route::get('/pontosReferencia',
		               array(
		                   'as' => 'api.cadastro.pontosReferencia.index',
		                   'uses' => 'API\Cadastro\PontosReferenciaController@index'
		               )
	        );
 
	        Route::post('/mapa',
		                array(
		                    'as' => 'api.cadastro.mapa.store',
		                    'uses' => 'API\Cadastro\MapasController@store'
		                )
	        );
	    
	    }); 
  
    });    
});