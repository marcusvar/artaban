<?php
namespace Artaban\API\Venda;

use Artaban\Model\Linha as ModelLinha;


class Linha {
    
    
    public static function listarDaEmpresa($empresaId) {
        $linhas = ModelLinha::daEmpresa($empresaId)
                            ->with('pontoReferencia', 'pontoReferencia.localidade', 'pontoReferencia.localidade.estado')
                            ->get();
        
        $listaDelinhas = [];
        foreach($linhas as $linha) {
            $listaDelinhas[] = array(
                'id'        => $linha->ID,
                'descricao' => $linha->pontoReferencia->descricaoCompleta()
            );
        }
        
        return $listaDelinhas;                    
    }
    	
        
    public static function listarItens($empresaId, $linhaId) {
        $linha = ModelLinha::daEmpresa($empresaId)
                           ->with('itens', 'itens.pontoReferencia', 'itens.pontoReferencia.localidade',
                                  'itens.pontoReferencia.localidade.estado')
                           ->find($linhaId);
        
        $listaDeItens = [];
        foreach($linha->itens as $item) {
            $listaDeItens[] = array(
                'id' => $item->ID,
                'descricao' => $item->pontoReferencia->descricaoCompleta()
            );
        }
        
        return $listaDeItens;        
    }
    
    
}