<?php
namespace Artaban\API\Venda;

use Artaban\Model\Horario as ModelHorario;
use Artaban\Model\Linha as ModelLinha;
use Artaban\Model\ItemLinha as ModelItemLinha;

use DB;
use DateTime;
use DateInterval;


class Horario {
    
    
    public static function listarDaEmpresa($empresaId, $linhaId, $linhaItemId) {
        $linha = ModelLinha::daEmpresa($empresaId)->find($linhaId);
        
        $itemLinha = ModelItemLinha::daEmpresa($empresaId)
                                   ->where('Linha_Id', '=', $linhaId)
                                   ->find($linhaItemId);
        
        if (($linha == null) || ($itemLinha == null)) {
            return [];
        }
        
        $origemId = $itemLinha->PontoReferencia_Id;
        $destinoId = $linha->PontoReferencia_Id_Destino;
                
        $select = DB::select('CALL Proc_ConsHorarios(NULL, ?, ?)', [$origemId, $destinoId]);
        
        $listaDeHorarios = [];
        foreach($select as $horario) {
            $partida = DateTime::createFromFormat('Y-m-d H:i:s', "$horario->DataInicio $horario->HoraInicio");
            $retorno = DateTime::createFromFormat('Y-m-d H:i:s', "$horario->DataInicio $horario->HoraInicio");
            
            $duracao = (int) $horario->Duracao;
            $duracao = $duracao > 0 ? $duracao : 1;
            
            $retorno->add(new DateInterval('P' . $duracao . 'D'));
            
            $duracao = $duracao . " dia(s)";
            
            $partida = $partida->format('d/m/Y H:i');
            $retorno = $retorno->format('d/m/Y H:i');
                        
            $listaDeHorarios[] = array(
                "viagemId" => $horario->Id,
                "partida"  => $partida,
                "retorno"  => $retorno,
                "servico"  => $horario->TipoVeiculo_Descricao,
                "valor"    => 100,
                "duracao"  => $duracao    
            );
        }
        
        return $listaDeHorarios;
    }
    	
}