<?php
namespace Artaban\API\Venda;

use Artaban\Model\Usuario as ModelUsuario;

use Auth;


class Usuario {
    
    
    public static function autenticado() {
        $usuario = ModelUsuario::find(Auth::user()->Id);
        
        return array(
            'Id' => $usuario->Id,
            'Nome' => $usuario->Nome,
            'Email' => $usuario->Email,
            'PessoaId' => $usuario->Pessoa_Id,
            'PontoReferenciaIdAgencia' => $usuario->PontoReferencia_Id_Agencia,
            'Data' => date('Y-m-d')."T12:00:00"
        );       
    }	
}
