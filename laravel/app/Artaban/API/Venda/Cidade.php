<?php
namespace Artaban\API\Venda;

use Artaban\Model\Localidade as ModelLocalidade;


class Cidade {
    
    
    public static function listarPorEstado($estadoId) {
        $cidades = ModelLocalidade::where('UF_Id', '=', $estadoId)
                                  ->orderBy('Nome')
                                  ->get(['Id', 'Nome'])
                                  ->toArray();
        
        return $cidades;                    
    }
       
}