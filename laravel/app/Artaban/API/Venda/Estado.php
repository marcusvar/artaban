<?php
namespace Artaban\API\Venda;

use Artaban\Model\Estado as ModelEstado;


class Estado {
    
    
    public static function listar() {
        $estados = ModelEstado::doBrasil()
                              ->orderBy('Nome')
                              ->get(['Id', 'Nome', 'Sigla'])
                              ->toArray();
        
        return $estados;                    
    }
       
}