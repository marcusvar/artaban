<?php
namespace Artaban\API\Venda;

use DB;


class Viagem {
    
    
    public static function mapa($viagemId) {
         $mapaDB = DB::select('CALL Proc_ConsMapaLugarViagem(?)', [$viagemId]);
         
         $mapa = [];
         foreach($mapaDB as $item) {
            $andar  = $item->Andar;
            $linha  = ((int) $item->Linha) - 1;
            $coluna = $item->Coluna;
            
            $mapa['Piso' . $andar][$linha]['Numero'      . $coluna] = $item->Numero; 
            $mapa['Piso' . $andar][$linha]['Situacao'    . $coluna] = $item->Situacao;
            $mapa['Piso' . $andar][$linha]['Promocao'    . $coluna] = $item->Promocao;
            $mapa['Piso' . $andar][$linha]['Passagem_Id' . $coluna] = $item->Passagem_Id;
         }
                  
         return $mapa;
    }	
}