<?php
namespace Artaban\API\Venda;

use Artaban\Model\Cliente as ModelCliente;


class Cliente {
    
    
    public static function listarDaEmpresa($empresaId) {
        $clientes = ModelCliente::daEmpresa($empresaId)->orderBy('Nome')->get();
        
        $listaDeClientes = [];
        foreach($clientes as $cliente) {
            $listaDeClientes[] = array(
                'id'        => $cliente->Id,
                'nome'      => $cliente->Nome,
                'telefone'  => $cliente->Telefone,
                'email'     => $cliente->Email,
                'sexo'      => $cliente->Sexo
            );
        }
                                     
        return $listaDeClientes;                    
    }
   
   
    public static function buscarPorCpfCnpj($empresaId, $cpfCnpj) {
        $cliente = ModelCliente::daEmpresa($empresaId)
                               ->where('CPFCNPJ', '=', $cpfCnpj)
                               ->first();
        
        $clienteArray = ['id' => '', 'nome' => '', 'telefone' => '', 'email' => '', 'sexo' => ''];
        
        if ($cliente) {
            $clienteArray = array(
                'id'       => $cliente->Id,
                'nome'     => $cliente->Nome,
                'telefone' => $cliente->Telefone,
                'email'    => $cliente->Email,
                'sexo'     => $cliente->Sexo
            );
        } 
        
        return $clienteArray;
    }
    
    
    public static function buscarPorId($empresaId, $clienteId) {
        $cliente = ModelCliente::daEmpresa($empresaId)->find($clienteId);
                               
        $clienteArray = ['id' => '', 'nome' => '', 'telefone' => '', 'email' => ''];
        
        if ($cliente) {
            $clienteArray = array(
                'id'       => $cliente->Id,
                'nome'     => $cliente->Nome,
                'telefone' => $cliente->Telefone,
                'email'    => $cliente->Email,
                'sexo'     => $cliente->Sexo
            );
        } 
        
        return $clienteArray;
    }
    
    
    public static function buscarSaldoDeFidelizacao($empresaId, $clienteId) {
        $cliente = ModelCliente::daEmpresa($empresaId)->find($clienteId);
        
        $saldo = ["valor" => 0];
        
        if ($cliente) {
            $saldo = ["valor" => $cliente->saldoDeFidelizacao()];
        }
        
        return $saldo;
    }
       
}