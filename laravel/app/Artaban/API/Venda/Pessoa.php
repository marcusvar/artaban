<?php
namespace Artaban\API\Venda;

use Artaban\Model\Pessoa as ModelPessoa;


class Pessoa {
    
    
    public static function listarDaEmpresa($empresaId) {
        $pessoas = ModelPessoa::daEmpresa($empresaId)->orderBy('Nome')->get();
        
        $listaDePessoas = [];
        foreach($pessoas as $pessoa) {
            $listaDePessoas[] = array(
                'id'        => $pessoa->Id,
                'nome'      => $pessoa->Nome,
                'telefone'  => $pessoa->Telefone,
                'email'     => $pessoa->Email,
                'sexo'      => $pessoa->Sexo
            );
        }
                                     
        return $listaDePessoas;                    
    }
   
   
    public static function buscarPorDocumento($empresaId, $documento) {
        $pessoa = ModelPessoa::daEmpresa($empresaId)
                             ->comDocumento($documento)
                             ->first();
        
        $pessoaArray = ['id' => '', 'nome' => '', 'telefone' => '', 'email' => '', 'sexo' => ''];
        
        if ($pessoa) {
            $pessoaArray = array(
                'id'       => $pessoa->Id,
                'nome'     => $pessoa->Nome,
                'telefone' => $pessoa->Telefone,
                'email'    => $pessoa->Email,
                'sexo'     => $pessoa->Sexo
            );
        } 
        
        return $pessoaArray;
    }
    
    
    public static function buscarPorId($empresaId, $pessoaId) {
        $pessoa = ModelPessoa::daEmpresa($empresaId)->find($pessoaId);
                               
        $pessoaArray = ['id' => '', 'nome' => '', 'telefone' => '', 'email' => ''];
        
        if ($pessoa) {
            $pessoaArray = array(
                'id'       => $pessoa->Id,
                'nome'     => $pessoa->Nome,
                'telefone' => $pessoa->Telefone,
                'email'    => $pessoa->Email,
                'sexo'     => $pessoa->Sexo
            );
        } 
        
        return $pessoaArray;
    }
    
    
    public static function buscarSaldoDeFidelizacao($empresaId, $pessoaId) {
        $pessoa = ModelPessoa::daEmpresa($empresaId)->find($pessoaId);
        
        $saldo = ["valor" => 0];
        
        if ($pessoa) {
            $saldo = ["valor" => $pessoa->saldoDeFidelizacao()];
        }
        
        return $saldo;
    }
       
}