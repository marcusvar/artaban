<?php
namespace Artaban\API\Venda;


class Passagem {
    
    
    public static function alterar($empresaId) {
        $resultado = ["success" => true];
        
        return $resultado;                    
    }
    
    
    public static function cancelar($empresaId) {
        $resultado = ["success" => true];
        
        return $resultado;                    
    }
    
    
    public static function buscarPassageiro($empresaId, $passagemId) {
        if ($passagemId === "123456") {
            $passageiro = array(
                "documento" => "871238713", "nome" => "Fulano de Tal",  "telefone" => "(69) 3422-3456",
                "embarque" => "Cascavel", "destino" => "Goiania", "valor" => 200, "passagemId" => $passagemId
            ); 
        } else {
            $passageiro = array(
                "documento" => "923845982", "nome" => "Beltrano da Silva", "telefone" => "(69) 3423-6543",
                "embarque" => "Foz do Iguaçu", "destino" => "Goiania", "valor" => 250, "passagemId" => $passagemId
            );
        }
        
        return $passageiro;                    
    }
    	
}