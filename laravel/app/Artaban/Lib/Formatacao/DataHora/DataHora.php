<?php
namespace Artaban\Lib\Formatacao\DataHora;

use Artaban\Lib\Formatacao\DataHora\DataHoraLib;

  
class DataHora {
    
    const EUA = 'Y-m-d H:i:s';
    const BR  = 'd/m/Y H:i:s';
    
    
    public static function formatar($dataHora, $formatoOriginal, $formatoNovo, $valorDataHoraInvalida = "") {
        return DataHoraLib::formatar($dataHora, $formatoOriginal, $formatoNovo, $valorDataHoraInvalida);
    }
    
}