<?php
namespace Artaban\Lib\Formatacao\DataHora;

use \DateTime;


class DataHoraLib {
    
    
    public static function formatar($dataHora, $formatoOriginal, $formatoNovo, $valorDataHoraInvalida = "") {
        $dataHora = DateTime::createFromFormat($formatoOriginal, $dataHora);
        
        $resultado = $valorDataHoraInvalida;
        
        if ($dataHora && $dataHora != "0000-00-00") {
            $resultado = $dataHora->format($formatoNovo);
        }
        
        return $resultado;
    }
    
}