<?php
namespace Artaban\Lib\Formatacao\DataHora;

use Artaban\Lib\Formatacao\DataHora\DataHoraLib;

use \DateTime;


class Data {
    
    const EUA = 'Y-m-d';
    const BR  = 'd/m/Y';
    
    
    public static function formatar($data, $formatoOriginal, $formatoNovo, $valorDataInvalida = "") {
        return DataHoraLib::formatar($data, $formatoOriginal, $formatoNovo, $valorDataInvalida);
    }
        
}