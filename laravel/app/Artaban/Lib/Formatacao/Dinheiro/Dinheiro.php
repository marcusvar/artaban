<?php
namespace Artaban\Lib\Formatacao\Dinheiro;


class Dinheiro {
    
    const SIGLA = 'R$ ';
    
    
    public static function comMascara($valor) {
       $valor = number_format($valor, 2, ',' ,'.');
       $valor = self::SIGLA . $valor;
       
       return $valor;
    }
     
     
    public static function semMascara($valor) {
       $valor = str_replace(self::SIGLA, '', $valor);
       $valor = str_replace('.', '', $valor);
       $valor = str_replace(',', '.', $valor);
       
       return $valor;
    }   
    
}