<?php
namespace Artaban\Lib\Formatacao\String;


class String {
    
     
    public static function resumir($texto, $maximo) {
        if (strlen($texto) > $maximo) {
            $texto = substr($texto, 0, $maximo) . ' ...';
        }
        
        return $texto;   
    }   
    
}