<?php
namespace Artaban\Lib\Exception;


class ExceptionValidator extends \Exception {
    
    
    protected $validator;
    
    
    public function __construct($validator, $exception = '', $code = 0, $previous = null) {
        if ($exception == '') {
            $exception = implode('', $validator->messages()->all());
        }
        
        parent::__construct($exception, $code, $previous);
        
        $this->validator = $validator;
    }
    
    
    public function getValidator() {
        return $this->validator;
    }
    
    
    public function htmlMessage() {
        return implode('', $this->validator->messages()->all('<p>:message</p>'));
    }
    
}