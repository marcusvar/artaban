<?php
namespace Artaban\Lib\Validator;

use Artaban\Validation\Documento;
use Artaban\Model\Pessoa;

use Exception;


class Validator extends \Illuminate\Validation\Validator {
 
 
    public function validateCpf($attribute, $value, $parameters) {
        try {
             $documento = new Documento($value, Pessoa::PF_BRASILEIRO);
             return true; 
        
        } catch (Exception $e) {
            return false;
        }
    }
 
 
    protected function replaceCPF($message, $attribute, $rule, $parameters) {
        if (count($parameters) > 0)
            return str_replace(':cpf', $parameters, $message);
        else
            return $message;
    }
 
 
    public function validateCnpj($attribute, $value, $parameters) {
        try {
             $documento = new Documento($value, Pessoa::PJ_BRASILEIRO);
             return true; 
        
        } catch (Exception $e) {
            return false;
        }
    }
 
 
    protected function replaceCNPJ($message, $attribute, $rule, $parameters) {
        if (count($parameters) > 0)
            return str_replace(':cnpj', $parameters, $message);
        else
            return $message;
    }
 
}