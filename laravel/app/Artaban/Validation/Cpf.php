<?php
namespace Artaban\Validation;


/**
 * @package Brasil
 * @subpackage Pessoa
 * @author Gabriel Felipe Soares <gabrielfs7@gmail.com>
 */
class Cpf extends Cnp {
	/*
	 * (non-PHPdoc) @see \GSoares\Brasil\Pessoa\Cnp::formata()
	 */
	public function formata()
	{
		return substr($this->cnp, 0, 3) . '.' . 
			   substr($this->cnp, 3, 3) . '.' .   
			   substr($this->cnp, 6, 3) . '-' .   
			   substr($this->cnp, 9, 2);
	}
	
	/* (non-PHPdoc)
	 * @see \GSoares\Brasil\Pessoa\Cnp::valida()
	 */
	protected function valida()
	{
		if (strlen($this->cnp) != 11 ||
			$this->cnp == 00000000000 ||
			$this->cnp == 11111111111 ||
			$this->cnp == 22222222222 ||
			$this->cnp == 33333333333 ||
			$this->cnp == 44444444444 || 
			$this->cnp == 55555555555 ||
			$this->cnp == 66666666666 ||
			$this->cnp == 77777777777 || 
			$this->cnp == 88888888888 ||  
			$this->cnp == 99999999999) {
 			throw new DocumentoInvalidoException('CPF ' . $this->cnp . ' inválido.');
		}
		
		
		for ($t = 9; $t < 11; $t++) {
             
                    for ($d = 0, $c = 0; $c < $t; $c++) {
                        $d += $this->cnp{$c} * (($t + 1) - $c);
                    }
                    
		    $d = ((10 * $d) % 11) % 10;
                    
		    if ($this->cnp{$c} != $d) {
                        throw new DocumentoInvalidoException('CPF ' . $this->cnp . ' inválido.');
                    }
                }
	
	}
}