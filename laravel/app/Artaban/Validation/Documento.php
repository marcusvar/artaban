<?php
namespace Artaban\Validation;

use Artaban\Model\Pessoa as ModelPessoa;


class Documento {
    
    private $documento;
    private $formatado;
    private $numero;
    
    
    public function __construct($documento, $tipo) {
        
        switch ($tipo) {
            case ModelPessoa::PF_BRASILEIRO:
                $this->documento = new Cpf(preg_replace('/[^0-9]/', '', $documento)); 
                $this->formatado = $this->documento->formata();
                $this->numero    = $this->documento->numero();
                break;
            
            case ModelPessoa::PJ_BRASILEIRO:
                $this->documento = new Cnpj(preg_replace('/[^0-9]/', '', $documento)); 
                $this->formatado = $this->documento->formata();
                $this->numero    = $this->documento->numero();
                break;
            
            case ModelPessoa::PF_ESTRANGEIRO:
                $this->documento = $documento; 
                $this->formatado = $this->documento->formata();
                $this->numero    = $this->documento->numero();
                break;
            
            case ModelPessoa::PJ_ESTRANGEIRO:
                $this->documento = $documento; 
                $this->formatado = $this->documento->formata();
                $this->numero    = $this->documento->numero();
                break;
        }
    }
	
    
    public function formatado() {
        return $this->formatado;	
    }
    	
	
    public function numero() {
        return $this->numero;	
    }

}