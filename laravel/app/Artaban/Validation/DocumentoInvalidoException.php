<?php
namespace Artaban\Validation;

/**
 * @package Brasil
 * @subpackage Pessoa
 * @author Gabriel Felipe Soares <gabrielfs7@gmail.com>
 */
class DocumentoInvalidoException extends \InvalidArgumentException {
}