<?php
namespace Artaban\Facade\Autorizacao;

use Illuminate\Support\Facades\Facade;


class Autorizacao extends Facade {

    
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        return \App::make('Autorizacao');
    }

}