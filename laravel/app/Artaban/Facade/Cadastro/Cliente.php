<?php
namespace Artaban\Facade\Cadastro;

use Illuminate\Support\Facades\Facade;


class Pessoa extends Facade {

    
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() {
        //return new \Artaban\Service\Cadastro\Pessoa;
    
        return \App::make('CadastroPessoa');
    }

}