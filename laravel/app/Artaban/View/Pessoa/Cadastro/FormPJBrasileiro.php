<?php
namespace Artaban\View\Pessoa\Cadastro;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Model\Estado;
use Artaban\Model\Localidade;
use Artaban\Model\Pessoa;
use Artaban\Validation\Documento;

use Artaban\View\Pessoa\Cadastro\FormBase as Pessoa_Cadastro_FormBase;

use Input;
use View;
use Form;
use Exception;


class FormPJBrasileiro extends Pessoa_Cadastro_FormBase {
    
    
    public function formFields() {
        $estados = Estado::DoBrasil()->orderBy('Nome')->lists('Nome', 'Id');
        $estados = array('' => '') + $estados;
        
        $ufId = object_get($this->pessoa, 'endereco.localidade.UF_Id');
        $estadoSelecionadoId = $ufId ? $ufId : Input::old('endereco')['Estado_Id'];
	
	    if ($estadoSelecionadoId) {
	        $localidades = Localidade::where('UF_Id', '=', $estadoSelecionadoId)->lists('Nome', 'Id'); 
	    } else {
            $localidades = array('' => '');
 	    }
        
        $emPotencialOpcoes = Pessoa::$emPotencialOpcoes;
        $visualizarFidelizacaoOpcoes = Pessoa::$visualizarFidelizacaoOpcoes;
         
        $params = compact('estadoSelecionadoId', 'estados', 'localidades', 'emPotencialOpcoes', 'visualizarFidelizacaoOpcoes');
        
        $pessoas = Pessoa::daEmpresa(HelperEmpresa::atual()->Id)
                         ->doTipoPF()
                         ->orderBy('Nome')
                         ->lists('Nome', 'Id');
                                 
        $params['pessoas'] = array('' => '') + $pessoas;
        $params['pessoaId'] = null;
            
        if ($this->pessoa) {
            $params['pessoaId'] = $this->pessoa->cnpj ? $this->pessoa->cnpj->Pessoa_Id_Responsavel : null;
        }         
 
        $form = '_form_pj_br';
        
        $this->html .= (string) View::make('Cadastro.pessoas.' . $form, $params);	 
    }
    
}