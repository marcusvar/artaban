<?php
namespace Artaban\View\Pessoa\Cadastro;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Model\Estado;
use Artaban\Model\Localidade;
use Artaban\Model\Pessoa;
use Artaban\Validation\Documento;

use Input;
use View;
use Form;
use Exception;


abstract class FormBase {
    
    protected $tipo;
    protected $html;
    protected $documento;
    protected $pessoa;
    
    
    public function __construct($pessoa = null) {
        $this->pessoa = $pessoa;
    }
    
    
    public static function gerar($pessoa = null) {
        $form = new static($pessoa);    
                
        $form->formOpen();
         
        $form->formFields();
         
        $form->formClose();
        
        return (string) $form;
    }
    
    
    public function formOpen() {
        if ($this->pessoa) {
            $this->formEdit();
        } else {
            $this->formCreate();
        } 
    }
    
    
    
    private function formCreate() {
        $pessoa = new Pessoa;
	    // $pessoa->Documento = $this->documento->numero();
	
        $this->html = Form::model($pessoa, array('route' => 'cadastro.pessoas.store'));
        
        
    }
    
    
    private function formEdit() {
        $this->html = Form::model($this->pessoa,
				                  array(
                                      'method' => 'PATCH',
				                      'route' => array('cadastro.pessoas.update', $this->pessoa->Id)
                                  )
        ); 
    }
    
    
    abstract public function formFields();
    
    
    public function formClose() {
        $this->html .= Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao', 'id' => 'btn-submit-cadastro-pessoa'));
	    $this->html .= Form::close();
    }
    
    
    public function __toString() {
        return $this->html;
    }
    
}