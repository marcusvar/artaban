<?php
namespace Artaban\View\Pessoa\Cadastro;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Model\Estado;
use Artaban\Model\Localidade;
use Artaban\Model\Pessoa;
use Artaban\Validation\Documento;

use Artaban\View\Pessoa\Cadastro\FormBase as Pessoa_Cadastro_FormBase;

use Input;
use View;
use Form;
use Exception;


class FormPFBrasileiro extends Pessoa_Cadastro_FormBase {
    
        
    
   // public static function gerar($pessoa = null) {
        //$pessoa = Pessoa::daEmpresa(HelperEmpresa::atual()->Id)
	    //                  ->comDocumento($documento)
		//	              ->with('endereco', 'endereco.localidade', 'endereco.localidade.estado')
         //                 ->first();      
        
     //   return __parent::gerar($pessoa);
  //  }
    
    
    public function formFields() {
        $estados = Estado::doBrasil()->orderBy('Nome')->lists('Nome', 'Id');
        $estados = array('' => '') + $estados;
	
        $ufId = object_get($this->pessoa, 'endereco.localidade.UF_Id');
        $estadoSelecionadoId = $ufId ? $ufId : Input::old('endereco')['Estado_Id'];
	
	    if ($estadoSelecionadoId) {
	        $localidades = Localidade::where('UF_Id', '=', $estadoSelecionadoId)->lists('Nome', 'Id'); 
	    } else {
            $localidades = array('' => '');
 	    }
 
        $emPotencialOpcoes = Pessoa::$emPotencialOpcoes;
        $visualizarFidelizacaoOpcoes = Pessoa::$visualizarFidelizacaoOpcoes;
 
        $form = '_form_pf_br';
        
        $params = compact('estadoSelecionadoId', 'estados', 'localidades', 'emPotencialOpcoes', 'visualizarFidelizacaoOpcoes');
        
        $this->html .= (string) View::make('Cadastro.pessoas.' . $form, $params);	 
    }
    
}