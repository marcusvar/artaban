<?php
namespace Artaban\View\Pessoa\Cadastro;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Model\Pais;
use Artaban\Model\Estado;
use Artaban\Model\Localidade;
use Artaban\Model\Pessoa;
use Artaban\Validation\Documento;

use Artaban\View\Pessoa\Cadastro\FormBase as Pessoa_Cadastro_FormBase;

use Input;
use View;
use Form;
use Exception;


class FormPFEstrangeiro extends Pessoa_Cadastro_FormBase {
    
    
    public function formFields() {
        $paises = Pais::Estrangeiro()->orderBy('Nome')->lists('Nome', 'Id');
        $paises = array('' => '') + $paises;
	    
        $paisId = object_get($this->pessoa, 'endereco.localidade.estado.Pais_Id');
        $paisSelecionadoId = $paisId ? $paisId : Input::old('endereco')['Pais_Id'];
	  
        if ($paisId) {
            $estados = Estado::where('Pais_Id', '=', $paisSelecionadoId)->lists('Nome', 'Id'); 
	    } else {
            $estados = array('' => '');                    
        }
      
        $ufId = object_get($this->pessoa, 'endereco.localidade.UF_Id');
        $estadoSelecionadoId = $ufId ? $ufId : Input::old('endereco')['Estado_Id'];
        
	    if ($estadoSelecionadoId) {
	        $localidades = Localidade::where('UF_Id', '=', $estadoSelecionadoId)->lists('Nome', 'Id'); 
	    } else {
            $localidades = array('' => '');
 	    }
 
        $emPotencialOpcoes = Pessoa::$emPotencialOpcoes;
        $visualizarFidelizacaoOpcoes = Pessoa::$visualizarFidelizacaoOpcoes;
 
        $form = '_form_pf_es';
        
        $params = compact('paises', 'paisSelecionadoId', 'estadoSelecionadoId', 'estados', 'localidades',
                          'emPotencialOpcoes', 'visualizarFidelizacaoOpcoes');
        
        $this->html .= (string) View::make('Cadastro.pessoas.' . $form, $params);	 
    }
    
}