<?php
namespace Artaban\Search;

use Artaban\Search\SearchLib;

use \Cache;
use \Config;
use \Paginator;
use \Input;


/*
 * ---------------------------------------------------------------------------------------
 * Artaban\Search\Search
 * ---------------------------------------------------------------------------------------
 *
 * Classe responsável por executar uma busca no banco de dados
 *
 */
class Search {
    
    
    /*
     * Executa a busca e retorna os valores encontrados
     *
     * @param $repository (O model onde será executada a busca)
     * @param $fields (Os filtros para executar a busca)
     * @param $params (Outros parâmetros)
     *
     * Exemplos:
     *   $fields = array(
     *	     'usuario_id' => array('=', 1),
     *        
     *       'created_at' => array('between',
     *                             array('01-10-2013', '31-10-2013'),
     *                             'date'),
     *        
     *       'url' => array('like', 'teste'),  
	 *		 'ip'  => array('like', '10.0.1'),  
     *    ); 
     *    
     *    $logs = Search::execute($log, $fields, ['order' => ['created_at' => 'DESC']]);
     *
     * A busca acima irá procurar os logs do usuário com ID '1'; criados entre '01-10-2013' e '31-10-1013';
     * que contém na url a palavra 'teste' que também contenha no ip o valor '10.0.1'. 
     * Os resultados serão ordenados pelo campo 'created_at' de forma descendente. 
     */
    public static function execute($repository, $fields = [], array $params = []) {
        $searchLib = new SearchLib($repository, $fields, $params);
        return $searchLib->execute();
    }
}

?>