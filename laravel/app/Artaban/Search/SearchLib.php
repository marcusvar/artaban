<?php
namespace Artaban\Search;

use Artaban\Lib\Formatacao\DataHora\Data;

use \Cache;
use \Config;
use \Paginator;
use \Input;
use \DB;


/**
 * ---------------------------------------------------------------------------------------
 * Artaban\Search\SearchLib
 * ---------------------------------------------------------------------------------------
 *
 * Classe responsável por executar a lógica de uma busca em um model
 *
 */
class SearchLib {
        
    private $repository;
    private $fields;
    private $params;
      
    
    /**
     *
     * @param  $repository (O model onde será executada a busca)
     * @param $fields (Os filtros para executar a busca)
     * @param $params (Outros parâmetros)
     *
     * Exemplos:
     *   $fields = array(
     *	     'usuario_id' => array('=', 1),
     *        
     *       'created_at' => array('between',
     *                             array('01-10-2013', '31-10-2013'),
     *                             'date'),
     *        
     *       'url' => array('like', 'teste'),  
	 *		 'ip'  => array('like', '10.0.1'),  
     *    ); 
     *    
     *    $logs = Search::execute($log, $fields, ['order' => ['created_at' => 'DESC']]);
     *
     * A busca acima irá procurar os logs do usuário com ID '1'; criados entre '01-10-2013' e '31-10-1013';
     * que contém na url a palavra 'teste' que também contenha no ip o valor '10.0.1'. 
     * Os resultados serão ordenados pelo campo 'created_at' de forma descendente. 
     */   
    public function __construct($repository, $fields = [], array $params = []) {
        $this->repository = $repository;
        $this->fields = $fields;
        $this->params = $params;
    }
    
    
    public function getRepository() {
        return $this->repository;
    }
    
    
    public function process() {
        $this->processFields();
                       
        $this->processRelations();       
        
        $this->processOrder();
    }
    
    
    /**
     * Executa uma busca no model e retorna os valores encontrados paginados 
     *   
     */
    public function execute() {
        $this->process();
        
        if ($this->paginate()) {
            $registrosPorPagina = $this->maxResults();
            return $this->repository->paginate($registrosPorPagina);
        } else {
            return $this->repository->get();
        }
        
    }
    
    
    /**
     * Faz o processamento dos filtros dos campos 
     *   
     */
    private function processFields() {
         
         foreach($this->fields as $field => $options) {
            $operator = $options[0];
            $value    = $options[1];
            $type     = isset($options[2]) ? $options[2] : "";
                        
            if ($value != "") {
                
                switch ($type) {
                    # Se for do tipo 'date', o campo será comparado no banco baseado apenas
                    # na data (ignorando o valor das horas). Isso é importante para comparar
                    # datas em campos do tipo DATETIME
                    case 'date':
                        $field = DB::raw('DATE(' . $field . ')');    
                        $value = $this->formatValue($value, $type);
                        break;
                }
                               
                switch ($operator) {
                    case 'between':
                        $start = $value[0];
                        $end   = $value[1];
                        
                        $this->generateFilterBetween($field, $start, $end);
                        break;
                    
                    case 'like':
                        $this->generateFilterLike($field, $value);
                        break;
                    
                    default:
                        $this->generateFilter($field, $operator, $value);
                }
            }
        }        
    }
    
    
    /**
     * Formata o valor de acordo com seu tipo
     * @param  $value (O valor que será formatado)
     * @param  $type (O tipo de formatação para ser aplicado)
     *   
     */
    private function formatValue($value, $type) {
        
        switch ($type) {
            
            # Tipo data, formatado para o padrão EUA '2013-10-01'
            case 'date':
                $function = function($value) {
                    $date = Data::formatar($value, Data::BR, Data::EUA);
                    return $date;
                };
                break;
            
            # Por padrão, outros tipos são pesquisados sem aplicar nenhuma formatação
            default:
                $function = function($value) {
                    return $value;
                };
        }
        
        return is_array($value) ? array_map($function, $value) : $function($value);    
    }
    
    
    /**
     * Gera um filtro do tipo between 
     * @param  $field (O campo que será pesquisado)
     * @param  $start (O valor inicial da busca)
     * @param  $end (O valor final da busca)
     *   
     */
    private function generateFilterBetween($field, $start, $end) {
        if ($start != "") {
            $this->generateFilter($field, '>=', $start);
        }
        
        if ($end != "") {
            $this->generateFilter($field, '<=', $end);    
        }    
    }
    
    
    /**
     * Gera um filtro do tipo like 
     * @param  $field (O campo que será pesquisado)
     * @param  $value (O valor da busca)
     *   
     */
    private function generateFilterLike($field, $value) {
        $value = '%' . $value . '%';
        $this->generateFilter($field, 'like', $value);        
    }
    
    
    /**
     * Gera um filtro 
     * @param  $field (O campo que será pesquisado)
     * @param  $operator (O operador que será aplicado ('=', '>=', 'like', etc)
     * @param  $value (O valor da busca)
     *   
     */
    private function generateFilter($field, $operator, $value) {
        if ($value === 'NULL') {
            $this->repository = $this->repository->whereNull($field);
        } else {
            $this->repository = $this->repository->where($field, $operator, $value);
        }
    }
    
    
    /**
     * Traz no resultado os models relacionados com esse model.
     * Obs: somente se o parâmetro 'relations' estiver presente
     *   
     */ 
    private function processRelations() {
        if (isset($this->params['relations'])) {      
            $this->repository = $this->repository->with($this->params['relations']);
        }    
    }
    
    
    /**
     * Faz a ordenação dos resultados.
     * Obs: somente se o parâmetro 'order' estiver presente.
     *   
     */
    private function processOrder() {
        if (isset($this->params['order'])) {
            $order = $this->params['order'];
            
            if (is_array($order)) {
                $field = key($order);
                $type = $order[$field];
            } else {
                $field = $order;
                $type = 'ASC';
            }
        
            $this->repository = $this->repository->orderBy($field, $type);
        }        
    }
    
    
    /**
     * Retorna se a busca deverá ser paginada ou não.
     *   
     */
    private function paginate() {
        return array_get($this->params, 'paginate', true);             
    }
    
    
    /**
     * Retorna a quantidade de registros por página.
     *   
     */
    private function maxResults() {
        $registrosPorPagina = 20;
        return array_get($this->params, 'maxResults', $registrosPorPagina);             
    }
    
}

?>