<?php
namespace Artaban\Search;

use \Cache;
use \Config;
use \Paginator;
use \Input;


class Search {
    
    
    public static function execute($repository, $fields = [], $relations = [], $usePaginator = true) {
  
        foreach($fields as $field => $options) {
            $operator = $options[0];
            $value    = $options[1];
            
            if ($value != "") {
                if ($operator === 'like') {
                    $value = '%' . $value . '%';
                }
                                
                $repository = $repository->where($field, $operator, $value);
            }
        }
                
        $repository = $repository->with($relations);

        if ($usePaginator) {
            $result = $repository->paginate(15);
            $paginator = Paginator::make($result->getItems(), $result->getTotal(), 15);
            return $paginator;    
        }
        
        return $repository->get();
        
         
    }
}


?>