<?php
namespace Artaban\Model;

use \Eloquent;


class DescontoGeral extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Desconto';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Descricao' => 'required',
		'Percentagem' => 'required',
		'Confirmado' => 'required',
                'AgenciaWeb' => 'required'
	);
    
    
    public static $confirmadoOpcoes = array(
        'Sim' => 'Sim',
	'Não' => 'Não'
    );
    
    
    public static $agenciaWebOpcoes = array(
        '0' => 'Agência e web',
        '1' => 'Somente agência',
	'2' => 'Somente web',
    );
    
    
    public function getPercentagemDescricaoAttribute(){
        return $this->Percentagem . '%';
    }
    
    
    public function getConfirmadoDescricaoAttribute(){
        return static::$confirmadoOpcoes[$this->Confirmado];
    }
    
    
    public function getAgenciaWebDescricaoAttribute(){
        return static::$agenciaWebOpcoes[$this->AgenciaWeb];
    }
    
}