<?php
namespace Artaban\Model;

use \Eloquent;


class PontoReferencia extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_PontoReferencia';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
	'Descricao' => 'required',
	'Localidade_Id' => 'required',
        'Pessoa_Id' => 'required'
	);
    
    
    public static $tipos = array(
        '' => '',
        'Ponto de Referência'  => 'Ponto de Referência',
        'Agência'              => 'Agência',
        //'Garagem'              => 'Garagem',
        //'Depósito'             => 'Depósito',
        //'Ponto de Apoio'       => 'Ponto de Apoio',
        //'Transportadora'       => 'Transportadora',
        //'Agência de Turismo'   => 'Agência de Turismo',
        'Agência Terceirizada' => 'Agência Terceirizada'
        //'Departamento'         => 'Departamento'
    );

    
    public function descricaoCompleta() {
        $descricao = sprintf("%s (%s) - %s",
                             $this->localidade->Nome,
                             $this->localidade->estado->Sigla,
                             $this->Descricao);
        
        return $descricao;
    }
    
        
    public function localidade() {
        return $this->belongsTo('Artaban\Model\Localidade', 'Localidade_Id'); 
    }
    
    
    public function empresa() {
        return $this->belongsTo('Artaban\Model\Empresa', 'Pessoa_Id'); 
    }
    
    
    public static function listarPorDescricaoCompleta($empresaId) {
        $pontosReferencia = static::daEmpresa($empresaId)->orderBy('Descricao')->get();
        
        $lista = [];
        foreach($pontosReferencia as $ponto) {
            $lista[$ponto->Id] = $ponto->descricaoCompleta();
        }
        
        return $lista;    
    }
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        return $query->where('Pessoa_Id', '=', $empresaId);
    }
    
    
    public function scopeNoPais($query, $paisId) {
        $estadosNoPais = Estado::where('Pais_Id', '=', $paisId)->lists('Id');
        $localidadesNoEstado = Localidade::whereIn('UF_Id', $estadosNoPais)->lists('Id');
        
        return $query->whereIn('Localidade_Id', $localidadesNoEstado);
    }
    
    
    public function scopeNoEstado($query, $estadoId) {
        $localidadesNoEstado = Localidade::where('UF_Id', '=', $estadoId)->lists('Id');
        
        return $query->whereIn('Localidade_Id', $localidadesNoEstado);
    }
    
}