<?php
namespace Artaban\Model;

use \Eloquent;


class TipoAdicionalTarifa extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_TipoAdicionalTarifa';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'Descricao' => 'required'
    );    
    
}