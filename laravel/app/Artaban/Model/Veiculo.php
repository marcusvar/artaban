<?php
namespace Artaban\Model;

use \Eloquent;


class Veiculo extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Veiculo';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Numero' => 'required',
		'Placa' => 'required',
		'TipoVeiculo_Id' => 'required'
	);
    
        
    public function tipoVeiculo() {
        return $this->belongsTo('Artaban\Model\TipoVeiculo', 'TipoVeiculo_Id'); 
    }
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        return $query->where('Pessoa_Id', '=', $empresaId); 
    }
    
}