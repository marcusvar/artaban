<?php
namespace Artaban\Model;

use Artaban\Model\Estado;

use \Eloquent;


class Localidade extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Localidade';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'Nome' => 'required',
        'UF_Id' => 'required'
    );
    
    public static $tipos = array(
        '' => '',
        'Município' => 'Município',
	    'Distrito' => 'Distrito',
	    'Povoado' => 'Povoado',
	    'Região administrativa' => 'Região administrativa'
    );
    
    
    public function estado() { 
        return $this->belongsTo('Artaban\Model\Estado', 'UF_Id'); 
    }
    
}