<?php
namespace Artaban\Model;

use Artaban\Model\TipoPermissao;
use Artaban\Model\Grupo;
use \Eloquent;


class Permissao extends Eloquent {
	
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Permissao';
    
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');

	public static $rules = array(
		'descricao' => 'required',
		'permissao' => 'required',
		'url' => 'required',
		'tipo_permissao_id' => 'required'
	);

    
    public function tipoPermissao() { 
 	    return $this->belongsTo('Artaban\Model\TipoPermissao'); 
	}
    
    
    public function grupos() {
         return $this->belongsToMany('Artaban\Model\Grupo', 'GrupoPermissao');
    }
    
}