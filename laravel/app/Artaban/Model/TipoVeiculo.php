<?php
namespace Artaban\Model;

use \Eloquent;


class TipoVeiculo extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_TipoVeiculo';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Descricao' => 'required',
		'Pessoa_Id' => 'required'
	);
    
    
    public function empresa() {
        return $this->belongsTo('Artaban\Model\Empresa', 'Pessoa_Id'); 
    }
    
    
    public function mapa() {
        $mapas = $this->mapas()
                      ->orderBy('Andar')
                      ->orderBy('Linha')
                      ->orderBy('Coluna')
                      ->get();
        
        $mapa = [];
        foreach($mapas as $m) {
            $mapa[$m->Andar]['Linha' . $m->Linha]['Coluna' . $m->Coluna] = $m->Numero; 
        }
        
        return $mapa;        
    }
    
    
    public function mapas() {
        return $this->hasMany('Artaban\Model\Mapa', 'TipoVeiculo_Id');
    }
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        return $query->where('Pessoa_Id', '=', $empresaId);
    }
    
}