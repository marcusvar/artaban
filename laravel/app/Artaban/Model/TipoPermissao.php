<?php
namespace Artaban\Model;

use Artaban\Model\Modulo;
use Artaban\Model\Permissao;
use \Eloquent;


class TipoPermissao extends Eloquent {
	
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'TipoPermissao';
    
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');

	public static $rules = array(
		'descricao' => 'required',
		'modulo_id' => 'required'
	);
    
    
    public function modulo() { 
 	    return $this->belongsTo('Artaban\Model\Modulo'); 
	}
    
    
    public function permissoes() {
        return $this->hasMany('Artaban\Model\Permissao');
    }

}