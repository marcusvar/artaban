<?php
namespace Artaban\Model;

use \Eloquent;


class Mapa extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_TipoVeiculoMapa';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'TipoVeiculo_Id' => 'required',
		'Numero' => 'required',
		'Linha' => 'required',
		'Coluna' => 'required',
		'Andar' => 'required'
	);

     
    public function poltronaJaCadastrada($tipoVeiculoId, $numero) {
        if ((int) $numero < 1) {
            return false;
        }
        
        $mapa = static::where('TipoVeiculo_Id', '=', $tipoVeiculoId)
                      ->where('Numero', '=', $numero)
                      ->first();
        
        return $mapa != null;     
    }
    
   
    public function salvarPoltrona($tipoVeiculoId, $andar, $coluna, $linha, $numero) {
        $numero = (int) $numero;
        
        if ($this->poltronaJaCadastrada($tipoVeiculoId, $numero)) {
            return false;
        }
        
        $mapa = static::where('TipoVeiculo_Id', '=', $tipoVeiculoId)
                      ->where('Andar',  '=', $andar)
                      ->where('Coluna', '=', $coluna)
                      ->where('Linha',  '=', $linha) 
                      ->delete();
        
        if ($numero > 0) {        
            static::create(array(
                'TipoVeiculo_Id' => $tipoVeiculoId,
 	            'Andar'  => $andar, 
	            'Coluna' => $coluna, 
	            'Linha'  => $linha,
	            'Numero' => $numero, 
            ));
        }
        
    }

}