<?php
namespace Artaban\Model;

use Artaban\Lib\Formatacao\DataHora\Data;
use Artaban\Model\Horario;

use Exception;
use DB;
use Eloquent;


class Viagem extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Viagem';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'DataInicio'     => 'required',
		'HoraInicio'     => 'required',
		'Horario_Id'     => 'required',
		'TipoVeiculo_Id' => 'required',
                'Duracao'        => 'required',
		'Confirmada'     => 'required',
		'Situacao'       => 'required'
	);
    
    
    public static $confirmadaOpcoes = array(
        'Sim' => 'Sim',
        'Não' => 'Não'
    );
    
    
    public static $situacoes = array(
        'Normal'    => 'Normal',
        'Bloqueada' => 'Bloqueada'
    );
  
  
    public function getDuracaoDescricaoAttribute() {
        $d = (int) $this->Duracao;
        return ($d > 1) ? "$d dias" : "$d dia";
    }
  
    
    public function setDataInicioAttribute($data) {
       $this->attributes['DataInicio'] = Data::formatar($data, Data::BR, Data::EUA, null);
    }
    
    
    public function getDataInicioAttribute($data) {
        return Data::formatar($data, Data::EUA, Data::BR);
    }
    
    
    public function horario() {
         return $this->belongsTo('Artaban\Model\Horario', 'Horario_Id');  
    }

    
    public function tipoVeiculo() {
        return $this->belongsTo('Artaban\Model\TipoVeiculo', 'TipoVeiculo_Id'); 
    }
    
    public function Veiculo() {
        return $this->belongsTo('Artaban\Model\Veiculo', 'Veiculo_Id'); 
    }
    

    public function itens() {
         return $this->hasMany('Artaban\Model\ItemViagem', 'Viagem_Id');  
    }    
    

    public function delete() {
        try {
            DB::transaction(function() {
                foreach($this->itens as $item) {
                    $item->delete();
                }
                
                parent::delete();
            });
            
        } catch (Exception $e)  {
            throw $e;
        }
    } 
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        $horariosDaEmpresa = Horario::daEmpresa($empresaId)->lists('ID');
        
        return $query->whereIn('Horario_Id', $horariosDaEmpresa);
    }
    
}