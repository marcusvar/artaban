<?php
namespace Artaban\Model;

use Artaban\Model\TipoPermissao;
use Artaban\Model\Grupo;
use \Eloquent;


class Modulo extends Eloquent {
	
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Modulo';
    
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');

	public static $rules = array(
		'nome' => 'required'
	);
    
    
    public function tiposPermissao($tipo = null) {
        if ($tipo === null) {
            return $this->hasMany('Artaban\Model\TipoPermissao');        
        }
        
        return $this->hasMany('Artaban\Model\TipoPermissao')->where('tipo', '=', $tipo)->get();  
    }
    
    
    public function grupos() {
         return $this->hasMany('Artaban\Model\Grupo');  
    }
    
}