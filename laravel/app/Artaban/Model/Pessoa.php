<?php
namespace Artaban\Model;

use Artaban\Lib\Formatacao\DataHora\Data;
use Artaban\Lib\Formatacao\Dinheiro\Dinheiro;
use Artaban\Model\Fidelizacao;
use Artaban\Validation\Documento;

use Eloquent;
use Auth;
use DB;


class Pessoa extends Eloquent {
	
    const PF_BRASILEIRO  = "PF Brasileiro";
    const PJ_BRASILEIRO  = "PJ Brasileiro";
    const PF_ESTRANGEIRO = "PF Estrangeiro";
    const PJ_ESTRANGEIRO = "PJ Estrangeiro";
    
    
    protected $table = 'ART_Pessoa';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        // 'Nome' => 'required',
        // 'Documento' => 'required',
        // 'Pessoa_Id' => 'required', 
        // 'NomeUsual' => 'required',        
        // 'RGIE' => 'required',
        // 'Email' => 'required',
        // 'Sexo' => 'required',
        // 'Responsavel' => 'required',
        // 'EmailResponsavel' => 'required',
        // 'Financeiro' => 'required',
        // 'EmailFinanceiro' => 'required',
        // 'Telefone' => 'required',
        // 'Celular' => 'required',
        // 'VisualizarFidelizacao' => 'required'
    );
    
    
    public static $tipos = array(
        'pf_br' => self::PF_BRASILEIRO,
        'pj_br' => self::PJ_BRASILEIRO,
        'pf_es' => self::PF_ESTRANGEIRO,
        'pj_es' => self::PJ_ESTRANGEIRO
    );
    
    
    public static $tiposOpcoes = array(
        self::PF_BRASILEIRO  => 'Pessoa Física Brasileira',
        self::PJ_BRASILEIRO  => 'Pessoa Jurídica Brasileira',
        self::PF_ESTRANGEIRO => 'Pessoa Física Estrangeira',
        self::PJ_ESTRANGEIRO => 'Pessoa Jurídica Estrangeira'
    );
   
        
    public static $sexoOpcoes = array(
        'F' => 'Feminino',
        'M' => 'Masculino'
    );
   
    
    public static $visualizarFidelizacaoOpcoes = array(
        '0' => 'Não',
        '1' => 'Sim'
    );
    
    
    public static $emPotencialOpcoes = array(
        '0' => 'Não',
        '1' => 'Sim'
    );
    
    
    public function documento() {
        return new Documento($this->CPFCNPJ, $this->Tipo);
    }
    
    
    public function setDataNascimentoAttribute($data) {
       $this->attributes['DataNascimento'] = Data::formatar($data, Data::BR, Data::EUA, null);
    }
    
    
    public function getDataNascimentoAttribute($data) {
        return Data::formatar($data, Data::EUA, Data::BR);
    }

    
    public function sexoDescricao() {
        return static::$sexoOpcoes[$this->Sexo];
    }
    
    
    public function visualizarFidelizacaoDescricao() {
        return static::$visualizarFidelizacaoOpcoes[$this->VisualizarFidelizacao];
    }
   
   
    public function emPotencialDescricao() {
        return static::$emPotencialOpcoes[$this->EmPotencial];
    }
    
    
    public function tipoDescricao() {
        return static::$tiposOpcoes[$this->Tipo];
    } 
   
    
    public function empresa() {
        return $this->belongsTo('Artaban\Model\Empresa', 'Pessoa_Id'); 
    }
    
    
    public function endereco() {
        return $this->hasOne('Artaban\Model\Endereco', 'Pessoa_Id'); 
    }

    
    public function telefones() {
        return $this->hasMany('Artaban\Model\PessoaTelefone', 'Pessoa_Id'); 
    }
    
    
    public function vinculados() {
        return $this->belongsToMany('Artaban\Model\Pessoa', 'ART_PessoaVinculo', 'Pessoa_Id', 'Pessoa_Id_Vinculo');
    }
    
    
    public function podeVincularPessoas() {
        if ( ($this->Tipo == Pessoa::PF_BRASILEIRO) && ((string) $this->Documento == "") ) {
            return false;
        }
        
        return true;
    }
   
   
    public function responsavel() {
        return $this->belongsTo('Artaban\Model\Pessoa', 'Pessoa_Id_Responsavel'); 
    } 
    
    
    public function registrosDeFidelizacao() {
        return $this->hasMany('Artaban\Model\Fidelizacao', 'Pessoa_Id'); 
    }
    
    
    public function saldoDeFidelizacao() {
        $creditos = Fidelizacao::Credito()->doPessoa($this->Id)->sum('ValorOperacao');
        $debitos  = Fidelizacao::Debito()->doPessoa($this->Id)->sum('ValorOperacao');
        
        return $creditos - $debitos;
    }
    
    
    public function saldoDeFidelizacaoFormatado() {
        return Dinheiro::comMascara($this->saldoDeFidelizacao());
    }
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        return $query->where('Pessoa_Id', '=', $empresaId);
    }
   
   
    public function scopeComDocumento($query, $documento) {
        return $query->where('Documento', '=', $documento);
    }
    
    
    public function scopeDoTipo($query, $tipo) {
        return $query->where('Tipo', '=', $tipo);
    }
    
    
    public function scopeDoTipoPF($query) {
        return $query->whereIn('Tipo', [self::PF_BRASILEIRO, self::PF_ESTRANGEIRO]);
    }
    
    
    public function scopeDoTipoPJ($query) {
        return $query->whereIn('Tipo', [self::PJ_BRASILEIRO, self::PJ_ESTRANGEIRO]);
    }
    
    
    public function scopeNaoVinculadoA($query, $pessoaId) {
        $vinculados = Pessoa::find($pessoaId)->vinculados()->lists('Id');
        
        return (count($vinculados) > 0) ? $query->whereNotIn('Id',  $vinculados) : $query;
    }
    
}