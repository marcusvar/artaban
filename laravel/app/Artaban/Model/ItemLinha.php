<?php
namespace Artaban\Model;

use Eloquent;


class ItemLinha extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_LinhaItem';
    
    protected $primaryKey = 'ID';
        
    protected $guarded = array('ID', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Linha_Id' => 'required',
                'Sequencia' => 'required',
		'PontoReferencia_Id' => 'required'
	);
  
    public static $webOpcoes = array(
        '1' => 'Sim',
	'0' => 'Não'
    );

    
    public function webDescricao() {
        return static::$webOpcoes[$this->Web];
    }
    
    
    public static function listarPorDescricaoCompleta($empresaId) {
        $linhas = static::daEmpresa($empresaId)->get();
	
        $lista = [];
        foreach($linhas as $linha) {
            $lista[$linha->ID] = $linha->pontoReferencia->descricaoCompleta();
        }
        
        return $lista;    
    }
    
    
    public function linha() {
        return $this->belongsTo('Artaban\Model\Linha', 'Linha_Id'); 
    }
    
    
    public function pontoReferencia() {
        return $this->belongsTo('Artaban\Model\PontoReferencia', 'PontoReferencia_Id'); 
    }
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        $linhasDaEmpresa = Linha::where('Pessoa_Id', '=', $empresaId)->lists('Id');
        
        return $query->whereIn('Linha_Id', $linhasDaEmpresa);
    }
    
    
}