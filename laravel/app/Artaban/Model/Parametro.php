<?php
namespace Artaban\Model;

use \Eloquent;


class Parametro extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Parametro';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'Nome' => 'required',
	'Descricao' => 'required'
    );
    
}