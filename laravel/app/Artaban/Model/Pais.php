<?php
namespace Artaban\Model;

use Artaban\Model\Localidade;

use \Eloquent;


class Pais extends Eloquent {
	    
    const BRASIL_ID = 1;
    
    
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Pais';
    
    protected $primaryKey = 'Id';
    
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'Nome' => 'required',
	    'Sigla' => 'required'
    );
    
    
    public function estados() {
         return $this->hasMany('Artaban\Model\Estado', 'Pais_Id');  
    }
    
    
    public function scopeEstrangeiro($query) {
        return $query->where('Id', '<>', Pais::BRASIL_ID);
    }
    
}