<?php
namespace Artaban\Model;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

use Artaban\Model\Grupo;
use Artaban\Model\Modulo;

use \Eloquent;


class Usuario extends Eloquent implements UserInterface, RemindableInterface {
    
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ART_Usuario';
    
    protected $primaryKey = 'Id';
        
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('Senha', 'super_usuario');
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at', 'super_usuario');
    
	public static $rules = array(
		'Nome' => 'required',
		'Email' => 'required',
		'Senha' => 'required'
	);
    
	
    /**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier() {
		return $this->getKey();
	}

    
    /**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword() {
		return $this->Senha;
	}

    
	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail() {
		return $this->Email;
	}


    public function empresa() {
        return $this->belongsTo('Artaban\Model\Empresa', 'Pessoa_Id'); 
    }
    
}