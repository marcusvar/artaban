<?php
namespace Artaban\Model;

use \Eloquent;


class Linha extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Linha';
    
    protected $primaryKey = 'ID';
        
    protected $guarded = array('ID', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Situacao' => 'required',
		'PontoReferencia_Id_Destino' => 'required'
	);
    
    
    public static $situacoes = array(
        'Normal'    => 'Normal',
	    'Bloqueada' => 'Bloqueada'
    );
    
    
    public static function listarPorDescricaoCompleta($empresaId) {
        $linhas = static::daEmpresa($empresaId)->get();
	
        $lista = [];
        foreach($linhas as $linha) {
            $lista[$linha->ID] = $linha->pontoReferencia->descricaoCompleta();
        }
        
        return $lista;    
    }
    

    public function pontoReferencia() {
        return $this->belongsTo('Artaban\Model\PontoReferencia', 'PontoReferencia_Id_Destino'); 
    }
    
    
    public function pontoReferenciaDestino() {
        return $this->belongsTo('Artaban\Model\PontoReferencia', 'PontoReferencia_Id_Destino'); 
    }
    
    
    public function pontoReferenciaVia() {
        return $this->belongsTo('Artaban\Model\PontoReferencia', 'PontoReferencia_Id_Via'); 
    }
    
    
    public function itens() {
        return $this->hasMany('Artaban\Model\ItemLinha', 'Linha_Id'); 
    }
    
    
    public function adicionaisTarifa() {
         return $this->hasMany('Artaban\Model\AdicionalTarifa', 'Linha_Id');  
    }
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        return $query->where('Pessoa_Id', '=', $empresaId);
    }
    
}