<?php
namespace Artaban\Model;

use Artaban\Lib\Formatacao\DataHora\DataHora;

use \Eloquent;


class ItemViagem extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_ViagemItem';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'Viagem_Id' => 'required',
        'HorarioItem_Id' => 'required',
        'Ativo' => 'required',
        'DataHoraEmbarque' => 'required'
    );
   
    
    public static $ativoOpcoes = array(
        '1' => 'Sim',
	'0' => 'Não'
    );
     
    
    public static $recolherOpcoes = array(
        '1' => 'Sim',
	'0' => 'Não'
    );

    public static $webOpcoes = array(
        '1' => 'Sim',
	'0' => 'Não'
    );
    
    public function descricaoTransbordoViagem() {
        $descricao = '';
        if(isset($this->itemHorarioTransbordo->pontoReferencia)) {
            $descricao = sprintf("%s", $this->itemHorarioTransbordo->pontoReferencia->descricaoCompleta());
        }
        return $descricao;        
    }
   
         
    public function ativoDescricao() {
        return static::$ativoOpcoes[$this->Ativo];
    }
   
    
    public function recolherDescricao() {
        return static::$recolherOpcoes[$this->Recolher];
    }
   
    
    public function webDescricao() {
        return static::$webOpcoes[$this->Web];
    }
    
    
    public function setDataHoraEmbarqueAttribute($dataHoraEmbarque) {
        $this->attributes['DataHoraEmbarque'] = DataHora::formatar($dataHoraEmbarque, DataHora::BR, DataHora::EUA, null);
    }
    
    
    public function getDataHoraEmbarqueAttribute($dataHoraEmbarque) {
        return DataHora::formatar($dataHoraEmbarque, DataHora::EUA, DataHora::BR);
    }
    
    
    public function getDataEmbarqueAttribute() {
        return DataHora::formatar($this->DataHoraEmbarque, DataHora::BR, 'd/m/Y');
    }
    
    
    public function getHoraEmbarqueAttribute() {
        return DataHora::formatar($this->DataHoraEmbarque, DataHora::BR, 'H:i:s');
    }
    
    
    public function viagem() {
        return $this->belongsTo('Artaban\Model\Viagem', 'Viagem_Id'); 
    }
    
    
    public function itemHorario() {
        return $this->belongsTo('Artaban\Model\ItemHorario', 'HorarioItem_Id'); 
    }
    

    public function itemHorarioTransbordo() {
        return $this->belongsTo('Artaban\Model\ItemLinha', 'HorarioItem_Id_Transbordo'); 
    }
    

    public function scopeDaEmpresa($query, $empresaId) {
        $viagensDaEmpresa = Viagem::daEmpresa($empresaId)->lists('Id');
        
        return $query->whereIn('Viagem_Id', $viagensDaEmpresa);
    }
    
}