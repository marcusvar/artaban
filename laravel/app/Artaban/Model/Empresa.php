<?php
namespace Artaban\Model;

use \Eloquent;


class Empresa extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Pessoa';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Nome' => 'required',
		'NomeUsual' => 'required',
		'CPFCNPJ' => 'required',
		'RGIE' => 'required',
		'Email' => 'required',
		'Sexo' => 'required',
		'Responsavel' => 'required',
		'EmailResponsavel' => 'required',
		'Financeiro' => 'required',
		'EmailFinanceiro' => 'required',
		'Telefone' => 'required',
		'Celular' => 'required',
		'VisualizarFidelizacao' => 'required',
		'Pessoa_Id' => 'required'
	);
    
    
    public function empresa() {
        return $this->belongsTo('Artaban\Model\Pessoa', 'Pessoa_Id'); 
    }
    
    
    public function pessoas() { 
        return $this->belongsTo('Artaban\Model\Estado', 'UF_Id'); 
    }
        
}