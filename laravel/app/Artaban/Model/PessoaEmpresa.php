<?php
namespace Artaban\Model;

use Artaban\Model\Fidelizacao;
use Artaban\Validation\Documento;

use \Eloquent;
use \Auth;


class PessoaEmpresa extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_PessoaCliente';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'Pessoa_Id_Responsavel' => 'required',
        'VisualizarFidelizacao' => 'required',
        'Pessoa_Id' => 'required'
    );
    
    
    public static $visualizarFidelizacaoOpcoes = array(
        '0' => 'Não',
        '1' => 'Sim'
    );
    
    
    public function visualizarFidelizacaoDescricao() {
        return static::$visualizarFidelizacaoOpcoes[$this->VisualizarFidelizacao];
    }
   
        
    public function pessoa() {
        return $this->belongsTo('Artaban\Model\Pessoa', 'Pessoa_Id'); 
    }
    
    
    public function responsavel() {
        return $this->belongsTo('Artaban\Model\Pessoa', 'Pessoa_Id_Responsavel'); 
    }
    
    
    public function registrosDeFidelizacao() {
        return $this->hasMany('Artaban\Model\Fidelizacao', 'Pessoa_Id'); 
    }
    
    
    public function saldoDeFidelizacao() {
        $creditos = Fidelizacao::Credito()->doPessoa($this->Id)->sum('ValorOperacao');
        $debitos  = Fidelizacao::Debito()->doPessoa($this->Id)->sum('ValorOperacao');
        
        return $creditos - $debitos;
    }

}