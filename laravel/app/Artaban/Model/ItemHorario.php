<?php
namespace Artaban\Model;

use \Eloquent;


class ItemHorario extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_HorarioItem';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
	'Horario_Id' => 'required',
	'LinhaItem_Id' => 'required',
        'DiaEmbarque' => 'required',
        'HoraEmbarque' => 'required'
	);
    
    
    public static $diasEmbarque = array(
        0 => 'No mesmo dia',
        1 => '1 dia depois',
        2 => '2 dias depois',
        3 => '3 dias depois'
    );
   
   
    public static $recolherOpcoes = array(
        '1' => 'Sim',
	'0' => 'Não'
    );
    
    public function recolherDescricao() {
        return static::$recolherOpcoes[$this->Recolher];
    }
    
    public static $webOpcoes = array(
        '1' => 'Sim',
	'0' => 'Não'
    );

    
    public function webDescricao() {
        return static::$webOpcoes[$this->Web];
    }
   
    
    public function diaEmbarqueDescricao() {
        return static::$diasEmbarque[$this->DiaEmbarque];
    }
   
   
    public function descricaoDaLinha() {
        $descricao = sprintf("%s", $this->itemLinha->pontoReferencia->descricaoCompleta());
        return $descricao;        
    }
   
    
    public function descricaoTransbordo() {
        $descricao = '';
        if(isset($this->itemHorario->pontoReferencia)) {
            $descricao = sprintf("%s", $this->itemHorario->pontoReferencia->descricaoCompleta());
        }
        return $descricao;        
    }
   
        
    public function descricaoTransbordoViagem() {
        $descricao = '';
        if(isset($this->itemHorarioTransbordo->pontoReferencia)) {
            $descricao = sprintf("%s", $this->itemHorarioTransbordo->pontoReferencia->descricaoCompleta());
        }
        return $descricao;        
    }
   
        
    public function descricaoDaLinhaComHorario() {
        $descricao = sprintf("%s (%s)",
                             $this->itemLinha->pontoReferencia->descricaoCompleta(),
                             $this->HoraEmbarque);
        
        return $descricao;        
    }
   
   
    public function itemLinha() {
        return $this->belongsTo('Artaban\Model\ItemLinha', 'LinhaItem_Id'); 
    } 


    public function itemHorario() {
        return $this->belongsTo('Artaban\Model\ItemLinha', 'LinhaItem_Id_Transbordo'); 
    } 


    //public function itemHorarioTransbordo() {
    //    return $this->belongsTo('Artaban\Model\ItemLinha', 'HorarioItem_Id_Transbordo'); 
    //} 


    public function scopeDaEmpresa($query, $empresaId) {
        $horariosDaEmpresa = Horario::daEmpresa($empresaId)->lists('Id');
        
        return $query->whereIn('Horario_Id', $horariosDaEmpresa);
    }
    
}