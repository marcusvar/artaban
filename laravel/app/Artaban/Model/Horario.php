<?php
namespace Artaban\Model;

use Exception;
use DB;
use Eloquent;


class Horario extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    
    protected $table = 'ART_Horario';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Linha_Id' => 'required',
        'TipoVeiculo_Id' => 'required',
		'Horario' => 'required',
		'Duracao' => 'required',
        'Frequencia' => 'required',
		'Feriado' => 'required',
		'Confirmado' => 'required',
		'Situacao' => 'required'
	);
    
    
    public static $sentidos = array(
        '' => '',
        'Ida'   => 'Ida',
	    'Volta' => 'Volta'
    );
    
    
    public static $frequencias = array(0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0, 6 => 0);
    
    public static $frequenciasDescricao = array(
        0 => 'Domingo',
        1 => 'Segunda',
        2 => 'Terça',
        3 => 'Quarta',
        4 => 'Quinta',
        5 => 'Sexta',
        6 => 'Sábado'
    );
    
    
    public static $situacoes = array(
        'Normal'    => 'Normal',
	    'Bloqueado' => 'Bloqueado'
    );
    
    
    public static $confirmadoOpcoes = array(
        'Sim' => 'Sim',
	    'Não' => 'Não'
    );
    
    
    public static $feriadoOpcoes = array(
        '0' => 'Excluir',
        '1' => 'Incluir',
	    '2' => 'Ignorar',
    );
 
 
    public function getDuracaoDescricaoAttribute() {
        $d = (int) $this->Duracao;
        return ($d > 1) ? "$d dias" : "$d dia";
    }
    
    
    public function setFrequenciaAttribute($frequencia){
         $frequencia = $frequencia + static::$frequencias;
         ksort($frequencia);
         
         $this->attributes['Frequencia'] = implode("", $frequencia);
    }
    
    
    public function getFrequenciaAttribute($frequencia){
        $frequencia = str_split($frequencia);
        return $frequencia + static::$frequencias;
    }
    
    
    public function frequenciaDescricao() {
        $frequencia = array_filter($this->Frequencia, function($valor) {
            return $valor;
        });
        
        $frequenciaDescricao = array_intersect_key(self::$frequenciasDescricao, $frequencia);
        
        return implode(", ", $frequenciaDescricao);            
    }
    
    
    public function dom() {
        return array_get($this->Frequencia, 0);          
    }
    
    
    public function seg() {
        return array_get($this->Frequencia, 1);          
    }
    
    
    public function ter() {
        return $this->Frequencia[2];          
    }
    
    
    public function qua() {
        return $this->Frequencia[3];          
    }
    
    
    public function qui() {
        return $this->Frequencia[4];          
    }
    
    
    public function sex() {
        return $this->Frequencia[5];          
    }
    
    
    public function sab() {
        return $this->Frequencia[6];          
    }
     
            
    public function feriadoDescricao() {
        return static::$feriadoOpcoes[$this->Feriado];
    }
    
    
    public function descricaoDaLinha() {
        return $this->linha->pontoReferencia->descricaoCompleta(); 
    }
    
    
    public function descricaoDaLinhaComHorario() {
        $descricao = sprintf("%s (%s) - %s",
                             $this->linha->pontoReferencia->descricaoCompleta(),
                             $this->Horario,
                             $this->Descricao);
        
        return $descricao;        
    }
    

    public function linha() {
        return $this->belongsTo('Artaban\Model\Linha', 'Linha_Id'); 
    }
    
        
    public function itemLinhaDestino() {
        return $this->belongsTo('Artaban\Model\ItemLinha', 'LinhaItem_Id_Destino'); 
    }
    
    
    public function tipoVeiculo() {
        return $this->belongsTo('Artaban\Model\TipoVeiculo', 'TipoVeiculo_Id'); 
    }
    
    
    public function itens() {
         return $this->hasMany('Artaban\Model\ItemHorario', 'Horario_Id');  
    }


    public function delete() {
        try {
            DB::transaction(function() {
                foreach($this->itens as $item) {
                    $item->delete();
                }
                
                parent::delete();
            });
            
        } catch (Exception $e)  {
            throw $e;
        }
    }    
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        $linhasDaEmpresa = Linha::daEmpresa($empresaId)->lists('ID');
        
        return $query->whereIn('Linha_Id', $linhasDaEmpresa);
    }
    
}