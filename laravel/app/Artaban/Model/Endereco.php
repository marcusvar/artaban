<?php
namespace Artaban\Model;

use \Eloquent;


class Endereco extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_PessoaEndereco';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Logradouro' => 'required',
		'Numero' => 'required',
		//'Bairro' => 'required',
		//'Complemento' => 'required',
		//'Cep' => 'required',
		'Localidade_Id' => 'required',
		'Pessoa_Id' => 'required'
	);
    
    
    public function pessoa() {
        return $this->belongsTo('Artaban\Model\Pessoa', 'Pessoa_Id'); 
    }
   
    
    public function localidade() {
        return $this->belongsTo('Artaban\Model\Localidade', 'Localidade_Id'); 
    } 
    
}