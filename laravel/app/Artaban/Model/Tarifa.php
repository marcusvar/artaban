<?php
namespace Artaban\Model;

use Artaban\Lib\Formatacao\DataHora\Data;
use Artaban\Lib\Formatacao\Dinheiro\Dinheiro;

use \Eloquent;


class Tarifa extends Eloquent {
	    
    const UNICA = 'unica';
    const MISTA = 'mista';
    
        
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Tarifa';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'LinhaItem_Id' => 'required',
        'TipoVeiculo_Id' => 'required',
        'DataVigencia' => 'required',
        'Valor' => 'required'
    );
    

    public function setDataVigenciaAttribute($data) {
       $this->attributes['DataVigencia'] = Data::formatar($data, Data::BR, Data::EUA, null);
    }
    
    
    public function getDataVigenciaAttribute($data) {
        return Data::formatar($data, Data::EUA, Data::BR);
    }


    public function setValorAttribute($valor) {
        $this->attributes['Valor'] = Dinheiro::semMascara($valor);
    }
    
    
    public function getValorAttribute($valor) {
        return Dinheiro::comMascara($valor);
    }
    
    
    public function setValor2Attribute($valor) {
        $this->attributes['Valor2'] = Dinheiro::semMascara($valor);
    }
    
    
    public function getValor2Attribute($valor) {
        return Dinheiro::comMascara($valor);
    }
    
    
    public function unica() {
        return (($this->exists) && ((float) $this->attributes['Valor2'] == 0));
    }
    
    
    public function mista() {
        return (($this->exists) && ((float) $this->attributes['Valor2'] > 0));
    }
    
    
    public function itemLinha() {
        return $this->belongsTo('Artaban\Model\ItemLinha', 'LinhaItem_Id'); 
    }
    
    
    public function tipoVeiculo() {
        return $this->belongsTo('Artaban\Model\TipoVeiculo', 'TipoVeiculo_Id'); 
    }

}