<?php
namespace Artaban\Model;

use Artaban\Lib\Formatacao\DataHora\Data;
use Artaban\Lib\Formatacao\Dinheiro\Dinheiro;
use Artaban\Lib\Formatacao\String\String;

use \Eloquent;


class Fidelizacao extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_Fidelizacao';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $operacaoOpcoes = array(
        'Crédito' => 'Crédito',
        'Débito'  => 'Débito'        
    );
    
    public static $rules = array(
        'Operacao' => 'required',
        'DataOperacao' => 'required',
        'ValorOperacao' => 'required',
        'Pessoa_Id' => 'required'
    );
    
    
    public function pessoa() {
        return $this->belongsTo('Artaban\Model\Pessoa', 'Pessoa_Id'); 
    }

    
    public function setDataOperacaoAttribute($data) {
       $this->attributes['DataOperacao'] = Data::formatar($data, Data::BR, Data::EUA, null);
    }
    
    
    public function getDataOperacaoAttribute($data) {
        return Data::formatar($data, Data::EUA, Data::BR);
    }
    
    
    public function setValorOperacaoAttribute($valor) {
        $this->attributes['ValorOperacao'] = Dinheiro::semMascara($valor);
    }
    
    
    public function getValorOperacaoAttribute($valor) {
        return Dinheiro::comMascara($valor);
    }    

    public function valorCompra() {
        return Dinheiro::comMascara($this->attributes['ValorOperacao']/0.01);
    }

    
    public function descricaoResumida() {
        return String::resumir($this->Descricao, 60);
    }
    
    
    public function scopeDoPessoa($query, $pessoaId) {
        return $query->where('Pessoa_Id', '=', $pessoaId);
    }
    
    
    public function scopeCredito($query) {
        return $query->where('Operacao', '=', 'Crédito');
    }
    
    
    public function scopeDebito($query) {
        return $query->where('Operacao', '=', 'Débito');
    }
    
}