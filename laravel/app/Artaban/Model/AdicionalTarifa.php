<?php
namespace Artaban\Model;

use Artaban\Lib\Formatacao\DataHora\Data;
use Artaban\Lib\Formatacao\Dinheiro\Dinheiro;

use \Eloquent;


class AdicionalTarifa extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_AdicionalTarifa';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Linha_Id' => 'required',
		'TipoAdicionalTarifa_Id' => 'required',
		'DataVigencia' => 'required',
		'Valor' => 'required'
	);

    
    public function setDataVigenciaAttribute($data) {
       $this->attributes['DataVigencia'] = Data::formatar($data, Data::BR, Data::EUA, null);
    }
    
    
    public function getDataVigenciaAttribute($data) {
        return Data::formatar($data, Data::EUA, Data::BR);
    }
    
    
    public function setValorAttribute($valor) {
        $this->attributes['Valor'] = Dinheiro::semMascara($valor);
    }
    
    
    public function getValorAttribute($valor) {
        return Dinheiro::comMascara($valor);
    }
    
    
    public function linha() { 
        return $this->belongsTo('Artaban\Model\Linha', 'Linha_Id'); 
    }
    
    
    public function tipoAdicionalTarifa() { 
        return $this->belongsTo('Artaban\Model\TipoAdicionalTarifa', 'TipoAdicionalTarifa_Id'); 
    }
    
    
    public function scopeDaEmpresa($query, $empresaId) {
        $linhasDaEmpresa = Linha::where('Pessoa_Id', '=', $empresaId)->lists('Id');
        
        return $query->whereIn('Linha_Id', $linhasDaEmpresa);
    }
    
}