<?php
namespace Artaban\Model;

use \Eloquent;


class PessoaTelefone extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_PessoaTelefone';
    
    protected $primaryKey = 'Id';
        
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
		'Descricao' => 'required',
		'Telefone' => 'required',
	);
    
    
    public function pessoa() {
        return $this->belongsTo('Artaban\Model\Pessoa', 'Pessoa_Id'); 
    }
       
}