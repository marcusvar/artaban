<?php
namespace Artaban\Model;

use Artaban\Model\Modulo;
use Artaban\Model\Usuario;
use Artaban\Model\Permissao;

use \Eloquent;


class Grupo extends Eloquent {
	
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'Grupo';
    
    protected $guarded = array('id', 'created_at', 'updated_at', 'deleted_at');

	public static $rules = array(
		'nome' => 'required',
		'modulo_id' => 'required'
	);
    
   
    public function modulo() { 
 	    return $this->belongsTo('Artaban\Model\Modulo'); 
	}
    
    
    public function usuarios() {
         return $this->belongsToMany('Artaban\Model\Usuario', 'UsuarioGrupo');
    }
    
    
    public function permissoes() {
         return $this->belongsToMany('Artaban\Model\Permissao', 'GrupoPermissao');
    }

}