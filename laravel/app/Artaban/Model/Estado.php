<?php
namespace Artaban\Model;

use Artaban\Model\Localidade;
use Artaban\Model\Pais;

use Eloquent;


class Estado extends Eloquent {
	
    /**
      * The database table used by the model.
      *
      * @var string
      */
    protected $table = 'ART_UF';
    
    protected $primaryKey = 'Id';
    
    protected $guarded = array('Id', 'created_at', 'updated_at', 'deleted_at');

    public static $rules = array(
        'Nome' => 'required',
	    'Sigla' => 'required'
    );
    
    
    public function pais() { 
        return $this->belongsTo('Artaban\Model\Pais', 'Pais_Id'); 
    }
    
    
    public function localidades() {
         return $this->hasMany('Artaban\Model\Localidade', 'UF_Id');  
    }
    
    
    public function scopeDoBrasil($query) {
        return $query->where('Pais_Id', '=', Pais::BRASIL_ID);
    }
    
}