<?php
namespace Artaban\Autorizacao;

use \Auth;
use \Cache;
use \Redirect;
use \Exception;
use \View;
use \Config;
use Artaban\Model\Usuario;


class Autorizacao {
    
    
    public function login($email, $senha, $redirect = "") {
        
        if (Auth::attempt(array('email' => $email, 'password' => $senha), false)) {
            
            if ($redirect !== "") {
                return Redirect::to($redirect);
            }
            
            return Redirect::route('index');
        }
    
        return Redirect::route('login')
                       ->with('erro_login', 'Por favor, verifique se o seu email e senha estão corretos.')
                       ->withInput();        
    }
        
    
    public function logout() {        
        Auth::logout();
        return Redirect::route('login');
    }
        
        
    public function verificarPermissao($tipo, $permissao) {
        if (($tipo === "") || ($permissao === "")) {
            return false;
        }
        
        if ($this->usuarioAutenticado()->super_usuario) {
            return true;
        }
        
        try {
            $permissaoParam = $permissao;
            $permissoes = $this->permissoes();
            
            foreach($permissoes as $permissao) {
                $tipoPermissao = $permissao->tipoPermissao;
                                
                if (($tipoPermissao->tipo === 'view') || (! preg_match('@(\*)$@', $permissao->permissao))) {
                    if ($permissao->permissao === $permissaoParam) {
                        return true;
                    }
                } else {
                    $_permissao = preg_replace('@(\*)$@', "", $permissao->permissao);
                    $regex = "@^($_permissao)@";
                
                    if (preg_match($regex, $permissaoParam)) {
                        return true;    
                    }
                }
            }
           
            return false;
        } catch (\Exception $e) {
            return false; 
        }
    }
        
    
    public function verificarAcesso($path, $urlIndexModulo) {
        if (! $this->usuarioAutenticado()->super_usuario) {
            $grupo = $this->grupoUsuarioAutenticado();    
        
            if ($grupo == null) {
                return $this->checarModulosERedirecionar($urlIndexModulo);
            }
            
            $autorizado = $this->verificarPermissao('menu', $path);
            
            if (! $autorizado) {
                return Redirect::to($urlIndexModulo);    
            }
        }
    }    
        
    
    public function usuarioAutenticado() {
        $id = Auth::user()->Id;
        $usuario = Usuario::find($id);
        
        return $usuario;               
    }
         
    
    private function checarModulosERedirecionar($urlPadrao){
        $modulos = $this->usuarioAutenticado()->getModulos();
        
        if ((! $this->usuarioAutenticado()->super_usuario) && (count($modulos) === 0 )) {
            Auth::logout();
            
            return Redirect::to(Config::get('eucatur.app.authenticationURL'). 'login')
                           ->with('erro_login', 'Você ainda não está cadastrado em nenhum módulo')
                           ->withInput();
        }
                
        $url = $urlPadrao;
        
        if (count($modulos) === 1) {
            $url = 'http://' . $modulos[0]->url . Config::get('eucatur.app.domain');
        }
                    
        return Redirect::to($url);        
    }    
    
        
    public function permissoes() {
        $modulo = \Artaban\Helper\Modulo::atual();
        $grupo = $this->grupoUsuarioAutenticado();
        
        $permissoes = $this->todasAsPermissoes();
        return $permissoes[$grupo->id];
    }
    
    
    public function grupoUsuarioAutenticado() {
        $modulo = \Artaban\Helper\Modulo::atual();
        $usuario = $this->usuarioAutenticado();
        
        $section = $this->cacheParam()['section'];
        $key = 'grupo_usuario_modulo_id_' . $modulo->id;   
        
        return Cache::section($section)
               ->remember($key,
                          15,
                          function() use ($usuario, $modulo) {
            
            return $usuario->grupoPorModulo($modulo);               
        });    
    }
    
    
    public function menu() {
        if (! Auth::check()) {
            $menu = [];
            //View::addNamespace('Artaban', base_path() . '/../Artaban/app/views/shared');
            return View::make('shared._menu', compact('menu'));
        }
        
        $modulo = \Artaban\Helper\Modulo::atual();        
        $section = $this->cacheParam()['section'];
        $key = 'menu_modulo_id_' . $modulo->id;        
                
        if (! Cache::section($section)->has($key)) {
            $tiposPermissao = $modulo->tiposPermissao('menu');
        
            $menu = [];
            foreach($tiposPermissao as $x => $tipo) {
                $menu[$x]['descricao'] = $tipo->descricao;
                $menu[$x]['links'] = []; 
            
                foreach ($tipo->permissoes as $y => $permissao) {
                    if ($this->verificarPermissao('menu', $permissao->permissao)) {
                        $menu[$x]['links'][$y] = array(
                            'url' => \Artaban\Helper\Menu::makeUrl($permissao->permissao),
                            'title' => $permissao->descricao
                        );       
                    }                 
                }
            }
                      
            View::addNamespace('Artaban', base_path() . '/../Artaban/app/views/shared');
            $view = View::make('Artaban::_menu', compact('menu'));
            
            Cache::section($section)->put($key, $view->render(), Config::get('eucatur.cache.tempo')); 
        }
        
        return Cache::section($section)->get($key);  
    }
    
    
    private function todasAsPermissoes() {
        $section = $this->cacheParam()['section'];
        $key = 'permissoes';
        
        return Cache::section($section)
               ->remember($key,
                          Config::get('eucatur.cache.tempo'),
                          function() {
            
            $grupos = $this->usuarioAutenticado()->grupos()->get();
            $listaPermissoes = [];
            foreach($grupos as $grupo) {
                $listaPermissoes[$grupo->id] = $grupo->permissoes()->with('tipoPermissao')->get();
            }
            
            return $listaPermissoes;    
        });    
    }
    
    
    private function cacheParam() {
        return array(
            'section' => 'section_autorizacao_usuario_id_' . $this->usuarioAutenticado()->id,
        );    
    }
    
}

?>