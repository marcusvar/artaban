<?php
namespace Artaban\Service\Cadastro;

use Artaban\Model\Horario as ModelHorario;
use Artaban\Model\ItemHorario as ModelItemHorario;
use Artaban\Model\TipoVeiculo as ModelTipoVeiculo;
use Artaban\Model\Linha as ModelLinha;

use Artaban\Lib\Exception\ExceptionValidator;

use DB;
use Exception;
use Validator;


class Horario {
    
    private $params;
    private $erros;
    private $linha;
    private $tipoVeiculo;
    private $horario;
    
    
    public function __construct($params) {
        $this->params = $params;
        
        $empresaId     = array_get($this->params, 'Empresa_Id');
        $linhaId       = array_get($this->params, 'Linha_Id');
        $tipoVeiculoId = array_get($this->params, 'TipoVeiculo_Id');
        
        $this->linha = ModelLinha::daEmpresa($empresaId)->findOrFail($linhaId);
        
        $this->tipoVeiculo = ModelTipoVeiculo::daEmpresa($empresaId)->findOrFail($tipoVeiculoId);
    }
   
    
    public function erros() {
        return $this->erros;
    }
   
    
    public function salvar() {
        try {
            DB::transaction(function() {
                $this->salvarModels();
            });
             
         } catch (ExceptionValidator $e) {
	        $this->erros = $e->getValidator();
            throw $e;     
        
        } catch (Exception $e) {
	        $this->erros = null;
            throw $e;     
        }   
        
        return $this->horario;
    }
    
        
    protected function salvarModels() {
        $this->validarHorario($this->params);   
        
        $id = array_get($this->params, 'id');
        
        $horarioParams = array('Linha_Id', 'TipoVeiculo_Id', 'Duracao', 'Horario', 'Frequencia',
                               'Feriado', 'Confirmado', 'Situacao', 'Descricao');
        
        $params = array_only($this->params, $horarioParams);
        
        if (! $id) {
            $horario = new ModelHorario($params);
            $horario->save();
            
            foreach($this->linha->itens as $item) {
                $params = array(
                    'Horario_Id'   => $horario->Id,
                    'LinhaItem_Id' => $item->ID,
                    'DiaEmbarque'  => ModelItemHorario::$diasEmbarque[0],
                    'HoraEmbarque' => $horario->Horario
                );
                
                $this->validarItemHorario($params);
                
                $itemHorario = new ModelItemHorario($params);
                $itemHorario->save();
            }
            
        } else {
            $horario = ModelHorario::find($id);
            $horario->update($params);
        }
        
        $this->horario = $horario;
    }
    
    
    protected function validarHorario($params) {
        $this->validar($params, ModelHorario::$rules);   
    }
    
    
    protected function validarItemHorario($params) {
        $this->validar($params, ModelItemHorario::$rules);    
    }
    
    
    protected function validar($params, $regras) {
        $validation = Validator::make($params, $regras);
        
        if ($validation->fails()) {
            throw new ExceptionValidator($validation); 
        }  
    }

}

?>