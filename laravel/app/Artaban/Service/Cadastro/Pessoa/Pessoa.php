<?php
namespace Artaban\Service\Cadastro\Pessoa;

use Artaban\Model\Pessoa as ModelPessoa;
use Artaban\Model\PessoaEmpresa as ModelPessoaEmpresa;
use Artaban\Model\Endereco as ModelEndereco;

use Artaban\Lib\Exception\ExceptionValidator;

use Artaban\Service\Cadastro\Pessoa\PessoaPFBrasileiro as Cadastro_PFBrasileiro;
use Artaban\Service\Cadastro\Pessoa\PessoaPJBrasileiro as Cadastro_PJBrasileiro;
use Artaban\Service\Cadastro\Pessoa\PessoaPFEstrangeiro as Cadastro_PFEstrangeiro;
use Artaban\Service\Cadastro\Pessoa\PessoaPJEstrangeiro as Cadastro_PJEstrangeiro;

use Exception;
use Validator;


class Pessoa {
    
    private $params;
    private $erros;
    
    
    public function __construct($params) {
        $this->params = $params;
    }
    
    
    public function salvar() {
        
        try {            
            switch ($this->params['Pessoa']['Tipo']) {
                case ModelPessoa::PF_BRASILEIRO:
                    $cadastro = new Cadastro_PFBrasileiro($this->params);
                    $pessoa = $cadastro->salvar();
                    break;
            
                case ModelPessoa::PJ_BRASILEIRO:
                    $cadastro = new Cadastro_PJBrasileiro($this->params);
                    $pessoa = $cadastro->salvar();
                    break;
            
                case ModelPessoa::PF_ESTRANGEIRO:
                    $cadastro = new Cadastro_PFEstrangeiro($this->params);
                    $pessoa = $cadastro->salvar();
                    break;
            
                case ModelPessoa::PJ_ESTRANGEIRO:
                    $cadastro = new Cadastro_PJEstrangeiro($this->params);
                    $pessoa = $cadastro->salvar();
                    break;
            }
            
            return $pessoa;
            
        } catch (ExceptionValidator $e) {
	        $this->erros = $e->getValidator();
            throw $e;     
        
        } catch (Exception $e) {
	        $this->erros = null;
            throw $e;     
        }   
    }
    
    
    public function erros() {
        return $this->erros;
    }
    
    
    
    /*
    
    private function validacaoPFBrasileiro($params) {
        $rules = array(
            'Nome' => 'required',
            'RGIE' => 'required',
        );
        
        $messages = array(
            'RGIE.required' => 'O campo RG é obrigatório.',
        );
        
        if (array_get($params, 'EmPotencial', 0)) {
            unset($rules['RGIE']);
        }
        
        return Validator::make($params, $rules, $messages);
        
    }
    
    
    private function validarPessoa($params) {
         switch ($params['Tipo']) {
            case ModelPessoa::PF_BRASILEIRO:
                $validacao = $this->validacaoPFBrasileiro($params); 
                break;
            
            case ModelPessoa::PJ_BRASILEIRO:
                $this->html = FormPJBrasileiro::gerar($pessoa); 
                break;
            
            case ModelPessoa::PF_ESTRANGEIRO:
                $this->html = FormPFEstrangeiro::gerar($pessoa); 
                break;
            
            case ModelPessoa::PJ_ESTRANGEIRO:
                $this->html = FormPJEstrangeiro::gerar($pessoa); 
                break;
        }
                
        if (! $validacao->passes()) {
            $this->erros = $validacao;
            throw new Exception('Erro ao salvar pessoa'); 
        }  
            
    }
    
    
    
    public function validar($params, $rules) {
        $messages = array(
           'Localidade_Id.required' => 'O campo Localidade é obrigatório.',
        );
        
        $validation = Validator::make($params, $rules, $messages);
        
        if (! $validation->passes()) {
            $this->erros = $validation;
            throw new Exception('Erro ao salvar pessoa'); 
        }    
    }
    
    
    public function salvarComEndereco($params = array('pessoaParams'  => [],
                                                      'enderecoParams' => [],
                                                      'cnpjParams'     => [])) {
        
        // Recebe os parâmetros enviados para salvar o pessoa
        $pessoaParams = $params['pessoaParams'];
        
        $sexo = array_get($pessoaParams, 'Sexo');
        $pessoaParams['Sexo'] = in_array($sexo, ['M', 'F']) ? $sexo : 'M'; 
        
        $enderecoParams = $params['enderecoParams'];
        $cnpjParams = $params['cnpjParams'];
                
        // Faz a validação dos dados do pessoa
        $this->validarPessoa($pessoaParams);
        
        // A regra de validação da chave Pessoa_Id é removida porque ela será inserida manualmente        
        $rulesEndereco = ModelEndereco::$rules;
	    unset($rulesEndereco['Pessoa_Id']);
	
        // Faz a validação dos dados do endereço
        $this->validar($enderecoParams, $rulesEndereco);
        
        if (isset($pessoaParams['id'])) {
            $pessoa = ModelPessoa::with('endereco')->find($pessoaParams['id']);
            $pessoa->update($pessoaParams);
            
            $pessoa->endereco->update($enderecoParams);
            
            if ($cnpjParams) {
                $pessoa->cnpj->update($cnpjParams);
            }
            
        } else {
            // Se as validações passaram, salvamos o pessoa e o seu endereço
            $pessoa = new ModelPessoa($pessoaParams);
            $pessoa->save();
            
            $endereco = new ModelEndereco($enderecoParams);
	        $endereco->Pessoa_Id = $pessoa->Id;
            $endereco->save();
            
            if ($cnpjParams) {
                $cnpj = new ModelPessoaEmpresa($cnpjParams);
	            $cnpj->Pessoa_Id = $pessoa->Id;
                $cnpj->save();
            }            
        }
        
        return $pessoa;    
    }
    
    */
   
}

?>