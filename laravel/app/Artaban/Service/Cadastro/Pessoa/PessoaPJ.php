<?php
namespace Artaban\Service\Cadastro\Pessoa;

use Artaban\Model\Pessoa as ModelPessoa;
use Artaban\Model\PessoaEmpresa as ModelPessoaEmpresa;
use Artaban\Model\Endereco as ModelEndereco;

use Artaban\Lib\Exception\ExceptionValidator;

use Artaban\Service\Cadastro\Pessoa\PessoaGenerico as Cadastro_PessoaGenerico;

use Exception;
use Validator;


class PessoaPJ extends Cadastro_PessoaGenerico  {
    
    
    protected function salvarModels() {
        $this->salvarPessoa();
        $this->salvarEndereco();
    }
    
    
    protected function validarPessoa() {
        $this->validarPessoa_Id_Responsavel();
        
        $rules = array(
            'Nome'      => 'required',
            'Documento' => 'required',
         //   'Pessoa_Id_Responsavel' => 'required'
        );
        
        $messages = array(
            'Documento.unique' => 'Já existe uma empresa cadastrada com esse Documento.',
            'Pessoa_Id_Responsavel.required' => 'O campo Responsável é obrigatório.',
        );
        
        if ($this->documentoEstaPreenchido() && ($this->params['Pessoa']['Tipo'] == ModelPessoa::PJ_BRASILEIRO)) {
            $regra = 'cnpj|unique:ART_Pessoa,Documento';
            $pessoaId = array_get($this->params['Pessoa'], 'Id');
            
            $rules['Documento'] = $pessoaId ? "$regra,$pessoaId" : $regra;
        }
        
        if ($this->clienteEmPotencial()) {
            unset($rules['Documento']);
            unset($rules['Pessoa_Id_Responsavel']);
        }
                
        $validator = Validator::make($this->params['Pessoa'], $rules, $messages);
        
        if (! $validator->passes()) {
            throw new ExceptionValidator($validator); 
        }         
    }
    
    
    protected function validarPessoa_Id_Responsavel() {
        $param = $this->params['Pessoa']['Pessoa_Id_Responsavel'];
        
        $this->params['Pessoa']['Pessoa_Id_Responsavel'] = ((int) $param > 0) ? $param : null; 
    }
    
}

?>