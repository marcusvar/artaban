<?php
namespace Artaban\Service\Cadastro\Pessoa;

use Artaban\Model\Pessoa as ModelPessoa;
use Artaban\Model\PessoaEmpresa as ModelPessoaEmpresa;
use Artaban\Model\Endereco as ModelEndereco;

use Artaban\Lib\Exception\ExceptionValidator;

use DB;
use Validator;
use Exception;


abstract class PessoaGenerico {
       
    protected $params;
    protected $pessoa;
   
    
    public function __construct($params) {
        $this->params = $params;    
    }
        
    
    public function salvar() {
        try {
            DB::transaction(function() {
                $this->salvarModels();
            });
             
        } catch (Exception $e)  {
            throw $e;
        }
        
        return $this->pessoa;
    }
    
    
    protected function salvarPessoa() {
        $this->validarPessoa();
        
        if ($this->novaPessoa()) {
            $pessoa = new ModelPessoa($this->params['Pessoa']);
            $pessoa->save();    
        } else {
            $pessoa = ModelPessoa::find($this->params['Pessoa']['Id']);
            $pessoa->update($this->params['Pessoa']);
        }
        
        $this->pessoa = $pessoa;
    }
    
    
    protected function salvarEndereco() {
        $this->validarEndereco();
        
        if ($this->enderecoEstaPreenchido()) {
            
            if ($this->novaPessoa() or is_null($this->pessoa->endereco)) {
                $endereco = new ModelEndereco($this->params['Endereco']);
	            $endereco->Pessoa_Id = $this->pessoa->Id;
                $endereco->save();
             } else {
                $endereco = $this->pessoa->endereco;
                $endereco->update($this->params['Endereco']);
             }
        }
    }
    
    
    abstract protected function salvarModels();
    
    
    abstract protected function validarPessoa(); 
    
    
    protected function validarEndereco() {
        $rules = array(
            'Logradouro' => 'required',
		    'Numero' => 'required',
		    'Localidade_Id' => 'required',
		);
        
        $messages = array(
            'Localidade_Id.required' => 'O campo Localidade é obrigatório.',
        );
        
        $rules = $this->enderecoEstaPreenchido() ? $rules : [];
        
        $validator = Validator::make($this->params['Endereco'], $rules, $messages);
        
        if (! $validator->passes()) {
            throw new ExceptionValidator($validator); 
        } 
        
    }
    
    
    protected function novaPessoa() {
        $pessoaId = array_get($this->params['Pessoa'], 'Id', null);
        return ($pessoaId == null);        
    }
    
    
    protected function documentoEstaPreenchido() {
        $pessoaDocumento = array_get($this->params['Pessoa'], 'Documento', "");
        return ($pessoaDocumento != "");
    }
    
    
    protected function enderecoEstaPreenchido() {
        $camposPreenchidos = array_filter($this->params['Endereco'], function($valor) {
            return ((string) $valor != "");
        });
        
        return (count($camposPreenchidos) > 0);
    }
    
    
    protected function clienteEmPotencial() {
        $emPotencial = array_get($this->params['Pessoa'], 'EmPotencial', 0);
        return ($emPotencial == 1);
    }
    
}

?>