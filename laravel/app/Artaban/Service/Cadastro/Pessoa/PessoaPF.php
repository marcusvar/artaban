<?php
namespace Artaban\Service\Cadastro\Pessoa;

use Artaban\Model\Pessoa as ModelPessoa;
use Artaban\Model\PessoaEmpresa as ModelPessoaEmpresa;
use Artaban\Model\Endereco as ModelEndereco;
use Artaban\Lib\Exception\ExceptionValidator;
use Artaban\Service\Cadastro\Pessoa\PessoaGenerico as Cadastro_PessoaGenerico;

use Exception;
use Validator;


class PessoaPF extends Cadastro_PessoaGenerico  {
    
    
    protected function salvarModels() {
        $this->salvarPessoa();
        $this->salvarEndereco();
    }
    
    
    protected function validarPessoa() {
        $rules = array(
            'Nome'      => 'required',
            'RGIE'      => 'required',
        );
        
        $messages = array(
            'RGIE.required' => 'O campo RG é obrigatório.',
            'Documento.unique' => 'Já existe uma pessoa cadastrada com esse Documento.'
        );
        
        if ($this->documentoEstaPreenchido()) {
            $regra = 'cpf|unique:ART_Pessoa,Documento';
            $pessoaId = array_get($this->params['Pessoa'], 'Id');
            
            $rules['Documento'] = $pessoaId ? "$regra,$pessoaId" : $regra;
        }
        
        if ($this->clienteEmPotencial()) {
            unset($rules['RGIE']);
        }
                
        $validator = Validator::make($this->params['Pessoa'], $rules, $messages);
        
        if (! $validator->passes()) {
            throw new ExceptionValidator($validator); 
        }         
    }
    
}

?>