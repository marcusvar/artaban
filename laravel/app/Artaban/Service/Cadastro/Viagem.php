<?php
namespace Artaban\Service\Cadastro;

use Artaban\Model\Viagem as ModelViagem;
use Artaban\Model\ItemViagem as ModelItemViagem;
use Artaban\Model\TipoVeiculo as ModelTipoVeiculo;
use Artaban\Model\Horario as ModelHorario;
use Artaban\Lib\Formatacao\DataHora\Data;
use Artaban\Lib\Formatacao\DataHora\DataHora;
use Artaban\Lib\Exception\ExceptionValidator;

use DB;
use Exception;
use Validator;
use DateTime;
use DateInterval;


class Viagem {
    
    private $params;
    private $erros;
    private $tipoVeiculo;
    private $horario;
    private $viagem;
    
    
    public function __construct($params) {
        $this->params = $params;
        
        $empresaId     = array_get($this->params, 'Empresa_Id');
        $tipoVeiculoId = array_get($this->params, 'TipoVeiculo_Id');
        $horarioId     = array_get($this->params, 'Horario_Id');
        
        $this->tipoVeiculo = ModelTipoVeiculo::daEmpresa($empresaId)->findOrFail($tipoVeiculoId);
        
        $this->horario = ModelHorario::daEmpresa($empresaId)->findOrFail($horarioId);
        
        $duracao = (int) array_get($this->params, 'Duracao');
        $this->params['Duracao'] = $duracao > 0 ? $duracao : $this->horario->Duracao;
        
        $descricao = (string) array_get($this->params, 'Descricao');
        $this->params['Descricao'] = $descricao != "" ? $descricao : $this->horario->Descricao;
    }
   
    
    public function erros() {
        return $this->erros;
    }
   
    
    public function salvar() {
        try {
            DB::transaction(function() {
                $this->salvarModels();
            });
             
         } catch (ExceptionValidator $e) {
	        $this->erros = $e->getValidator();
            throw $e;     
        
        } catch (Exception $e) {
	        $this->erros = null;
            throw $e;     
        }   
        
        return $this->viagem;
    }
    
        
    protected function salvarModels() {
        $this->validarViagem($this->params);   
        
        $id = array_get($this->params, 'id');
        
        $viagemParams = array('DataInicio', 'HoraInicio', 'Horario_Id', 'TipoVeiculo_Id', 'Veiculo_Id',
                              'Confirmada', 'Situacao', 'Duracao', 'Descricao');
        
        $params = array_only($this->params, $viagemParams);
        
        if (! $id) {
            $viagem = new ModelViagem($params);
            $viagem->save();
            
            foreach($this->horario->itens as $item) {                
                $dataHoraEmbarque = $this->calcularDataHoraEmbarque($viagem, $item);
                
                $params = array(
                    'Viagem_Id'        => $viagem->Id,
                    'HorarioItem_Id'   => $item->Id,
                    'Ativo'            => 1,
                    'DataHoraEmbarque' => $dataHoraEmbarque
                );
                
                $this->validarItemViagem($params);
                
                $itemViagem = new ModelItemViagem($params);
                $itemViagem->save();
            }
            
        } else {
            $viagem = ModelViagem::find($id);
            $viagem->update($params);
        }
        
        $this->viagem = $viagem;
    }
    
    
    protected function calcularDataHoraEmbarque($viagem, $itemHorario) {
        $dataHora = DateTime::createFromFormat(DataHora::BR, $viagem->DataInicio . ' ' . $itemHorario->HoraEmbarque);
                
        $diaEmbarque = ((int) $itemHorario->DiaEmbarque > 0) ? $itemHorario->DiaEmbarque : 0;
                
        $dataHora->add(new DateInterval('P' . $diaEmbarque . 'D'));
        
        return $dataHora->format(DataHora::BR);    
    }
    
    
    protected function validarViagem($params) {
        $this->validar($params, ModelViagem::$rules);   
    }
    
    
    protected function validarItemViagem($params) {
        $this->validar($params, ModelItemViagem::$rules);    
    }
    
    
    protected function validar($params, $regras) {
        $validation = Validator::make($params, $regras);
        
        if ($validation->fails()) {
            throw new ExceptionValidator($validation); 
        }  
    }

}

?>