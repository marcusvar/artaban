<?php
namespace Artaban\Service\Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Pessoa;
use Artaban\Model\PessoaTelefone as Telefone;
use Artaban\Model\Endereco;
use Artaban\Model\Localidade;
use Artaban\Model\Estado;
use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Service\Cadastro\Pessoa\Pessoa as CadastroPessoa;
use Artaban\Helper\View\FormPessoa;
use Artaban\Helper\View\ShowPessoa;
use Artaban\Validation\Documento;

use DB;
use Validator;
use Lang;
use Exception;
use Form;
use Response;


class PessoaTelefone {

    /**
      * Pessoa Repository
      *
      * @var Pessoa
      */
    protected $pessoa;
    

    public function __construct($pessoaId) {
        $empresaAtual = HelperEmpresa::atual();
        $this->pessoa = Pessoa::daEmpresa($empresaAtual->Id)->findOrFail($pessoaId);
    }

        
    public function salvar($telefones) {
        try {
            $telefones = array_map(function($t) {
                $descricao = (string) array_get($t, 'Descricao', '');
                $telefone  = (string) array_get($t, 'Telefone', '');
                
                if ($telefone !== "") {
                    $descricao = ($descricao !== "") ? $descricao : 'Telefone';
                    
                    return array(
                        'Descricao' => $descricao,
                        'Telefone'  => $telefone
                    );
                }                
            }, $telefones);    
            
            
            DB::transaction(function() use ($telefones) {
                Telefone::where('Pessoa_Id', '=', $this->pessoa->Id)->delete();
                    
                foreach($telefones as $telefone) {
                    Telefone::create(array(
                        'Pessoa_Id' => $this->pessoa->Id,
                        'Descricao' => $telefone['Descricao'],
                        'Telefone'  => $telefone['Telefone']
                    ));
                }                
            });
                
        } catch (Exception $e)  {
            throw $e;
        } 
        
    }
   
}