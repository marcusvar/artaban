<?php
namespace Artaban\Service\Cadastro;

use Artaban\Model\Tarifa as ModelTarifa;
use Artaban\Lib\Formatacao\DataHora\Data;
use Artaban\Lib\Formatacao\DataHora\DataHora;
use Artaban\Lib\Exception\ExceptionValidator;

use DB;
use Exception;
use Validator;
use DateTime;
use DateInterval;


class Tarifa {
    
    private $params;
    private $erros;
    private $tarifa;
    
    
    public function __construct($params) {
        $this->params = $params;
    }
   
    
    public function erros() {
        return $this->erros;
    }
   
    
    public function salvar() {
        try {
            $this->validar();
            
            $tarifaParams = array('LinhaItem_Id', 'TipoVeiculo_Id', 'DataVigencia', 'Valor', 'Valor2');
            $params = array_only($this->params, $tarifaParams);
            
            $tipoTarifa = array_get($this->params, 'TipoVeiculoTarifa');
            
            if ($tipoTarifa != ModelTarifa::MISTA) {
                $params['Valor2'] = null;  
            } 
            
            $id = array_get($this->params, 'id');
            
            if (! $id) {
                $tarifa = new ModelTarifa($params);
                $tarifa->save();
            } else {
                $tarifa = ModelTarifa::find($id);
                $tarifa->update($params);
            }
            
            return $tarifa;
        
         } catch (ExceptionValidator $e) {
	        $this->erros = $e->getValidator();
            throw $e;     
        
        } catch (Exception $e) {
	        $this->erros = null;
            throw $e;     
        }   
    }
        
    
    protected function validar() {
        $regras = array(
            'LinhaItem_Id'   => 'required',
            'TipoVeiculo_Id' => 'required',
            'DataVigencia'   => 'required',
            'Valor'          => 'required'
        );
        
        $mensagens = array(
            'Valor.required' => 'O campo Valor da tarifa é obrigatório.',
        );
        
        $tipoTarifa = array_get($this->params, 'TipoVeiculoTarifa');
                
        if ($tipoTarifa === ModelTarifa::MISTA) {
            $regras['Valor2'] = 'required';
            
            $mensagens['Valor.required']  = 'O campo Valor da tarifa para o piso inferior é obrigatório.';
            $mensagens['Valor2.required'] = 'O campo Valor da tarifa para o piso superior é obrigatório.';
        }
        
        $validation = Validator::make($this->params, $regras, $mensagens);
        
        if ($validation->fails()) {
            throw new ExceptionValidator($validation); 
        }        
    }
    
}

?>