<?php
namespace Artaban\Service\Gerenciamento;

use Artaban\Model\Horario as ModelHorario;
use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Lib\Formatacao\DataHora\Data;

use DB;
use DateTime;
use Exception;


class Viagem {
    
    
    public static function gerar($horarioId, $dataInicio, $dataFim) {
        $horario = ModelHorario::daEmpresa(HelperEmpresa::atual()->Id)->find($horarioId);
        
        $dataInicio = Data::formatar($dataInicio, Data::BR, Data::EUA, null);
        $dataFim    = Data::formatar($dataFim,    Data::BR, Data::EUA, null);
        
        if (! $dataInicio) {
            throw new Exception('É necessário selecionar uma data inicial');     
        }

        if (! $dataFim) {
            throw new Exception('É necessário selecionar uma data final');     
        }  
        
        if (! $horario) {
            throw new Exception('É necessário selecionar um horário');     
        }
        
        if ($dataInicio > $dataFim) {
            throw new Exception('A data inicial deve ser menor ou igual à data final');     
        }
        
        try {
            $select = DB::statement('CALL Proc_GerarViagem(?, ?, ?)', [$dataInicio, $dataFim, $horarioId]);
        } catch (\Exception $e) {
            DB::reconnect('mysql');
        }
        
        return $select;
    }
    	
}