<?php
namespace Artaban\Helper;

use \Config;
use \Cache;
use Artaban\Model\Modulo as ModelModulo;


class Modulo {
        
        
    public static function atual() {
        $nome = Config::get('app.name');
        $fields['nome'] = $nome;
        
        /*
        $cacheParam = CacheParam::generateForFields(new ModelModulo, $fields);
        
        $result = Cache::section($cacheParam['section'])
                  ->remember($cacheParam['key'],
                             Config::get('eucatur.cache.tempo'),
                             function() use($nome) {
                                
            return ModelModulo::where('nome', '=', $nome)->firstOrFail();                     
        });
        */
        
        return ModelModulo::where('nome', '=', $nome)->firstOrFail();
    }
       
}