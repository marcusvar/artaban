<?php
namespace Artaban\Helper;

use Artaban\Model\Empresa as ModelEmpresa;

use Config;
use Cache;
use Auth;


class Empresa {
        
        
    public static function atual() {
        $empresa = ModelEmpresa::find(Auth::user()->Pessoa_Id);
        return $empresa;
    }
       
}