<?php
namespace Artaban\Helper\View;

use Artaban\Model\Empresa as ModelEmpresa;
use Artaban\Helper\Empresa as HelperEmpresa;


class Mapa {

    
    private $html;
    private $andar;
    private $mapa;
    
    const LINHAS_ANDAR_1 = 20;
   
    
    public function __construct($mapa, $andar) {
        $this->html = '';
        $this->mapa = isset($mapa[$andar]) ? $mapa[$andar] : [];
        $this->andar = $andar;
    }
    
    
    public function iniciar() {
        $html = sprintf('<table id="mapa-andar-%d" data-andar="%d" class="%s">',
                        $this->andar,
                        $this->andar,
                        'mapa-veiculo table table-striped table-bordered');
        
        $this->html .= $html;             
    }
       
    
    public function finalizar() {
        $this->html .= '</table>';
    }
    
    
    public function montarThead() {
        $this->html .= '
        <thead>
          <tr>
	    <th>Janela</th>
	    <th>Corredor</th>
            <th>Corredor</th>
            <th>Janela</th>                       
	  </tr>
        </thead>';
    }
    
    
    private function poltrona($mapaAndar, $linha, $coluna) {
        if (isset($mapaAndar[$linha]) && isset($mapaAndar[$linha][$coluna])) {
            return $mapaAndar[$linha][$coluna];    
        }
        
        return "";
    }
    
    
    public function montarTBody() {
        $this->html .= '<tbody>';
        
        $mapaAndar = $this->mapa; // [$this->andar];
        for($i = 1; $i <= self::LINHAS_ANDAR_1; $i++) {
            $this->html .= '<tr>';
	    $this->html .= '  <td><a href="#">' . $this->poltrona($mapaAndar, 'Linha' . $i, 'Coluna1') . '</a></td>';
            $this->html .= '  <td><a href="#">' . $this->poltrona($mapaAndar, 'Linha' . $i, 'Coluna2') . '</a></td>';
            $this->html .= '  <td><a href="#">' . $this->poltrona($mapaAndar, 'Linha' . $i, 'Coluna3') . '</a></td>';
            $this->html .= '  <td><a href="#">' . $this->poltrona($mapaAndar, 'Linha' . $i, 'Coluna4') . '</a></td>';
            $this->html .= '</tr>';
        }
        
        $this->html .= '</tbody>';
    }
    
    
    public function __toString() {
        return $this->html;
    }
    
           
    public static function montarPara($mapa, $numeroAndar) {
        $mapaObj = new self($mapa, $numeroAndar);
        $mapaObj->iniciar();
        $mapaObj->montarThead();
        $mapaObj->montarTBody();
        $mapaObj->finalizar();
        
        return $mapaObj;
    }

}