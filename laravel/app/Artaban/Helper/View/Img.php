<?php
namespace Artaban\Helper\View;

use Artaban\Model\Empresa as ModelEmpresa;
use Artaban\Helper\Empresa as HelperEmpresa;


class Img {
        
        
    public static function logoEmpresaAtual() {
        $empresa = HelperEmpresa::atual(); 
        
        $img = '<img src="/img/logo-' . $empresa->Id . '.png" />';
        return $img;
    }

}