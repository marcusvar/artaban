<?php
namespace Artaban\Helper\View;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Model\Estado;
use Artaban\Model\Localidade;
use Artaban\Model\Pessoa;
use Artaban\Validation\Documento;

use Input;
use View;
use Form;


class ShowPessoa {
    
    private $html;
    private $pessoa;
    private $telefones;
    private $endereco;  
	private $localidade; 
	private $estado;     
       
    
    public function __construct($pessoaId) {
        try {
            $this->pessoa = Pessoa::daEmpresa(HelperEmpresa::atual()->Id)
	                              ->with('telefones', 'endereco', 'endereco.localidade', 'endereco.localidade.estado')
                                  ->find($pessoaId);
            
            $this->telefones  = object_get($this->pessoa, 'telefones');            
            $this->endereco   = object_get($this->pessoa, 'endereco');
	        $this->localidade = object_get($this->endereco, 'localidade');
	        $this->estado     = object_get($this->localidade, 'estado');
            
            switch ($this->pessoa->Tipo) {
                case Pessoa::PF_BRASILEIRO:
                    $this->showPFBrasileiro(); 
                    break;
            
                case Pessoa::PJ_BRASILEIRO:
                    $this->showPJBrasileiro(); 
                    break;
            
                case Pessoa::PF_ESTRANGEIRO:
                    $this->showPFEstrangeiro(); 
                    break;
            
                case Pessoa::PJ_ESTRANGEIRO:
                    $this->showPJEstrangeiro(); 
                    break;
            }
          
        } catch (Exception $e) {
            $this->html = "";
        }    
    }
    
    
    private function showPFBrasileiro() {
        $this->html = (string) View::make('Cadastro.pessoas._show_pf_br', $this->params());     
    }
    
    
    private function showPFEstrangeiro() {
        $this->html = (string) View::make('Cadastro.pessoas._show_pf_es', $this->params());     
    }
    
    
    private function showPJBrasileiro() {
        $responsavel = $this->pessoa->responsavel;
        $params = $this->params()  + compact('responsavel');
        
        $this->html = (string) View::make('Cadastro.pessoas._show_pj_br', $params);
    }
    
    
    private function showPJEstrangeiro() {
        $responsavel = $this->pessoa->responsavel;
        $params = $this->params() + compact('responsavel');
        
        $this->html = (string) View::make('Cadastro.pessoas._show_pj_es', $params);
    }
    
    
    private function params() {
        return array(
            'pessoa'     => $this->pessoa,
            'telefones'  => $this->telefones,
            'endereco'   => $this->endereco,
	        'localidade' => $this->localidade,
	        'estado'     => $this->estado
	    );
    }
    
    
    public function __toString() {
        return $this->html;
    }
    
}