<?php
namespace Artaban\Helper\View;

use Artaban\Model\Tarifa as ModelTarifa;


class Tarifa {
    
        
    public static function checkedTarifaUnica($tarifa, $tipoTarifa) {
        if ((! isset($tarifa)) || $tarifa->unica() || $tipoTarifa === ModelTarifa::UNICA) {
            return "checked";    
        }
    }
    
    
    public static function checkedTarifaMista($tarifa, $tipoTarifa) {
        if (isset($tarifa) && $tarifa->mista() || $tipoTarifa === ModelTarifa::MISTA) {
            return "checked";    
        }
    }
    
    
    public static function labelCampoValor($tarifa, $tipoTarifa) {
        if (isset($tarifa) && $tarifa->mista() || $tipoTarifa === ModelTarifa::MISTA) {
            return 'Valor da tarifa (completo/superior/esquerdo):';
        }
            
        return 'Valor da tarifa:';        
    }
    
    
    public static function displayTarifaMista($tarifa, $tipoTarifa) {
        if ($tipoTarifa === ModelTarifa::MISTA) {
            return;
        }
                
        if (! isset($tarifa) || $tarifa->unica()) {
            return 'style="display: none"';    
        }
    }

}