<?php
namespace Artaban\Helper\View;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Model\Estado;
use Artaban\Model\Localidade;
use Artaban\Model\Pessoa;
use Artaban\Validation\Documento;

use Artaban\View\Pessoa\Cadastro\FormPFBrasileiro;
use Artaban\View\Pessoa\Cadastro\FormPJBrasileiro;
use Artaban\View\Pessoa\Cadastro\FormPFEstrangeiro;
use Artaban\View\Pessoa\Cadastro\FormPJEstrangeiro;

use Input;
use View;
use Form;
use Exception;


class FormPessoa {
    
    private $tipo;
    private $html;
    private $documento;
    
    
    public function __construct($pessoaTipo, $pessoaId = null) {
        $tipos = Pessoa::$tipos;
     
        if ((! array_key_exists($pessoaTipo, $tipos)) && (! in_array($pessoaTipo, $tipos))) {
            throw new Exception('Tipo de pessoa inválido');    
        }
        
        $this->tipo = array_key_exists($pessoaTipo, $tipos) ? $tipos[$pessoaTipo] : $pessoaTipo; 
                
        $pessoa = Pessoa::daEmpresa(HelperEmpresa::atual()->Id)
	                    ->with('endereco', 'endereco.localidade', 'endereco.localidade.estado')
                        ->find($pessoaId);
                
        switch ($this->tipo) {
            case Pessoa::PF_BRASILEIRO:
                $this->html = FormPFBrasileiro::gerar($pessoa); 
                break;
            
            case Pessoa::PJ_BRASILEIRO:
                $this->html = FormPJBrasileiro::gerar($pessoa); 
                break;
            
            case Pessoa::PF_ESTRANGEIRO:
                $this->html = FormPFEstrangeiro::gerar($pessoa); 
                break;
            
            case Pessoa::PJ_ESTRANGEIRO:
                $this->html = FormPJEstrangeiro::gerar($pessoa); 
                break;
        }
                
        
        
        /*
        try {
            $this->documento = new Documento($documento);
	    
	        $pessoa = Pessoa::daEmpresa(HelperEmpresa::atual()->Id)
	                          ->comDocumento($this->documento->numero())
			                  ->with('endereco', 'endereco.localidade', 'endereco.localidade.estado')
                              ->first();
	                
            if ($pessoa) {
                $this->formEdit($pessoa);
            } else {
                $this->formCreate();
            }
         
            $this->formFields($pessoa);
         
            $this->formClose();
	    
        } catch (Exception $e) {
            $this->html = "";
        }
        */
    }
    
    
    private function formCreate() {
        $pessoa = new Pessoa;
	    $pessoa->CPFCNPJ = $this->documento->numero();
	
        $this->html = Form::model($pessoa, array('route' => 'cadastro.pessoas.store'));
    }
    
    
    private function formEdit($pessoa) {
        $this->html = Form::model($pessoa,
				                  array(
                                      'method' => 'PATCH',
				                      'route' => array('cadastro.pessoas.update', $pessoa->Id)
                                  )
        ); 
    }
    
    
    private function formFields($pessoa = null) {
        $estados = Estado::orderBy('Nome')->lists('Nome', 'Id');
        $estados = array('' => '') + $estados;
	
        $ufId = object_get($pessoa, 'endereco.localidade.UF_Id');
        $estadoSelecionadoId = $ufId ? $ufId : Input::old('endereco')['Estado_Id'];
	
	    if ($estadoSelecionadoId) {
	        $localidades = Localidade::where('UF_Id', '=', $estadoSelecionadoId)->lists('Nome', 'Id'); 
	    } else {
            $localidades = array('' => '');
 	    }
 
        $form = $this->documento->cpf() ? '_form_cpf' : '_form_cnpj';
        
        $params = compact('estadoSelecionadoId', 'estados', 'localidades');
        
        if ($this->documento->cnpj()) {
            $pessoas = Pessoa::daEmpresa(HelperEmpresa::atual()->Id)
                               ->pessoaFisica()
                               ->orderBy('Nome')
                               ->lists('Nome', 'Id');
                                 
            $params['pessoas'] = $pessoas;
            $params['pessoaId'] = null;
            
            if ($pessoa) {
                $params['pessoaId'] = $pessoa->cnpj ? $pessoa->cnpj->Pessoa_Id_Responsavel : null;
            }            
        }
        
        $this->html .= (string) View::make('Cadastro.pessoas.' . $form, $params);	 
    }
    
    
    private function formClose() {
        $this->html .= Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao'));
	    $this->html .= Form::close();
    }
    
    
    public function __toString() {
        return $this->html;
    }
    
}