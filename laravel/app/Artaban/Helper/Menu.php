<?php
namespace Artaban\Helper;

use \Config;


class Menu {
        
    
    public static function makeUrl($url) {
        $url = preg_replace('@(\*)$@', "", $url);
        return Config::get('app.url') . $url;
    }
    
}
