<?php
namespace Artaban\ServiceProvider\Cadastro;

use Illuminate\Support\ServiceProvider;


class Cliente extends ServiceProvider {
    

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $app = $this->app;

        $app->bind('CadastroCliente', function($app) {
            return new \Artaban\Service\Cadastro\Cliente;    
        });
    }

}