<?php
namespace Artaban\ServiceProvider\Cadastro;

use Illuminate\Support\ServiceProvider;


class Pessoa extends ServiceProvider {
    

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $app = $this->app;

        $app->bind('CadastroPessoa', function($app) {
            return new \Artaban\Service\Cadastro\Pessoa;    
        });
    }

}