<?php
namespace Artaban\ServiceProvider\Autorizacao;

use Illuminate\Support\ServiceProvider;


class Autorizacao extends ServiceProvider {
    

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $app = $this->app;

        $app->bind('Autorizacao', function($app) {
            return new \Artaban\Autorizacao\Autorizacao;    
        });
    }

}