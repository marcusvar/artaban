<?php
namespace API\Venda;

use Artaban\API\Venda\Pessoa;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class PessoasController extends BaseController {
    
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function index() {
        $pessoas = Pessoa::listarDaEmpresa($this->empresaId);
        $json['result'] = $pessoas;        
        
        return Response::json($json);
    }
    
    
    public function buscarPorDocumento() {
        $documento = Input::get('documento');
        
        $pessoa = Pessoa::buscarPorDocumento($this->empresaId, $documento);
        $json['result'] = $pessoa;        
        
        return Response::json($json);
    }
    
    
    public function buscarPorId() {
        $pessoaId = Input::get('pessoaId');
        
        $pessoa = Pessoa::buscarPorId($this->empresaId, $pessoaId);
        $json['result'] = $pessoa;        
        
        return Response::json($json);
    }
    
    
    public function fidelizacao() {
        $pessoaId = Input::get('pessoaId');
        
        $saldo = Pessoa::buscarSaldoDeFidelizacao($this->empresaId, $pessoaId);
        $json['result'] = $saldo;        
        
        return Response::json($json);
    }
    
}