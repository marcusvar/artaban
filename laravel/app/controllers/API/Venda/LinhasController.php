<?php
namespace API\Venda;

use Artaban\API\Venda\Linha;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class LinhasController extends BaseController {
        
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function index() {
        $linhas = Linha::listarDaEmpresa($this->empresaId);
        $json['result'] = $linhas;        
        
        return Response::json($json);
    }
    
    
    public function itens() {
        $linhaId = Input::get('linhaId');
        
        $itens = Linha::listarItens($this->empresaId, $linhaId);
        $json['result'] = $itens;        
        
        return Response::json($json);
    }

}