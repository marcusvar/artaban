<?php
namespace API\Venda;

use Artaban\API\Venda\Usuario;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class UsuariosController extends BaseController {
    
  
    public function autenticado() {
        $usuario = Usuario::autenticado();
        $json['result'] = $usuario;        
        
        return Response::json($json);
    }
        
}