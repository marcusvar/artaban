<?php
namespace API\Venda;

use Artaban\API\Venda\Cidade;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class CidadesController extends BaseController {
        
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function porEstado() {
        $estadoId = Input::get('estadoId');
        
        $cidades = Cidade::listarPorEstado($estadoId);
        $json['result'] = $cidades;        
        
        return Response::json($json);
    }

}