<?php
namespace API\Venda;

use Artaban\API\Venda\Cliente;

use Artaban\Helper\Empresa as HelperEmpresa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Response;


class ClientesController extends BaseController {
    
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function index() {
        $clientes = Cliente::listarDaEmpresa($this->empresaId);
        $json['result'] = $clientes;        
        
        return Response::json($json);
    }
    
    
    public function buscarPorCpfCnpj() {
        $cpfCnpj = Input::get('cpfCnpj');
        
        $cliente = Cliente::buscarPorCpfCnpj($this->empresaId, $cpfCnpj);
        $json['result'] = $cliente;        
        
        return Response::json($json);
    }
    
    
    public function buscarPorId() {
        $clienteId = Input::get('clienteId');
        
        $cliente = Cliente::buscarPorId($this->empresaId, $clienteId);
        $json['result'] = $cliente;        
        
        return Response::json($json);
    }
    
    
    public function fidelizacao() {
        $clienteId = Input::get('clienteId');
        
        $saldo = Cliente::buscarSaldoDeFidelizacao($this->empresaId, $clienteId);
        $json['result'] = $saldo;        
        
        return Response::json($json);
    }
    
}