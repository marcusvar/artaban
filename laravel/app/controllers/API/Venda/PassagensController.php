<?php
namespace API\Venda;

use Artaban\API\Venda\Passagem;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class PassagensController extends BaseController {
    
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function alterar() {
        $resultado = Passagem::alterar($this->empresaId);
        $json['result'] = $resultado;        
        
        return Response::json($json);
    }
    
    
    public function cancelar() {
        $resultado = Passagem::cancelar($this->empresaId);
        $json['result'] = $resultado;        
        
        return Response::json($json);
    }
    
    
    public function buscarPassageiro() {
        $passagemId = Input::get('passagemId');
        
        $passageiro = Passagem::buscarPassageiro($this->empresaId, $passagemId);
        $json['result'] = $passageiro;        
        
        return Response::json($json);
    }
        
}