<?php
namespace API\Venda;

use Artaban\API\Venda\Estado;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class EstadosController extends BaseController {
        
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function index() {
        $estados = Estado::listar();
        $json['result'] = $estados;        
        
        return Response::json($json);
    }

}