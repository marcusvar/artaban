<?php
namespace API\Venda;

use Artaban\API\Venda\Check;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class ChecksController extends BaseController {
        
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function index() {
        //$checks = Check::listar();
        $json['result'] = "Teste";        
        
        return Response::json($json);
    }

}