<?php
namespace API\Venda;

use Artaban\API\Venda\Viagem;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class ViagensController extends BaseController {
    
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function mapa() {
        $viagemId = Input::get('viagemId');
        
        $mapa = Viagem::mapa($viagemId);
        $json['result'] = $mapa;        
        
        return Response::json($json);
    }
        
}