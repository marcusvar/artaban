<?php
namespace API\Venda;

use Artaban\API\Venda\Horario;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Response;


class HorariosController extends BaseController {
    
    protected $empresaId;


    public function __construct() {
        $this->empresaId = HelperEmpresa::atual()->Id;
    }
  
  
    public function index() {
        $linhaId  = Input::get('linhaId');
        $linhaItemId = Input::get('linhaItemId');
        
        $horarios = Horario::listarDaEmpresa($this->empresaId, $linhaId, $linhaItemId);
        $json['result'] = $horarios;        
        
        return Response::json($json);
    }
    
}