<?php
namespace API\Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Mapa;
use Artaban\Model\TipoVeiculo;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;


class MapasController extends BaseController {

    /**
      * Mapa Repository
      *
      * @var Mapa
      */
    protected $mapa;


    public function __construct(Mapa $mapa) {
        $this->mapa = $mapa;
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = array_only(Input::all(), ['TipoVeiculo_Id', 'Andar', 'Coluna', 'Linha', 'Numero']);
        
	    $tipoVeiculoId = $input['TipoVeiculo_Id'];
	    $andar         = $input['Andar'];
	    $coluna        = $input['Coluna'];
	    $linha         = $input['Linha'];
	    $numero        = $input['Numero'];
	
	    if ($this->mapa->poltronaJaCadastrada($tipoVeiculoId, $numero)) {
	        $mensagem = 'Essa poltrona já está cadastrada!';
            $json = ['success' => false, 'msg' => $mensagem];	
   
	        return \Response::json($json);
	    }
	
	    $this->mapa->salvarPoltrona($tipoVeiculoId, $andar, $coluna, $linha, $numero);
	    return \Response::json(['success' => true]);
    }

}