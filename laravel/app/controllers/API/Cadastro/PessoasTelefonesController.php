<?php
namespace API\Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Pessoa;
use Artaban\Model\PessoaTelefone as Telefone;
use Artaban\Model\Endereco;
use Artaban\Model\Localidade;
use Artaban\Model\Estado;
use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Service\Cadastro\PessoaTelefone as CadastroPessoaTelefone;
use Artaban\Helper\View\FormPessoa;
use Artaban\Helper\View\ShowPessoa;
use Artaban\Validation\Documento;

use DB;
use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;
use Form;
use Response;


class PessoasTelefonesController extends BaseController {

    /**
      * Pessoa Repository
      *
      * @var Pessoa
      */
    protected $pessoa;
    

    public function __construct(Pessoa $pessoa) {
        $empresaAtual = HelperEmpresa::atual();
        $this->pessoa = $pessoa->daEmpresa($empresaAtual->Id);
    }


    public function index($pessoaId) {
        try {
            $telefones = $this->pessoa->findOrFail($pessoaId)->telefones;
            
            $lista = [];
            foreach($telefones as $telefone) {
                $lista[] = ['Descricao' => $telefone->Descricao, 'Telefone' => $telefone->Telefone];         
            }
                        
            return Response::json($lista);
     
        } catch (Exception $e) {
            $json = array('mensagem' => 'Erro ao buscar telefones');            
            return Response::json($json, 400);
        }

    }
    
        
    public function salvar() {
        try {
            $pessoaId   = Input::get('PessoaId');
            
            $telefones  = Input::get('Telefone',  []);
            $descricoes = Input::get('Descricao', []);
            
            $params = [];
            foreach($telefones as $chave => $telefone) {
                 $params[] = array(
                     'Descricao' => array_get($descricoes, $chave, ''),
                     'Telefone' => str_replace('_', '', $telefone) 
                 );             
            }
            
            $cadastro = new CadastroPessoaTelefone($pessoaId);
            
            $cadastro->salvar($params);
            
            return Response::json(['mensagem' => 'OK']);
                
        } catch (Exception $e)  {
            return Response::json(array('mensagem' => 'Erro ao salvar telefones'), 400);
        } 
        
    }
   
}