<?php
namespace API\Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Horario;
use Artaban\Model\Linha;
use Artaban\Model\ItemLinha;
use Artaban\Model\PontoReferencia;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;


class PontosReferenciaController extends BaseController {

    /**
      * PontoReferencia Repository
      *
      * @var PontoReferencia
      */
    protected $pontoReferencia;


    public function __construct(PontoReferencia $pontoReferencia) {
        $empresaAtual = HelperEmpresa::atual();
        $this->pontoReferencia = $pontoReferencia->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $horarioId = Input::get('Horario_Id');
	
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
        $horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horarioId);

        $pontosReferencia = [];
	foreach($horario->itens as $itemHorario) {
	    $pontoReferencia = $itemHorario->itemLinha->pontoReferencia; 
	    
	    $pontosReferencia[] = array(
	        'Id' => $pontoReferencia->Id,
		'Descricao' => $pontoReferencia->descricaoCompleta()
	    );
        }
        		
	return json_encode($pontosReferencia);
   }

    
}