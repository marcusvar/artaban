<?php
namespace API\Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Linha;
use Artaban\Model\ItemLinha;
use Artaban\Model\Estado;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use Response;


class EstadosController extends BaseController {

    
    public function porPais() {
        $paisId = Input::get('Pais_Id');
	     
        $estados = Estado::where('Pais_Id', '=', $paisId)->get(['Nome', 'Id']); 
	    
        return Response::json($estados);  
	}
    
}