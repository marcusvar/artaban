<?php
namespace API\Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Pessoa;
use Artaban\Model\Endereco;
use Artaban\Model\Localidade;
use Artaban\Model\Estado;
use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Service\Cadastro\Pessoa\Pessoa as CadastroPessoa;
use Artaban\Helper\View\FormPessoa;
use Artaban\Helper\View\ShowPessoa;
use Artaban\Validation\Documento;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;
use Form;
use Response;


class PessoasController extends BaseController {

    /**
      * Pessoa Repository
      *
      * @var Pessoa
      */
    protected $pessoa;
    

    public function __construct(Pessoa $pessoa) {
        $empresaAtual = HelperEmpresa::atual();
        $this->pessoa = $pessoa->daEmpresa($empresaAtual->Id);
    }


    public function validarDocumento() {
        try {
            $documento = Input::get('Documento');
            $tipo = Input::get('Tipo');
            
            $documento = new Documento($documento, $tipo);
	    
            $json = array('mensagem' => 'CREATE');
            
            if (in_array($tipo, [Pessoa::PF_BRASILEIRO, Pessoa::PJ_BRASILEIRO])) {
                $pessoa = $this->pessoa->doTipo($tipo)->comDocumento($documento->formatado())->first();
	 
                if ($pessoa) {
                    $json = array(
	                    'mensagem' => 'EDIT',
	                    'url' => route('cadastro.pessoas.edit', [$pessoa->Id])
	                );        
                } 
            }
            
            return Response::json($json);
     
        } catch (Exception $e) {
            $json = array(
	            'mensagem' => $e->getMessage()
	        );
            
            return Response::json($json, 400);
        }

    }
   
}