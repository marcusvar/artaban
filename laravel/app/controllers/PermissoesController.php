<?php
use Eucatur\Lib\Search\Search as EucaturSearch;
use Artaban\Model\Usuario;
use Artaban\Model\Grupo;


class PermissoesController extends BaseController {

	    
	public function index($usuario_id) {
	    $usuario = Usuario::find($usuario_id);
        return View::make('permissoes.index', compact('usuario'));
	}
    
    
    public function grupo($grupo_id) {
	    $grupo = Grupo::find($grupo_id);
        return View::make('permissoes.grupo', compact('grupo'));
	}

}