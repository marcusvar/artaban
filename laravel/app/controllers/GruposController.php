<?php
use Eucatur\Lib\Search\Search as EucaturSearch;
use Artaban\Model\Grupo;
use Artaban\Model\Modulo;


class GruposController extends BaseController {

	/**
	 * Grupo Repository
	 *
	 * @var Grupo
	 */
	protected $grupo;

	public function __construct(Grupo $grupo) {
		$this->grupo = $grupo;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {
		$fields = array(
			 'nome' => array('like', Input::get('nome')),  
			 'modulo_id' => array('=', Input::get('modulo_id')),  

        ); 
        
        $grupos = EucaturSearch::execute($this->grupo, $fields);
        $modulos = Modulo::lists('descricao', 'id');
        
		return View::make('grupos.index', compact('grupos', 'modulos'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
        $modulos = Modulo::lists('descricao', 'id');
		return View::make('grupos.create', compact('modulos'));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store() {
		$input = Input::all();
		$validation = Validator::make($input, Grupo::$rules);

		if ($validation->passes()) {
			$grupo = $this->grupo->create($input);
            
			return Redirect::route('grupos.show', $grupo->id);
		}

		return Redirect::route('grupos.create')
			->withInput()
			->withErrors($validation)
			->with('message', Lang::get('validation.form.invalid'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) {
		$grupo = $this->grupo->findOrFail($id);

		return View::make('grupos.show', compact('grupo'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id) {
		$grupo = $this->grupo->find($id);

		if (is_null($grupo)) {
			return Redirect::route('grupos.index');
		}

        $modulos = Modulo::lists('descricao', 'id');
		return View::make('grupos.edit', compact('grupo', 'modulos'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id) {
		$input = array_except(Input::all(), '_method');
		$validation = Validator::make($input, Grupo::$rules);

		if ($validation->passes()) {
			$grupo = $this->grupo->find($id);
			$grupo->update($input);

			return Redirect::route('grupos.show', $id);
		}

		return Redirect::route('grupos.edit', $id)
			->withInput()
			->withErrors($validation)
			->with('message', Lang::get('validation.form.invalid'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		$this->grupo->find($id)->delete();

		return Redirect::route('grupos.index');
	}

}