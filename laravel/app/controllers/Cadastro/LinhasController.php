<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Linha;
use Artaban\Model\PontoReferencia;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;


class LinhasController extends BaseController {

    /**
      * Linha Repository
      *
      * @var Linha
      */
    protected $linha;


    public function __construct(Linha $linha) {
        $empresaAtual = HelperEmpresa::atual();
        $this->linha = $linha->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
			 'Situacao' => array('like', Input::get('Situacao')),  
			 'PontoReferencia_Id_Destino' => array('=', Input::get('PontoReferencia_Id_Destino')),
             'PontoReferencia_Id_Via' => array('=', Input::get('PontoReferencia_Id_Via')) 
        ); 

        $linhas = Search::execute($this->linha, $fields);
	
	    $situacoes = array('' => '') + Linha::$situacoes;
	
	    $empresaAtualId = HelperEmpresa::atual()->Id;
	
	    $pontosReferencia = PontoReferencia::listarPorDescricaoCompleta($empresaAtualId);
	    $pontosReferencia = array('' => '') + $pontosReferencia;
	
        return View::make('Cadastro.linhas.index', compact('linhas', 'situacoes', 'pontosReferencia'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
       	$situacoes = Linha::$situacoes;
	
	    $empresaAtualId = HelperEmpresa::atual()->Id;
	
	    $pontosReferencia = PontoReferencia::listarPorDescricaoCompleta($empresaAtualId);
	        	
        return View::make('Cadastro.linhas.create', compact('situacoes', 'pontosReferencia'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
	
	    $input['Pessoa_Id'] = HelperEmpresa::atual()->Id;
		
        $validation = Validator::make($input, Linha::$rules);

        if ($validation->passes()) {
            $linha = Linha::create($input);
   
            return Redirect::route('cadastro.linhas.show', $linha->ID);
        }

        return Redirect::route('cadastro.linhas.create')
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $linha = $this->linha->findOrFail($id);

        return View::make('Cadastro.linhas.show', compact('linha'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $linha = $this->linha->find($id);

        if (is_null($linha)) {
	        return Redirect::route('cadastro.linhas.index');
        }

	    $situacoes = Linha::$situacoes;
	
	    $empresaAtualId = HelperEmpresa::atual()->Id;
	
	    $pontosReferencia = PontoReferencia::listarPorDescricaoCompleta($empresaAtualId);
	
        return View::make('Cadastro.linhas.edit', compact('linha', 'situacoes', 'pontosReferencia'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
     
        $input['Pessoa_Id'] = HelperEmpresa::atual()->Id;

        $validation = Validator::make($input, Linha::$rules);

        if ($validation->passes()) {
            $linha = $this->linha->find($id);
            $linha->update($input);

            return Redirect::route('cadastro.linhas.show', $id);
        }

        return Redirect::route('cadastro.linhas.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->linha->find($id)->delete();
            return Redirect::route('cadastro.linhas.index');
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}