<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Fidelizacao;
use Artaban\Model\Pessoa;

use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;


class VinculadosController extends BaseController {

    
    protected $pessoa;
    protected $empresaAtualId;
    

    public function __construct(Pessoa $pessoa) {
        
        $empresaAtual = HelperEmpresa::atual();
        $this->pessoa = $pessoa->daEmpresa($empresaAtual->Id);
        $this->empresaAtualId = HelperEmpresa::atual($empresaAtual->Id)->Id;       
    }

    
    
    private function validarPessoa($pessoa) {
        if (! $pessoa->podeVincularPessoas()) {
            return Redirect::route('cadastro.pessoas.vinculados.index', [$pessoa->Id]);    
        }
    }
   

    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($pessoaId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
        $vinculados = $pessoa->vinculados()->orderBy('Nome')->get();  
        
        return View::make('Cadastro.vinculados.index', compact('pessoa', 'vinculados'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create($pessoaId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
        
        if (! $pessoa->podeVincularPessoas()) {
            return Redirect::route('cadastro.pessoas.vinculados.index', [$pessoa->Id]);    
        }
       
        $fields = array(
	        'Nome' => array('like', Input::get('Nome')),
            
        );
        
        $pessoas = Search::execute($this->pessoa->NaoVinculadoA($pessoa->Id), $fields, ['order' => 'nome']);
      
        return View::make('Cadastro.vinculados.create', compact('pessoa', 'pessoas'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store($pessoaId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
        
        if (! $pessoa->podeVincularPessoas()) {
            return Redirect::route('cadastro.pessoas.vinculados.index', [$pessoa->Id]);    
        }
        
        $pessoaIdVinculo = Input::get('Pessoa_Id_Vinculo');
        
        $pessoaVinculada = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaIdVinculo);
       
        $pessoa->vinculados()->attach($pessoaVinculada);
        
        return Redirect::route('cadastro.pessoas.vinculados.index', [$pessoa->Id]);
        
        // return Redirect::route('cadastro.pessoas.vinculados.create', $pessoa->Id)
        //               ->withInput()
        //                ->withErrors($validation)
        //               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($pessoaId, $pessoaIdVinculo) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
        
        if (! $pessoa->podeVincularPessoas()) {
            return Redirect::route('cadastro.pessoas.vinculados.index', [$pessoa->Id]);    
        }
        
        $pessoaVinculada = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaIdVinculo);
       
        $pessoa->vinculados()->detach($pessoaVinculada);
        
        return Redirect::route('cadastro.pessoas.vinculados.index', $pessoa->Id);
    }

}