<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Linha;
use Artaban\Model\AdicionalTarifa;
use Artaban\Model\TipoAdicionalTarifa;

use Artaban\Helper\Empresa as HelperEmpresa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class AdicionaisTarifaController extends BaseController {

    /**
      * AdicionalTarifa Repository
      *
      * @var AdicionalTarifa
      */
    protected $adicionalTarifa;

    protected $empresaAtualId;
    
 
    public function __construct(AdicionalTarifa $adicionalTarifa) {
        $empresaAtual = HelperEmpresa::atual();
        $this->adicionalTarifa = $adicionalTarifa->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($linhaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	
        $fields = array(
            'Linha_Id' => array('=', $linha->Id),  
            'TipoAdicionalTarifa_Id' => array('=', Input::get('TipoAdicionalTarifa_Id')),		 
            'DataVigencia' => array(
                'between',
                array(Input::get('DataVigencia_inicial'), Input::get('DataVigencia_final')),
                'date'
            ),  
        ); 

        $adicionaisTarifa = Search::execute($this->adicionalTarifa, $fields);
        
	$tiposAdicionaisTarifa = TipoAdicionalTarifa::orderBy('Descricao')->lists('Descricao', 'Id');	
        $tiposAdicionaisTarifa = array('' => '') + $tiposAdicionaisTarifa; 
	
	return View::make('Cadastro.adicionaisTarifa.index',
			              compact('adicionaisTarifa', 'linha', 'tiposAdicionaisTarifa'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create($linhaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
        $tiposAdicionaisTarifa = TipoAdicionalTarifa::orderBy('Descricao')->lists('Descricao', 'Id');	
	
        return View::make('Cadastro.adicionaisTarifa.create', compact('linha', 'tiposAdicionaisTarifa'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store($linhaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	
        $input = Input::all();
        $input['Linha_Id'] = $linha->ID;
	
	    $validation = Validator::make($input, AdicionalTarifa::$rules);

        if ($validation->passes()) {
            $adicionalTarifa = adicionalTarifa::create($input);
   
            return Redirect::route('cadastro.linhas.adicionaisTarifa.show', [$linha->ID, $adicionalTarifa->Id]);
        }

        return Redirect::route('cadastro.linhas.adicionaisTarifa.create', [$linha->ID])
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($linhaId, $adicionalTarifaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	
        $adicionalTarifa = $this->adicionalTarifa
	                        ->where('Linha_Id', '=', $linha->ID)
				->findOrFail($adicionalTarifaId);

        return View::make('Cadastro.adicionaisTarifa.show', compact('adicionalTarifa', 'linha'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($linhaId, $adicionalTarifaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	
        $adicionalTarifa = $this->adicionalTarifa
	                        ->where('Linha_Id', '=', $linha->ID)
				->find($adicionalTarifaId);

        if (is_null($adicionalTarifa)) {
	    return Redirect::route('cadastro.linhas.adicionaisTarifa.index', [$linha->ID]);
        }

	$tiposAdicionaisTarifa = TipoAdicionalTarifa::orderBy('Descricao')->lists('Descricao', 'Id');	
	
        return View::make('Cadastro.adicionaisTarifa.edit',
			  compact('adicionalTarifa', 'tiposAdicionaisTarifa', 'linha'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($linhaId, $adicionalTarifaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	
        $input = array_except(Input::all(), '_method');
        $input['Linha_Id'] = $linha->ID;
	
	$validation = Validator::make($input, AdicionalTarifa::$rules);

        if ($validation->passes()) {
             $adicionalTarifa = $this->adicionalTarifa
	                        ->where('Linha_Id', '=', $linha->ID)
				->find($adicionalTarifaId);

	    $adicionalTarifa->update($input);

            return Redirect::route('cadastro.linhas.adicionaisTarifa.show', [$linha->ID, $adicionalTarifa->Id]);
        }

        return Redirect::route('cadastro.linhas.adicionaisTarifa.edit', [$linha->ID, $adicionalTarifa->Id])
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($linhaId, $adicionalTarifaId) {
        try {
            $empresaAtualId = HelperEmpresa::atual()->Id;
	
            $linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	
            $adicionalTarifa = $this->adicionalTarifa
	                                ->where('Linha_Id', '=', $linha->ID)
				                    ->find($adicionalTarifaId)
                                    ->delete();

            return Redirect::route('cadastro.linhas.adicionaisTarifa.index', [$linha->ID]);    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}