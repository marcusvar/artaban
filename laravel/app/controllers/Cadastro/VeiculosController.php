<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Veiculo;
use Artaban\Model\TipoVeiculo;

use Artaban\Helper\Empresa as HelperEmpresa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class VeiculosController extends BaseController {

    /**
      * Veiculo Repository
      *
      * @var Veiculo
      */
    protected $veiculo;


    public function __construct(Veiculo $veiculo) {
       $empresaAtual = HelperEmpresa::atual();
       $this->veiculo = $veiculo->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $empresaAtualId = HelperEmpresa::atual()->Id;
      
        $tipoVeiculoId = Input::get('TipoVeiculo_Id');
	$tipoVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->find($tipoVeiculoId);
	
	if ($tipoVeiculo === null) {
	    $tipoVeiculoId = ""; 
	}
		
	$fields = array(
			 'Numero' => array('like', Input::get('Numero')),  
			 'Placa' => array('like', Input::get('Placa')),  
			 'TipoVeiculo_Id' => array('=', $tipoVeiculoId),  

        ); 

        $veiculos = Search::execute($this->veiculo, $fields);
	
	$tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->lists('Descricao', 'Id');
	$tiposVeiculo = array('' => '') + $tiposVeiculo;
	
        return View::make('Cadastro.veiculos.index', compact('tiposVeiculo', 'veiculos'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
       $empresaAtualId = HelperEmpresa::atual()->Id;
       
       $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->lists('Descricao', 'Id');
	      
       return View::make('Cadastro.veiculos.create', compact('tiposVeiculo'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        
	$empresaAtualId = HelperEmpresa::atual()->Id;
      
        $tipoVeiculoId = $input['TipoVeiculo_Id'];
	$tipoVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->find($tipoVeiculoId);
		
	if ($tipoVeiculo === null) {
	    $input['TipoVeiculo_Id'] = ""; 
	}
	
	$input['Pessoa_Id'] = $empresaAtualId;
	
	$validation = Validator::make($input, Veiculo::$rules);

        if ($validation->passes()) {
            $veiculo = Veiculo::create($input);
   
            return Redirect::route('cadastro.veiculos.show', $veiculo->Id);
        }

        return Redirect::route('cadastro.veiculos.create')
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $veiculo = $this->veiculo->findOrFail($id);

        return View::make('Cadastro.veiculos.show', compact('veiculo'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $veiculo = $this->veiculo->find($id);

	$empresaAtualId = HelperEmpresa::atual()->Id;
       
        $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->lists('Descricao', 'Id');
		
        if (is_null($veiculo)) {
	    return Redirect::route('cadastro.veiculos.index');
        }

        return View::make('Cadastro.veiculos.edit', compact('tiposVeiculo', 'veiculo'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        
	$empresaAtualId = HelperEmpresa::atual()->Id;
      
        $tipoVeiculoId = $input['TipoVeiculo_Id'];
	$tipoVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->find($tipoVeiculoId);
		
	if ($tipoVeiculo === null) {
	    $input['TipoVeiculo_Id'] = ""; 
	}
	
	$input['Pessoa_Id'] = $empresaAtualId;
	
	
	$validation = Validator::make($input, Veiculo::$rules);

        if ($validation->passes()) {
            $veiculo = $this->veiculo->find($id);
            $veiculo->update($input);

            return Redirect::route('cadastro.veiculos.show', $id);
        }

        return Redirect::route('cadastro.veiculos.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->veiculo->find($id)->delete();
            return Redirect::route('cadastro.veiculos.index');    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}