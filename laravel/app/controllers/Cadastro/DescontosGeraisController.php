<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\DescontoGeral;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;


class DescontosGeraisController extends BaseController {

    protected $descontoGeral;


    public function __construct(DescontoGeral $descontoGeral) {
        $empresaAtual = HelperEmpresa::atual();
        $this->descontoGeral = $descontoGeral;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
		    'Descricao'  => array('like', Input::get('Descricao')),  
                    'Confirmado' => array('=', Input::get('Confirmado')),  
                    'AgenciaWeb' => array('=', Input::get('AgenciaWeb')),
                    'Pessoa_Id'  => array('=', HelperEmpresa::atual()->Id)
        ); 
        
        $descontosGerais = Search::execute($this->descontoGeral, $fields, ['order' => 'Descricao']);
	    
        $confirmadoOpcoes = ['' => ''] + DescontoGeral::$confirmadoOpcoes;
        $agenciaWebOpcoes = ['' => ''] + DescontoGeral::$agenciaWebOpcoes;
        
        return View::make('Cadastro.descontosGerais.index',
                          compact('descontosGerais', 'confirmadoOpcoes', 'agenciaWebOpcoes'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
        $confirmadoOpcoes = DescontoGeral::$confirmadoOpcoes;
        $agenciaWebOpcoes = DescontoGeral::$agenciaWebOpcoes;
        
        return View::make('Cadastro.descontosGerais.create', compact('confirmadoOpcoes', 'agenciaWebOpcoes'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        $input['Pessoa_Id'] = HelperEmpresa::atual()->Id;
        
	$validation = Validator::make($input, DescontoGeral::$rules);

        if ($validation->passes()) {
            $descontoGeral = DescontoGeral::create($input);
   
            return Redirect::route('cadastro.descontosGerais.show', $descontoGeral->Id);
        }

        return Redirect::route('cadastro.descontosGerais.create')
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $descontoGeral = DescontoGeral::findOrFail($id);

        return View::make('Cadastro.descontosGerais.show', compact('descontoGeral'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $descontoGeral = DescontoGeral::find($id);

        $confirmadoOpcoes = DescontoGeral::$confirmadoOpcoes;
        $agenciaWebOpcoes = DescontoGeral::$agenciaWebOpcoes;
        
        if (is_null($descontoGeral)) {
	        return Redirect::route('cadastro.descontosGerais.index');
        }

        return View::make('Cadastro.descontosGerais.edit', compact('descontoGeral', 'confirmadoOpcoes', 'agenciaWebOpcoes'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $input['Pessoa_Id'] = HelperEmpresa::atual()->Id;
        
	$validation = Validator::make($input, DescontoGeral::$rules);

        if ($validation->passes()) {
            $descontoGeral = DescontoGeral::find($id);
            $descontoGeral->update($input);

            return Redirect::route('cadastro.descontosGerais.show', $id);
        }

        return Redirect::route('cadastro.descontosGerais.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            DescontoGeral::find($id)->delete();
            return Redirect::route('cadastro.descontosGerais.index');    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}