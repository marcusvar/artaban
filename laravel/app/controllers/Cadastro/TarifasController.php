<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Tarifa;
use Artaban\Model\Linha;
use Artaban\Model\ItemLinha;
use Artaban\Model\TipoVeiculo;
use Artaban\Service\Cadastro\Tarifa as CadastroDeTarifa;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;


class TarifasController extends BaseController {

    /**
      * Tarifa Repository
      *
      * @var Tarifa
      */
    protected $tarifa;
    
    protected $empresaAtualId;


    public function __construct(Tarifa $tarifa) {
        $this->empresaAtualId = HelperEmpresa::atual()->Id;
        $this->tarifa = $tarifa;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($linhaId, $itemLinhaId) {
        $linha = Linha::daEmpresa($this->empresaAtualId)->findOrFail($linhaId);
	
	$itemLinha = ItemLinha::daEmpresa($this->empresaAtualId)->findOrFail($itemLinhaId);
		
	$fields = array(
	    'LinhaItem_Id' => array('=', $itemLinha->ID),  
	    'TipoVeiculo_Id' => array('=', Input::get('TipoVeiculo_Id')),  
	    'DataVigencia' => array(
                 'between',
                 array(Input::get('DataVigencia_inicial'), Input::get('DataVigencia_final')),
                 'date'
            )	        
        ); 

        $tarifas = Search::execute($this->tarifa, $fields);
        
	$tiposVeiculo = TipoVeiculo::orderBy('Descricao')->lists('Descricao', 'Id');
	$tiposVeiculo = array('' => '') + $tiposVeiculo;
		
	return View::make('Cadastro.tarifas.index', compact('tarifas', 'tiposVeiculo', 'linha', 'itemLinha'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create($linhaId, $itemLinhaId) {
        $linha = Linha::daEmpresa($this->empresaAtualId)->findOrFail($linhaId);
	
	$itemLinha = ItemLinha::daEmpresa($this->empresaAtualId)->findOrFail($itemLinhaId);
	
	$tiposVeiculo = TipoVeiculo::daEmpresa($this->empresaAtualId)->orderBy('Descricao')->lists('Descricao', 'Id');	
	
        return View::make('Cadastro.tarifas.create', compact('linha', 'itemLinha', 'tiposVeiculo'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store($linhaId, $itemLinhaId) {
        try {
            $linha = Linha::daEmpresa($this->empresaAtualId)->findOrFail($linhaId);
	        $itemLinha = ItemLinha::daEmpresa($this->empresaAtualId)->findOrFail($itemLinhaId);
		
            $input = Input::all();
            $input['LinhaItem_Id'] = $itemLinha->ID;
	    
            $cadastro = new CadastroDeTarifa($input);
        
            $tarifa = $cadastro->salvar();
        
            return Redirect::route('cadastro.linhas.itensLinha.tarifas.show', [$linha->ID, $itemLinha->ID, $tarifa->Id]);
                        
        } catch (Exception $e) {
            return Redirect::route('cadastro.linhas.itensLinha.tarifas.create', [$linha->ID, $itemLinha->ID])
                           ->withInput()
                           ->withErrors($cadastro->erros())
                           ->with('message', Lang::get('validation.form.invalid'));
        }
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($linhaId, $itemLinhaId, $tarifaId) {
        $linha = Linha::daEmpresa($this->empresaAtualId)->findOrFail($linhaId);
	
	$itemLinha = ItemLinha::daEmpresa($this->empresaAtualId)->findOrFail($itemLinhaId);
		
        $tarifa = $this->tarifa->where('LinhaItem_Id', '=', $itemLinha->ID)->findOrFail($tarifaId);

        return View::make('Cadastro.tarifas.show', compact('linha', 'itemLinha', 'tarifa'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($linhaId, $itemLinhaId, $tarifaId) {
        $linha = Linha::daEmpresa($this->empresaAtualId)->findOrFail($linhaId);
	
	$itemLinha = ItemLinha::daEmpresa($this->empresaAtualId)->findOrFail($itemLinhaId);
		
        $tarifa = $this->tarifa->where('LinhaItem_Id', '=', $itemLinha->ID)->find($tarifaId);

        if (is_null($tarifa)) {
	    return Redirect::route('cadastro.linhas.itensLinha.tarifas.index', [$linha->ID, $itemLinha->ID]);
        }

	$tiposVeiculo = TipoVeiculo::daEmpresa($this->empresaAtualId)->orderBy('Descricao')->lists('Descricao', 'Id');	
        return View::make('Cadastro.tarifas.edit', compact('linha', 'itemLinha', 'tiposVeiculo', 'tarifa'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($linhaId, $itemLinhaId, $tarifaId) {
        try {
            $linha = Linha::daEmpresa($this->empresaAtualId)->findOrFail($linhaId);
	    $itemLinha = ItemLinha::daEmpresa($this->empresaAtualId)->findOrFail($itemLinhaId);
		
            $input = Input::all();
            $input['LinhaItem_Id'] = $itemLinha->ID;
	        
            $input['id'] = $tarifaId;
        
            $cadastro = new CadastroDeTarifa($input);
            $tarifa = $cadastro->salvar();
        
            return Redirect::route('cadastro.linhas.itensLinha.tarifas.show', [$linha->ID, $itemLinha->ID, $tarifa->Id]);
                        
        } catch (Exception $e) {
            return Redirect::route('cadastro.linhas.itensLinha.tarifas.edit', [$linha->ID, $itemLinha->ID, $tarifaId])
                           ->withInput()
                           ->withErrors($cadastro->erros())
                           ->with('message', Lang::get('validation.form.invalid'));
        }
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($linhaId, $itemLinhaId, $tarifaId) {
        try {
            $linha = Linha::daEmpresa($this->empresaAtualId)->findOrFail($linhaId);
	
	        $itemLinha = ItemLinha::daEmpresa($this->empresaAtualId)->findOrFail($itemLinhaId);
		
            $tarifa = $this->tarifa
	                       ->where('LinhaItem_Id', '=', $itemLinha->ID)
		                   ->find($tarifaId)
		                   ->delete();

            return Redirect::route('cadastro.linhas.itensLinha.tarifas.index', [$linha->ID, $itemLinha->ID]);
        
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}