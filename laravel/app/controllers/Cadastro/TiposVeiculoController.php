<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\TipoVeiculo;
use Artaban\Helper\Empresa as HelperEmpresa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class TiposVeiculoController extends BaseController {

    /**
      * TipoVeiculo Repository
      *
      * @var TipoVeiculo
      */
    protected $tipoVeiculo;


    public function __construct(TipoVeiculo $tipoVeiculo) {
       $empresaAtual = HelperEmpresa::atual();
       $this->tipoVeiculo = $tipoVeiculo->daEmpresa($empresaAtual->Id);
       
/*
       $empresaAtual = HelperEmpresa::atual();
       $this->pessoa = $pessoa->daEmpresa($empresaAtual->Id);
       $this->empresaAtualId = HelperEmpresa::atual($empresaAtual->Id)->Id;       
*/
        
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
            'Descricao' => array('like', Input::get('Descricao')),  
	    'Pessoa_Id' => array('=', HelperEmpresa::atual()->Id),  

        );
	
        $tiposVeiculo = Search::execute($this->tipoVeiculo, $fields);
        return View::make('Cadastro.tiposVeiculo.index', compact('tiposVeiculo'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
       return View::make('Cadastro.tiposVeiculo.create');
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
	$input['Pessoa_Id'] = HelperEmpresa::atual()->Id;
		
        $validation = Validator::make($input, TipoVeiculo::$rules);

        if ($validation->passes()) {
            $tipoVeiculo = TipoVeiculo::create($input);
   
            return Redirect::route('cadastro.tiposVeiculo.show', $tipoVeiculo->Id);
        }

        return Redirect::route('cadastro.tiposVeiculo.create')
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $tipoVeiculo = $this->tipoVeiculo->findOrFail($id);

        return View::make('Cadastro.tiposVeiculo.show', compact('tipoVeiculo'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $tipoVeiculo = $this->tipoVeiculo->find($id);
        	
        if (is_null($tipoVeiculo)) {
	    return Redirect::route('cadastro.tiposVeiculo.index');
        }

        return View::make('Cadastro.tiposVeiculo.edit', compact('tipoVeiculo'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $input['Pessoa_Id'] = HelperEmpresa::atual()->Id;
	
	
	$validation = Validator::make($input, TipoVeiculo::$rules);

        if ($validation->passes()) {
            $tipoVeiculo = $this->tipoVeiculo->find($id);
            $tipoVeiculo->update($input);

            return Redirect::route('cadastro.tiposVeiculo.show', $id);
        }

        return Redirect::route('cadastro.tiposVeiculo.edit', $id)
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->tipoVeiculo->find($id)->delete();
            return Redirect::route('cadastro.tiposVeiculo.index');    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}