<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Linha;
use Artaban\Model\ItemLinha;
use Artaban\Model\PontoReferencia;

use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;


class ItensLinhaController extends BaseController {

    /**
      * ItemLinha Repository
      *
      * @var ItemLinha
      */
    protected $itemLinha;


    public function __construct(ItemLinha $itemLinha) {
        $empresaAtual = HelperEmpresa::atual();
        $this->itemLinha = $itemLinha->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($linhaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	
	$fields = array(
            'Linha_Id' => array('=', $linha->ID),  
            'PontoReferencia_Id' => array('=', Input::get('PontoReferencia_Id')),
            'Sequencia' => array('=', Input::get('Sequencia')),
            'Web' => array('=', Input::get('Web'))
        );
	        
        $itensLinha = Search::execute($this->itemLinha, $fields, ['order' => 'Sequencia']);
		
        $pontosReferencia = ['' => ''] + PontoReferencia::listarPorDescricaoCompleta($empresaAtualId);
	$webOpcoes = array('' => '') + ItemLinha::$webOpcoes;
        
	return View::make('Cadastro.itensLinha.index', compact('linha', 'itensLinha', 'pontosReferencia', 'webOpcoes'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create($linhaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
	$pontosReferencia = PontoReferencia::listarPorDescricaoCompleta($empresaAtualId);
	$webOpcoes = array('' => '') + ItemLinha::$webOpcoes;
        
        return View::make('Cadastro.itensLinha.create', compact('linha', 'pontosReferencia', 'webOpcoes'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store($linhaId) {
        $input = Input::all();
        
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)
	                ->findOrFail($linhaId);

	$pontoReferencia = PontoReferencia::daEmpresa($empresaAtualId)
	                                     ->findOrFail($input['PontoReferencia_Id']);
        
	$input['Linha_Id'] = $linha->ID;
	
	$input['PontoReferencia_Id'] = $pontoReferencia->Id;
	
	$validation = Validator::make($input, ItemLinha::$rules);

        if ($validation->passes()) {
            $itemLinha = ItemLinha::create($input);
   
            return Redirect::route('cadastro.linhas.itensLinha.show',
				   array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID));
        }

        return Redirect::route('cadastro.linhas.itensLinha.create', $linha->ID)
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($linhaId, $itemLinhaId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)
	                ->findOrFail($linhaId);
	
	$itemLinha = $this->itemLinha->findOrFail($itemLinhaId);

        return View::make('Cadastro.itensLinha.show', compact('linha', 'itemLinha'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($linhaId, $itemLinhaId) {
        $itemLinha = $this->itemLinha->find($itemLinhaId);

        if (is_null($itemLinha)) {
	    return Redirect::route('cadastro.linhas.itensLinha.index');
        }

	$empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)
	                ->findOrFail($linhaId);
	
	$pontosReferencia = PontoReferencia::listarPorDescricaoCompleta($empresaAtualId);
	$webOpcoes = ItemLinha::$webOpcoes;
        
        return View::make('Cadastro.itensLinha.edit', compact('linha', 'pontosReferencia', 'itemLinha', 'webOpcoes'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($linhaId, $itemLinhaId) {
        $input = array_except(Input::all(), '_method');
        
	$empresaAtualId = HelperEmpresa::atual()->Id;
	
	$linha = Linha::daEmpresa($empresaAtualId)
	                ->findOrFail($linhaId);

	$pontoReferencia = PontoReferencia::daEmpresa($empresaAtualId)
	                                     ->findOrFail($input['PontoReferencia_Id']);
        
	$input['Linha_Id'] = $linha->ID;
	
	$input['PontoReferencia_Id'] = $pontoReferencia->Id;
	
	$validation = Validator::make($input, ItemLinha::$rules);

        if ($validation->passes()) {
            $itemLinha = $this->itemLinha->find($itemLinhaId);
            $itemLinha->update($input);
 
            return Redirect::route('cadastro.linhas.itensLinha.show',
				   array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID));
        }

        return Redirect::route('cadastro.linhas.itensLinha.edit',
			       array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID))
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($linhaId, $itemLinhaId) {
        try {
            $this->itemLinha->find($itemLinhaId)->delete();
            return Redirect::route('cadastro.linhas.itensLinha.index', $linhaId);    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}