<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Horario;
use Artaban\Model\Linha;
use Artaban\Model\ItemLinha;
use Artaban\Model\TipoVeiculo;
use Artaban\Service\Cadastro\Horario as Service_Cadastro_Horario;
use Artaban\Helper\Empresa as HelperEmpresa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class HorariosController extends BaseController {

    /**
      * Horario Repository
      *
      * @var Horario
      */
    protected $horario;


    public function __construct(Horario $horario) {
        $empresaAtual = HelperEmpresa::atual();
        $this->horario = $horario->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
	        'Linha_Id' => array('=', Input::get('Linha_Id')),  
	        'LinhaItem_Id_Destino' => array('=', Input::get('LinhaItem_Id_Destino')),  
	        'TipoVeiculo_Id' => array('=', Input::get('TipoVeiculo_Id')),  
	    
	        'Horario' => array('like', Input::get('Horario')),  
	        'Sentido' => array('like', Input::get('Sentido')),  
	        'Frequencia' => array('like', Input::get('Frequencia')),  
	        'Feriado' => array('like', Input::get('Feriado')),  
	        'Confirmado' => array('like', Input::get('Confirmado')),  
	        'Situacao' => array('like', Input::get('Situacao'))
        ); 

	    $empresaAtualId = HelperEmpresa::atual()->Id;
        
	    $linhas = Linha::listarPorDescricaoCompleta($empresaAtualId);
	    $linhas = array('' => '') + $linhas;
	
	    $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->lists('Descricao', 'Id');
	    $tiposVeiculo = array('' => '') + $tiposVeiculo;
	
	    $horarios = Search::execute($this->horario, $fields);
	
	    $sentidos = Horario::$sentidos;
	    $situacoes = array('' => '') + Horario::$situacoes;
	    $confirmadoOpcoes = array('' => '') + Horario::$confirmadoOpcoes;
	    $feriadoOpcoes = array('' => '') +  Horario::$feriadoOpcoes;
		
        return View::make('Cadastro.horarios.index',
			              compact('linhas', 'horarios', 'tiposVeiculo', 'sentidos',
				                  'situacoes', 'confirmadoOpcoes', 'feriadoOpcoes'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
        $empresaAtualId = HelperEmpresa::atual()->Id;
        
	    $linhas = Linha::listarPorDescricaoCompleta($empresaAtualId);
	    
	    $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->lists('Descricao', 'Id');
	    
        $itensLinha = [];
      
	    $linhaSelecionadaId = null;
	    $tipoVeiculoSelecionadoId = null;
	    $itemLinhaSelecionadaId = null;
	
	    $sentidos = Horario::$sentidos;
   	    $situacoes = Horario::$situacoes;
	    $confirmadoOpcoes = Horario::$confirmadoOpcoes;
	    $feriadoOpcoes = Horario::$feriadoOpcoes;
	
        $horario = new Horario;
    
	    return View::make('Cadastro.horarios.create',
			              compact('horario', 'linhas', 'tiposVeiculo', 'itensLinha',
				                  'linhaSelecionadaId', 'tipoVeiculoSelecionadoId', 'itemLinhaSelecionadaId',
				                  'sentidos', 'situacoes', 'confirmadoOpcoes', 'feriadoOpcoes'
        ));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        try {
            $input = Input::all();
        
            $input['Empresa_Id'] = HelperEmpresa::atual()->Id;
            
	        $cadastroHorario = new Service_Cadastro_Horario($input);	     
	        $horario = $cadastroHorario->salvar();  
            
            return Redirect::route('cadastro.horarios.show', $horario->Id);
    
        } catch (Exception $e) {
            return Redirect::route('cadastro.horarios.create')
                           ->withInput()
                           ->withErrors($cadastroHorario->erros())
                           ->with('message', Lang::get('validation.form.invalid'));
        }
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $horario = $this->horario->findOrFail($id);

	    $tipoVeiculo = $horario->tipoVeiculo;
	
	    $linha = $horario->linha;
	
	    $itemLinhaDestino = $horario->itemLinhaDestino;
	
        return View::make('Cadastro.horarios.show',
			              compact('tipoVeiculo', 'linha', 'itemLinhaDestino', 'horario'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $horario = $this->horario->find($id);

	    $empresaAtualId = HelperEmpresa::atual()->Id;
        
	    $linhas = Linha::listarPorDescricaoCompleta($empresaAtualId);
	    
	    $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->lists('Descricao', 'Id');
	    
	    $itensLinha = [];
	    foreach($horario->linha->itens as $item) {
	        $itensLinha[$item->ID] = $item->pontoReferencia->descricaoCompleta();
	    }
	
	    $linhaSelecionadaId = $horario->Linha_Id;
	    $tipoVeiculoSelecionadoId = $horario->TipoVeiculo_Id;
		
	    $sentidos = Horario::$sentidos;
	    $situacoes = Horario::$situacoes;
	    $confirmadoOpcoes = Horario::$confirmadoOpcoes;
	    $feriadoOpcoes = Horario::$feriadoOpcoes;
	
        if (is_null($horario)) {
	        return Redirect::route('cadastro.horarios.index');
        }

        return View::make('Cadastro.horarios.edit',
			              compact('horario', 'linhas', 'tiposVeiculo', 'itensLinha',
				                  'linhaSelecionadaId', 'tipoVeiculoSelecionadoId', 'itemLinhaSelecionadaId',
				                  'sentidos', 'situacoes', 'confirmadoOpcoes', 'feriadoOpcoes'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        try {
            $input = array_except(Input::all(), '_method');
        
            $input['id'] = $id;
            $input['Empresa_Id'] = HelperEmpresa::atual()->Id;
            
	        $cadastroHorario = new Service_Cadastro_Horario($input);	     
	        $horario = $cadastroHorario->salvar();  
            
            return Redirect::route('cadastro.horarios.show', $horario->Id);
    
        } catch (Exception $e) {
            return Redirect::route('cadastro.horarios.edit', $id)
                           ->withInput()
                           ->withErrors($cadastroHorario->erros())
                           ->with('message', Lang::get('validation.form.invalid'));
        }
        
        
        /*
        $input = array_except(Input::all(), '_method');
        
	    $linhaId = $input['Linha_Id'];
	    $tipoVeiculo_Id = $input['TipoVeiculo_Id'];
	
	    $empresaAtualId = HelperEmpresa::atual()->Id;
        
	    if ($tipoVeiculo_Id === "") {
	        unset($input['TipoVeiculo_Id']);
	    } else {
	        $tipoVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->findOrFail($tipoVeiculo_Id);
 	    }
	
	    $validation = Validator::make($input, Horario::$rules);

        if ($validation->passes()) {
            $linha = Linha::daEmpresa($empresaAtualId)->findOrFail($linhaId);
        
            $horario = $this->horario->find($id);
            $horario->update($input);

            return Redirect::route('cadastro.horarios.show', $id);
        }

        return Redirect::route('cadastro.horarios.edit', $id)
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
               */
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->horario->find($id)->delete();
            return Redirect::route('cadastro.horarios.index');    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }       
    }

}