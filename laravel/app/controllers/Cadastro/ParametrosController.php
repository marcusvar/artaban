<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Parametro;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;


class ParametrosController extends BaseController {

    /**
      * Parametro Repository
      *
      * @var Parametro
      */
    protected $parametro;


    public function __construct(Parametro $parametro) {
       $this->parametro = $parametro;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
			 'Nome' => array('like', Input::get('Nome')),  
			 'Valor' => array('like', Input::get('Valor')),  
			 'Descricao' => array('like', Input::get('Descricao')),  

        ); 

        $parametros = Search::execute($this->parametro, $fields);
        return View::make('Cadastro.parametros.index', compact('parametros'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
       return View::make('Cadastro.parametros.create');
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        $validation = Validator::make($input, Parametro::$rules);

        if ($validation->passes()) {
            $parametro = $this->parametro->create($input);
   
            return Redirect::route('cadastro.parametros.show', $parametro->Id);
        }

        return Redirect::route('cadastro.parametros.create')
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $parametro = $this->parametro->findOrFail($id);

        return View::make('Cadastro.parametros.show', compact('parametro'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $parametro = $this->parametro->find($id);

        if (is_null($parametro)) {
	    return Redirect::route('cadastro.parametros.index');
        }

        return View::make('Cadastro.parametros.edit', compact('parametro'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Parametro::$rules);

        if ($validation->passes()) {
            $parametro = $this->parametro->find($id);
            $parametro->update($input);

            return Redirect::route('cadastro.parametros.show', $id);
        }

        return Redirect::route('cadastro.parametros.edit', $id)
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        $this->parametro->find($id)->delete();

        return Redirect::route('cadastro.parametros.index');
    }

}