<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\TipoAdicionalTarifa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class TiposAdicionaisTarifasController extends BaseController {

    /**
      * TipoAdicionalTarifa Repository
      *
      * @var TipoAdicionalTarifa
      */
    protected $tipoAdicionalTarifa;


    public function __construct(TipoAdicionalTarifa $tipoAdicionalTarifa) {
       $this->tipoAdicionalTarifa = $tipoAdicionalTarifa;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
            'Descricao' => array('like', Input::get('Descricao')),  
        ); 

        $tiposAdicionaisTarifas = Search::execute($this->tipoAdicionalTarifa, $fields);
        return View::make('Cadastro.tiposAdicionaisTarifas.index', compact('tiposAdicionaisTarifas'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
        return View::make('Cadastro.tiposAdicionaisTarifas.create');
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        $validation = Validator::make($input, TipoAdicionalTarifa::$rules);

        if ($validation->passes()) {
            $tipoAdicionalTarifa = $this->tipoAdicionalTarifa->create($input);
   
            return Redirect::route('cadastro.tiposAdicionaisTarifas.show', $tipoAdicionalTarifa->Id);
        }

        return Redirect::route('cadastro.tiposAdicionaisTarifas.create')
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $tipoAdicionalTarifa = $this->tipoAdicionalTarifa->findOrFail($id);

        return View::make('Cadastro.tiposAdicionaisTarifas.show', compact('tipoAdicionalTarifa'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $tipoAdicionalTarifa = $this->tipoAdicionalTarifa->find($id);

        if (is_null($tipoAdicionalTarifa)) {
	        return Redirect::route('cadastro.tiposAdicionaisTarifas.index');
        }

        return View::make('Cadastro.tiposAdicionaisTarifas.edit', compact('tipoAdicionalTarifa'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, TipoAdicionalTarifa::$rules);

        if ($validation->passes()) {
            $tipoAdicionalTarifa = $this->tipoAdicionalTarifa->find($id);
            $tipoAdicionalTarifa->update($input);

            return Redirect::route('cadastro.tiposAdicionaisTarifas.show', $id);
        }

        return Redirect::route('cadastro.tiposAdicionaisTarifas.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->tipoAdicionalTarifa->find($id)->delete();
            return Redirect::route('cadastro.tiposAdicionaisTarifas.index');
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}