<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Mapa;
use Artaban\Model\TipoVeiculo;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;


class MapasController extends BaseController {

    /**
      * Mapa Repository
      *
      * @var Mapa
      */
    protected $mapa;


    public function __construct(Mapa $mapa) {
        $this->mapa = $mapa;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($tipoVeiculoId) {
        $tipoVeiculo = TipoVeiculo::findOrFail($tipoVeiculoId);  
        $mapa = $tipoVeiculo->mapa();
     
        return View::make('Cadastro.mapas.index', compact('tipoVeiculo', 'mapa'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
        return View::make('Cadastro.mapas.create');
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        $validation = Validator::make($input, Mapa::$rules);

        if ($validation->passes()) {
            $mapa = $this->mapa->create($input);
   
            return Redirect::route('cadastro.mapas.show', $mapa->Id);
        }

        return Redirect::route('cadastro.mapas.create')
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $mapa = $this->mapa->findOrFail($id);

        return View::make('Cadastro.mapas.show', compact('mapa'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $mapa = $this->mapa->find($id);

        if (is_null($mapa)) {
	        return Redirect::route('cadastro.mapas.index');
        }

        return View::make('Cadastro.mapas.edit', compact('mapa'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Mapa::$rules);

        if ($validation->passes()) {
            $mapa = $this->mapa->find($id);
            $mapa->update($input);

            return Redirect::route('cadastro.mapas.show', $id);
        }

        return Redirect::route('cadastro.mapas.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        $this->mapa->find($id)->delete();
        return Redirect::route('cadastro.mapas.index');
    }

}