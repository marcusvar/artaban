<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\ItemLinha;
use Artaban\Model\Viagem;
use Artaban\Model\Horario;
use Artaban\Model\TipoVeiculo;
use Artaban\Model\Veiculo;
use Artaban\Model\PontoReferencia;
use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Service\Cadastro\Viagem as Service_Cadastro_Viagem;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;


class ViagensController extends BaseController {

    /**
      * Viagem Repository
      *
      * @var Viagem
      */
    protected $viagem;


    public function __construct(Viagem $viagem) {
        $empresaAtual = HelperEmpresa::atual();
        $this->viagem = $viagem->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
            'DataInicio' => array(
                 'between',
                 array(Input::get('DataInicio_inicial'), Input::get('DataInicio_final')),
                 'date'
             ),
        
             'Horario_Id' => array('=', Input::get('Horario_Id')),  
	     'LinhaItem_Id_Destino' => array('=', Input::get('LinhaItem_Id_Destino')),  
	     'TipoVeiculo_Id' => array('=', Input::get('TipoVeiculo_Id')),  
	     'Confirmada' => array('like', Input::get('Confirmada')),  
	     'Situacao' => array('like', Input::get('Situacao'))
        ); 

        $viagens = Search::execute($this->viagem, $fields, ['order' => ['DataInicio' => 'DESC']]);
	
	$empresaAtual = HelperEmpresa::atual();
       
	$horariosLista = Horario::daEmpresa($empresaAtual->Id)->get();
        $horarios = [];
        foreach($horariosLista as $horario) {
            $horarios[$horario->Id] = $horario->descricaoDaLinhaComHorario();
        }
	$horarios = array('' => '') + $horarios;
      	
	$pontosReferencia = [];
	
        $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtual->Id)->lists('Descricao', 'Id');
        $tiposVeiculo = array('' => '') + $tiposVeiculo;
	
        $veiculos = Veiculo::daEmpresa($empresaAtual->Id)->lists('Numero', 'Id');
        //$veiculos = array('' => '') + $veiculos;
	
        $situacoes = array('' => '') + Viagem::$situacoes;
        $confirmadaOpcoes = array('' => '') + Viagem::$confirmadaOpcoes;

	    return View::make('Cadastro.viagens.index',
			              compact('viagens', 'horarios', 'pontosReferencia', 'tiposVeiculo',
				              'veiculos', 'situacoes', 'confirmadaOpcoes'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
        $empresaAtual = HelperEmpresa::atual();
        
        $horariosLista = Horario::daEmpresa($empresaAtual->Id)->get();
       
        $horarios = [];
        foreach($horariosLista as $horario) {
            $horarios[$horario->Id] = $horario->descricaoDaLinhaComHorario();
        }
       
        $pontosReferencia = [];
       
        $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtual->Id)->lists('Descricao', 'Id');
       
        $veiculos = Veiculo::daEmpresa($empresaAtual->Id)->orderBy('Numero')->lists('Numero', 'Id');
        $veiculos = array('' => '') + $veiculos;
        
        $situacoes = Viagem::$situacoes;
        $confirmadaOpcoes = Viagem::$confirmadaOpcoes;

        return View::make('Cadastro.viagens.create',
			              compact('horarios', 'pontosReferencia', 'tiposVeiculo',
				              'veiculos', 'situacoes', 'confirmadaOpcoes'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        try {
            $input = Input::all();
        
            $input['Empresa_Id'] = HelperEmpresa::atual()->Id;
            
	    $cadastroViagem = new Service_Cadastro_Viagem($input);	     
	    $viagem = $cadastroViagem->salvar();  
            
            return Redirect::route('cadastro.viagens.show', $viagem->Id);
    
        } catch (Exception $e) {
            return Redirect::route('cadastro.viagens.create')
                           ->withInput()
                           ->withErrors($cadastroViagem->erros())
                           ->with('message', Lang::get('validation.form.invalid'));
        }
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $viagem = $this->viagem->findOrFail($id);

        return View::make('Cadastro.viagens.show', compact('viagem'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $viagem = $this->viagem->find($id);

        if (is_null($viagem)) {
	        return Redirect::route('cadastro.viagens.index');
        }
	        
        $empresaAtual = HelperEmpresa::atual();
	
        $horariosLista = Horario::daEmpresa($empresaAtual->Id)->get();
              
        $horarios = [];
        foreach($horariosLista as $horario) {
            $horarios[$horario->Id] = $horario->descricaoDaLinhaComHorario();
        }
              
        $pontosReferencia = [];
	    foreach($viagem->horario->itens as $itemHorario) {
	        $pontoReferencia = $itemHorario->itemLinha->pontoReferencia; 
	        $pontosReferencia[$pontoReferencia->Id] = $pontoReferencia->descricaoCompleta();
        }
              
        $tiposVeiculo = TipoVeiculo::daEmpresa($empresaAtual->Id)->lists('Descricao', 'Id');
       
        $veiculos = Veiculo::daEmpresa($empresaAtual->Id)->orderBy('Numero')->lists('Numero', 'Id');
        //$veiculos = array('' => '') + $veiculos;
	
        $situacoes = Viagem::$situacoes;
        $confirmadaOpcoes = Viagem::$confirmadaOpcoes;
	
        return View::make('Cadastro.viagens.edit',
			              compact('viagem', 'horarios', 'pontosReferencia', 'tiposVeiculo',
				              'veiculos', 'situacoes', 'confirmadaOpcoes'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        	
	$empresaAtualId = HelperEmpresa::atual()->Id;
       	
	$tipoVeiculo_Id = $input['TipoVeiculo_Id'];
        $tipoVeiculo = TipoVeiculo::daEmpresa($empresaAtualId)->findOrFail($tipoVeiculo_Id);
	
	$veiculo_Id = $input['Veiculo_Id'];
        $veiculo = Veiculo::daEmpresa($empresaAtualId)->orderBy('Numero')->findOrFail($veiculo_Id);
	
	$horario_Id = $input['Horario_Id'];
        $horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horario_Id);
		
	$validation = Validator::make($input, Viagem::$rules);

        if ($validation->passes()) {
            $viagem = $this->viagem->find($id);
            $viagem->update($input);

            return Redirect::route('cadastro.viagens.show', $id);
        }

        return Redirect::route('cadastro.viagens.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->viagem->find($id)->delete();
            return Redirect::route('cadastro.viagens.index');
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}