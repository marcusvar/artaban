<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Fidelizacao;
use Artaban\Model\Pessoa;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Lib\Formatacao\Dinheiro\Dinheiro;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;


class FidelizacoesController extends BaseController {

    /**
      * Fidelizacao Repository
      *
      * @var Fidelizacao
      */
    protected $fidelizacao;

    protected $empresaAtualId;
    

    public function __construct(Fidelizacao $fidelizacao) {
       $this->fidelizacao = $fidelizacao;
       $this->empresaAtualId = HelperEmpresa::atual()->Id;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($pessoaId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
      
        $fields = array(
	    'Operacao'      => array('=', Input::get('Operacao')),   
            'DataOperacao'  => array( 'between',
                                     array(Input::get('DataOperacao_inicial'), Input::get('DataOperacao_final')),
                                    'date'),
            'ValorOperacao' => array('=', Input::get('ValorOperacao')),  
            'Pessoa_Id'     => array('=', $pessoa->Id),              
        ); 

        $fidelizacoes = Search::execute($this->fidelizacao, $fields, ['order' => ['DataOperacao' => 'DESC']]);
	    
        $operacaoOpcoes = array('' => '') + Fidelizacao::$operacaoOpcoes;
        
        return View::make('Cadastro.fidelizacoes.index', compact('pessoa', 'operacaoOpcoes', 'fidelizacoes'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create($pessoaId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
        $fidelizacao = new Fidelizacao; 
        $operacaoOpcoes = Fidelizacao::$operacaoOpcoes;
        $valorOperacao = 0;
        $valorBonus    = 0;

       return View::make('Cadastro.fidelizacoes.create', compact('pessoa', 'fidelizacao', 'operacaoOpcoes', 'valorOperacao', 'valorBonus' ));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store($pessoaId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
       
	$input = Input::all();
	$input['Pessoa_Id'] = $pessoa->Id;
        $input['ValorOperacao'] = Dinheiro::comMascara(Dinheiro::semMascara($input['ValorOperacao'])*($input['Operacao'] === 'Crédito' ? 0.01 : 1));
	
        $validation = Validator::make($input, Fidelizacao::$rules);

        if ($validation->passes()) {
            $fidelizacao = $this->fidelizacao->create($input);
            
            return Redirect::route('cadastro.pessoas.fidelizacoes.show', [$pessoa->Id, $fidelizacao->Id]);
        }

        return Redirect::route('cadastro.pessoas.fidelizacoes.create', $pessoa->Id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($pessoaId, $fidelizacaoId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
	$fidelizacao = $this->fidelizacao
	                    ->where('Pessoa_Id', '=', $pessoa->Id)
			    ->findOrFail($fidelizacaoId);

        return View::make('Cadastro.fidelizacoes.show', compact('pessoa', 'fidelizacao'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($pessoaId, $fidelizacaoId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
	
	$fidelizacao = $this->fidelizacao
	                    ->where('Pessoa_Id', '=', $pessoa->Id)
			    ->findOrFail($fidelizacaoId);
        
        if (is_null($fidelizacao)) {
	    return Redirect::route('cadastro.pessoas.fidelizacoes.index', [$pessoa->Id]);
        }

	$operacaoOpcoes = Fidelizacao::$operacaoOpcoes;
        $valorOperacao = Dinheiro::comMascara(Dinheiro::semMascara($fidelizacao->ValorOperacao)/($fidelizacao->Operacao === 'Crédito' ? 0.01 : 1));
        $valorBonus    = $fidelizacao->ValorOperacao;
                
        return View::make('Cadastro.fidelizacoes.edit', compact('pessoa', 'fidelizacao', 'operacaoOpcoes', 'valorOperacao', 'valorBonus'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($pessoaId, $fidelizacaoId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
	
        $input = array_except(Input::all(), '_method');
        $input['Pessoa_Id'] = $pessoa->Id;
	
	$validation = Validator::make($input, Fidelizacao::$rules);

        if ($validation->passes()) {
            $fidelizacao = $this->fidelizacao
	                        ->where('Pessoa_Id', '=', $pessoa->Id)
			        ->findOrFail($fidelizacaoId);
            $input['ValorOperacao'] = Dinheiro::comMascara(Dinheiro::semMascara($input['ValorOperacao'])*($input['Operacao'] === 'Crédito' ? 0.01 : 1));
	    $fidelizacao->update($input);

            return Redirect::route('cadastro.pessoas.fidelizacoes.show', [$pessoa->Id, $fidelizacao->Id]);
        }

        return Redirect::route('cadastro.pessoas.fidelizacoes.edit', [$pessoa->Id, $fidelizacaoId])
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($pessoaId, $fidelizacaoId) {
        $pessoa = Pessoa::daEmpresa($this->empresaAtualId)->findOrFail($pessoaId);
	
	$fidelizacao = $this->fidelizacao
	                    ->where('Pessoa_Id', '=', $pessoa->Id)
			    ->find($fidelizacaoId)
			    ->delete();

        return Redirect::route('cadastro.pessoas.fidelizacoes.index', $pessoa->Id);
    }

}