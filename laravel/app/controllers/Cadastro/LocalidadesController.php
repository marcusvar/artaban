<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Localidade;
use Artaban\Model\Estado;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Request;


class LocalidadesController extends BaseController {

    /**
      * Localidade Repository
      *
      * @var Localidade
      */
    protected $localidade;


    public function __construct(Localidade $localidade) {
       $this->localidade = $localidade;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
            'Nome' => array('like', Input::get('Nome')),  
            'Tipo' => array('=', Input::get('Tipo')),  
	        'Fuso' => array('=', Input::get('Fuso')),  
	        'UF_Id' => array('=', Input::get('UF_Id'))  
        ); 

	    if (Request::format() === 'json') {
	        $localidades = Search::execute($this->localidade, $fields, ['paginate' => false]);
	        return $localidades->toJson(); 
	    }
	
	    $localidades = Search::execute($this->localidade, $fields);
	    $estados = Estado::lists('Nome', 'Id');
	    $tiposLocalidade = Localidade::$tipos;
       
        return View::make('Cadastro.localidades.index', compact('localidades', 'estados', 'tiposLocalidade'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
       $estados = Estado::lists('Nome', 'Id');
       $tiposLocalidade = Localidade::$tipos;
       
       return View::make('Cadastro.localidades.create', compact('estados', 'tiposLocalidade'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        $validation = Validator::make($input, Localidade::$rules);

        if ($validation->passes()) {
            $localidade = $this->localidade->create($input);
   
            return Redirect::route('cadastro.localidades.show', $localidade->Id);
        }

        return Redirect::route('cadastro.localidades.create')
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $localidade = $this->localidade->findOrFail($id);

        return View::make('Cadastro.localidades.show', compact('localidade'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $localidade = $this->localidade->find($id);

        if (is_null($localidade)) {
	        return Redirect::route('cadastro.localidades.index');
        }

	    $estados = Estado::lists('Nome', 'Id');  
        $tiposLocalidade = Localidade::$tipos;
       
	    return View::make('Cadastro.localidades.edit', compact('localidade', 'estados', 'tiposLocalidade'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Localidade::$rules);

        if ($validation->passes()) {
            $localidade = $this->localidade->find($id);
            $localidade->update($input);

            return Redirect::route('cadastro.localidades.show', $id);
        }

        return Redirect::route('cadastro.localidades.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        $this->localidade->find($id)->delete();

        return Redirect::route('cadastro.localidades.index');
    }

}