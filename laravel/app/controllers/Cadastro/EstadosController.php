<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Estado;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;


class EstadosController extends BaseController {

    /**
      * Estado Repository
      *
      * @var Estado
      */
    protected $estado;


    public function __construct(Estado $estado) {
       $this->estado = $estado;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
            'Nome' => array('like', Input::get('Nome')),  
	    'Sigla' => array('like', Input::get('Sigla')),  

        ); 

        $estados = Search::execute($this->estado, $fields);
        return View::make('Cadastro.estados.index', compact('estados'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
       return View::make('Cadastro.estados.create');
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        $validation = Validator::make($input, Estado::$rules);

        if ($validation->passes()) {
            $estado = $this->estado->create($input);
   
            return Redirect::route('cadastro.estados.show', $estado->Id);
        }

        return Redirect::route('cadastro.estados.create')
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $estado = $this->estado->findOrFail($id);

        return View::make('Cadastro.estados.show', compact('estado'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $estado = $this->estado->find($id);

        if (is_null($estado)) {
	    return Redirect::route('cadastro.estados.index');
        }

        return View::make('Cadastro.estados.edit', compact('estado'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Estado::$rules);

        if ($validation->passes()) {
            $estado = $this->estado->find($id);
            $estado->update($input);

            return Redirect::route('cadastro.estados.show', $id);
        }

        return Redirect::route('cadastro.estados.edit', $id)
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        $this->estado->find($id)->delete();

        return Redirect::route('cadastro.estados.index');
    }

}