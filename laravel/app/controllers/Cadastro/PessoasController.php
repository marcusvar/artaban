<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Pessoa;
use Artaban\Model\Endereco;
use Artaban\Model\Localidade;
use Artaban\Model\Estado;
use Artaban\Helper\Empresa as HelperEmpresa;

use Artaban\Service\Cadastro\Pessoa\Pessoa as CadastroPessoa;

use Artaban\Helper\View\FormPessoa;
use Artaban\Helper\View\ShowPessoa;
use Artaban\Validation\Documento;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;
use Form;
use Response;


class PessoasController extends BaseController {

    /**
      * Pessoa Repository
      *
      * @var Pessoa
      */
    protected $pessoa;
    
    protected $form;


    public function __construct(Pessoa $pessoa) {
        $empresaAtual = HelperEmpresa::atual();
        $this->pessoa = $pessoa->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
	    'Nome'      => array('like', Input::get('Nome')),
            'Documento' => array('like', Input::get('Documento')),
            'RGIE'      => array('like', Input::get('RGIE')),
            'Tipo'      => array('=', Input::get('Tipo'))
        ); 

        $pessoas = Search::execute($this->pessoa, $fields, ['order' => 'nome']);
        
        $tipos = array('' => '') + Pessoa::$tiposOpcoes;
        
	    return View::make('Cadastro.pessoas.index', compact('pessoas', 'tipos'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
        try {
            $tipo = Input::get('tipo');
	        
            $form = (string) new FormPessoa($tipo);
            
            return View::make('Cadastro.pessoas.create', compact('form'));
        
	    } catch (Exception $e) {
            $form = (string) View::make('Cadastro.pessoas._form_create');
	        return View::make('Cadastro.pessoas.create', compact('form'));   
        }    
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        try {
            $input = $this->getInputData();
            
            $cadastroPessoa = new CadastroPessoa($input);
	     
            $pessoa = $cadastroPessoa->salvar();
	        
	        return Redirect::route('cadastro.pessoas.show', $pessoa->Id);
    
        } catch (Exception $e) {
	        $tipo = Input::get('Tipo');
        	    
            return Redirect::route('cadastro.pessoas.create', compact('tipo'))
                           ->withInput()
                           ->withErrors($cadastroPessoa->erros())
                           ->with('message', Lang::get('validation.form.invalid'));
        }
        
    }

   
    private function getInputData() {
        $pessoa = Input::only(
            'Tipo', 'EmPotencial', 'Nome', 'NomeUsual', 'Documento', 'RGIE', 'Sexo', 'DataNascimento',
	        'VisualizarFidelizacao', 'Pessoa_Id_Responsavel', 'Email', 'Telefone', 'Celular'
        ); 
	
	    $pessoa['Pessoa_Id'] = \Artaban\Helper\Empresa::atual()->Id;
	
	    $endereco = array(
            'Localidade_Id' => Input::get('endereco.Localidade_Id'),
	        'Logradouro'    => Input::get('endereco.Logradouro'),
	        'Numero'        => Input::get('endereco.Numero'),
	        'Bairro'        => Input::get('endereco.Bairro'),
	        'Complemento'   => Input::get('endereco.Complemento'),
	        'Cep'           => Input::get('endereco.Cep')
        );
        
	    return ['Pessoa' => $pessoa, 'Endereco' => $endereco];
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $pessoa = $this->pessoa->findOrFail($id);
        
	    $show = (string) new ShowPessoa($pessoa->Id);	

        return View::make('Cadastro.pessoas.show', compact('pessoa', 'show'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $pessoa = Pessoa::daEmpresa(HelperEmpresa::atual()->Id)->find($id);
        
        if (is_null($pessoa)) {
	        return Redirect::route('cadastro.pessoas.index');
        }

	    $form = (string) new FormPessoa($pessoa->Tipo, $pessoa->Id);
        
        return View::make('Cadastro.pessoas.edit', compact('pessoa', 'form'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        try {
             $input = $this->getInputData();
             
             $input['Pessoa']['Id'] = $id;
	        
             $cadastroPessoa = new CadastroPessoa($input);
	     
             $pessoa = $cadastroPessoa->salvar();
	     
	     return Redirect::route('cadastro.pessoas.show', $pessoa->Id);
         
	 } catch (Exception $e) {
             return Redirect::route('cadastro.pessoas.edit', $id)
                            ->withInput()
                            ->withErrors($cadastroPessoa->erros())
                            ->with('message', Lang::get('validation.form.invalid'));
         }
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->pessoa->find($id)->delete();
            return Redirect::route('cadastro.pessoas.index');    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }        
    }

}