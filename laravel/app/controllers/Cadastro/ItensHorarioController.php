<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Horario;
use Artaban\Model\ItemHorario;
use Artaban\Helper\Empresa as HelperEmpresa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class ItensHorarioController extends BaseController {

    /**
      * ItemHorario Repository
      *
      * @var ItemHorario
      */
    protected $itemHorario;


    public function __construct(ItemHorario $itemHorario) {
        $empresaAtual = HelperEmpresa::atual();
        $this->itemHorario = $itemHorario->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($horarioId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
        $horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horarioId);

        $itensLinha = [];
        foreach($horario->linha->itens as $item) {
	        $itensLinha[$item->ID] = $item->pontoReferencia->Descricao;
        }
        $itensLinha = array('' => '') + $itensLinha;	
	
        $fields = array(
	        'Horario_Id' => array('=', $horario->Id),  
	        'LinhaItem_Id' => array('=', Input::get('LinhaItem_Id')),
                'LinhaItem_Id_Transbordo' => array('=', Input::get('LinhaItem_Id_Transbordo')),
                'Recolher' => array('=', Input::get('Recolher')),
                'Web' => array('=', Input::get('Web'))
        ); 

        $itensHorario = Search::execute($this->itemHorario, $fields);
	
        $recolherOpcoes = array('' => '') + ItemHorario::$recolherOpcoes;
        $webOpcoes = array('' => '') + ItemHorario::$webOpcoes;
        
        return View::make('Cadastro.itensHorario.index', 
                compact('horario', 'itensLinha', 'itensHorario', 'recolherOpcoes', 'webOpcoes'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create($horarioId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	
        $horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horarioId);
 
        $itensLinha = ['' => ''];
        foreach($horario->linha->itens as $item) {
	        $itensLinha[$item->ID] = $item->pontoReferencia->descricaoCompleta();
        }
       
        $itemLinhaSelecionadaId = null;
        
        $itensLinhaTransbordo = ['' => null];
        foreach($horario->linha->itens as $item) {
	        $itensLinhaTransbordo[$item->ID] = $item->pontoReferencia->descricaoCompleta();
        }
       
        $itemLinhaTransbordoId = null;
        
        $diasEmbarque = ItemHorario::$diasEmbarque;
       	$recolherOpcoes = array('' => '') + ItemHorario::$recolherOpcoes;
        $webOpcoes = array('' => '') + ItemHorario::$webOpcoes;
        return View::make('Cadastro.itensHorario.create',
			              compact('itensLinha', 'itemLinhaSelecionadaId', 'itensLinhaTransbordo', 
                                              'itemLinhaTransbordoId', 'horario', 'diasEmbarque', 'recolherOpcoes', 'webOpcoes'));
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store($horarioId) {
        $input = Input::all();
	
	$empresaAtualId = HelperEmpresa::atual()->Id;
	
        $horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horarioId);
        $input['Horario_Id'] = $horario->Id;
		
	$itemLinhaId = $input['LinhaItem_Id'];
	
	if (! $horario->linha->itens->contains($itemLinhaId)) {
	    unset($input['LinhaItem_Id']); 
	}
	
        $itemLinhaIdTransbordo = $input['LinhaItem_Id_Transbordo'];
	
	if (! $horario->linha->itens->contains($itemLinhaIdTransbordo)) {
	    unset($input['LinhaItem_Id_Transbordo']); 
	}
	
        $validation = Validator::make($input, ItemHorario::$rules);

        if ($validation->passes()) {
            $itemHorario = ItemHorario::create($input);
            
            return Redirect::route('cadastro.horarios.itensHorario.show',
				                   array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id));
        }

        return Redirect::route('cadastro.horarios.itensHorario.create', array($horario->Id))
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($horarioId, $itemHorarioId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	    $horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horarioId);
        $itemHorario = $this->itemHorario->findOrFail($itemHorarioId);

        return View::make('Cadastro.itensHorario.show', compact('horario', 'itemHorario'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($horarioId, $itemHorarioId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
	$horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horarioId);
        $itemHorario = $this->itemHorario->find($itemHorarioId);

        if (is_null($itemHorario)) {
	        return Redirect::route('cadastro.horarios.itensHorario.index',
				                   array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id));
        }

	$itensLinha = [];
        foreach($horario->linha->itens as $item) {
	        $itensLinha[$item->ID] = $item->pontoReferencia->descricaoCompleta();
        }
       
        $itemLinhaSelecionadaId = $itemHorario->LinhaItem_Id;
        
	$itensLinhaTransbordo = ['' => null];
        foreach($horario->linha->itens as $item) {
	        $itensLinhaTransbordo[$item->ID] = $item->pontoReferencia->descricaoCompleta();
        }
       
        $itemLinhaTransbordoId = $itemHorario->LinhaItem_Id_Transbordo;
        
        $diasEmbarque = ItemHorario::$diasEmbarque;
       	$recolherOpcoes = ItemHorario::$recolherOpcoes;
        $webOpcoes = ItemHorario::$webOpcoes;
        
        return View::make('Cadastro.itensHorario.edit',
			   compact('itensLinha', 'itemLinhaSelecionadaId', 'itensLinhaTransbordo', 'itemLinhaTransbordoId', 
                                   'horario', 'itemHorario', 'diasEmbarque', 'recolherOpcoes', 'webOpcoes'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($horarioId, $itemHorarioId) {
        $input = array_except(Input::all(), '_method');
        
	$empresaAtualId = HelperEmpresa::atual()->Id;
	
        $horario = Horario::daEmpresa($empresaAtualId)->findOrFail($horarioId);
        $input['Horario_Id'] = $horario->Id;
		
	$itemLinhaId = $input['LinhaItem_Id'];
	
        if (! $horario->linha->itens->contains($itemLinhaId)) {
            unset($input['LinhaItem_Id']); 
        }

        $itemLinhaIdTransbordo = $input['LinhaItem_Id_Transbordo'] === "" ? null: $input['LinhaItem_Id_Transbordo'];
        
	if (! $horario->linha->itens->contains($itemLinhaIdTransbordo) and $itemLinhaIdTransbordo !== null) {
	    unset($input['LinhaItem_Id_Transbordo']); 
	}
	
        $input['LinhaItem_Id_Transbordo'] = $itemLinhaIdTransbordo;
        
        $validation = Validator::make($input, ItemHorario::$rules);

        if ($validation->passes()) {
            $itemHorario = $this->itemHorario->find($itemHorarioId);
            $itemHorario->update($input);

            return Redirect::route('cadastro.horarios.itensHorario.show',
				                   array('horarios' => $horario->Id,
					                     'itensHorario' => $itemHorario->Id));
        }

        return Redirect::route('cadastro.horarios.itensHorario.edit',
			                   array('horarios' => $horario->Id,
				                     'itensHorario' => $itemHorario->Id))
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($horarioId, $itemHorarioId) {
        try {
            $this->itemHorario->find($itemHorarioId)->delete();
            return Redirect::route('cadastro.horarios.itensHorario.index', $horarioId);    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }       
    }

}