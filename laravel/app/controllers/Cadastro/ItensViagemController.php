<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Viagem;
use Artaban\Model\ItemViagem;
use Artaban\Model\ItemHorario;
use Artaban\Helper\Empresa as HelperEmpresa;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class ItensViagemController extends BaseController {

    /**
      * ItemViagem Repository
      *
      * @var ItemViagem
      */
    protected $itemViagem;


    public function __construct(ItemViagem $itemViagem) {
        $empresaAtual = HelperEmpresa::atual();
        $this->itemViagem = $itemViagem->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index($viagemId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
    
        $viagem = Viagem::daEmpresa($empresaAtualId)->findOrFail($viagemId);
	
        $fields = array(
			 'Viagem_Id' => array('=', $viagem->Id),  
			 'HorarioItem_Id' => array('=', Input::get('HorarioItem_Id')),  
			 'HorarioItem_Id_Transbordo' => array('=', Input::get('HorarioItem_Id_Transbordo')),  
			 'Horario' => array('like', Input::get('Horario')),  
			 'Ativo' => array('like', Input::get('Ativo')),  
			 'Recolher' => array('like', Input::get('Recolher')),
                         'Web' => array('like', Input::get('Web'))
        ); 

        $itensViagem = Search::execute($this->itemViagem, $fields);
	
	    	
	
	$itensHorarioLista = $viagem->horario->itens;
	
	$itensHorario = [];
	foreach($itensHorarioLista as $item) {
	    $itensHorario[$item->Id] = $item->descricaoDaLinhaComHorario();   
	}
	$itensHorario = array('' => '') + $itensHorario;
	
	$ativoOpcoes = array('' => '') + ItemViagem::$ativoOpcoes;
       
	$recolherOpcoes = array('' => '') + ItemViagem::$recolherOpcoes;
	$webOpcoes = array('' => '') + ItemViagem::$webOpcoes;
       
        return View::make('Cadastro.itensViagem.index',
			  compact('viagem', 'itensViagem', 'itensHorario', 'ativoOpcoes', 'recolherOpcoes', 'webOpcoes'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create($viagemId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
        	
        $viagem = Viagem::daEmpresa($empresaAtualId)->findOrFail($viagemId);
       
        $itensHorarioLista = $viagem->horario->itens;
	
	$itensHorario = ['' => ''];
	foreach($itensHorarioLista as $item) {
	    $itensHorario[$item->Id] = $item->descricaoDaLinhaComHorario();   
	}

        // Busca itens da linha para seleção da localidade de transbordo
        $itensHorarioTransbordo = ['' => ''];
        foreach($viagem->horario->linha->itens as $item) {
            $itensHorarioTransbordo[$item->ID] = $item->pontoReferencia->descricaoCompleta();
        }
        $itemHorarioTransbordoId = null;
        
        $ativoOpcoes = ItemViagem::$ativoOpcoes;
        $recolherOpcoes = ItemViagem::$recolherOpcoes;
        $webOpcoes = ItemViagem::$webOpcoes;
        $dataEmbarque = null;
        $horaEmbarque = null;
       
        $params = compact('viagem', 'itensHorario', 'itensHorarioTransbordo', 'ativoOpcoes', 'recolherOpcoes', 'webOpcoes', 
                          'dataEmbarque', 'horaEmbarque');
       
        return View::make('Cadastro.itensViagem.create', $params);
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store($viagemId) {
        $input = Input::all();
        
        $input['DataHoraEmbarque'] = trim($input['DataEmbarque']) . ' ' . trim($input['HoraEmbarque']);
        
        unset($input['DataEmbarque']);
        unset($input['HoraEmbarque']);
        
        $empresaAtualId = HelperEmpresa::atual()->Id;
        	
        $viagem = Viagem::daEmpresa($empresaAtualId)->findOrFail($viagemId);
        $input['Viagem_Id'] = $viagemId;
	
        $horarioItem_Id = $input['HorarioItem_Id'];
        $itemHorario = ItemHorario::daEmpresa($empresaAtualId)->findOrFail($horarioItem_Id);
        
        $horarioItemIdTransbordo = $input['HorarioItem_Id_Transbordo'];
        if (! $viagem->horario->itens->contains($horarioItemIdTransbordo)) {
            unset($input['HorarioItem_Id_Transbordo']);
        }

        $validation = Validator::make($input, ItemViagem::$rules);

        if ($validation->passes()) {
            $itemViagem = ItemViagem::create($input);
   
            return Redirect::route('cadastro.viagens.itensViagem.show', [$viagemId, $itemViagem->Id]);
        }

        return Redirect::route('cadastro.viagens.itensViagem.create', $viagemId)
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($viagemId, $itemViagemId) {
        $empresaAtualId = HelperEmpresa::atual()->Id;
       
        $viagem = Viagem::daEmpresa($empresaAtualId)->findOrFail($viagemId);
	    $itemViagem = $this->itemViagem->findOrFail($itemViagemId);

        return View::make('Cadastro.itensViagem.show', compact('viagem', 'itemViagem'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($viagemId, $itemViagemId) {
        $itemViagem = $this->itemViagem->find($itemViagemId);

        if (is_null($itemViagem)) {
	        return Redirect::route('cadastro.itemViagens.index');
        }
        
	$empresaAtualId = HelperEmpresa::atual()->Id;
        	
        $viagem = Viagem::daEmpresa($empresaAtualId)->findOrFail($viagemId);
        
        $itensHorarioLista = $viagem->horario->itens;
	
	$itensHorario = [];
	foreach($itensHorarioLista as $item) {
	    $itensHorario[$item->Id] = $item->descricaoDaLinhaComHorario();   
	}

        // Busca itens da linha para seleção da localidade de transbordo
        $itensHorarioTransbordo = ['' => ''];
        foreach($viagem->horario->linha->itens as $item) {
            $itensHorarioTransbordo[$item->ID] = $item->pontoReferencia->descricaoCompleta();
        }

        $itemHorarioTransbordoId = null;
        
        $ativoOpcoes = ItemViagem::$ativoOpcoes;
        $recolherOpcoes = ItemViagem::$recolherOpcoes;
        $webOpcoes = ItemViagem::$webOpcoes;
        
        $dataEmbarque = $itemViagem->DataEmbarque;
        $horaEmbarque = $itemViagem->HoraEmbarque;
       
        $params = compact('viagem', 'itemViagem', 'itensHorario', 'itensHorarioTransbordo', 'ativoOpcoes', 'recolherOpcoes', 'webOpcoes', 'dataEmbarque', 'horaEmbarque');
       
        return View::make('Cadastro.itensViagem.edit', $params);
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($viagemId, $itemViagemId) {
        $input = array_except(Input::all(), '_method');
	
        $empresaAtualId = HelperEmpresa::atual()->Id;
        
        $input['DataHoraEmbarque'] = trim($input['DataEmbarque']) . ' ' . trim($input['HoraEmbarque']);
        
        unset($input['DataEmbarque']);
        unset($input['HoraEmbarque']);
        	
	$viagem = Viagem::daEmpresa($empresaAtualId)->findOrFail($viagemId);
	$input['Viagem_Id'] = $viagemId;
	
        $horarioItemIdTransbordo = $input['HorarioItem_Id_Transbordo'] === "" ? null: $input['HorarioItem_Id_Transbordo'];
        
        if (! $viagem->horario->linha->itens->contains($horarioItemIdTransbordo) and $horarioItemIdTransbordo !== null) {
            unset($input['HorarioItem_Id_Transbordo']);
        }
        
        $input['HorarioItem_Id_Transbordo'] = $horarioItemIdTransbordo;
        
        //$horarioItemIdTransbordo = ItemHorario::daEmpresa($empresaAtualId)->findOrFail($horarioItemIdTransbordo);

        $horarioItem_Id = $input['HorarioItem_Id'];
        $itemHorario = ItemHorario::daEmpresa($empresaAtualId)->findOrFail($horarioItem_Id);

        $validation = Validator::make($input, ItemViagem::$rules);

        if ($validation->passes()) {
            $itemViagem = $this->itemViagem->find($itemViagemId);
            $itemViagem->update($input);

            return Redirect::route('cadastro.viagens.itensViagem.show', [$viagemId, $itemViagemId]);
        }

        return Redirect::route('cadastro.viagens.itensViagem.edit', [$viagemId, $itemViagemId])
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($viagemId, $itemViagemId) {
        try {
            $this->itemViagem->find($itemViagemId)->delete();
            return Redirect::route('cadastro.itensViagem.index', $viagemId);
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }    
    }

}