<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\PontoReferencia;
use Artaban\Model\Pais;
use Artaban\Model\Estado;
use Artaban\Model\Localidade;
use Artaban\Helper\Empresa as HelperEmpresa;

use BaseController;
use Input;
use View;
use Validator;
use Redirect;
use Lang;
use Exception;


class PontosReferenciaController extends BaseController {

    /**
      * PontoReferencia Repository
      *
      * @var PontoReferencia
      */
    protected $pontoReferencia;


    public function __construct(PontoReferencia $pontoReferencia) {
        $empresaAtual = HelperEmpresa::atual();
        $this->pontoReferencia = $pontoReferencia->daEmpresa($empresaAtual->Id);
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $paisId = Input::get('Pais_Id', '');
        $estadoId = Input::get('Estado_Id', '');
	
	    if ($estadoId != "") {
            $this->pontoReferencia = $this->pontoReferencia->noEstado($estadoId); 
	    } elseif ($paisId != "") {
            $this->pontoReferencia = $this->pontoReferencia->noPais($paisId); 
	    } 
		
	    $fields = array(
            'Descricao' => array('like', Input::get('Descricao')),  
            'Tipo' => array('=', Input::get('Tipo')),  
            'Localidade_Id' => array('=', Input::get('Localidade_Id'))
        );
	
        $pontosReferencia = Search::execute($this->pontoReferencia, $fields, ['order' => 'Descricao']);
        
	    $tipos = PontoReferencia::$tipos;
	
	    $paises = ['' => ''] + Pais::orderBy('Nome')->lists('Nome', 'Id');
	
	    return View::make('Cadastro.pontosReferencia.index', compact('pontosReferencia', 'tipos', 'paises'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
        $paises = ['' => ''] + Pais::orderBy('Nome')->lists('Nome', 'Id');
        $estados = [];
	    $localidades = [];
        $tipos = PontoReferencia::$tipos;
        
        $paisSelecionadoId = null;
	    $estadoSelecionadoId = null;
	    
        $params = compact('paises', 'estados', 'localidades', 'tipos', 'paisSelecionadoId', 'estadoSelecionadoId');
		
        return View::make('Cadastro.pontosReferencia.create', $params);
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = array_except(Input::all(), ['Pais_Id', 'Estado_Id']);
	
	    $input['Pessoa_Id'] = HelperEmpresa::atual()->Id;
		
        $validation = Validator::make($input, PontoReferencia::$rules);

        if ($validation->passes()) {
            $pontoReferencia = PontoReferencia::create($input);
   
            return Redirect::route('cadastro.pontosReferencia.show', $pontoReferencia->Id);
        }

        return Redirect::route('cadastro.pontosReferencia.create')
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $pontoReferencia = $this->pontoReferencia
	                            ->with('localidade', 'localidade.estado', 'localidade.estado.pais')
				                ->findOrFail($id);
				
        $localidade = $pontoReferencia->localidade;
  	    $estado = $localidade->estado;
        $pais = $estado->pais;

        return View::make('Cadastro.pontosReferencia.show', compact('pontoReferencia', 'localidade', 'estado', 'pais'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $pontoReferencia = $this->pontoReferencia
	                            ->with('localidade', 'localidade.estado') 
	                            ->find($id);

        if (is_null($pontoReferencia)) {
	        return Redirect::route('cadastro.pontosReferencia.index');
        }
        
        $paisSelecionadoId   = $pontoReferencia->localidade->estado->Pais_Id;
	    $estadoSelecionadoId = $pontoReferencia->localidade->estado->Id;
        
        $paises      = Pais::orderBy('Nome')->lists('Nome', 'Id');
        $estados     = Estado::where('Pais_Id', '=', $paisSelecionadoId)->lists('Nome', 'Id');
	    $localidades = Localidade::where('UF_Id', '=', $estadoSelecionadoId)->lists('Nome', 'Id');
	    $tipos       = PontoReferencia::$tipos;
        
        $params = compact('pontoReferencia', 'paises', 'estados', 'localidades', 'tipos', 'paisSelecionadoId', 'estadoSelecionadoId');
        
        return View::make('Cadastro.pontosReferencia.edit', $params);
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), ['Pais_Id', 'Estado_Id', '_method']);
        
	    $input['Pessoa_Id'] = HelperEmpresa::atual()->Id;
	
	    $validation = Validator::make($input, PontoReferencia::$rules);

        if ($validation->passes()) {
            $pontoReferencia = $this->pontoReferencia->find($id);
            $pontoReferencia->update($input);

            return Redirect::route('cadastro.pontosReferencia.show', $id);
        }

        return Redirect::route('cadastro.pontosReferencia.edit', $id)
                       ->withInput()
                       ->withErrors($validation)
                       ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        try {
            $this->pontoReferencia->find($id)->delete();
            return Redirect::route('cadastro.pontosReferencia.index');    
        } catch (Exception $e) {
            return View::make('500', ['mensagem' => Lang::get('validation.record.dont_delete')]); 
        }
    }

}