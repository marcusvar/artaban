<?php
use Artaban\Model\Usuario;
use Artaban\Model\Modulo;


class AutorizacaoController extends BaseController {

	/**
	 * Usuario Repository
	 *
	 * @var Usuario
	 */
	protected $usuario;
    

	public function __construct(Usuario $usuario) {
		$this->usuario = $usuario;
	}

    
    public function loginForm() {
		return View::make('login'); 
	}
    
	
	public function login() {
		$email    = Input::get('email');
        $senha    = Input::get('senha');
        $redirect = Input::get('redirect');
        
        return Autorizacao::login($email, $senha, $redirect);
	}
    
    
    public function logout() {
		return Autorizacao::logout();
	}
        
    
    public function menu() {
        return View::make('menu'); 
    }
    
    
    public function modulos() {
        $usuario = Autorizacao::usuarioAutenticado();
		if ($usuario->super_usuario) {
           $modulos = Modulo::all();
        } else {
           $modulos = $usuario->getModulos();    
        }
        
        return View::make('modulos', compact('modulos'));
	}
    
}