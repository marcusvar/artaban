<?php
namespace Gerenciamento;

use Artaban\Helper\Empresa as HelperEmpresa;
use Artaban\Model\Horario;
use Artaban\Service\Gerenciamento\Viagem as Service_Gerenciamento_Viagem;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;
use \Exception;


class ViagensController extends BaseController {

    
    public function formGerar() {
        $horariosLista = Horario::daEmpresa(HelperEmpresa::atual()->Id)->get();
        
        $horarios = [];
        foreach($horariosLista as $horario) {
            $horarios[$horario->Id] = $horario->descricaoDaLinhaComHorario();
        }
	    
        return View::make('Gerenciamento.viagens.gerar', compact('horarios'));    
    }
    
    
    public function gerar() {
        try {
            $horarioId  = Input::get('Horario_Id');
            $dataInicio = Input::get('Data_Inicio');
            $dataFim    = Input::get('Data_Fim');
        
            Service_Gerenciamento_Viagem::gerar($horarioId, $dataInicio, $dataFim);
            
            return Redirect::route('gerenciamento.formGerarViagens')
                           ->withInput()
                           ->with('mensagem_ok', 'Viagens geradas com sucesso');
        
        } catch (Exception $e) {
            return Redirect::route('gerenciamento.formGerarViagens')
                           ->withInput()
                           ->with('mensagem_erro', $e->getMessage());
        }
    }

}