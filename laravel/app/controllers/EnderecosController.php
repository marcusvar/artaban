<?php
namespace Cadastro;

use Artaban\Search\Search;
use Artaban\Model\Endereco;

use \BaseController;
use \Input;
use \View;
use \Validator;
use \Redirect;
use \Lang;


class EnderecosController extends BaseController {

    /**
      * Endereco Repository
      *
      * @var Endereco
      */
    protected $endereco;


    public function __construct(Endereco $endereco) {
       $this->endereco = $endereco;
    }


    /**
      * Display a listing of the resource.
      *
      * @return Response
      */
    public function index() {
        $fields = array(
			 'Logradouro' => array('like', Input::get('Logradouro')),  
			 'Numero' => array('like', Input::get('Numero')),  
			 'Bairro' => array('like', Input::get('Bairro')),  
			 'Complemento' => array('like', Input::get('Complemento')),  
			 'Cep' => array('like', Input::get('Cep')),  
			 'Localidade_Id' => array('=', Input::get('Localidade_Id')),  
			 'Pessoa_Id' => array('=', Input::get('Pessoa_Id')),  

        ); 

        $enderecos = Search::execute($this->endereco, $fields);
        return View::make('Cadastro.enderecos.index', compact('enderecos'));
    }

    
    /**
      * Show the form for creating a new resource.
      *
      * @return Response
      */
    public function create() {
       return View::make('Cadastro.enderecos.create');
    }


    /**
      * Store a newly created resource in storage.
      *
      * @return Response
      */
    public function store() {
        $input = Input::all();
        $validation = Validator::make($input, Endereco::$rules);

        if ($validation->passes()) {
            $endereco = $this->endereco->create($input);
   
            return Redirect::route('cadastro.enderecos.show', $endereco->Id);
        }

        return Redirect::route('cadastro.enderecos.create')
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }

    
    /**
      * Display the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function show($id) {
        $endereco = $this->endereco->findOrFail($id);

        return View::make('Cadastro.enderecos.show', compact('endereco'));
    }

    
    /**
      * Show the form for editing the specified resource.
      *
      * @param  int  $id
      * @return Response
      */
    public function edit($id) {
        $endereco = $this->endereco->find($id);

        if (is_null($endereco)) {
	    return Redirect::route('cadastro.enderecos.index');
        }

        return View::make('Cadastro.enderecos.edit', compact('endereco'));
    }

    
    /**
      * Update the specified resource in storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function update($id) {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Endereco::$rules);

        if ($validation->passes()) {
            $endereco = $this->endereco->find($id);
            $endereco->update($input);

            return Redirect::route('cadastro.enderecos.show', $id);
        }

        return Redirect::route('cadastro.enderecos.edit', $id)
               ->withInput()
               ->withErrors($validation)
               ->with('message', Lang::get('validation.form.invalid'));
    }


    /**
      * Remove the specified resource from storage.
      *
      * @param  int  $id
      * @return Response
      */
    public function destroy($id) {
        $this->endereco->find($id)->delete();

        return Redirect::route('cadastro.enderecos.index');
    }

}