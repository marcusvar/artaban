@extends('layouts.app')

@section('descricaoTipoMenu')
  Gerenciamento
@stop


@section('descricaoMenu')
  Gerar viagem
@stop


@section('submenu')
  <a href="/gerenciamento/gerarViagens" class="btn btn-success active">
    <span class="glyphicon glyphicon-road"></span> Gerar viagem
  </a>
@stop


@section('principal')
  
     {{ Form::open(array('method' => 'POST', 'route' => 'gerenciamento.gerarViagens')) }}
     
     <div class="row">
       <div class="col-xs-8">
         <div class="form-group">
           <label for="Horario_Id">Horário:</label>
           {{ Form::select('Horario_Id',
                           $horarios,
                           null,
                           array(
                               'class' => 'form-control chosen-select',
                               'data-placeholder' => 'Selecione um horário',
                               'required', 
                           )
              )
           }}
         </div>
       </div>
       
       <div class="col-xs-2">
         <div class="form-group">
           <label for="Data_Inicio">Data inicial:</label>
           {{ Form::text('Data_Inicio', null, array('class' => 'form-control datepicker', 'required')) }}
         </div>
       </div>
	
       <div class="col-xs-2">
         <div class="form-group">
         <label for="Data_Fim">Data final:</label>
         {{ Form::text('Data_Fim', null, array('class' => 'form-control datepicker', 'required')) }}
         </div>
       </div>
     </div>
      
     {{ Form::submit('Gerar', array('class' => 'btn btn-default botao-padrao')) }}                  
   {{ Form::close() }}
   
   @if (Session::has('mensagem_erro'))
     <div class="alert alert-warning" style="margin-top: 20px">
       <p>{{ Session::get('mensagem_erro') }}</p>
     </div>
   @endif
   
   @if (Session::has('mensagem_ok'))
     <div class="alert alert-success" style="margin-top: 20px">
       <p>As viagens foram geradas com sucesso</p>
       <a href="/cadastro/viagens" class="btn btn-sm btn-default" style="margin-top: 10px">
         <b>Ir para o cadastro de viagens</b>
       </a> 
     </div>
   @endif
  
@stop