<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
	<title>Artaban</title>
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" >
    <link href="/chosen/chosen.css" rel="stylesheet" >
    <link href="/css/app.css" rel="stylesheet" media="screen">    
  </head>
  <body>
     <div id="wrap" class="background-login">
       <div id="main-container">
         @yield('principal')
       </div>
    </div>

    <div id="footer">
      <div class="container" style="text-align: center">
        <p class="text-muted">&copy; Artaban - Todos os direitos reservados
        @if (App::environment() !== 'production')
          <span style="color: red"><b>({{ ucwords(App::environment()) }})</b></span>
        @endif
        </p>
      </div>
    </div>
    
    <script src="/bootstrap/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/chosen/chosen.jquery.js"></script>
    <script src="/js/app.js"></script>
  </body>
</html>