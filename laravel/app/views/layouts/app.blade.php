@extends('layouts.base')

@section('conteudo')
  <h1 class="pagina-atual">
    @yield('descricaoTipoMenu')
	        
    <span style="font-size: 18px;">
	  @yield('descricaoMenu')
	</span>      
  </h1>
	  
  <div id="submenu" class="btn-group">
    @yield('submenu') 
  </div>
          
  <div id="conteudo-detalhes">
    @yield('principal')
  </div>  
@stop