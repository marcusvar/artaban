<!doctype html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <title>Artaban</title>
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" media="screen">
    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet" >
    <link href="/chosen/chosen.css" rel="stylesheet" >
    <link href="/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet">
    <link href="/datepicker/css/datepicker.css" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet" media="screen">    
  </head>
  <body>
    <div id="wrap">
      
      <div id="main-container">
        <div id="topo">
          <a href="/">{{ \Artaban\Helper\View\Img::logoEmpresaAtual() }}</a>
          
          <div class="user btn-group pull-right">
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">
	          <i class="icon-user"></i> {{ Auth::user()->Nome }}
	        </a>
            
            <a class="btn btn-default dropdown-toggle" data-toggle="dropdown" href="#">
              <span class="icon-caret-down"></span>
	        </a>
            
            <ul class="dropdown-menu">
              <li><a href="#"><i class="icon-fixed-width icon-pencil"></i> Editar meus dados</a></li>
              <li class="divider"></li>
              <li><a href="/logout"><i class="icon-fixed-width icon-signout"></i> Sair</a></li>
            </ul>
          </div>	  
        </div>
        
        <div id="menu">
          <div class="panel-group" id="accordion">
                        
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-parent="#accordion" href="/">
                    <i class="icon-fixed-width icon-home"></i> Página inicial
                  </a>
                </h4>
              </div>
            </div>
                        
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseCadastro">
                    <i class="icon-fixed-width icon-book"></i> Cadastro
                  </a>
                </h4>
              </div>
              
              <div id="collapseCadastro" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-pills nav-stacked">
                    <li><a href="/cadastro/pessoas">Pessoa</a></li>		      
		            <li><a href="/cadastro/tiposVeiculo">Tipo de veículo</a></li>
		            <li><a href="/cadastro/veiculos">Veículo</a></li>
		            <li><a href="/cadastro/pontosReferencia">Ponto de referência</a></li>
		            <li><a href="/cadastro/linhas">Linha</a></li>
		            <li><a href="/cadastro/horarios">Horário</a></li>
		            <li><a href="/cadastro/viagens">Viagem</a></li>
		            <li><a href="/cadastro/tiposAdicionaisTarifas">Tipo de adicional para tarifa</a></li>
                    <li><a href="/cadastro/descontosGerais">Desconto</a></li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseGerenciamento">
                    <i class="icon-fixed-width icon-gears"></i> Gerenciamento
                  </a>
                </h4>
              </div>
              
              <div id="collapseGerenciamento" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-pills nav-stacked">
		            <li><a href="/ext/reservarPoltrona">Reservar poltrona</a></li>
                    <li><a href="/gerenciamento/gerarViagens">Gerar viagem</a></li>
                  </ul> 
                </div>
              </div>
            </div>
            
            <!--div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseRelatorio">
                    <i class="icon-fixed-width icon-bar-chart"></i> Relatório
                  </a>
                </h4>
              </div>
              
              <div id="collapseRelatorio" class="panel-collapse collapse">
                <div class="panel-body">
                  <ul class="nav nav-pills nav-stacked">
		            <li><a href="/ext/listas">Listas e Extratos</a></li>
                    
                  </ul>
                </div>
              </div>
            </div-->
          </div>
        </div>

        <div id="conteudo">
          @yield('conteudo')            
        </div>
      </div>
    </div>

    <div id="footer">
      <div class="container" style="text-align: center">
        <p class="text-muted">&copy; Artaban - Todos os direitos reservados
        @if (App::environment() !== 'production')
          <span style="color: red"><b>({{ ucwords(App::environment()) }})</b></span>
        @endif
        </p>
      </div>
    </div>
    
    <script src="/bootstrap/js/jquery.js"></script>
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <script src="/chosen/chosen.jquery.js"></script>
    <script src="/bootstrap3-editable/js/bootstrap-editable.js"></script>
    <script src="/inputmask/js/jquery.inputmask.js"></script>
    <script src="/datepicker/js/bootstrap-datepicker.js"></script>
    <script src="/datepicker/js/bootstrap-datepicker.pt-BR.js"></script>
    <script src="/jquery-maskmoney/jquery.maskMoney.js"></script>
    <script src="/js/app.js"></script>
  </body>
</html>