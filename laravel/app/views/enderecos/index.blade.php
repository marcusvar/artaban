@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  enderecos
@stop


@section('submenu')
  <a href="{{ route('cadastro.enderecos.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.enderecos.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($enderecos->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.enderecos.index')) }}
     
         <div class="form-group">
    <label for="Logradouro">Logradouro:</label>
    {{ Form::text('Logradouro', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Numero">Numero:</label>
    {{ Form::text('Numero', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Bairro">Bairro:</label>
    {{ Form::text('Bairro', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Complemento">Complemento:</label>
    {{ Form::text('Complemento', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Cep">Cep:</label>
    {{ Form::text('Cep', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Localidade_Id">Localidade:</label>
    {{ Form::select('Localidade_Id',  array('' => '') + Localidade::lists('Nome', 'Id'), null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Pessoa_Id">Pessoa:</label>
    {{ Form::select('Pessoa_Id',  array('' => '') + Pessoa::lists('Nome', 'Id'), null, array('class' => 'form-control')) }}
  </div>
  
         
       {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Logradouro</th>
				<th>Numero</th>
				<th>Bairro</th>
				<th>Complemento</th>
				<th>Cep</th>
				<th>Localidade</th>
				<th>Pessoa</th>
				<th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($enderecos as $endereco)
            <tr>
	        <td>{{{ $endereco->Logradouro }}}</td>
					<td>{{{ $endereco->Numero }}}</td>
					<td>{{{ $endereco->Bairro }}}</td>
					<td>{{{ $endereco->Complemento }}}</td>
					<td>{{{ $endereco->Cep }}}</td>
					<td>{{{ $endereco->localidade->nome }}}</td>
					<td>{{{ $endereco->pessoa->nome }}}</td>
                    <td>{{ link_to_route('cadastro.enderecos.show',
                                         'Detalhes',
                                         array($endereco->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
                        {{ link_to_route('cadastro.enderecos.edit',
                                         'Editar',
                                         array($endereco->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
                        <div class="pull-left">
                          {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.enderecos.destroy', $endereco->Id))) }}
                            <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                          {{ Form::close() }}
                        </div>
                    </td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $enderecos->appends(Input::except('page'))->links(); ?>
@else
    Não há enderecos
@endif

@stop