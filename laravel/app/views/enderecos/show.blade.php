@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do Endereco
@stop


@section('submenu')
  <a href="{{ route('cadastro.enderecos.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.enderecos.create') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.enderecos.show', array($endereco->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.enderecos.edit', array($endereco->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.enderecos.destroy', $endereco->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <h2 class="detalhes">Logradouro</h2>
<p class="detalhes">{{{ $endereco->Logradouro }}}</p>

<h2 class="detalhes">Numero</h2>
<p class="detalhes">{{{ $endereco->Numero }}}</p>

<h2 class="detalhes">Bairro</h2>
<p class="detalhes">{{{ $endereco->Bairro }}}</p>

<h2 class="detalhes">Complemento</h2>
<p class="detalhes">{{{ $endereco->Complemento }}}</p>

<h2 class="detalhes">Cep</h2>
<p class="detalhes">{{{ $endereco->Cep }}}</p>

<h2 class="detalhes">Localidade</h2>
<p class="detalhes">{{{ $endereco->Localidade->nome }}}</p>

<h2 class="detalhes">Pessoa</h2>
<p class="detalhes">{{{ $endereco->Pessoa->nome }}}</p>


@stop