@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar endereco
@stop


@section('submenu')
  <a href="{{ route('cadastro.enderecos.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.enderecos.show', array($endereco->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.enderecos.edit', array($endereco->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($endereco, array('method' => 'PATCH', 'route' => array('cadastro.enderecos.update', $endereco->Id))) }}
    @include('Cadastro.enderecos._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop