  <div class="form-group">
    <label for="Logradouro">Logradouro:</label>
    {{ Form::text('Logradouro', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Numero">Numero:</label>
    {{ Form::text('Numero', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Bairro">Bairro:</label>
    {{ Form::text('Bairro', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Complemento">Complemento:</label>
    {{ Form::text('Complemento', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Cep">Cep:</label>
    {{ Form::text('Cep', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Localidade_Id">Localidade:</label>
    {{ Form::select('Localidade_Id',  Localidade::lists('Nome', 'Id'), null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Pessoa_Id">Pessoa:</label>
    {{ Form::select('Pessoa_Id',  Pessoa::lists('Nome', 'Id'), null, array('class' => 'form-control')) }}
  </div>
  