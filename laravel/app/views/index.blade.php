@extends('layouts.base')

@section('conteudo')
  <h1 class="pagina-atual">
    Bem vindo
	        
    <span style="font-size: 20px;">
      {{{ Auth::user()->Nome }}}
    </span>   
  </h1>
    
  <div class="row">
    <div class="col-xs-4">
     <div class="alert alert-warning">
        <div class="media">
          <i class="icon-fixed-width icon-calendar pull-left" style="font-size: 48px; text-align: center"></i>
     
          <div class="media-body">
            <a href="/ext/reservarPoltrona" style="display: block; font-size: 18px; margin-top: 12px">Reserva de poltronas</a>
          </div>
        </div>      
      </div>        
    </div>
        
    <div class="col-xs-4">
     <div class="alert alert-warning">
        <div class="media">
          <i class="icon-fixed-width icon-road pull-left" style="font-size: 48px; text-align: center"></i>
     
          <div class="media-body">
            <a href="/gerenciamento/gerarViagens" style="display: block; font-size: 18px; margin-top: 12px">Geração de viagens</a>
          </div>
        </div>      
      </div>        
    </div>
        
    <div class="col-xs-4">
     <!--div class="alert alert-warning">
        <div class="media">
          <i class="icon-fixed-width icon-group pull-left" style="font-size: 48px; text-align: center"></i>
     
          <div class="media-body">
            <a href="/ext/listas" style="display: block; font-size: 18px; margin-top: 12px">Listas e Extratos</a>
          </div>
        </div>      
      </div-->        
    </div>    
  </div>
@stop