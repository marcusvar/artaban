@extends('layouts.base')

@section('main')
<ul class="list-group">
  @foreach ($modulos as $modulo)
    <a href="http://{{ $modulo->nome }}{{ Config::get('eucatur.app.domain') }}" class="list-group-item">
      <span class="badge">Acessar >></span><b>{{ $modulo->nome }}</b>       
    </a> 
  @endforeach
</ul>
@stop