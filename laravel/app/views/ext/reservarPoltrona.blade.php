@extends('layouts.app')

@section('descricaoTipoMenu')
  Gerenciamento
@stop


@section('descricaoMenu')
  Reservar poltrona
@stop


@section('submenu')
  <a href="/ext/reservarPoltrona" class="btn btn-success active">
    <span class="glyphicon glyphicon-shopping-cart"></span> Reservar poltrona
  </a>
@stop


@section('principal')
  <iframe src="/ext/index.html" width="810px" height="640px" frameborder="0"></iframe>
@stop