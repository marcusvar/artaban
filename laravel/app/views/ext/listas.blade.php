@extends('layouts.app')

@section('descricaoTipoMenu')
  Relatório
@stop


@section('descricaoMenu')
  Listas e Extratos
@stop


@section('submenu')
  <a href="/ext/listas" class="btn btn-success active">
    <span class="glyphicon glyphicon-shopping-cart"></span> Listas e Extratos
  </a>
@stop


@section('principal')
  <iframe src="/ext/relatorios/index.html" width="810px" height="640px" frameborder="0"></iframe>
@stop