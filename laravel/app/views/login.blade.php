@extends('layouts.login')

@section('principal')

<div id="login">
  <img class="logo" src="img/logo-1.png" width="200px" height="56px"/>
  
  <form class="form-signin" method="POST" action="/login">
    
    @if (Session::has('erro_login'))
      <div class="alert alert-warning">{{ Session::get('erro_login') }}</div>    
    @endif
    
    <div class="form-group">
      <label for="email">Email:</label>
      <input id="email" name="email" type="email" class="form-control" placeholder="Digite seu email" required autofocus>
    </div>

    <div class="form-group">
      <label for="senha">Senha:</label>
      <input id="senha" name="senha" type="password" class="form-control" placeholder="Digite sua senha" required>
    </div>

    <a href="#" class="recuperar-senha">Esqueci minha senha</a>

    <button class="btn btn-lg btn-success btn-block" type="submit">Entrar</button>
  </form>

</div>
  
@stop