@extends('layouts.scaffold')

@section('main')

<h1>Adicionar Grupo</h1>

@include('shared._error_message')

<div class="submenu-nav">
  <ul class="nav nav-tabs">
    <li>
      <a href="{{ route('grupos.index') }}"><span class="glyphicon glyphicon-home"></span> Principal</a>
    </li>
      
    <li class="active">
      <a href="{{ route('grupos.create') }}"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar</a>
    </li>      
  </ul>
</div>

{{ Form::open(array('route' => 'grupos.store')) }}
  @include('grupos._form')
  
  {{ Form::submit('Adicionar', array('class' => 'btn btn-primary')) }}
{{ Form::close() }}

@stop