@extends('layouts.scaffold')

@section('main')

<h1>Grupo</h1>

<div class="submenu-nav">
  <ul class="nav nav-tabs">
    <li>
      <a href="{{ route('grupos.index') }}"><span class="glyphicon glyphicon-home"></span> Principal</a>
    </li>
    
    <li>
      <a href="{{ route('grupos.create') }}"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar</a>
    </li>  
      
    <li class="active">
      <a href="{{ route('grupos.show', array($grupo->id)) }}"><span class="glyphicon glyphicon-list"></span> Detalhes</a>
    </li>
      
    <li>
      <a href="{{ route('grupos.edit', array($grupo->id)) }}"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
    </li>
    
    <li>
      {{ Form::open(array('method' => 'DELETE', 'route' => array('grupos.destroy', $grupo->id))) }}
      <a href="#" class="submit-form-delete label label-danger" style="display: block; color: #FFF; font-weight: normal; font-size: 14px; margin-left: 0px; margin-top: 8px">
        <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
      {{ Form::close() }}
    </li>
  </ul>
</div>

<h3 class="detalhes">Nome</h3>
<p>{{{ $grupo->nome }}}</p>

<h3 class="detalhes">Modulo</h3>
<p>{{{ $grupo->modulo->nome }}}</p>



@stop
