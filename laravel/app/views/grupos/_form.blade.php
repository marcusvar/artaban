  <div class="form-group">
    <label for="nome">Nome:</label>
    {{ Form::text('nome', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="modulo_id">Modulo:</label>
    {{ Form::select('modulo_id',  $modulos, null, array('class' => 'form-control')) }}
  </div>
  