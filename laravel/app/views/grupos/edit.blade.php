@extends('layouts.scaffold')

@section('main')

<h1>Editar Grupo</h1>

@include('shared._error_message')

<div class="submenu-nav">
  <ul class="nav nav-tabs">
    <li>
      <a href="{{ route('grupos.index') }}"><span class="glyphicon glyphicon-home"></span> Principal</a>
    </li>
      
    <li>
      <a href="{{ route('grupos.show', array($grupo->id)) }}"><span class="glyphicon glyphicon-list"></span> Detalhes</a>
    </li>
      
    <li class="active"> 
      <a href="{{ route('grupos.edit', array($grupo->id)) }}"><span class="glyphicon glyphicon-pencil"></span> Editar</a>
    </li>
  </ul>
</div>

{{ Form::model($grupo, array('method' => 'PATCH', 'route' => array('grupos.update', $grupo->id))) }}
  @include('grupos._form')
  
  {{ Form::submit('Editar', array('class' => 'btn btn-primary')) }}
  {{ link_to_route('grupos.show', 'Cancelar', $grupo->id, array('class' => 'btn btn-default')) }}
{{ Form::close() }}

@stop