@extends('layouts.scaffold')

@section('main')

<h1>Grupos</h1>


<div class="submenu-nav">
  <ul class="nav nav-tabs">
    <li class="active">
      <a href="{{ route('grupos.index') }}"><span class="glyphicon glyphicon-home"></span> Principal</a>
    </li>
      
    <li>
      <a href="{{ route('grupos.create') }}"><span class="glyphicon glyphicon-plus-sign"></span> Adicionar</a>
    </li>      
  </ul>
</div>

@if ($grupos->count())
    <a href="#" class="search-button"><span class="glyphicon glyphicon-search"></span> Filtrar resultados</a>
    
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET', 'route' => 'grupos.index')) }}
     
         <div class="form-group">
    <label for="nome">Nome:</label>
    {{ Form::text('nome', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="modulo_id">Modulo:</label>
    {{ Form::select('modulo_id',  array('' => '') + $modulos, null, array('class' => 'form-control')) }}
  </div>
  
         
       {{ Form::submit('Filtrar', array('class' => 'btn btn-info btn-sm')) }}                  
     {{ Form::close() }}
    </div>
    
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Nome</th>
				<th>Modulo</th>
				<th>Ações</th>
			</tr>
		</thead>

		<tbody>
			@foreach ($grupos as $grupo)
				<tr>
					<td>{{{ $grupo->nome }}}</td>
					<td>{{{ $grupo->modulo->nome }}}</td>
                    <td>{{ link_to_route('grupos.show', 'Detalhes', array($grupo->id), array('style' => 'margin-right: 10px', 'class' => 'pull-left btn btn-primary btn-sm')) }}
                                        
                        {{ link_to_route('grupos.edit', 'Editar', array($grupo->id), array('style' => 'margin-right: 10px', 'class' => 'pull-left btn btn-primary btn-sm')) }}
                    
                        <div class="pull-left">
                          {{ Form::open(array('method' => 'DELETE', 'route' => array('grupos.destroy', $grupo->id))) }}
                            <a href="#" class="submit-form-delete btn btn-danger btn-sm">Excluir</a>   
                          {{ Form::close() }}
                        </div>
                        
                        {{ link_to_route('permissoes.grupo', 'Permissões', array($grupo->id), array('style' => 'margin-left: 10px', 'class' => 'pull-left btn btn-primary btn-sm')) }}
                        
                    </td>
				</tr>
			@endforeach
		</tbody>
	</table>
    
    <?php echo $grupos->appends(Input::except('page'))->links(); ?>
@else
	Não há grupos
@endif

@stop
