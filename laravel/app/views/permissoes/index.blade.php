@extends('layouts.scaffold')

@section('main')

<h1 class="pagina-x">Permissões de {{ $usuario->nome }}</h1>

<div style="margin: 20px 0">
  <ul class="nav nav-pills">
    <li class="active"><a href="#">Comum</a></li>
    <li><a href="#">RH</a></li>
  </ul>
</div>



    
<div class="panel panel-info">
  
  <div class="panel-heading">
    <b>Admin</b>
  </div>
     
  <div class="panel-body">
    <p>O usuário <b>{{ $usuario->nome }}</b> está atualmente no grupo <b>Admin</b> no módulo <b>Comum</b></p>
  </div>
  
  
  <ul class="nav nav-tabs">
  <li class="active"><a href="#home" data-toggle="tab">Permissões</a></li>
  <li><a href="#profile" data-toggle="tab">Alterar grupo</a></li>
  </ul>
  
  <div class="tab-content">
  
  <div class="tab-pane active" id="home" style="padding: 1%">
    <ul class="list-group">
    <li class="list-group-item"><b>Cadastro</b></li>
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Dapibus ac facilisis in
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Morbi leo risus
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Porta ac consectetur ac
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Vestibulum at eros
    </li>
    
    <li class="list-group-item"><b>Movimentação</b></li>
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Dapibus ac facilisis in
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Morbi leo risus
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Porta ac consectetur ac
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Vestibulum at eros
    </li>
  </ul>  
  </div>
  
  <div class="tab-pane" id="profile" style="padding: 1%">
    
    <div class="alert alert-success">
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat
      </p>
    </div>
    
    {{ Form::open(array('route' => 'usuarios.store')) }}
    <div class="form-group">
     
      <label for="senha">Selecione um grupo:</label>
      
      <select class="form-control">
        <option value="volvo">Sem acesso</option>
        <option value="saab">Suporte</option>
        <option value="mercedes">Admin</option>
        <option value="audi">Diretoria</option>
      </select>
     </div>
    
       
     {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
     {{ Form::close() }}  
  </div>
  
  <div class="tab-pane" id="messages">...</div>
  
  <div class="tab-pane" id="settings">...</div>
</div>
  
    
</div>


@stop
