@extends('layouts.scaffold')

@section('main')

<h1 class="pagina-x">Permissões do grupo {{ $grupo->nome }}</h1>

<div style="margin: 20px 0">
  <ul class="nav nav-pills">
    <li class="active"><a href="#">Cadastro</a></li>
    <li><a href="#">Movimentação</a></li>
    <li><a href="#">Relatório</a></li>
    <li><a href="#">Gráfico</a></li>
  </ul>
</div>

    
<div class="panel panel-info">
     
  <div class="panel-body">
    <p>Adicione ou exclua as permissões necessárias para esse grupo e clique em salvar.</p>
    
    {{ Form::open(array('route' => 'usuarios.store')) }}
    <div class="form-group">
     
      <label for="senha">Permissões:</label><br>
      
      <select data-placeholder="" class="chosen-select" multiple style="width:350px;" tabindex="4">
        <option value=""></option>
        <option value="1" selected>Grupos</option>
        <option value="2">Módulos</option>
        <option value="3">Usuários</option>
        <option value="4">Vagas</option>
      </select>
     </div>
       
     {{ Form::submit('Salvar', array('class' => 'btn btn-primary')) }}
     {{ Form::close() }}  
    
  </div>
  
  
  
</div>
  
    
</div>


@stop
