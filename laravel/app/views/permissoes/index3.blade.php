@extends('layouts.scaffold')

@section('main')

<h1 class="pagina-x">Permissões de {{ $usuario->nome }}</h1>

<div style="margin: 20px 0">
  <ul class="nav nav-pills">
    <li class="active"><a href="#">Comum</a></li>
    <li><a href="#">RH</a></li>
  </ul>
</div>



    
    <div class="panel panel-success">
     
    <div class="panel-heading">
      <b>Admin</b>
    </div>
     
  <div class="panel-body">
    <p>O usuário <b>{{ $usuario->nome }}</b> está atualmente no grupo <b>Admin</b> no módulo <b>Comum</b> <br />
    Todas as permissões desse grupo estão listadas abaixo.</p>
  </div>

  <!-- List group -->
  <ul class="list-group">
    <li class="list-group-item"><b>Cadastro</b></li>
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Dapibus ac facilisis in
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Morbi leo risus
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Porta ac consectetur ac
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Vestibulum at eros
    </li>
    
    <li class="list-group-item"><b>Movimentação</b></li>
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Dapibus ac facilisis in
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Morbi leo risus
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Porta ac consectetur ac
    </li>
    
    <li class="list-group-item"><span class="glyphicon glyphicon-circle-arrow-right"></span>
      Vestibulum at eros
    </li>
  </ul>
</div>
    
  
  
  
</div>


@stop
