@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do Mapa
@stop


@section('submenu')
  <a href="{{ route('cadastro.mapas.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.mapas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.mapas.show', array($mapa->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.mapas.edit', array($mapa->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.mapas.destroy', $mapa->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    <h2>TipoVeiculo</h2>
<p>{{{ $mapa->TipoVeiculo->nome }}}</p>

<h2>Numero</h2>
<p>{{{ $mapa->Numero }}}</p>

<h2>Linha</h2>
<p>{{{ $mapa->Linha }}}</p>

<h2>Coluna</h2>
<p>{{{ $mapa->Coluna }}}</p>

<h2>Andar</h2>
<p>{{{ $mapa->Andar }}}</p>


  </div>
@stop