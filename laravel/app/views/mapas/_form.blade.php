  <div class="form-group">
    <label for="TipoVeiculo_Id">TipoVeiculo:</label>
    {{ Form::select('TipoVeiculo_Id',  TipoVeiculo::lists('Nome', 'Id'), null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Numero">Numero:</label>
    {{ Form::input('number', 'Numero') }}
  </div>
  
  <div class="form-group">
    <label for="Linha">Linha:</label>
    {{ Form::input('number', 'Linha') }}
  </div>
  
  <div class="form-group">
    <label for="Coluna">Coluna:</label>
    {{ Form::input('number', 'Coluna') }}
  </div>
  
  <div class="form-group">
    <label for="Andar">Andar:</label>
    {{ Form::input('number', 'Andar') }}
  </div>
  