@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  mapas
@stop


@section('submenu')
  <a href="{{ route('cadastro.mapas.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.mapas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($mapas->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.mapas.index')) }}
     
         <div class="form-group">
    <label for="TipoVeiculo_Id">TipoVeiculo:</label>
    {{ Form::select('TipoVeiculo_Id',  array('' => '') + TipoVeiculo::lists('Nome', 'Id'), null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Numero">Numero:</label>
    {{ Form::input('number', 'Numero') }}
  </div>
  
  <div class="form-group">
    <label for="Linha">Linha:</label>
    {{ Form::input('number', 'Linha') }}
  </div>
  
  <div class="form-group">
    <label for="Coluna">Coluna:</label>
    {{ Form::input('number', 'Coluna') }}
  </div>
  
  <div class="form-group">
    <label for="Andar">Andar:</label>
    {{ Form::input('number', 'Andar') }}
  </div>
  
         
       {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>TipoVeiculo</th>
				<th>Numero</th>
				<th>Linha</th>
				<th>Coluna</th>
				<th>Andar</th>
				<th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($mapas as $mapa)
            <tr>
	        <td>{{{ $mapa->tipoVeiculo->nome }}}</td>
					<td>{{{ $mapa->Numero }}}</td>
					<td>{{{ $mapa->Linha }}}</td>
					<td>{{{ $mapa->Coluna }}}</td>
					<td>{{{ $mapa->Andar }}}</td>
                    <td>{{ link_to_route('cadastro.mapas.show',
                                         'Detalhes',
                                         array($mapa->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
                        {{ link_to_route('cadastro.mapas.edit',
                                         'Editar',
                                         array($mapa->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
                        <div class="pull-left">
                          {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.mapas.destroy', $mapa->Id))) }}
                            <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                          {{ Form::close() }}
                        </div>
                    </td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $mapas->appends(Input::except('page'))->links(); ?>
@else
    Não há mapas
@endif

@stop