@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.viagens.show', $viagem->Id) }}">
  <b>{{ $viagem->horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Detalhes desse item da viagem
@stop


@section('submenu')
  <a href="{{ route('cadastro.viagens.itensViagem.index', $viagem->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.viagens.itensViagem.create', $viagem->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.viagens.itensViagem.show', array($viagem->Id, $itemViagem->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.viagens.itensViagem.edit', array($viagem->Id, $itemViagem->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.viagens.itensViagem.destroy', $viagem->Id, $itemViagem->Id),
                      'class' => 'delete-submenu')) }}
    
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    <div class="row">
      <div class="col-xs-8">
        <h3>Item do horário</h3>
        <p>{{{ $itemViagem->itemHorario->descricaoDaLinhaComHorario() }}}</p>
      </div>
      
      <div class="col-xs-8">
        <h3>Ponto de badeação</h3>
        <p>{{{ $itemViagem->descricaoTransbordoViagem() }}}</p>
      </div>
      
      <div class="col-xs-8">
        <h3>Data e hora do embarque</h3>
        <p>{{{ $itemViagem->DataHoraEmbarque }}}</p>
      </div>
        
      <div class="col-xs-3">
        <h3>Ativo?</h3>
        <p>{{{ $itemViagem->ativoDescricao() }}}</p>
      </div>
        
      <div class="col-xs-3">
        <h3>Recolher?</h3>
        <p>{{{ $itemViagem->recolherDescricao() }}}</p>
      </div>  
        
      <div class="col-xs-3">
        <h3>Mostrar na Web?</h3>
        <p>{{{ $itemViagem->webDescricao() }}}</p>
      </div>  

    </div>
  </div>
@stop