@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.viagens.show', $viagem->Id) }}">
  <b>{{ $viagem->horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Adicionar um item nessa viagem
@stop


@section('submenu')
  <a href="{{ route('cadastro.viagens.itensViagem.index', $viagem->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.viagens.itensViagem.create', $viagem->Id) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
  
  {{ Form::open(array('route' => ['cadastro.viagens.itensViagem.store', $viagem->Id])) }}
    @include('Cadastro.itensViagem._form')
  
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop