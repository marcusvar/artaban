<div class="row">
  <div class="col-xs-7">
    <div class="form-group">
      <label for="HorarioItem_Id">Item do horário:</label>
      {{ Form::select('HorarioItem_Id', $itensHorario, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-7">
    <div class="form-group">
      <label for="HorarioItem_Id_Transbordo">Ponto de baldeação:</label>
      {{ Form::select('HorarioItem_Id_Transbordo', $itensHorarioTransbordo, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-5">
    <div class="form-group">
      <label class="pull-left" style="width: 100%">Data e hora do embarque:</label> <br />
      
      {{ Form::text('DataEmbarque',
                    $dataEmbarque,
                    array('class' => 'form-control datepicker pull-left', 'style' => 'max-width: 120px; margin-right: 20px')) }}
      
      {{ Form::text('HoraEmbarque',
                    $horaEmbarque,
                    array('class' => 'form-control horario-mask pull-left', 'style' => 'max-width: 120px')) }}
    </div>
  </div>
    
  <div class="col-xs-2">
    <div class="form-group">
      <label for="Ativo">Ativo?</label>
      {{ Form::select('Ativo', $ativoOpcoes, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>
    
  <div class="col-xs-2">
    <div class="form-group">
      <label for="Recolher">Recolher?</label>
      {{ Form::select('Recolher', $recolherOpcoes, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>  
    
  <div class="col-xs-3">
    <div class="form-group">
      <label for="Web">Mostrar na Web?</label>
      {{ Form::select('Web', $webOpcoes, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>   

</div>
