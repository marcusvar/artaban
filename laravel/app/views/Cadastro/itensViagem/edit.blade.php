@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.viagens.show', $viagem->Id) }}">
  <b>{{ $viagem->horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Editar esse item da viagem
@stop


@section('submenu')
  <a href="{{ route('cadastro.viagens.itensViagem.index', $viagem->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.viagens.itensViagem.show', array($viagem->Id, $itemViagem->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.viagens.itensViagem.edit', array($viagem->Id, $itemViagem->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($itemViagem,
                 array(
                     'method' => 'PATCH',
                     'route'  => array(
                         'cadastro.viagens.itensViagem.update',
                         $viagem->Id,
                         $itemViagem->Id
                     )
                 )
     )
  }}
    
    @include('Cadastro.itensViagem._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop