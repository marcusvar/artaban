@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.viagens.show', $viagem->Id) }}">
  <b>{{ $viagem->horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Itens da viagem
@stop


@section('submenu')
  <a href="{{ route('cadastro.viagens.itensViagem.index', $viagem->Id) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.viagens.itensViagem.create', $viagem->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($itensViagem->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET',
                          'route' => ['cadastro.viagens.itensViagem.index', $viagem->Id])) }}
     
         <div class="row">
          <div class="col-xs-9">
            <div class="form-group">
              <label for="HorarioItem_Id">Item do horário:</label>
              {{ Form::select('HorarioItem_Id',
                              $itensHorario,
                              null,
                              array('class' => 'form-control chosen-select',
                                    'data-placeholder' => 'Filtrar por item do horário')) }}
            </div>
          </div>
            
          <div class="col-xs-3">
            <div class="form-group">
              <label for="Ativo">Ativo:</label>
              {{ Form::select('Ativo',
                              $ativoOpcoes,
                              null,
                              array('class' => 'form-control chosen-select',
                                    'data-placeholder' => 'Filtrar por situação')) }}
            </div>
          </div>  
         </div>
           
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
  
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Item do horário</th>
	      <th>Data e hora do embarque</th>
          <th>Ativo</th>            
	      <th>Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($itensViagem as $itemViagem)
          <tr>
		    <td>{{{ $itemViagem->itemHorario->descricaoDaLinhaComHorario() }}}</td>
		    <td>{{{ $itemViagem->DataHoraEmbarque }}}</td>
            <td>{{{ $itemViagem->ativoDescricao() }}}</td>
                
		    <td>
              {{ link_to_route('cadastro.viagens.itensViagem.show',
                               'Detalhes',
                               array($viagem->Id, $itemViagem->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.viagens.itensViagem.edit',
                               'Editar',
                               array($viagem->Id, $itemViagem->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array(
                       'method' => 'DELETE',
			           'route' => array(
                          'cadastro.viagens.itensViagem.destroy', $viagem->Id, $itemViagem->Id
                       )
                   ))
                }}
				
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                          
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $itensViagem->appends(Input::except('page'))->links(); ?>
@else
  Nenhum item encontrado nessa viagem.
@endif

@stop