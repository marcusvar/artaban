@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Localidades
@stop


@section('submenu')
  <a href="{{ route('cadastro.localidades.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.localidades.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($localidades->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.localidades.index')) }}
     
        <div class="form-group">
          <label for="Nome">Nome:</label>
          {{ Form::text('Nome', null, array('class' => 'form-control')) }}
        </div>
  
        <div class="form-group">
          <label for="Tipo">Tipo:</label>
          {{ Form::select('Tipo',  $tiposLocalidade, null, array('class' => 'form-control')) }}
        </div>
  
        <div class="form-group">
          <label for="Fuso">Fuso:</label>
          {{ Form::text('Fuso', null, array('class' => 'form-control')) }}
        </div>
  
        <div class="form-group">
          <label for="UF_Id">UF:</label>
          {{ Form::select('UF_Id',  array('' => '') + $estados, null, array('class' => 'form-control')) }}
        </div>
   
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Nome</th>
	  <th>Tipo</th>
	  <th>Fuso</th>
	  <th>UF</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($localidades as $localidade)
            <tr>
	        <td>{{{ $localidade->Nome }}}</td>
		<td>{{{ $localidade->Tipo }}}</td>
		<td>{{{ $localidade->Fuso }}}</td>
		<td>{{{ $localidade->estado->Nome }}}</td>
                <td>{{ link_to_route('cadastro.localidades.show',
                                     'Detalhes',
                                     array($localidade->Id),
                                     array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
                    {{ link_to_route('cadastro.localidades.edit',
                                     'Editar',
                                     array($localidade->Id),
                                     array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
                    <div class="pull-left">
                      {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.localidades.destroy', $localidade->Id))) }}
                        <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                      {{ Form::close() }}
                    </div>
                </td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $localidades->appends(Input::except('page'))->links(); ?>
@else
    Não há localidades
@endif

@stop