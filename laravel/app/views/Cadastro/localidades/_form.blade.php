  <div class="form-group">
    <label for="Nome">Nome:</label>
    {{ Form::text('Nome', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Tipo">Tipo:</label>
    {{ Form::select('Tipo',  $tiposLocalidade, null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Fuso">Fuso:</label>
    {{ Form::text('Fuso', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="UF_Id">UF:</label>
    {{ Form::select('UF_Id',  $estados, null, array('class' => 'form-control')) }}
  </div>
  