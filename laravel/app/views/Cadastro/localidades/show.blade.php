@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes da localidade
@stop


@section('submenu')
  <a href="{{ route('cadastro.localidades.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.localidades.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.localidades.show', array($localidade->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.localidades.edit', array($localidade->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.localidades.destroy', $localidade->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <h2 class="detalhes">Nome</h2>
<p class="detalhes">{{{ $localidade->Nome }}}</p>

<h2 class="detalhes">Tipo</h2>
<p class="detalhes">{{{ $localidade->Tipo }}}</p>

<h2 class="detalhes">Fuso</h2>
<p class="detalhes">{{{ $localidade->Fuso }}}</p>

<h2 class="detalhes">UF</h2>
<p class="detalhes">{{{ $localidade->estado->Nome }}}</p>


@stop