@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Adicionar tipo de veículo
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposVeiculo.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.tiposVeiculo.create') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
    
  {{ Form::open(array('route' => 'cadastro.tiposVeiculo.store')) }}
    @include('Cadastro.tiposVeiculo._form')
    
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}

@stop