@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do tipo de veículo: {{ $tipoVeiculo->Descricao }}
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposVeiculo.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.tiposVeiculo.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.tiposVeiculo.show', array($tipoVeiculo->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>
    
  <a href="{{ route('cadastro.tiposVeiculo.mapa.index', array($tipoVeiculo->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-th"></span> Mapa
  </a>  
  
  <a href="{{ route('cadastro.tiposVeiculo.edit', array($tipoVeiculo->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.tiposVeiculo.destroy', $tipoVeiculo->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
<div class="show">
  <h2>Descrição</h2>
  <p>{{{ $tipoVeiculo->Descricao }}}</p>
</div>

@stop