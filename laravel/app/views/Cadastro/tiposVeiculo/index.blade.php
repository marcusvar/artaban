@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Tipos de veículo
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposVeiculo.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.tiposVeiculo.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  
  @if ($tiposVeiculo->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.tiposVeiculo.index')) }}
     
        <div class="form-group">
          <label for="Descricao">Descrição:</label>
          {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
        </div>
     
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      
      {{ Form::close() }}
    </div>
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Descrição</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($tiposVeiculo as $tipoVeiculo)
          <tr>
	    <td>{{{ $tipoVeiculo->Descricao }}}</td>
            <td>
	      {{ link_to_route('cadastro.tiposVeiculo.show',
                               'Detalhes',
                               array($tipoVeiculo->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.tiposVeiculo.mapa.index',
                               'Mapa',
                               array($tipoVeiculo->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                        			
	      {{ link_to_route('cadastro.tiposVeiculo.edit',
                               'Editar',
                               array($tipoVeiculo->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.tiposVeiculo.destroy', $tipoVeiculo->Id))) }}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $tiposVeiculo->appends(Input::except('page'))->links(); ?>
@else
  Nenhum tipo de veículo encontrado.
@endif

@stop