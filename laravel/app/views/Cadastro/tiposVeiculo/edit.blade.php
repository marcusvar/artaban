@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar tipo de veículo: {{ $tipoVeiculo->Descricao }}
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposVeiculo.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.tiposVeiculo.show', array($tipoVeiculo->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.tiposVeiculo.edit', array($tipoVeiculo->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($tipoVeiculo, array('method' => 'PATCH', 'route' => array('cadastro.tiposVeiculo.update', $tipoVeiculo->Id))) }}
    @include('Cadastro.tiposVeiculo._form')
  
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop