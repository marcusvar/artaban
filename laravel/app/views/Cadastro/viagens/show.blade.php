@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes da viagem
@stop


@section('submenu')
  <a href="{{ route('cadastro.viagens.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.viagens.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.viagens.show', array($viagem->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>
    
  <a href="{{ route('cadastro.viagens.itensViagem.index', array($viagem->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list-alt"></span> Itens
  </a>
  
  <a href="{{ route('cadastro.viagens.edit', array($viagem->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.viagens.destroy', $viagem->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    
    <h2>Horário</h2>
    <p>{{{ $viagem->horario->descricaoDaLinhaComHorario() }}}</p>
    
    <div class="row">
      <div class="col-xs-2">
        <h3>Data inicial</h3>
        <p>{{{ $viagem->DataInicio }}}</p>
      </div>
        
      <div class="col-xs-2">
        <h3>Hora inicial</h3>
        <p>{{{ $viagem->HoraInicio }}}</p>
      </div>
      
      <div class="col-xs-2">
        <h3>Duração</h3>
        <p>{{{ $viagem->DuracaoDescricao }}}</p>
      </div>
        
      <div class="col-xs-2">
        <h3>Confirmada</h3>
        <p>{{{ $viagem->Confirmada }}}</p>
      </div>
        
      <div class="col-xs-2">
        <h3>Situação</h3>
        <p>{{{ $viagem->Situacao }}}</p>
      </div>
        
      <div class="col-xs-2">
        <h3>Veículo</h3>
        <!--p> $viagem->Veiculo->Numero </p-->
      </div> 

        <div class="col-xs-2">
        <h3>Tipo de veículo</h3>
        <p>{{{ $viagem->tipoVeiculo->Descricao }}}</p>
      </div> 
    </div>
        
    <div class="row">
      <div class="col-xs-12">
        <h3>Descrição</h3>
        <p>{{{ $viagem->Descricao }}}</p>
      </div>
    </div>    
  </div>
@stop