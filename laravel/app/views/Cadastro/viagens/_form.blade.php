  <div class="form-group">
    <label for="Horario_Id">Horário:</label>
    {{ Form::select('Horario_Id', $horarios, null, array('class' => 'form-control chosen-select')) }}
  </div>
    
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="DataInicio">Data inicial:</label>
        {{ Form::text('DataInicio', null, array('class' => 'form-control datepicker')) }}
      </div>
    </div>
      
    <div class="col-xs-4">
      <div class="form-group">
        <label for="HoraInicio">Hora inicial:</label>
        {{ Form::text('HoraInicio', null, array('class' => 'form-control horario-mask')) }}
      </div>
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-3">
      <div class="form-group">
        <label for="Veiculo_Id">Veículo:</label>
        {{ Form::select('Veiculo_Id',  $veiculos, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
        
    <div class="col-xs-3">
      <div class="form-group">
        <label for="TipoVeiculo_Id">Tipo de veículo:</label>
        {{ Form::select('TipoVeiculo_Id',  $tiposVeiculo, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
        
    <div class="col-xs-3">
      <div class="form-group">
        <label for="Confirmada">Confirmada?</label>
        {{ Form::select('Confirmada', $confirmadaOpcoes, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
        
    <div class="col-xs-3">
      <div class="form-group">
        <label for="Situacao">Situação:</label>
        {{ Form::select('Situacao', $situacoes, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
        
    <div class="col-xs-3">
      <div class="form-group">
        <label for="Duracao">Duração (dias):</label>
        {{ Form::text('Duracao', null, array('class' => 'form-control')) }}
      </div>
    </div>    
  </div>
    
  <div class="row">
    <div class="col-xs-12">
      <div class="form-group">
        <label for="Descricao">Descrição:</label>
        {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
      </div>    
    </div> 
  </div>