@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar viagem
@stop


@section('submenu')
  <a href="{{ route('cadastro.viagens.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.viagens.show', array($viagem->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.viagens.edit', array($viagem->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($viagem, array('method' => 'PATCH', 'route' => array('cadastro.viagens.update', $viagem->Id))) }}
    @include('Cadastro.viagens._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop