@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Viagens
@stop


@section('submenu')
  <a href="{{ route('cadastro.viagens.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.viagens.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($viagens->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.viagens.index')) }}
     
     <div class="form-group">
       <label for="Horario_Id">Horário:</label>
       {{ Form::select('Horario_Id',
                    array('' => '') + $horarios,
                    null,
                    array('class' => 'form-control chosen-select horario-select',
                               'data-placeholder' => 'Filtrar por horário',
                               'data-children' => 'PontoReferencia_Id_Destino')) }}
     </div>
     
     <div class="row">
       <div class="col-xs-4">
         <div class="form-group">
           <label for="DataInicio">Data inicial (início):</label>
           {{ Form::text('DataInicio_inicial', null, array('class' => 'form-control datepicker')) }}
         </div>
       </div>
	
       <div class="col-xs-4">
         <div class="form-group">
         <label for="DataInicio">Data inicial (fim):</label>
         {{ Form::text('DataInicio_final', null, array('class' => 'form-control datepicker')) }}
         </div>
       </div>
     </div>
     
     <div class="row">
       <div class="col-xs-4">
         <div class="form-group">
           <label for="TipoVeiculo_Id">Tipo de veículo:</label>
           {{ Form::select('TipoVeiculo_Id',
                           $tiposVeiculo,
                           null,
                           array('class' => 'form-control chosen-select',
                                 'data-placeholder' => 'Filtrar por tipo de veículo')) }}
         </div>
       </div>
        
       <div class="col-xs-4">
         <div class="form-group">
           <label for="Confirmada">Confirmada:</label>
           {{ Form::select('Confirmada',
                           $confirmadaOpcoes,
                           null,
                           array('class' => 'form-control chosen-select',
                                 'data-placeholder' => 'Filtrar por confirmação')) }}
         </div>
       </div>
        
       <div class="col-xs-4">
         <div class="form-group">
           <label for="Situacao">Situação:</label>
           {{ Form::select('Situacao',
                           $situacoes,
                           null,
                           array('class' => 'form-control chosen-select',
                                 'data-placeholder' => 'Filtrar por situação')) }}
         </div>
       </div>
     </div>
     
     {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
   {{ Form::close() }}
  </div>
    
    
  <table class="table table-striped table-bordered">
    <thead>
      <tr>
        <th>Horário</th>  
	    <th>Data inicial</th>
	    <th>Hora inicial</th>
        <th>Duração</th>  
	    <th>Ações</th>
	  </tr>
    </thead>

    <tbody>
      @foreach ($viagens as $viagem)
        <tr>
	 	  <td>{{{ $viagem->horario->descricaoDaLinhaComHorario() }}}</td>
          <td>{{{ $viagem->DataInicio }}}</td>
		  <td>{{{ $viagem->HoraInicio }}}</td>
		  <td>{{{ $viagem->DuracaoDescricao }}}</td>
          
          <td>
            {{ link_to_route('cadastro.viagens.show',
                             'Detalhes',
                             array($viagem->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
					 
			{{ link_to_route('cadastro.viagens.itensViagem.index',
                             'Itens',
                             array($viagem->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
            {{ link_to_route('cadastro.viagens.edit',
                             'Editar',
                             array($viagem->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
            <div class="pull-left">
              {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.viagens.destroy', $viagem->Id))) }}
                <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
              {{ Form::close() }}
            </div>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
    
  <?php echo $viagens->appends(Input::except('page'))->links(); ?>
@else
  Nenhuma viagem encontrada.
@endif

@stop