@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Veículos
@stop


@section('submenu')
  <a href="{{ route('cadastro.veiculos.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.veiculos.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($veiculos->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.veiculos.index')) }}
     
      <div class="row">
        <div class="col-xs-4">
          <div class="form-group">
            <label for="TipoVeiculo_Id">Tipo de veículo:</label>
            {{ Form::select('TipoVeiculo_Id',
                            $tiposVeiculo,
                            null,
                            array('class' => 'form-control chosen-select',
                                  'data-placeholder' => 'Filtrar por tipo de veículo')) }}
          </div>
        </div>
            
        <div class="col-xs-4">
          <div class="form-group">
            <label for="Numero">Número:</label>
            {{ Form::text('Numero', null, array('class' => 'form-control')) }}
          </div>
        </div>
            
        <div class="col-xs-4">
          <div class="form-group">
            <label for="Placa">Placa:</label>
            {{ Form::text('Placa', null, array('class' => 'form-control')) }}
          </div>
        </div>    
      </div>
                 
      {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      
      {{ Form::close() }}
    </div>
        
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Tipo de veículo</th>
	  <th>Número</th>
	  <th>Placa</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($veiculos as $veiculo)
          <tr>
	    <td>{{{ $veiculo->tipoVeiculo->Descricao }}}</td>
            <td>{{{ $veiculo->Numero }}}</td>
	    <td>{{{ $veiculo->Placa }}}</td>
	    
	    <td>
	      {{ link_to_route('cadastro.veiculos.show',
                               'Detalhes',
                               array($veiculo->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.veiculos.edit',
                               'Editar',
                               array($veiculo->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.veiculos.destroy', $veiculo->Id))) }}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $veiculos->appends(Input::except('page'))->links(); ?>
@else
  Nenhum veículo encontrado.
@endif

@stop