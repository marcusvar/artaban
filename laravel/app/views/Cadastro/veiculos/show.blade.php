@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do veículo
@stop


@section('submenu')
  <a href="{{ route('cadastro.veiculos.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.veiculos.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.veiculos.show', array($veiculo->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.veiculos.edit', array($veiculo->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.veiculos.destroy', $veiculo->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show">
    
    <div class="row">
      <div class="col-xs-4">
        <h3>Tipo de veículo</h3>
        <p>{{{ $veiculo->tipoVeiculo->Descricao }}}</p>
      </div>
    
      <div class="col-xs-4">
        <h3>Número</h3>
        <p>{{{ $veiculo->Numero }}}</p>
      </div>
    
      <div class="col-xs-4">
        <h3>Placa</h3>
        <p>{{{ $veiculo->Placa }}}</p>
      </div>
    </div>
        
  </div>
@stop