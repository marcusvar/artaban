  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="TipoVeiculo_Id">Tipo de veículo:</label>
        {{ Form::select('TipoVeiculo_Id', $tiposVeiculo, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Numero">Número:</label>
        {{ Form::text('Numero', null, array('class' => 'form-control')) }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Placa">Placa:</label>
        {{ Form::text('Placa', null, array('class' => 'form-control placa-veiculo-mask')) }}
      </div>
    </div>
  </div>