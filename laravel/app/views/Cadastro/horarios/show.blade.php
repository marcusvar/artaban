@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do horário
@stop


@section('submenu')
  <a href="{{ route('cadastro.horarios.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.horarios.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.horarios.show', array($horario->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>
    
  <a href="{{ route('cadastro.horarios.itensHorario.index', array($horario->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list-alt"></span> Itens
  </a>  
  
  <a href="{{ route('cadastro.horarios.edit', array($horario->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.horarios.destroy', $horario->Id),
                      'class' => 'delete-submenu')) }}
     <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    <div class="row">
      <div class="col-xs-6">
        <h2>Linha</h2>
        <p>{{{ $linha->pontoReferencia->descricaoCompleta() }}}</p>
      </div>
        
      <div class="col-xs-4">
        <h2>Horário</h2>
        <p>{{{ $horario->Horario }}}</p>
      </div>
    </div>
    
    <div class="row">
      <div class="col-xs-6">
        <h3>Frequência</h3>
        <p>{{{ $horario->frequenciaDescricao() }}}</p>
      </div>
        
      <div class="col-xs-4">
        <h3>Tipo de veículo</h3>
        <p>{{{ $tipoVeiculo->Descricao }}}</p>
      </div>
    </div>
        
    <div class="row">
      <div class="col-xs-3">
       <h3>Duração</h3>
       <p>{{{ $horario->DuracaoDescricao }}}</p>
      </div>
        
      <div class="col-xs-3">
        <h3>Feriado</h3>
        <p>{{{ $horario->feriadoDescricao() }}}</p>
      </div>
        
      <div class="col-xs-3">
        <h3>Confirmado</h3>
        <p>{{{ $horario->Confirmado }}}</p>
      </div>
        
      <div class="col-xs-3">
        <h3>Situação</h3>
        <p>{{{ $horario->Situacao }}}</p>
      </div>
    </div>
    
    <div class="row">
      <div class="col-xs-12">
        <h3>Descrição</h3>
        <p>{{{ $horario->Descricao }}}</p>
      </div>
    </div>
  </div>
@stop