<div class="row">
  <div class="col-xs-8">
    <div class="form-group">
      <label for="Linha_Id">Linha:</label>
      {{ Form::select('Linha_Id',
                    $linhas,
                    $linhaSelecionadaId,
                    array('class' => 'form-control chosen-select')) }}
    </div>    
  </div>
    
  <div class="col-xs-4">
    <div class="form-group">
      <label for="TipoVeiculo_Id">Tipo de veículo:</label>
      {{ Form::select('TipoVeiculo_Id', $tiposVeiculo, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>  
</div>

<div class="row">
  <div class="col-xs-2">
    <div class="form-group">
      <label for="Horario">Horário:</label>
      {{ Form::text('Horario', null, array('class' => 'form-control horario-mask')) }}
    </div>
  </div>
    
  <div class="col-xs-3">
    <div class="form-group">
      <label for="Confirmado">Confirmado:</label>
      {{ Form::select('Confirmado', $confirmadoOpcoes, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>
    
  <div class="col-xs-4">
    <div class="form-group">
      <label for="Situacao">Situação:</label>
      {{ Form::select('Situacao', $situacoes, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>
    
  <div class="col-xs-3">
    <div class="form-group">
      <label for="Duracao">Duração (dias):</label>
      {{ Form::text('Duracao', null, array('class' => 'form-control')) }}
    </div>
  </div>  
</div>
  
<div class="row">
  <div class="col-xs-1">
      <label for="Frequencia[0]">Dom</label> 
      {{ Form::checkbox('Frequencia[0]', '1', $horario->dom(), array('id' => 'Frequencia[0]')) }}  
  </div>
    
  <div class="col-xs-1">
      <label for="Frequencia[1]">Seg</label> 
      {{ Form::checkbox('Frequencia[1]', '1', $horario->seg(), array('id' => 'Frequencia[1]')) }}  
  </div>
  
  <div class="col-xs-1">
      <label for="Frequencia[2]">Ter</label> 
      {{ Form::checkbox('Frequencia[2]', '1', $horario->ter(), array('id' => 'Frequencia[2]')) }}  
  </div>
    
  <div class="col-xs-1">
      <label for="Frequencia[3]">Qua</label> 
      {{ Form::checkbox('Frequencia[3]', '1', $horario->qua(), array('id' => 'Frequencia[3]')) }}  
  </div>
    
  <div class="col-xs-1">
      <label for="Frequencia[4]">Qui</label> 
      {{ Form::checkbox('Frequencia[4]', '1', $horario->qui(), array('id' => 'Frequencia[4]')) }}  
  </div>
    
  <div class="col-xs-1">
      <label for="Frequencia[5]">Sex</label> 
      {{ Form::checkbox('Frequencia[5]', '1', $horario->sex(), array('id' => 'Frequencia[5]')) }}  
  </div>
    
  <div class="col-xs-1">
      <label for="Frequencia[6]">Sab</label> 
      {{ Form::checkbox('Frequencia[6]', '1', $horario->sab(), array('id' => 'Frequencia[6]')) }}  
  </div>
    
  <div class="col-xs-2">
    <div class="form-group">
      <label for="Feriado">Feriado:</label>
      {{ Form::select('Feriado', $feriadoOpcoes, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>
</div>
    
<div class="row">
  <div class="col-xs-12">
    <div class="form-group">
      <label for="Descricao">Descrição:</label>
      {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
    </div>    
  </div> 
</div>