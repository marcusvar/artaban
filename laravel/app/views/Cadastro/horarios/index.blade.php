@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Horários
@stop


@section('submenu')
  <a href="{{ route('cadastro.horarios.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.horarios.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($horarios->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.horarios.index')) }}
     
       <div class="form-group">
         <label for="Linha_Id">Linha:</label>
         {{ Form::select('Linha_Id',
                         $linhas,
                         null,
                         array('class' => 'form-control chosen-select',
                               'data-placeholder' => 'Filtrar por linha')) }}
       </div>
  
       <div class="row">
         <div class="col-xs-4">
           <div class="form-group">
             <label for="TipoVeiculo_Id">Tipo de veículo:</label>
             {{ Form::select('TipoVeiculo_Id',
                             $tiposVeiculo,
                             null,
                             array('class' => 'form-control chosen-select',
                                   'data-placeholder' => 'Filtrar por tipo de veículo')) }}
           </div>    
         </div>
         
         <div class="col-xs-4">
           <div class="form-group">
             <label for="Confirmado">Confirmado:</label>
            {{ Form::select('Confirmado',
                            $confirmadoOpcoes,
                            null,
                            array('class' => 'form-control chosen-select',
                                  'data-placeholder' => 'Filtrar por confirmação')) }}
           </div>  
         </div>
            
         <div class="col-xs-4">
           <div class="form-group">
             <label for="Situacao">Situação:</label>
             {{ Form::select('Situacao',
                             $situacoes,
                             null,
                             array('class' => 'form-control chosen-select',
                                   'data-placeholder' => 'Filtrar por situação')) }}
           </div>         
         </div>
       </div>     
  
       {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Linha</th>
          <th>Horário</th>
          <th>Descrição</th>
          <th>Frequência</th>
          <th>Duração</th>
          <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($horarios as $horario)
        <tr>
	      <td>{{{ $horario->descricaoDaLinha() }}}</td>
          <td>{{{ $horario->Horario }}}</td>
          <td>{{{ $horario->Descricao }}}</td>
          <td>{{{ $horario->frequenciaDescricao() }}}</td>
          <td>{{{ $horario->DuracaoDescricao }}}</td>
          
          <td>
	        {{ link_to_route('cadastro.horarios.show',
                             'Detalhes',
                             array($horario->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
            {{ link_to_route('cadastro.horarios.itensHorario.index',
                             'Itens',
                             array($horario->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
             	      
	        {{ link_to_route('cadastro.horarios.edit',
                             'Editar',
                             array($horario->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
             	      
	        <div class="pull-left">
              {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.horarios.destroy', $horario->Id))) }}
                <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
              {{ Form::close() }}
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $horarios->appends(Input::except('page'))->links(); ?>
@else
    Nenhum horário encontrado.
@endif

@stop