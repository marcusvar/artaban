  <div class="form-group">
    <label for="Nome">Nome:</label>
    {{ Form::text('Nome', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Valor">Valor:</label>
    {{ Form::text('Valor', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="form-group">
    <label for="Descricao">Descrição:</label>
    {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
  </div>
  