@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Parâmetros
@stop


@section('submenu')
  <a href="{{ route('cadastro.parametros.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.parametros.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($parametros->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.parametros.index')) }}
     
        <div class="form-group">
          <label for="Nome">Nome:</label>
          {{ Form::text('Nome', null, array('class' => 'form-control')) }}
        </div>
  
        <div class="form-group">
          <label for="Valor">Valor:</label>
          {{ Form::text('Valor', null, array('class' => 'form-control')) }}
        </div>
  
        <div class="form-group">
          <label for="Descricao">Descrição:</label>
          {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
        </div>
   
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Nome</th>
	  <th>Valor</th>
	  <th>Descrição</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($parametros as $parametro)
          <tr>
	    <td>{{{ $parametro->Nome }}}</td>
            <td>{{{ $parametro->Valor }}}</td>
	    <td>{{{ $parametro->Descricao }}}</td>
            
	    <td>
	      {{ link_to_route('cadastro.parametros.show',
                               'Detalhes',
                               array($parametro->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.parametros.edit',
                               'Editar',
                               array($parametro->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.parametros.destroy', $parametro->Id))) }}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $parametros->appends(Input::except('page'))->links(); ?>
@else
  Nenhum parâmetro encontrado.
@endif

@stop