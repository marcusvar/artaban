@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Detalhes do adicional de tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.index', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.create', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.show', array($linha->ID, $adicionalTarifa->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.edit', array($linha->ID, $adicionalTarifa->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.linhas.adicionaisTarifa.destroy', $linha->ID, $adicionalTarifa->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    <h2>Linha</h2>
    <p>{{{ $adicionalTarifa->linha->pontoReferencia->descricaoCompleta() }}}</p>

    <div class="row">
      <div class="col-xs-4">
        <h3>Tipo de adicional</h3>
        <p>{{{ $adicionalTarifa->tipoAdicionalTarifa->Descricao }}}</p>
      </div>
        
      <div class="col-xs-4">
        <h3>Data da vigência</h3>
        <p>{{{ $adicionalTarifa->DataVigencia }}}</p>
      </div>
            
      <div class="col-xs-4">
        <h3>Valor</h3>
        <p>{{{ $adicionalTarifa->Valor }}}</p>
      </div>
    </div>
  </div>
@stop