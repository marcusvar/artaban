@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Editar um adicional de tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.index', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.show', array($linha->ID, $adicionalTarifa->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.edit', array($linha->ID, $adicionalTarifa->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($adicionalTarifa,
                 array('method' => 'PATCH',
                       'route' => array('cadastro.linhas.adicionaisTarifa.update', $linha->ID, $adicionalTarifa->Id))) }}
    
    @include('Cadastro.adicionaisTarifa._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop