<div class="row">
  <div class="col-xs-4">
    <div class="form-group">
      <label for="TipoAdicionalTarifa_Id">Tipo de adicional:</label>
      {{ Form::select('TipoAdicionalTarifa_Id', $tiposAdicionaisTarifa, null, array('class' => 'form-control')) }}
    </div>
  </div>
    
  <div class="col-xs-4">
    <div class="form-group">
      <label for="DataVigencia">Data da vigência:</label>
      {{ Form::text('DataVigencia', null, array('class' => 'form-control datepicker')) }}
    </div>
  </div>
    
  <div class="col-xs-4">
    <div class="form-group">
      <label for="Valor">Valor:</label>
      {{ Form::text('Valor', null, array('class' => 'form-control money-mask')) }}
    </div> 
  </div>
</div>