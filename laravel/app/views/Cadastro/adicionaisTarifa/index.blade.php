@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Adicionais de tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.index', $linha->ID) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.create', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($adicionaisTarifa->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
    {{ Form::open(array('method' => 'GET',
                         'route' => ['cadastro.linhas.adicionaisTarifa.index', $linha->ID])) }}
      
     <div class="row">
       <div class="col-xs-4">
         <div class="form-group">
           <label for="TipoAdicionalTarifa_Id">Tipo de adicional:</label>
           {{ Form::select('TipoAdicionalTarifa_Id',  $tiposAdicionaisTarifa, null, array('class' => 'form-control')) }}
         </div>       
       </div>
    
       <div class="col-xs-4">
         <div class="form-group">
           <label for="DataVigencia">Data da vigência (início):</label>
           {{ Form::text('DataVigencia_inicial', null, array('class' => 'form-control datepicker')) }}
         </div>
       </div>
	
       <div class="col-xs-4">
         <div class="form-group">
         <label for="DataVigencia">Data da vigência (fim):</label>
         {{ Form::text('DataVigencia_final', null, array('class' => 'form-control datepicker')) }}
         </div>
       </div>
     </div>
     
       {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Linha</th>
				<th>Tipo de adicional</th>
				<th>Data da vigência</th>
				<th>Valor</th>
				<th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($adicionaisTarifa as $adicionalTarifa)
            <tr>
	        <td>{{{ $adicionalTarifa->linha->pontoReferencia->descricaoCompleta() }}}</td>
		<td>{{{ $adicionalTarifa->tipoAdicionalTarifa->Descricao }}}</td>
		<td>{{{ $adicionalTarifa->DataVigencia }}}</td>
		<td>{{{ $adicionalTarifa->Valor }}}</td>
                
		<td>{{ link_to_route('cadastro.linhas.adicionaisTarifa.show',
                                         'Detalhes',
                                         array($linha->ID, $adicionalTarifa->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
                    {{ link_to_route('cadastro.linhas.adicionaisTarifa.edit',
                                         'Editar',
                                         array($linha->ID, $adicionalTarifa->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
                    <div class="pull-left">
                      {{ Form::open(array('method' => 'DELETE',
		                          'route' => array('cadastro.linhas.adicionaisTarifa.destroy',
					  $linha->ID, $adicionalTarifa->Id))) }}
                        <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                      {{ Form::close() }}
                    </div>
                </td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $adicionaisTarifa->appends(Input::except('page'))->links(); ?>
@else
  Nenhum adicional de tarifa encontrado.
@endif

@stop