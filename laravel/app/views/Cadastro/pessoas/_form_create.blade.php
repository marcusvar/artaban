<div class="row">
  <div class="col-xs-12">
    <h3 class="detalhes" >Selecione o tipo da pessoa:</h3>
  </div>   
</div>
    
<div class="row">
  <div class="col-xs-3">
    {{ link_to_route('cadastro.pessoas.create',
                     'Pessoa Física Brasileira',
                     array('tipo' => 'pf_br'),
                     array('class' => 'btn btn-default btn-sm botao-padrao'))
    }}    
  </div>
    
  <div class="col-xs-3">
    {{ link_to_route('cadastro.pessoas.create',
                     'Pessoa Jurídica Brasileira',
                     array('tipo' => 'pj_br'),
                     array('class' => 'btn btn-default btn-sm botao-padrao'))
    }}   
  </div>
    
  <div class="col-xs-3">
    {{ link_to_route('cadastro.pessoas.create',
                     'Pessoa Física Estrangeira',
                     array('tipo' => 'pf_es'),
                     array('class' => 'btn btn-default btn-sm botao-padrao'))
    }}  
  </div>
    
  <div class="col-xs-3">
    {{ link_to_route('cadastro.pessoas.create',
                     'Pessoa Jurídica Estrangeira',
                     array('tipo' => 'pj_es'),
                     array('class' => 'btn btn-default btn-sm botao-padrao'))
    }}   
  </div>  
</div>