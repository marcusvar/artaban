{{ Form::hidden('Tipo', 'PJ Brasileiro', array('id' => 'Tipo')) }}

<fieldset style="margin-top: 0">
  <legend>Dados da empresa:</legend>

  <div class="form-group">
    <label for="Nome">Nome (Razão social):</label>
    {{ Form::text('Nome', null, array('class' => 'form-control')) }}
  </div>
    
  <div class="form-group">
    <label for="NomeUsual">Nome (Fantasia):</label>
    {{ Form::text('NomeUsual', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Documento">Documento (CNPJ):</label>
        {{ Form::text('Documento', null, array('class' => 'form-control cnpj-mask', 'id' => 'pessoa-documento')) }}
        <div id="pessoa-documento-erro" style="color: #941F21; font-weight: bold;"></div>
      </div> 
    </div>
  
    <div class="col-xs-4">
      <div class="form-group">
        <label for="RGIE">IE:</label>
        {{ Form::text('RGIE', null, array('class' => 'form-control')) }}
      </div> 
    </div> 
  </div>
</fieldset>

<fieldset>
  <legend>Responsável:</legend>

  <div class="row">
    <div class="col-xs-8">
      <div class="form-group">
        <label for="Pessoa_Id_Responsavel">Nome:</label>
        {{ Form::select('Pessoa_Id_Responsavel',
                        $pessoas,
                        $pessoaId,
                        array('class' => 'form-control chosen-select',
                              'data-placeholder' => 'Selecione o responsável')) }}
      </div> 
    </div>  
  </div>
</fieldset>
 
<fieldset>
  <legend>Informações de contato:</legend>

  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="Email">Email:</label>
        {{ Form::email('Email', null, array('class' => 'form-control')) }}
      </div> 
    </div>  
  </div>
</fieldset>  


<fieldset>
  <legend>Endereço:</legend>
  
  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Estado_Id]">Estado:</label>
        {{ Form::select('endereco[Estado_Id]',
                         $estados,
                         $estadoSelecionadoId,
                         array('class' => 'form-control chosen-select estado-select',
                               'data-placeholder' => 'Selecione o estado',
                               'data-children' => 'endereco_Localidade_Id' )) }}
      </div>
    </div>
    
    <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Localidade_Id]">Localidade:</label>
        {{ Form::select('endereco[Localidade_Id]',
                         $localidades,
                         null,
                         array('class' => 'form-control chosen-select',
                                'id' => 'endereco_Localidade_Id',
                                'data-placeholder' => 'Selecione a localidade')) }}
      </div>
    </div>  
  </div>
  
  <div class="row">
     <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Bairro]">Bairro:</label>
        {{ Form::text('endereco[Bairro]', null, array('class' => 'form-control')) }}
      </div>
    </div>
    
    <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Logradouro]">Logradouro:</label>
        {{ Form::text('endereco[Logradouro]', null, array('class' => 'form-control')) }}
      </div>
    </div>
  </div>
    
  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Complemento]">Complemento:</label>
        {{ Form::text('endereco[Complemento]', null, array('class' => 'form-control')) }}
      </div>
    </div>
    
    <div class="col-xs-3">
      <div class="form-group">
        <label for="endereco[Numero]">Número:</label>
        {{ Form::text('endereco[Numero]', null, array('class' => 'form-control')) }}
      </div>
    </div>
      
    <div class="col-xs-3">
      <div class="form-group">
        <label for="endereco[Cep]">CEP:</label>
        {{ Form::text('endereco[Cep]', null, array('class' => 'form-control cep-mask')) }}
      </div>
    </div>
      
  </div>
<fieldset>


<fieldset>
  <legend>Informações gerais:</legend>

  <div class="row">
    <div class="col-xs-3">
      <div class="form-group">
        <label for="VisualizarFidelizacao">Visualiza fidelização?</label>
        {{ Form::select('VisualizarFidelizacao',
                        $visualizarFidelizacaoOpcoes,
                        null,
                        array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
    
    <div class="col-xs-3">
      <div class="form-group">
        <label for="EmPotencial">Cliente em potencial?</label>
        {{ Form::select('EmPotencial',
                        $emPotencialOpcoes,
                        null,
                        array('class' => 'form-control chosen-select')) }}
      </div>
    </div>    
  </div>
</fieldset>