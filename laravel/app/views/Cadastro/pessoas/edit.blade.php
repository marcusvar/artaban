@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar pessoa
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.pessoas.show', array($pessoa->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>
    
  <a href="#" class="btn btn-success btn-pessoa-telefones" data-pessoa-id="{{ $pessoa->Id }}">
    <span class="glyphicon glyphicon-earphone"></span> Telefones
  </a>  
  
  <a href="{{ route('cadastro.pessoas.edit', array($pessoa->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
  
  <div id="form-pessoas">
    {{ $form }}
  </div>
  
  @include('Cadastro.pessoas._modal_telefones')   
  
@stop