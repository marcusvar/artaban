  <h2>Telefones</h2>
  
  <div class="row">
    @foreach ($telefones as $telefone)
      <div class="col-xs-3">
        <p><b>{{{ $telefone->Descricao }}}:</b> {{{ $telefone->Telefone }}}</p>
      </div>
    @endforeach    
  </div>