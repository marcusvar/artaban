  <h2>Dados da empresa</h2>

  <h3>Nome (Razão social)</h3>
  <p>{{{ $pessoa->Nome }}}</p>

  <h3>Nome (Fantasia)</h3>
  <p>{{{ $pessoa->NomeUsual }}}</p>

  <div class="row">
    <div class="col-xs-4">
      <h3>Documento (CNPJ)</h3>
      <p>{{{ $pessoa->Documento }}}</p>
    </div>
    
    <div class="col-xs-4">
      <h3>IE</h3>
      <p>{{{ $pessoa->RGIE }}}</p>
    </div>        
  </div>

  
  @if ($responsavel)
    <h2>Responsável</h2>

    <div class="row">
      <div class="col-xs-6">
        <h3>Nome</h3>  
        <p>{{{ $responsavel->Nome }}}</p>
      </div>
    </div>  
  @endif

  
  <h2>Informações de contato</h2>

  <div class="row">
    <div class="col-xs-6">
      <h3>Email</h3>  
      <p>{{{ $pessoa->Email }}}</p>
    </div>    
  </div>  
 
 
  {{ View::make('Cadastro.pessoas._show_telefones', compact('telefones')) }}     
  

  @if ($endereco) 
    <h2>Endereço</h2>

    <div class="row">
      <div class="col-xs-6">
        <h3>Estado</h3>  
        <p>{{{ $estado->Nome }}}</p>
      </div>
    
      <div class="col-xs-6">
        <h3>Localidade</h3>
        <p>{{{ $localidade->Nome }}}</p>
      </div>    
    </div> 

    <div class="row">
      <div class="col-xs-6">
        <h3>Bairro</h3>
        <p>{{{ $endereco->Bairro }}}</p>
      </div>
  
      <div class="col-xs-4">
        <h3>Logradouro</h3>  
        <p>{{{ $endereco->Logradouro }}}</p>
      </div>    
    </div>
  
    <div class="row">    
      <div class="col-xs-6">
        <h3>Complemento</h3>
        <p>{{{ $endereco->Complemento }}}</p>
      </div>
    
      <div class="col-xs-3">
        <h3>Número</h3>
        <p>{{{ $endereco->Numero }}}</p>
      </div>
   
      <div class="col-xs-3">
        <h3>CEP</h3>
        <p>{{{ $endereco->Cep }}}</p>
      </div>  
    </div>
  @endif
  
  
  <h2>Informações gerais</h2>

  <div class="row">
    <div class="col-xs-3">
      <h3>Visualiza fidelização?</h3>
      <p>{{{ $pessoa->visualizarFidelizacaoDescricao() }}}</p>
    </div>
        
    <div class="col-xs-3">
      <h3>Cliente em potencial?</h3>  
      <p>{{{ $pessoa->emPotencialDescricao() }}}</p>
    </div>    
  </div>