{{ Form::hidden('Tipo', 'PF Estrangeiro', array('id' => 'Tipo')) }}

<fieldset style="margin-top: 0">
  <legend>Dados pessoais:</legend>

  <div class="form-group">
    <label for="Nome">Nome:</label>
    {{ Form::text('Nome', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="RGIE">RG:</label>
        {{ Form::text('RGIE', null, array('class' => 'form-control')) }}
      </div> 
    </div>
  
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Sexo">Sexo:</label>
        {{ Form::select('Sexo',
                        array('M' => 'Masculino', 'F' => 'Feminino'),
                        null,
                        array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
        
    <div class="col-xs-3">
      <div class="form-group">
        <label for="DataNascimento">Data de nascimento:</label>
        {{ Form::text('DataNascimento', null, array('class' => 'form-control datepicker')) }}  
      </div>
    </div>    
  </div>
</fieldset>
    
<fieldset>
  <legend>Informações de contato:</legend>

  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="Email">Email:</label>
        {{ Form::email('Email', null, array('class' => 'form-control')) }}
      </div> 
    </div>  
  </div>
</fieldset>  


<fieldset>
  <legend>Endereço:</legend>
  
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="endereco[Pais_Id]">País:</label>
        {{ Form::select('endereco[Pais_Id]',
                         $paises,
                         $paisSelecionadoId,
                         array('class' => 'form-control chosen-select pais-select',
                               'data-placeholder' => 'Selecione o país',
                               'data-estado-id' => 'endereco_Estado_Id',
                               'data-cidade-id' => 'endereco_Localidade_Id')) }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="endereco[Estado_Id]">Estado:</label>
        {{ Form::select('endereco[Estado_Id]',
                         $estados,
                         $estadoSelecionadoId,
                         array('class' => 'form-control chosen-select estado-select',
                               'id' => 'endereco_Estado_Id',
                               'data-placeholder' => 'Selecione o estado',
                               'data-children' => 'endereco_Localidade_Id' )) }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="endereco[Localidade_Id]">Localidade:</label>
        {{ Form::select('endereco[Localidade_Id]',
                         $localidades,
                         null,
                         array('class' => 'form-control chosen-select',
                                'id' => 'endereco_Localidade_Id',
                                'data-placeholder' => 'Selecione a localidade')) }}
      </div>
    </div>  
  </div>
  
  <div class="row">
     <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Bairro]">Bairro:</label>
        {{ Form::text('endereco[Bairro]', null, array('class' => 'form-control')) }}
      </div>
    </div>
    
    <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Logradouro]">Logradouro:</label>
        {{ Form::text('endereco[Logradouro]', null, array('class' => 'form-control')) }}
      </div>
    </div>
  </div>
    
    
  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="endereco[Complemento]">Complemento:</label>
        {{ Form::text('endereco[Complemento]', null, array('class' => 'form-control')) }}
      </div>
    </div>
    
    <div class="col-xs-3">
      <div class="form-group">
        <label for="endereco[Numero]">Número:</label>
        {{ Form::text('endereco[Numero]', null, array('class' => 'form-control')) }}
      </div>
    </div>
      
    <div class="col-xs-3">
      <div class="form-group">
        <label for="endereco[Cep]">CEP:</label>
        {{ Form::text('endereco[Cep]', null, array('class' => 'form-control cep-mask')) }}
      </div>
    </div>
      
  </div>
<fieldset>


<fieldset>
  <legend>Informações gerais:</legend>

  <div class="row">
    <div class="col-xs-3">
      <div class="form-group">
        <label for="VisualizarFidelizacao">Visualiza fidelização?</label>
        {{ Form::select('VisualizarFidelizacao',
                        $visualizarFidelizacaoOpcoes,
                        null,
                        array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
    
    <div class="col-xs-3">
      <div class="form-group">
        <label for="EmPotencial">Cliente em potencial?</label>
        {{ Form::select('EmPotencial',
                        $emPotencialOpcoes,
                        null,
                        array('class' => 'form-control chosen-select')) }}
      </div>
    </div>    
  </div>
</fieldset>