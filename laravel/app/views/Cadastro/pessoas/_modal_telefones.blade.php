<div class="modal fade" id="box-modal-pessoa-telefones" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog" style="width: 60%">
    <div class="modal-content">
      
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h2 class="modal-title modal-titulo" id="myModalLabel">Telefones</h2>
      </div>
      
      <div class="modal-body">
        {{ Form::open(array('url' => '#', 'id' => 'modal-form-pessoa-telefones')) }}
        {{ Form::close() }}
          
        <datalist id="lista-telefones-tipos">
          <option value="Celular" />
          <option value="Comercial" />
          <option value="Fax" />
          <option value="Principal" />
          <option value="Residencial" />
          <option value="Telefone" />
        </datalist>
            
        <a href="#" class="btn btn-default btn-sm botao-padrao modal-btn-pessoa-adicionar-telefone">
          <span class="glyphicon glyphicon-plus"></span> Adicionar
        </a>    
        
        <div id="modal-form-mensagem-pessoa-telefones">
        </div>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default botao-padrao modal-btn-pessoa-salvar-telefones">Salvar alterações</button>
        <button type="button" class="btn btn-default botao-padrao" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>