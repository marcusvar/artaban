  <h2>Dados pessoais</h2>

  <h3>Nome</h3>
  <p>{{{ $pessoa->Nome }}}</p>

  <div class="row">
    <div class="col-xs-4">
      <h3>RG</h3>
      <p>{{{ $pessoa->RGIE }}}</p>
    </div>
    
    <div class="col-xs-4">
      <h3>Sexo</h3>
      <p>{{{ $pessoa->sexoDescricao() }}}</p>
    </div>
        
    <div class="col-xs-3">
      <h3>Data de nascimento</h3>
      <p>{{{ $pessoa->DataNascimento }}}</p>
    </div>    
  </div>


  <h2>Informações de contato</h2>

  <div class="row">
    <div class="col-xs-6">
      <h3>Email</h3>  
      <p>{{{ $pessoa->Email }}}</p>
    </div>    
  </div>
    
    
  {{ View::make('Cadastro.pessoas._show_telefones', compact('telefones')) }}     
    

  @if ($endereco)
    <h2>Endereço</h2>

    <div class="row">
      <div class="col-xs-4">
        <h3>País</h3>  
        <p>{{{ $estado->pais->Nome }}}</p>
      </div>
    
      <div class="col-xs-4">
        <h3>Estado</h3>  
        <p>{{{ $estado->Nome }}}</p>
      </div>
    
      <div class="col-xs-4">
        <h3>Localidade</h3>
        <p>{{{ $localidade->Nome }}}</p>
      </div>    
    </div> 

    <div class="row">
      <div class="col-xs-6">
        <h3>Bairro</h3>
        <p>{{{ $endereco->Bairro }}}</p>
      </div>
  
      <div class="col-xs-4">
        <h3>Logradouro</h3>  
        <p>{{{ $endereco->Logradouro }}}</p>
      </div>    
    </div>
  
    <div class="row">    
      <div class="col-xs-6">
        <h3>Complemento</h3>
        <p>{{{ $endereco->Complemento }}}</p>
      </div>
    
      <div class="col-xs-3">
        <h3>Número</h3>
        <p>{{{ $endereco->Numero }}}</p>
      </div>
   
      <div class="col-xs-3">
        <h3>CEP</h3>
        <p>{{{ $endereco->Cep }}}</p>
      </div>  
    </div>
  @endif
   
   
  <h2>Informações gerais</h2>

  <div class="row">
    <div class="col-xs-3">
      <h3>Visualiza fidelização?</h3>
      <p>{{{ $pessoa->visualizarFidelizacaoDescricao() }}}</p>
    </div>
        
    <div class="col-xs-3">
      <h3>Cliente em potencial?</h3>  
      <p>{{{ $pessoa->emPotencialDescricao() }}}</p>
    </div>    
  </div>