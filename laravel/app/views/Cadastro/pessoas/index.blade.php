@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Pessoas
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.pessoas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($pessoas->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.pessoas.index')) }}
        
        <div class="form-group">
          <label for="Nome">Nome:</label>
          {{ Form::text('Nome', null, array('class' => 'form-control')) }}
        </div>
        
        <div class="row">
          <div class="col-xs-4">
            <div class="form-group">
              <label for="Nome">Documento:</label>
              {{ Form::text('Documento', null, array('class' => 'form-control')) }}
            </div>
          </div>
            
          <div class="col-xs-4">
            <div class="form-group">
              <label for="Nome">RG/IE:</label>
              {{ Form::text('RGIE', null, array('class' => 'form-control')) }}
            </div>
          </div>
            
          <div class="col-xs-4">
            <div class="form-group"> 
              <label for="Nome">Tipo:</label>
              {{ Form::select('Tipo',
                              $tipos,
                              null,
                              array('class' => 'form-control chosen-select',
                                    'data-placeholder' => 'Filtrar por tipo'))
              }}
            </div>
          </div>
        </div>
        
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
  
      {{ Form::close() }}
    </div>
       
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Nome</th>
	      <th>Documento</th>
	      <th>RG/IE</th>
	      <th style="width: 45%">Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($pessoas as $pessoa)
          <tr>
	        <td>{{{ $pessoa->Nome }}}</td>
		    <td>{{{ $pessoa->Documento }}}</td>
		    <td>{{{ $pessoa->RGIE }}}</td>
					
            <td>
              {{ link_to_route('cadastro.pessoas.show',
                               'Detalhes',
                               array($pessoa->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                
              {{ link_to_route('cadastro.pessoas.fidelizacoes.index',
                               'Fidelização',
                               array($pessoa->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                   
              {{ link_to_route('cadastro.pessoas.vinculados.index',
                               'Vinculados',
                               array($pessoa->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                      
              {{ link_to_route('cadastro.pessoas.edit',
                               'Editar',
                               array($pessoa->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.pessoas.destroy', $pessoa->Id))) }}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $pessoas->appends(Input::except('page'))->links(); ?>
@else
  Nenhuma pessoa encontrada.
@endif

@stop