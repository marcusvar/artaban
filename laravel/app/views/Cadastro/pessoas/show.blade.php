@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes da pessoa
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.pessoas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.pessoas.show', array($pessoa->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>
    
  <a href="{{ route('cadastro.pessoas.fidelizacoes.index', array($pessoa->Id)) }}" class="btn btn-success">
      <span class="glyphicon glyphicon-list-alt"></span> Fidelização
  </a>
    
  <a href="{{ route('cadastro.pessoas.vinculados.index', array($pessoa->Id)) }}" class="btn btn-success">
      <span class="glyphicon glyphicon-link"></span> Vinculados
  </a>
  
  <a href="#" class="btn btn-success btn-pessoa-telefones" data-pessoa-id="{{ $pessoa->Id }}" data-recarregar-pagina="true">
    <span class="glyphicon glyphicon-earphone"></span> Telefones
  </a>
  
  <a href="{{ route('cadastro.pessoas.edit', array($pessoa->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.pessoas.destroy', $pessoa->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')

<div class="show">
  {{ $show }}  
</div>
    
@include('Cadastro.pessoas._modal_telefones')
    
@stop