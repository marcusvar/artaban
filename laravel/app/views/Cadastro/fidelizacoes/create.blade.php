@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.pessoas.show', $pessoa->Id) }}">
  <b>{{ $pessoa->Nome }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i>
    Adicionar registro de fidelização (Saldo <b><span style="font-family: Arial">{{ $pessoa->saldoDeFidelizacaoFormatado() }}</span></b>)
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.fidelizacoes.index', $pessoa->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.pessoas.fidelizacoes.create', $pessoa->Id) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
  
  {{ Form::open(array('route' => ['cadastro.pessoas.fidelizacoes.store', $pessoa->Id])) }}
    @include('Cadastro.fidelizacoes._form')
  
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop