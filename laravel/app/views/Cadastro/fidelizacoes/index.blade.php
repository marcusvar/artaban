@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.pessoas.show', $pessoa->Id) }}">
  <b>{{ $pessoa->Nome }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i>
    Fidelização (Saldo <b><span style="font-family: Arial">{{ $pessoa->saldoDeFidelizacaoFormatado() }}</span></b>)
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.fidelizacoes.index', $pessoa->Id) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.pessoas.fidelizacoes.create', $pessoa->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($fidelizacoes->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => array('cadastro.pessoas.fidelizacoes.index', $pessoa->Id))) }}
        
        <div class="row">
          <div class="col-xs-3">
            <div class="form-group">
              <label for="Operacao">Operação:</label>
              {{ Form::select('Operacao',
                              $operacaoOpcoes,
                              null,
                              array('class' => 'form-control chosen-select',
                                    'data-placeholder' => 'Filtrar por operação'))
              }}
            </div>
          </div>
          
          <div class="col-xs-1">
          </div>
            
          <div class="col-xs-3">
            <div class="form-group">
              <label for="DataOperacao">Data inicial:</label>
              {{ Form::text('DataOperacao_inicial', null, array('class' => 'form-control datepicker')) }}
            </div>
          </div>
                      
          <div class="col-xs-3">
            <div class="form-group">
              <label for="DataOperacao">Data final:</label>
              {{ Form::text('DataOperacao_final', null, array('class' => 'form-control datepicker')) }}
            </div>
          </div>  
        </div>
  
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Operação</th>
	      <th>Data</th>
	      <th>Valor</th>
	      <th>Valor da Compra</th>
          <th>Descrição</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($fidelizacoes as $fidelizacao)
          <tr>
	    <td>{{{ $fidelizacao->Operacao }}}</td>
	    <td>{{{ $fidelizacao->DataOperacao }}}</td>
	    <td>{{{ $fidelizacao->ValorOperacao }}}</td>
	    <td>{{{ ($fidelizacao->Operacao === 'Crédito' AND strpos($fidelizacao->Descricao, 'Estorno') !== 0) ? $fidelizacao->valorCompra() : '' }}}</td>
	    <td>{{{ $fidelizacao->descricaoResumida() }}}</td>
	    
	    
        <td>
	      {{ link_to_route('cadastro.pessoas.fidelizacoes.show',
                               'Detalhes',
                               array($pessoa->Id, $fidelizacao->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.pessoas.fidelizacoes.edit',
                               'Editar',
                               array($pessoa->Id, $fidelizacao->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array(
		       'method' => 'DELETE',
		       'route' => array(
		           'cadastro.pessoas.fidelizacoes.destroy',
			   $pessoa->Id,
			   $fidelizacao->Id
			)
		   ))
		}}
                  
		  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                
		{{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $fidelizacoes->appends(Input::except('page'))->links(); ?>
@else
  Nenhum registro de fidelização encontrado.
@endif

@stop