<div class="row">
  <div class="col-xs-3">
    <div class="form-group">
      <label for="Operacao">Operação:</label>
      {{ Form::select('Operacao', $operacaoOpcoes, null, array('class' => 'form-control chosen-select')) }}
    </div>  
  </div>

  <div class="col-xs-3">
    <div class="form-group">
      <label for="DataOperacao">Data:</label>
      {{ Form::text('DataOperacao', null, array('class' => 'form-control datepicker')) }}
    </div>  
  </div>
    
  <div class="col-xs-3">
    <div class="form-group">
      <label for="ValorOperacao">{{$fidelizacao->Operacao === 'Débito' ? 'Valor do bonus:': 'Valor:' }}</label>
      {{ Form::text('ValorOperacao', $valorOperacao, array('class' => 'form-control money-mask')) }}
    </div>  
  </div>
  @if($fidelizacao->Operacao === 'Crédito') 
  <div class="col-xs-3">
    <div class="form-group">
      <label for="ValorOperacao">Valor do bonus:</label>
      <p>{{ $valorBonus }}</p>
    </div>  
  </div>
  @endif
</div>

<div class="row">
  <div class="col-xs-6">
    <div class="form-group">
      <label for="Descricao">Descrição:</label>
      {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
     </div>
  </div>
</div>