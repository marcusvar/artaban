@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.pessoas.show', $pessoa->Id) }}">
  <b>{{ $pessoa->Nome }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i>
   Registro de fidelização (Saldo <b><span style="font-family: Arial">{{ $pessoa->saldoDeFidelizacaoFormatado() }}</span></b>)
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.fidelizacoes.index', $pessoa->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.pessoas.fidelizacoes.create', $pessoa->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.pessoas.fidelizacoes.show', array($pessoa->Id, $fidelizacao->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.pessoas.fidelizacoes.edit', array($pessoa->Id, $fidelizacao->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array(
         'method' => 'DELETE',
         'route' => array(
             'cadastro.pessoas.fidelizacoes.destroy',
             $pessoa->Id,
             $fidelizacao->Id
         ),
         'class' => 'delete-submenu'
     ))
  }}
  
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir
    </a>
      
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show">
   
    <div class="row">
      <div class="col-xs-4">
        <h3>Operação</h3>
        <p>{{{ $fidelizacao->Operacao }}}</p>
      </div>
        
      <div class="col-xs-4">
        <h3>Data</h3>
        <p>{{{ $fidelizacao->DataOperacao }}}</p>
      </div>
        
      <div class="col-xs-4">
         <h3>Valor</h3>
         <p>{{{ $fidelizacao->ValorOperacao }}}</p>
      </div>
      @if($fidelizacao->Operacao === 'Crédito') 
      <div class="col-xs-4">
         <h3>Valor da compra</h3>
         <p>{{{ $fidelizacao->valorCompra() }}}</p>
      </div>
      @endif
    </div>
        
    <h3>Descrição</h3>
    <p>{{{ $fidelizacao->Descricao }}}</p>    
 
  </div>
@stop