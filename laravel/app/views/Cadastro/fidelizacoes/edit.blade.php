@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.pessoas.show', $pessoa->Id) }}">
  <b>{{ $pessoa->Nome }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i>
    Editar registro de fidelização (Saldo <b><span style="font-family: Arial">{{ $pessoa->saldoDeFidelizacaoFormatado() }}</span></b>)
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.fidelizacoes.index', $pessoa->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.pessoas.fidelizacoes.show', array($pessoa->Id, $fidelizacao->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.pessoas.fidelizacoes.edit', array($pessoa->Id, $fidelizacao->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($fidelizacao, array('method' => 'PATCH',
   'route' => array('cadastro.pessoas.fidelizacoes.update', $pessoa->Id, $fidelizacao->Id))) }}
    @include('Cadastro.fidelizacoes._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop