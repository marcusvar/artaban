@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Detalhes do item dessa linha
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.index', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.linhas.itensLinha.create', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.linhas.itensLinha.show',
                     array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>
    
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.index',
                     array($linha->ID, $itemLinha->ID)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list-alt"></span> Tarifas
  </a>
  
  <a href="{{ route('cadastro.linhas.itensLinha.edit',
                    array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array(
         'method' => 'DELETE',
         'route' => array(
             'cadastro.linhas.itensLinha.destroy',
             'linhas' => $linha->ID,
             'itensLinha' => $itemLinha->ID
         ),
         'class' => 'delete-submenu'
     ))
  }}
    
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show">
    
    <div class="row">
      <div class="col-xs-5">
        <h3>Linha (Destino)</h3>
        <p>{{{ $itemLinha->Linha->pontoReferencia->descricaoCompleta() }}}</p>
      </div>
  
      <div class="col-xs-5">
        <h3>Ponto de referência (Origem)</h3>
        <p>{{{ $itemLinha->PontoReferencia->descricaoCompleta() }}}</p>
      </div>
        
      <div class="col-xs-2">
        <h3>Sequência</h3>
        <p>{{{ $itemLinha->Sequencia }}}</p>
      </div>
        
      <div class="col-xs-2">
        <h3>Mostrar na Web?</h3>
        <p>{{{ $itemLinha->webDescricao() }}}</p>
      </div> 
    </div>
        
  </div>
@stop