@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Editar item dessa linha
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.index', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.linhas.itensLinha.show',
                     array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.linhas.itensLinha.edit',
                    array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($itemLinha,
                 array('method' => 'PATCH',
                       'route' => array('cadastro.linhas.itensLinha.update',
                                        'linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID))) }}
                       
    @include('Cadastro.itensLinha._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop