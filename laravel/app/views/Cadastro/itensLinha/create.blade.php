@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Adicionar um item nessa linha
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.index', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.linhas.itensLinha.create', $linha->ID) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
  
  {{ Form::open(array('route' => array('cadastro.linhas.itensLinha.store', $linha->ID))) }}
    @include('Cadastro.itensLinha._form')
  
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop