<div class="row">
  <div class="col-xs-8">
    <div class="form-group">
      <label for="PontoReferencia_Id">Ponto de referência (Origem):</label>
  
      {{ Form::select('PontoReferencia_Id',
                      $pontosReferencia,
		              null,
                      array(
		                  'class' => 'form-control chosen-select',
                          'data-placeholder' => 'Selecione um ponto de referência'
		              )
         )
      }}
    </div>
  </div>
        
    <div class="col-xs-2">
        <div class="form-group">
            <label for="Sequencia">Sequência:</label>
            {{ Form::text('Sequencia', null, array('class' => 'form-control')) }}
        </div>
    </div>  
    
    <div class="col-xs-3">
      <div class="form-group">
        <label for="Web">Mostrar na Web?</label>
        {{ Form::select('Web', $webOpcoes, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>  
</div>