@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Itens da linha 
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.index', $linha->ID) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.linhas.itensLinha.create', $linha->ID) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($itensLinha->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array(
             'method' => 'GET',
             'route' => array('cadastro.linhas.itensLinha.index', $linha->ID)
	     ))
      }}
        
        <div class="row">
          <div class="col-xs-8">
            <div class="form-group">
              <label for="PontoReferencia_Id">Ponto de referência (Origem):</label>
              {{ Form::select('PontoReferencia_Id',
                              $pontosReferencia,
	        	              null,
                              array(
		                          'class' => 'form-control chosen-select',
                                  'data-placeholder' => 'Filtrar por ponto de referência'
		                      )
                 )
              }}
            </div>
          </div>
            
          <div class="col-xs-2">
            <div class="form-group">
              <label for="Sequencia">Sequência:</label>
              {{ Form::text('Sequencia', null, array('class' => 'form-control')) }}
            </div>
          </div>
        </div>
    
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Ponto de referência (Origem)</th>
          <th>Sequência</th>
	      <th>Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($itensLinha as $itemLinha)
          <tr>
	        <td>{{{ $itemLinha->pontoReferencia->descricaoCompleta() }}}</td>
            <td>{{{ $itemLinha->Sequencia }}}</td>
            <td>
	          {{ link_to_route('cadastro.linhas.itensLinha.show',
                               'Detalhes',
                               array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.linhas.itensLinha.tarifas.index',
                               'Tarifas',
                               array($linha->ID, $itemLinha->ID),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
              	      
	          {{ link_to_route('cadastro.linhas.itensLinha.edit',
                               'Editar',
                               array('linhas' => $linha->ID, 'itensLinha' => $itemLinha->ID),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array(
	                   'method' => 'DELETE',
	                   'route' => array(
		                   'cadastro.linhas.itensLinha.destroy',
		                   'linhas' => $linha->ID,
			               'itensLinha' => $itemLinha->ID,
                       )
		           ))
	            }}
        
	            <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
        
	            {{ Form::close() }}
               </div>
             </td>
           </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $itensLinha->appends(Input::except('page'))->links(); ?>
@else
  Nenhum item dessa linha encontrado.
@endif

@stop