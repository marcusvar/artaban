@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes da linha
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.linhas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.linhas.show', array($linha->ID)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.linhas.itensLinha.index', array($linha->ID)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list-alt"></span> Itens
  </a>
    
  <a href="{{ route('cadastro.linhas.adicionaisTarifa.index', array($linha->ID)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list-alt"></span> Adicionais de Tarifa
  </a>
    
  <a href="{{ route('cadastro.linhas.edit', array($linha->ID)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.linhas.destroy', $linha->ID),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show">
    
    <div class="row">
      <div class="col-xs-6">
        <h3>Ponto de referência (destino)</h3>
        <p>{{{ $linha->pontoReferencia->descricaoCompleta() }}}</p>
      </div>
        
      <div class="col-xs-6">
        <h3>Ponto de referência (via)</h3>
        <p>{{{ $linha->pontoReferenciaVia->descricaoCompleta() }}}</p>
      </div>
    </div>
        
    <div class="row">
      <div class="col-xs-6">
        <h3>Situação</h3>
        <p>{{{ $linha->Situacao }}}</p>
      </div>
    </div>
  
  </div>
@stop