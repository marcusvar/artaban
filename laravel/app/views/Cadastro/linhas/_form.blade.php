  <div class="row">
    <div class="col-xs-6">
      <div class="form-group">
        <label for="PontoReferencia_Id_Destino">Ponto de referência (destino):</label>
        {{ Form::select('PontoReferencia_Id_Destino',
                        $pontosReferencia,
		                null,
                        array(
		                    'class' => 'form-control chosen-select',
                            'data-placeholder' => 'Selecione um ponto de referência'
		                )
           )
        }}
      </div>   
    </div>
    
    <div class="col-xs-6">
      <div class="form-group">
        <label for="PontoReferencia_Id_Via">Ponto de referência (via):</label>
        {{ Form::select('PontoReferencia_Id_Via',
                        $pontosReferencia,
		                null,
                        array(
		                    'class' => 'form-control chosen-select',
                            'data-placeholder' => 'Selecione um ponto de referência'
		                )
           )
        }}
      </div>   
    </div>
  </div>
    
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Situacao">Situação:</label>
        {{ Form::select('Situacao', $situacoes, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>
  </div> 