@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Linhas
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.linhas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($linhas->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.linhas.index')) }}
      
      <div class="row">
        <div class="col-xs-6">
          <div class="form-group">
            <label for="PontoReferencia_Id_Destino">Ponto de referência (destino):</label>
            {{ Form::select('PontoReferencia_Id_Destino',
                            $pontosReferencia,
		                    null,
                            array('class' => 'form-control chosen-select',
                                 'data-placeholder' => 'Filtrar por ponto de referência'))
            }}
          </div>
        </div> 
        
        <div class="col-xs-6">
          <div class="form-group">
            <label for="PontoReferencia_Id_Via">Ponto de referência (via):</label>
            {{ Form::select('PontoReferencia_Id_Via',
                            $pontosReferencia,
		                    null,
                            array('class' => 'form-control chosen-select',
                                  'data-placeholder' => 'Filtrar por ponto de referência'))
            }}
          </div>
        </div>
      </div>    
     
      <div class="row">
        <div class="col-xs-4">
          <div class="form-group">
            <label for="Situacao">Situação:</label>
            {{ Form::select('Situacao',
                            $situacoes,
                            null,
                            array('class' => 'form-control chosen-select',
                                  'data-placeholder' => 'Filtrar por situação')) }}
          </div>
        </div>
      </div>
    
      {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Ponto de referência (destino)</th>
          <th>Situação</th>
	      <th>Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($linhas as $linha)
          <tr>
	        <td>{{{ $linha->pontoReferencia->descricaoCompleta() }}}</td>
             <td>{{{ $linha->Situacao }}}</td>
	         <td>
	          {{ link_to_route('cadastro.linhas.show',
                               'Detalhes',
                               array($linha->ID),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
              
	          {{ link_to_route('cadastro.linhas.itensLinha.index',
                               'Itens',
                               array('linhas' => $linha->ID),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
              
	          {{ link_to_route('cadastro.linhas.adicionaisTarifa.index',
                               'Adicionais de Tarifa',
                               array('linhas' => $linha->ID),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
              	         		                       
              {{ link_to_route('cadastro.linhas.edit',
                               'Editar',
                               array($linha->ID),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
              
	          <div class="pull-left">
                {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.linhas.destroy', $linha->ID))) }}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $linhas->appends(Input::except('page'))->links(); ?>
@else
  Nenhuma linha encontrada.
@endif

@stop