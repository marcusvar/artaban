@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar ponto de referência
@stop


@section('submenu')
  <a href="{{ route('cadastro.pontosReferencia.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.pontosReferencia.show', array($pontoReferencia->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.pontosReferencia.edit', array($pontoReferencia->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($pontoReferencia, array('method' => 'PATCH', 'route' => array('cadastro.pontosReferencia.update', $pontoReferencia->Id))) }}
    @include('Cadastro.pontosReferencia._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop