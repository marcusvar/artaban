@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Adicionar ponto de referência
@stop


@section('submenu')
  <a href="{{ route('cadastro.pontosReferencia.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.pontosReferencia.create') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
  
  {{ Form::open(array('route' => 'cadastro.pontosReferencia.store')) }}
    @include('Cadastro.pontosReferencia._form')
  
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop