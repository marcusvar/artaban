  <div class="row">
    <div class="col-xs-8">
      <div class="form-group">
        <label for="Descricao">Descrição:</label>
        {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
      </div>  
    </div>
  
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Tipo">Tipo:</label>
        {{ Form::select('Tipo',
                        $tipos,
                        null,
                        array('class' => 'form-control chosen-select',
                              'data-placeholder' => ' ')) }}
      </div>  
    </div>
  </div>
  
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Pais_Id">País:</label>
        {{ Form::select('Pais_Id',
                        $paises,
                        $paisSelecionadoId,
                        array('class' => 'form-control chosen-select pais-select',
                              'data-placeholder' => 'Selecione o país',
                              'data-estado-id' => 'Estado_Id',
                              'data-cidade-id' => 'Localidade_Id')) }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Estado_Id">Estado:</label>
        {{ Form::select('Estado_Id',
                         $estados,
                         $estadoSelecionadoId,
                         array('class' => 'form-control chosen-select estado-select',
                               'id' => 'Estado_Id',
                               'data-placeholder' => 'Selecione o estado',
                               'data-children' => 'Localidade_Id' )) }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Localidade_Id">Localidade:</label>
        {{ Form::select('Localidade_Id',
                         $localidades,
                         null,
                         array('class' => 'form-control chosen-select',
                                'id' => 'Localidade_Id',
                                'data-placeholder' => 'Selecione a localidade')) }}
      </div>
    </div>  
  </div>