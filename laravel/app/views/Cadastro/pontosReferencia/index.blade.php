@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Pontos de referência
@stop


@section('submenu')
  <a href="{{ route('cadastro.pontosReferencia.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.pontosReferencia.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  
  @if ($pontosReferencia->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.pontosReferencia.index')) }}
     
       <div class="row">
         <div class="col-xs-8">
           <div class="form-group">
             <label for="Descricao">Descrição:</label>
             {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
           </div>         
         </div>
          
         <div class="col-xs-4">
           <div class="form-group">
             <label for="Tipo">Tipo:</label>
             {{ Form::select('Tipo',
                             $tipos,
                             null,
                             array('class' => 'form-control chosen-select',
                                   'data-placeholder' => 'Filtrar por tipo')) }}
           </div>
         </div>
       </div>
       
       <div class="row">
         <div class="col-xs-4">
           <div class="form-group">
             <label for="Pais_Id">País:</label>
             {{ Form::select('Pais_Id',
                        $paises,
                        null,
                        array('class' => 'form-control chosen-select pais-select',
                              'data-placeholder' => 'Filtrar por país',
                              'data-estado-id' => 'Estado_Id',
                              'data-cidade-id' => 'Localidade_Id')) }}
           </div>
         </div>
    
         <div class="col-xs-4">
           <div class="form-group">
             <label for="Estado_Id">Estado:</label>
             {{ Form::select('Estado_Id',
                         [],
                         null,
                         array('class' => 'form-control chosen-select estado-select',
                               'id' => 'Estado_Id',
                               'data-placeholder' => 'Filtrar por estado',
                               'data-children' => 'Localidade_Id' )) }}
           </div>
         </div>
    
         <div class="col-xs-4">
           <div class="form-group">
             <label for="Localidade_Id">Localidade:</label>
             {{ Form::select('Localidade_Id',
                         [],
                         null,
                         array('class' => 'form-control chosen-select',
                                'id' => 'Localidade_Id',
                                'data-placeholder' => 'Filtrar por localidade')) }}
           </div>
         </div>  
       </div>
    
       {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
   
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Descrição</th>
	      <th>Tipo</th>
	      <th>Localidade</th>
	      <th>Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($pontosReferencia as $pontoReferencia)
          <tr>
	        <td>{{{ $pontoReferencia->Descricao }}}</td>
	        <td>{{{ $pontoReferencia->Tipo }}}</td>
	        <td>{{{ $pontoReferencia->localidade->Nome }}}</td>
            
	        <td>
	          {{ link_to_route('cadastro.pontosReferencia.show',
                               'Detalhes',
                               array($pontoReferencia->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.pontosReferencia.edit',
                               'Editar',
                               array($pontoReferencia->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.pontosReferencia.destroy', $pontoReferencia->Id))) }}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $pontosReferencia->appends(Input::except('page'))->links(); ?>
@else
  Nenhum ponto de referência encontrado.
@endif

@stop