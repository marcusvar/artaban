@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do ponto de referência
@stop


@section('submenu')
  <a href="{{ route('cadastro.pontosReferencia.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.pontosReferencia.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.pontosReferencia.show', array($pontoReferencia->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.pontosReferencia.edit', array($pontoReferencia->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.pontosReferencia.destroy', $pontoReferencia->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    
    <div class="row">
      <div class="col-xs-6">
        <h3>Descrição</h3>
        <p>{{{ $pontoReferencia->Descricao }}}</p>
      </div>
        
      <div class="col-xs-6">
        <h3>Tipo</h3>
        <p>{{{ $pontoReferencia->Tipo }}}</p>
      </div>  
    </div>
    
    <div class="row">
      <div class="col-xs-4">
        <h3>País</h3>
        <p>{{{ $pais->Nome }}}</p>
      </div>
        
      <div class="col-xs-4">
        <h3>Estado</h3>
        <p>{{{ $estado->Nome }}}</p>
      </div>
      
      <div class="col-xs-4">
        <h3>Localidade</h3>
        <p>{{{ $localidade->Nome }}}</p>
      </div>
    </div>
  
  </div>
@stop