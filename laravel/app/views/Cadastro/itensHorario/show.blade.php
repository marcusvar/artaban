@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.horarios.show', $horario->Id) }}">
  <b>{{ $horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Detalhes do item desse horário
@stop


@section('submenu')
  <a href="{{ route('cadastro.horarios.itensHorario.index', $horario->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.horarios.itensHorario.create', $horario->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.horarios.itensHorario.show',
                    array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.horarios.itensHorario.edit',
                     array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array(
                          'cadastro.horarios.itensHorario.destroy',
                          'horarios' => $horario->Id,
                          'itensHorario' => $itemHorario->Id
                       ),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir
    </a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show">
  
    <div class="row">
      <div class="col-xs-6">
        <h3>Ponto de embarque</h3>
        <p>{{{ $itemHorario->descricaoDaLinha() }}}</p>
      </div>

      <div class="col-xs-6">
        <h3>Ponto de baldeação</h3>
        <p>{{{ $itemHorario->descricaoTransbordo() }}}</p>
      </div>
        
      <div class="col-xs-2">
        <h3>Recolher?</h3>
        <p>{{{ $itemHorario->recolherDescricao() }}}</p>
      </div>  
  
    </div>
      
    <div class="row">
      <div class="col-xs-6">
        <h3>Dia de embarque nesse ponto</h3>
        <p>{{{ $itemHorario->diaEmbarqueDescricao() }}}</p>
      </div>
      
      <div class="col-xs-6">
        <h3>Horário de embarque nesse ponto</h3>
        <p>{{{ $itemHorario->HoraEmbarque }}}</p>
      </div>
      <div class="col-xs-2">
        <h3>Mostrar na Web?</h3>
        <p>{{{ $itemHorario->webDescricao() }}}</p>
      </div>  
    </div>  
    
  </div>
@stop