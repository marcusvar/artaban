@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.horarios.show', $horario->Id) }}">
  <b>{{ $horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Adicionar um item nesse horário
@stop


@section('submenu')
  <a href="{{ route('cadastro.horarios.itensHorario.index', $horario->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.horarios.itensHorario.create', $horario->Id) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
  
  {{ Form::open(array('route' => array('cadastro.horarios.itensHorario.store', $horario->Id) )) }}
    @include('Cadastro.itensHorario._form')
  
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop