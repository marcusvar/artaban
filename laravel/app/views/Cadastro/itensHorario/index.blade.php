@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.horarios.show', $horario->Id) }}">
  <b>{{ $horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Itens do horário
@stop


@section('submenu')
  <a href="{{ route('cadastro.horarios.itensHorario.index', $horario->Id) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.horarios.itensHorario.create', $horario->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($itensHorario->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET',
                         'route' => array('cadastro.horarios.itensHorario.index', $horario->Id))) }}
  
        <div class="form-group">
          <label for="LinhaItem_Id">Item da linha:</label>
          {{ Form::select('LinhaItem_Id',
                          $itensLinha,
                          null,
                          array('class' => 'form-control chosen-select',
                                'data-placeholder' => 'Filtar por ponto de embarque')) }}
        </div>
  
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Ponto de embarque</th>
          <th>Dia de embarque</th>
          <th>Horário de embarque</th>  
	      <th>Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($itensHorario as $itemHorario)
          <tr>
	        <td>{{{ $itemHorario->itemLinha->pontoReferencia->descricaoCompleta() }}}</td>
	        <td>{{{ $itemHorario->diaEmbarqueDescricao() }}}</td>
            <td>{{{ $itemHorario->HoraEmbarque }}}</td>
	        
            <td>
	          {{ link_to_route('cadastro.horarios.itensHorario.show',
                               'Detalhes',
                               array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.horarios.itensHorario.edit',
                               'Editar',
                               array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    		    
              <div class="pull-left">
                {{ Form::open(array(
		               'method' => 'DELETE',
			           'route' => array(
			               'cadastro.horarios.itensHorario.destroy',
			               'horarios' => $horario->Id,
			               'itensHorario' => $itemHorario->Id
			           )
		           ))
		        }}
                          
		          <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                  
		        {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $itensHorario->appends(Input::except('page'))->links(); ?>
@else
  Nenhum item desse horário encontrado.
@endif

@stop