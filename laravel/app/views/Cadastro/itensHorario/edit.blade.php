@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.horarios.show', $horario->Id) }}">
  <b>{{ $horario->descricaoDaLinhaComHorario() }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Editar item desse horário
@stop


@section('submenu')
  <a href="{{ route('cadastro.horarios.itensHorario.index', $horario->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.horarios.itensHorario.show',
                    array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.horarios.itensHorario.edit',
                     array('horarios' => $horario->Id, 'itensHorario' => $itemHorario->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($itemHorario, array('method' => 'PATCH',
                                     'route' => array(
                                         'cadastro.horarios.itensHorario.update',
                                         'horarios' => $horario->Id,
                                         'itensHorario' => $itemHorario->Id
                                      ))) }}
    
    @include('Cadastro.itensHorario._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop