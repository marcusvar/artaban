  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="LinhaItem_Id">Ponto de embarque:</label>
        {{ Form::select('LinhaItem_Id',
                        $itensLinha,
                        $itemLinhaSelecionadaId,
                        array('class' => 'form-control chosen-select',
                              'data-placeholder' => 'Selecione a linha'))
        }}
      </div>
    </div>

    <div class="col-xs-4">
      <div class="form-group">
        <label for="LinhaItem_Id_Transbordo">Ponto de baldeação:</label>
        {{ Form::select('LinhaItem_Id_Transbordo',
                        $itensLinhaTransbordo,
                        $itemLinhaTransbordoId,
                        array('class' => 'form-control chosen-select',
                              'data-placeholder' => 'Selecione a linha'))
        }}
      </div>
    </div>
      
    <div class="col-xs-2">
      <div class="form-group">
        <label for="Recolher">Recolher?</label>
        {{ Form::select('Recolher', $recolherOpcoes, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>  

  </div>
  
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="DiaEmbarque">Dia de embarque nesse ponto:</label>
        {{ Form::select('DiaEmbarque',
                        $diasEmbarque,
                        null,
                        array('class' => 'form-control chosen-select'))
        }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="HoraEmbarque">Horário de embarque nesse ponto:</label>
        {{ Form::text('HoraEmbarque', null, array('class' => 'form-control horario-mask')) }}
      </div>
    </div>
      
    <div class="col-xs-3">
      <div class="form-group">
        <label for="Web">Mostrar na Web?</label>
        {{ Form::select('Web', $webOpcoes, null, array('class' => 'form-control chosen-select')) }}
      </div>
    </div>  
      
  </div>    