@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i>
  <a href="{{ route('cadastro.linhas.itensLinha.show', [$linha->ID, $itemLinha->ID]) }}">
  <b>{{ $itemLinha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Adicionar tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.index', [$linha->ID, $itemLinha->ID]) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.create', [$linha->ID, $itemLinha->ID]) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @include('shared._error_message')
  
  {{ Form::open(array('route' => array('cadastro.linhas.itensLinha.tarifas.store', $linha->ID, $itemLinha->ID))) }}
    @include('Cadastro.tarifas._form')
  
    {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop