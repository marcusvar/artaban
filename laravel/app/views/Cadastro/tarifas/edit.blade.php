@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i>
  <a href="{{ route('cadastro.linhas.itensLinha.show', [$linha->ID, $itemLinha->ID]) }}">
  <b>{{ $itemLinha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Editar tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.index', [$linha->ID, $itemLinha->ID]) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.show', [$linha->ID, $itemLinha->ID, $tarifa->Id]) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.edit', [$linha->ID, $itemLinha->ID, $tarifa->Id]) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($tarifa,
                 array(
                     'method' => 'PATCH',
                     'route' => array(
                         'cadastro.linhas.itensLinha.tarifas.update',
                         $linha->ID, $itemLinha->ID, $tarifa->Id
                     )
                 ))
  }}
    
    @include('Cadastro.tarifas._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop