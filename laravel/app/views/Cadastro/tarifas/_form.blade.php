<div class="row">
  <div class="col-xs-4">
    <div class="form-group">
      <label for="TipoVeiculo_Id">Tipo de veículo:</label>
      {{ Form::select('TipoVeiculo_Id', $tiposVeiculo, null, array('class' => 'form-control chosen-select')) }}
    </div>
  </div>
  
  <div class="col-xs-4">
    <div class="form-group"> 
      <label for="DataVigencia">Data da vigência:</label>
      {{ Form::text('DataVigencia', null, array('class' => 'form-control datepicker')) }}
    </div>
  </div>
</div>

<div class="radio">
  <label>
    <input type="radio" name="TipoVeiculoTarifa" id="tipo-veiculo-tarifa-unica"
           value="unica" {{ Artaban\Helper\View\Tarifa::checkedTarifaUnica(isset($tarifa) ? $tarifa : null,
                                                                           Input::old('TipoVeiculoTarifa')) }}>
    
    Definir uma tarifa única para todo o veículo
  </label>
</div>
    
<div class="radio">
  <label>
    <input type="radio" name="TipoVeiculoTarifa" id="tipo-veiculo-tarifa-mista"
           value="mista" {{ Artaban\Helper\View\Tarifa::checkedTarifaMista(isset($tarifa) ? $tarifa : null,
                                                                           Input::old('TipoVeiculoTarifa')) }}>
    
    Definir uma tarifa para cada piso/lado do veículo 
  </label>
</div>


<div id="box-tipo-veiculo-tarifas" class="row" style="margin-top: 20px">
  <div id="box-tipo-veiculo-tarifa-unica" class="col-xs-4">
    <div class="form-group">
      <label for="Valor"><!--Valor da tarifa (completo/superior/esquerdo):-->
          {{ Artaban\Helper\View\Tarifa::labelCampoValor(isset($tarifa) ? $tarifa : null, Input::old('TipoVeiculoTarifa')) }}
      </label>
      
      {{ Form::text('Valor', null, array('class' => 'form-control money-mask')) }}
    </div>
  </div>
  
  <div id="box-tipo-veiculo-tarifa-mista"
       class="col-xs-4" {{ Artaban\Helper\View\Tarifa::displayTarifaMista(isset($tarifa) ? $tarifa : null,
                                                                          Input::old('TipoVeiculoTarifa')) }}>
    
    <div class="form-group">
      <label for="Valor2">Valor da tarifa - piso inferior ou lado direito:</label>
      {{ Form::text('Valor2', null, array('class' => 'form-control money-mask')) }}          
    </div>
  </div>
</div>