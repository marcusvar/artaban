@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
    <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i>
  
  <a href="{{ route('cadastro.linhas.itensLinha.show', [$linha->ID, $itemLinha->ID]) }}">
    <b>{{ $itemLinha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Tarifas
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.index', [$linha->ID, $itemLinha->ID]) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.create', [$linha->ID, $itemLinha->ID]) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($tarifas->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
     {{ Form::open(array('method' => 'GET',
                         'route' => ['cadastro.linhas.itensLinha.tarifas.index', $linha->ID, $itemLinha->ID])) }}
    
      <div class="row">
        <div class="col-xs-4">
          <div class="form-group">
            <label for="TipoVeiculo_Id">Tipo de veículo:</label>
            {{ Form::select('TipoVeiculo_Id',
                            $tiposVeiculo,
                            null,
                            array('class' => 'form-control chosen-select',
                                  'data-placeholder' => 'Filtrar por tipo de veículo')) }}
          </div>  
        </div>
      
       <div class="col-xs-4">
         <div class="form-group">
           <label for="DataInicio">Data da vigência (início):</label>
           {{ Form::text('DataVigencia_inicial', null, array('class' => 'form-control datepicker')) }}
         </div>
       </div>
	
       <div class="col-xs-4">
         <div class="form-group">
         <label for="DataInicio">Data da vigência (fim):</label>
         {{ Form::text('DataVigencia_final', null, array('class' => 'form-control datepicker')) }}
         </div>
       </div>
     </div>   
         
       {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
        
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Item da linha</th>
		  <th>Tipo de veículo</th>
		  <th>Data da vigência</th>
		  <th>Tarifa(s)</th>
		  <th>Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($tarifas as $tarifa)
        <tr>
	      <td>{{{ $tarifa->itemLinha->pontoReferencia->descricaoCompleta() }}}</td>
		  <td>{{{ $tarifa->tipoVeiculo->Descricao }}}</td>
		  <td>{{{ $tarifa->DataVigencia }}}</td>
		  
          <td>
            @if ($tarifa->unica())
              {{{ $tarifa->Valor }}}
            @else
              {{{ 'superior/esquerdo:' }}} <br />
              {{{ $tarifa->Valor }}} <br />
              {{{ 'inferior/direito:' }}} <br />
              {{{ $tarifa->Valor2 }}} 

            @endif            
          </td>
                
		  <td>
            {{ link_to_route('cadastro.linhas.itensLinha.tarifas.show',
                             'Detalhes',
                             array($linha->ID, $itemLinha->ID, $tarifa->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
            {{ link_to_route('cadastro.linhas.itensLinha.tarifas.edit',
                             'Editar',
                             array($linha->ID, $itemLinha->ID, $tarifa->Id),
                             array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
            <div class="pull-left">
              {{ Form::open(array('method' => 'DELETE',
		                          'route' => array(
                                      'cadastro.linhas.itensLinha.tarifas.destroy',
					                  $linha->ID, $itemLinha->ID, $tarifa->Id
                                  ))) }}
                        
                <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
              
              {{ Form::close() }}
            </div>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $tarifas->appends(Input::except('page'))->links(); ?>
@else
  Nenhuma tarifa encontrada.
@endif

@stop