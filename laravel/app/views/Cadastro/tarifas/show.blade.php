@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.linhas.show', $linha->ID) }}">
  <b>{{ $linha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i>
  <a href="{{ route('cadastro.linhas.itensLinha.show', [$linha->ID, $itemLinha->ID]) }}">
  <b>{{ $itemLinha->pontoReferencia->descricaoCompleta() }}</b>
  </a> <i class="icon-fixed-width icon-angle-right"></i> Detalhes da tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.index', [$linha->ID, $itemLinha->ID]) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.create', [$linha->ID, $itemLinha->ID]) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.show', [$linha->ID, $itemLinha->ID, $tarifa->Id]) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.linhas.itensLinha.tarifas.edit', [$linha->ID, $itemLinha->ID, $tarifa->Id]) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array(
         'method' => 'DELETE',
         'route' => array('cadastro.linhas.itensLinha.tarifas.destroy', $linha->ID, $itemLinha->ID, $tarifa->Id),
         'class' => 'delete-submenu'
     ))
  }}
  
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    <h2>Item da linha</h2>
    <p>{{{ $tarifa->itemLinha->pontoReferencia->descricaoCompleta() }}}</p>

    <div class="row">
      <div class="col-xs-3">
        <h3>Tipo de veículo</h3>
        <p>{{{ $tarifa->tipoVeiculo->Descricao }}}</p>
      </div>
   
      <div class="col-xs-3">
        <h3>Data da vigência</h3>
        <p>{{{ $tarifa->DataVigencia }}}</p>
      </div>
      
      @if ($tarifa->unica())
        <div class="col-xs-3">
          <h3>Valor da tarifa</h3>
          <p>{{{ $tarifa->Valor }}}</p>
        </div>      
      @else
        <div class="col-xs-3">
          <h3>Valor da tarifa - superior/esquerdo</h3>
          <p>{{{ $tarifa->Valor }}}</p>
        </div>
        
        <div class="col-xs-3">
          <h3>Valor da tarifa - inferior/direito</h3>
          <p>{{{ $tarifa->Valor2 }}}</p>
        </div>    
      @endif
      
    </div>    
  </div>
@stop