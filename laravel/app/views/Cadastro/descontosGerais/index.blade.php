@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Descontos gerais
@stop


@section('submenu')
  <a href="{{ route('cadastro.descontosGerais.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.descontosGerais.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($descontosGerais->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.descontosGerais.index')) }}
        <div class="row">
          <div class="col-xs-6">
            <div class="form-group">
              <label for="Descricao">Descrição:</label>
              {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
            </div>
          </div>
          
          <div class="col-xs-2">
            <div class="form-group">
              <label for="Confirmado">Confirmado?</label>
              {{ Form::select('Confirmado',  $confirmadoOpcoes, null, array('class' => 'form-control')) }}
            </div>
          </div>
            
          <div class="col-xs-4">
            <div class="form-group">
              <label for="AgenciaWeb">Tipo:</label>
              {{ Form::select('AgenciaWeb',  $agenciaWebOpcoes, null, array('class' => 'form-control')) }}
            </div>
          </div>
        </div>
        
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Descrição</th>
	  <th>Percentagem</th>
	  <th>Confirmado?</th>
	  <th>Tipo</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($descontosGerais as $desconto)
          <tr>
	        <td>{{{ $desconto->Descricao }}}</td>
		    <td>{{{ $desconto->PercentagemDescricao }}}</td>
		    <td>{{{ $desconto->ConfirmadoDescricao }}}</td>
		    <td>{{{ $desconto->AgenciaWebDescricao }}}</td>
            <td>{{ link_to_route('cadastro.descontosGerais.show',
                                 'Detalhes',
                                 array($desconto->Id),
                                 array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
                {{ link_to_route('cadastro.descontosGerais.edit',
                                 'Editar',
                                 array($desconto->Id),
                                 array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
                <div class="pull-left">
                  {{ Form::open(array('method' => 'DELETE',
                                      'route' => array('cadastro.descontosGerais.destroy', $desconto->Id))) }}
                    <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                  {{ Form::close() }}
                </div>
              </td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $descontosGerais->appends(Input::except('page'))->links(); ?>
@else
    Nenhum desconto encontrado.
@endif

@stop