@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar desconto geral
@stop


@section('submenu')
  <a href="{{ route('cadastro.descontosGerais.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.descontosGerais.show', array($descontoGeral->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.descontosGerais.edit', array($descontoGeral->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($descontoGeral, array('method' => 'PATCH',
                                       'route' => array('cadastro.descontosGerais.update', $descontoGeral->Id))) }}
    
    @include('Cadastro.descontosGerais._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop