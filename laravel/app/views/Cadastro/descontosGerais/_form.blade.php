  <div class="form-group">
    <label for="Descricao">Descrição:</label>
    {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
  </div>
  
  <div class="row">
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Percentagem">Percentagem:</label>
        {{ Form::text('Percentagem', null, array('class' => 'form-control')) }}
      </div>
    </div>
    
    <div class="col-xs-4">
      <div class="form-group">
        <label for="Confirmado">Confirmado?</label>
        {{ Form::select('Confirmado', $confirmadoOpcoes, null, array('class' => 'form-control')) }}
      </div>
    </div>
        
    <div class="col-xs-4">
      <div class="form-group">
        <label for="AgenciaWeb">Tipo:</label>
        {{ Form::select('AgenciaWeb',  $agenciaWebOpcoes, null, array('class' => 'form-control')) }}
      </div>  
    </div>
  </div>