@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do desconto geral
@stop


@section('submenu')
  <a href="{{ route('cadastro.descontosGerais.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.descontosGerais.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.descontosGerais.show', array($descontoGeral->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.descontosGerais.edit', array($descontoGeral->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.descontosGerais.destroy', $descontoGeral->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    <div class="row">
      <div class="col-xs-12">
        <h3>Descrição</h3>
        <p>{{{ $descontoGeral->Descricao }}}</p>
      </div>
      
      <div class="col-xs-4">
        <h3>Percentagem</h3>
        <p>{{{ $descontoGeral->PercentagemDescricao }}}</p>
      </div>
        
      <div class="col-xs-4">
        <h3>Confirmado?</h3>
        <p>{{{ $descontoGeral->ConfirmadoDescricao }}}</p>
      </div>
        
      <div class="col-xs-4">
        <h3>Tipo</h3>
        <p>{{{ $descontoGeral->AgenciaWebDescricao }}}</p>
      </div>
    </div>
  </div>
@stop