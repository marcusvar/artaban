@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.pessoas.show', $pessoa->Id) }}">
    <b>{{ $pessoa->Nome }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Adicionar vinculado
@stop


@section('submenu')
  <a href="{{ route('cadastro.pessoas.vinculados.index', $pessoa->Id) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.pessoas.vinculados.create', $pessoa->Id) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  
  <a href="#" class="search-button">
    <span class="glyphicon glyphicon-search"></span> Opções de filtros
  </a>
 
  <div id="form-search">    
    {{ Form::open(array('method' => 'GET', 'route' => ['cadastro.pessoas.vinculados.create', $pessoa->Id])) }}
        
      <div class="form-group">
        <label for="Nome">Nome:</label>
        {{ Form::text('Nome', null, array('class' => 'form-control')) }}
      </div>
        
      {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
  
    {{ Form::close() }}
  </div>
  
  @if ($pessoas->count())
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	      <th>Nome</th>
	      <th>Documento</th>
	      <th>RG</th>
	      <th style="width: 15%">Ações</th>
	    </tr>
      </thead>

      <tbody>
        @foreach ($pessoas as $p)
          <tr>
	        <td>{{{ $p->Nome }}}</td>
		    <td>{{{ $p->Documento }}}</td>
		    <td>{{{ $p->RGIE }}}</td>
					
            <td>
              <div class="pull-left">
                {{ Form::open(array('method' => 'POST', 'route' => array('cadastro.pessoas.vinculados.store', $pessoa->Id))) }}
                  {{ Form::hidden('Pessoa_Id_Vinculo', $p->Id) }}
                  {{ Form::submit('Vincular', array('class' => 'btn btn-default btn-sm botao-padrao')) }}   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $pessoas->appends(Input::except('page'))->links(); ?>
@else
  Nenhuma pessoa encontrada.
@endif 
 
@stop