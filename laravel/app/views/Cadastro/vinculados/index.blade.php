@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  <a href="{{ route('cadastro.pessoas.show', $pessoa->Id) }}">
    <b>{{ $pessoa->Nome }}</b>
  </a><i class="icon-fixed-width icon-angle-right"></i> Vinculados
@stop


@section('submenu')
  @if ($pessoa->podeVincularPessoas())
    <a href="{{ route('cadastro.pessoas.vinculados.index', $pessoa->Id) }}" class="btn btn-success active">
      <span class="glyphicon glyphicon-home"></span> Principal
    </a>
    
    <a href="{{ route('cadastro.pessoas.vinculados.create', $pessoa->Id) }}" class="btn btn-success">
      <span class="glyphicon glyphicon-plus"></span> Adicionar
    </a>    
  @endif  
@stop


@section('principal')
  
  @if (! $pessoa->podeVincularPessoas())
    <div class="alert alert-warning">
      <p>Antes de vincular pessoas, será necessário informar o CPF de <b>{{ $pessoa->Nome }}</b>.</p>
      <br> 
      
      {{ link_to_route('cadastro.pessoas.edit',
                       'Atualizar cadastro',
                       array($pessoa->Id),
                       array('class' => 'btn btn-default btn-sm botao-padrao'))
      }}          
    </div>
  @else  
    
    @if ($vinculados->count())
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
	        <th>Nome</th>
	        <th>Documento</th>
	        <th>RG</th>
	        <th style="width: 25%">Ações</th>
	      </tr>
        </thead>

        <tbody>
          @foreach ($vinculados as $vinculado)
            <tr>
	          <td>{{{ $vinculado->Nome }}}</td>
		      <td>{{{ $vinculado->Documento }}}</td>
		      <td>{{{ $vinculado->RGIE }}}</td>
					
              <td>
                {{ link_to_route('cadastro.pessoas.show',
                               'Ver cadastro',
                               array($vinculado->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao', 'target' => '_blank'))
                }}
            
                <div class="pull-left">
                  {{ Form::open(
                       array(
                           'method' => 'DELETE',
                           'route' => array('cadastro.pessoas.vinculados.destroy', $pessoa->Id, $vinculado->Id)
                       )
                   )    
				  }}
                
                    <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                
                  {{ Form::close() }}
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    @else
      Nenhuma pessoa vinculada.
    @endif
  @endif

@stop