@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Estados
@stop


@section('submenu')
  <a href="{{ route('cadastro.estados.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.estados.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($estados->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.estados.index')) }}
     
        <div class="form-group">
          <label for="descricao">Nome:</label>
          {{ Form::text('Nome', null, array('class' => 'form-control')) }}
        </div>
  
        <div class="form-group">
          <label for="sigla">Sigla:</label>
          {{ Form::text('Sigla', null, array('class' => 'form-control')) }}
        </div>
   
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Nome</th>
				<th>Sigla</th>
				<th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($estados as $estado)
            <tr>
	        <td>{{{ $estado->Nome }}}</td>
					<td>{{{ $estado->Sigla }}}</td>
                    <td>{{ link_to_route('cadastro.estados.show',
                                         'Detalhes',
                                         array($estado->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
                        {{ link_to_route('cadastro.estados.edit',
                                         'Editar',
                                         array($estado->Id),
                                         array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
                        <div class="pull-left">
                          {{ Form::open(array('method' => 'DELETE', 'route' => array('cadastro.estados.destroy', $estado->Id))) }}
                            <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                          {{ Form::close() }}
                        </div>
                    </td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $estados->appends(Input::except('page'))->links(); ?>
@else
    Não há estados
@endif

@stop