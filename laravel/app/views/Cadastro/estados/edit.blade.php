@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar estado
@stop


@section('submenu')
  <a href="{{ route('cadastro.estados.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.estados.show', array($estado->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.estados.edit', array($estado->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($estado, array('method' => 'PATCH', 'route' => array('cadastro.estados.update', $estado->Id))) }}
    @include('Cadastro.estados._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop