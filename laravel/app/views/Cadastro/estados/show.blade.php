@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do estado
@stop


@section('submenu')
  <a href="{{ route('cadastro.estados.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.estados.show', array($estado->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.estados.edit', array($estado->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.estados.destroy', $estado->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <h2 class="detalhes">Nome</h2>
<p class="detalhes">{{{ $estado->Nome }}}</p>

<h2 class="detalhes">Sigla</h2>
<p class="detalhes">{{{ $estado->Sigla }}}</p>


@stop