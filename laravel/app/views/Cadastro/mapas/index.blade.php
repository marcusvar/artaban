@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Tipo de veículo: {{ $tipoVeiculo->Descricao }} <i class="icon-fixed-width icon-angle-right"></i> Mapa
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposVeiculo.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
   
  <a href="{{ route('cadastro.tiposVeiculo.show', array($tipoVeiculo->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>
    
  <a href="{{ route('cadastro.tiposVeiculo.mapa.index', array($tipoVeiculo->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-th"></span> Mapa
  </a>  
@stop


@section('principal')  
  <fieldset>
    <legend>Mapa</legend>
    <input type="hidden" value="{{ $tipoVeiculo->Id }}" name="tipoVeiculoId" id="tipoVeiculoId" />  

    <div class="row">
      <div class="col-xs-8">
        <ul class="nav nav-tabs" id="myTab">
          <li class="active"><a href="#tab-andar-1" data-toggle="tab">Piso superior</a></li>
          <li><a href="#tab-andar-2" data-toggle="tab">Piso inferior</a></li>
        </ul>
  
        <div class="tab-content">
          <div class="tab-pane active" id="tab-andar-1">
            {{ Artaban\Helper\View\Mapa::montarPara($mapa, 1) }}
          </div>
          
          <div class="tab-pane" id="tab-andar-2">
            {{ Artaban\Helper\View\Mapa::montarPara($mapa, 2) }}
          </div>
        </div> 
      </div>
    </div>
  </fieldset>
@stop