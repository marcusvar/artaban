@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Editar tipo de adicional para tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.show', array($tipoAdicionalTarifa->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.edit', array($tipoAdicionalTarifa->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
@stop


@section('principal')
  @include('shared._error_message')

  {{ Form::model($tipoAdicionalTarifa,
                 array('method' => 'PATCH',
                       'route' => array('cadastro.tiposAdicionaisTarifas.update', $tipoAdicionalTarifa->Id))) }}
    
    @include('Cadastro.tiposAdicionaisTarifas._form')
  
  {{ Form::submit('Salvar', array('class' => 'btn btn-default botao-padrao')) }}
  {{ Form::close() }}
@stop