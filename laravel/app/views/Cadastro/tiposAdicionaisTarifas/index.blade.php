@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Tipo de adicional para tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($tiposAdicionaisTarifas->count())
    <a href="#" class="search-button">
      <span class="glyphicon glyphicon-search"></span> Opções de filtros
    </a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'cadastro.tiposAdicionaisTarifas.index')) }}
     
        <div class="form-group">
          <label for="Descricao">Descrição:</label>
          {{ Form::text('Descricao', null, array('class' => 'form-control')) }}
        </div>
  
        {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
      {{ Form::close() }}
    </div>
    
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Descrição</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($tiposAdicionaisTarifas as $tipoAdicionalTarifa)
          <tr>
	    <td>{{{ $tipoAdicionalTarifa->Descricao }}}</td>
            <td>
	      {{ link_to_route('cadastro.tiposAdicionaisTarifas.show',
                               'Detalhes',
                               array($tipoAdicionalTarifa->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                                        
              {{ link_to_route('cadastro.tiposAdicionaisTarifas.edit',
                               'Editar',
                               array($tipoAdicionalTarifa->Id),
                               array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array(
		       'method' => 'DELETE',
		       'route' => array('cadastro.tiposAdicionaisTarifas.destroy', $tipoAdicionalTarifa->Id)
		   ))
		}}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
    
    <?php echo $tiposAdicionaisTarifas->appends(Input::except('page'))->links(); ?>
@else
  Nenhum tipo de adicional tarifa encontrado.
@endif

@stop