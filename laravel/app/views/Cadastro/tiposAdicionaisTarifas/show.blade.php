@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do tipo de adicional para tarifa
@stop


@section('submenu')
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
  
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
    
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.show', array($tipoAdicionalTarifa->Id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('cadastro.tiposAdicionaisTarifas.edit', array($tipoAdicionalTarifa->Id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('cadastro.tiposAdicionaisTarifas.destroy', $tipoAdicionalTarifa->Id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <div class="show"> 
    <h2>Descrição</h2>
    <p>{{{ $tipoAdicionalTarifa->Descricao }}}</p>
  </div>
@stop