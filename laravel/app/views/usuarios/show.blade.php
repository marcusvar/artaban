@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Detalhes do usuário
@stop


@section('submenu')
  <a href="{{ route('usuarios.index') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('usuarios.show', array($usuario->id)) }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-list"></span> Detalhes
  </a>  
  
  <a href="{{ route('usuarios.edit', array($usuario->id)) }}" class="btn btn-success">
    <span class="glyphicon glyphicon-pencil"></span> Editar
  </a>
    
  {{ Form::open(array('method' => 'DELETE',
                      'route' => array('usuarios.destroy', $usuario->id),
                      'class' => 'delete-submenu')) }}
    <a href="#" class="submit-form-delete btn btn-danger">
      <span class="glyphicon glyphicon-remove-circle"></span> Excluir</a>                    
  {{ Form::close() }}      
@stop


@section('principal')
  <h2 class="detalhes">Nome</h2>
  <p class="detalhes">{{{ $usuario->nome }}}</p>

  <h2 class="detalhes">Email</h2>
  <p class="detalhes">{{{ $usuario->email }}}</p>
@stop