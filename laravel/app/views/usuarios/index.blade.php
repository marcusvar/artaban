@extends('layouts.app')

@section('descricaoTipoMenu')
  Cadastro
@stop


@section('descricaoMenu')
  Usuários
@stop


@section('submenu')
  <a href="{{ route('usuarios.index') }}" class="btn btn-success active">
    <span class="glyphicon glyphicon-home"></span> Principal
  </a>
    
  <a href="{{ route('usuarios.create') }}" class="btn btn-success">
    <span class="glyphicon glyphicon-plus"></span> Adicionar
  </a>
@stop


@section('principal')
  @if ($usuarios->count())
    <a href="#" class="search-button"><span class="glyphicon glyphicon-search"></span> Opções de filtros</a>
 
    <div id="form-search">    
      {{ Form::open(array('method' => 'GET', 'route' => 'usuarios.index')) }}
      
      <div class="form-group">
        <label for="email">Nome:</label>
        {{ Form::text('nome', null, array('class' => 'form-control')) }}
      </div>
  
      <div class="form-group">
        <label for="senha">Email:</label>
        {{ Form::text('email', null, array('class' => 'form-control')) }}
      </div>
    
      {{ Form::submit('Filtrar', array('class' => 'btn btn-default btn-sm botao-padrao')) }}                  
     {{ Form::close() }}
    </div>
    
    <table class="table table-striped table-bordered">
      <thead>
        <tr>
	  <th>Nome</th>
	  <th>Email</th>
	  <th>Ações</th>
	</tr>
      </thead>

      <tbody>
        @foreach ($usuarios as $usuario)
          <tr>
            <td>{{{ $usuario->nome }}}</td>
	    <td>{{{ $usuario->email }}}</td>
	    <td>                
              {{ link_to_route('usuarios.show',
	                       'Detalhes',
			       array($usuario->id),
			       array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
              
	      {{ link_to_route('usuarios.edit',
	                       'Editar',
			       array($usuario->id),
			       array('class' => 'pull-left btn btn-default btn-sm botao-padrao')) }}
                    
              <div class="pull-left">
                {{ Form::open(array('method' => 'DELETE', 'route' => array('usuarios.destroy', $usuario->id))) }}
                  <a href="#" class="submit-form-delete btn btn-default btn-sm botao-padrao-excluir">Excluir</a>   
                {{ Form::close() }}
              </div>
            </td>
	  </tr>
	@endforeach
      </tbody>
    </table>
    
    <?php echo $usuarios->appends(Input::except('page'))->links(); ?>
@else
  Não há usuarios
@endif

@stop