@if (Session::has('message'))
  <div class="panel panel-warning mensagens-validacao">
    <div class="panel-heading">
      <h3 class="panel-title">{{ Session::get('message') }}</h3>
    </div>
            
    <div class="panel-body">
      @if ($errors->any())
        <ul>
          {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
      @endif
    </div>
  </div>          
@endif