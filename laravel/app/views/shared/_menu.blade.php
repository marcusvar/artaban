<nav class="navbar navbar-default barra-navegacao-principal" role="navigation">
  <div class="container">    

    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
          
      <a class="navbar-brand" href="/">
        <img src="/img/logo.png" width="160px" height="21px" style="float: left;" />
        <span style="display: block; float: left; margin-left: 14px; font-weight: bold; color: #386E38; ">| {{ \Artaban\Helper\Modulo::atual()->nome }}</span>
      </a>
      
    </div>
        
    <div class="collapse navbar-collapse navbar-ex1-collapse">
      
      <ul class="nav navbar-nav">
        @foreach ($menu as $tipo)
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{{ $tipo['descricao'] }}}	<b class="caret"></b></a>
          <ul class="dropdown-menu">
            @foreach ($tipo['links'] as $link)
              <li><a href="{{ $link['url'] }}">{{{ $link['title'] }}}</a></li>
            @endforeach
          </ul>
        </li>              		
        @endforeach
      </ul>
                  
      @if (Auth::check())           
        <div class="btn-group pull-right" style="margin-top: 6px;">
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="glyphicon glyphicon-user"></span> Olá, {{ Auth::user()->nome }} <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Alterar senha</a></li>
            <li><a href="/logout">Sair</a></li>
          </ul>
        </div>
      @endif
      
    </div>
  </div>
</nav>