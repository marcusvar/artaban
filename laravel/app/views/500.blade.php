@extends('layouts.app')

@section('descricaoTipoMenu')
  Aviso
@stop


@section('descricaoMenu')
   
@stop


@section('submenu')
  <a href="#" class="btn-voltar-pagina btn btn-success">
    <span class="glyphicon glyphicon-circle-arrow-left"></span> Voltar
  </a>
@stop


@section('principal')
  <div class="alert alert-warning">
    <p>
      @if (isset($mensagem))
        {{ $mensagem }}  
      @else
        A operação solicitada não pôde ser concluída devido a um erro interno.  
      @endif
    </p>
        
    <p>Se o problema persistir ou se estiver com dúvidas para realizar essa operação, entre em contato conosco.</p>    
  </div>
       
@stop