<?php

use Mockery as m;
use Way\Tests\Factory;

class TipopermissaosTest extends TestCase {

	public function __construct()
	{
		$this->mock = m::mock('Eloquent', 'TipoPermissao');
		$this->collection = m::mock('Illuminate\Database\Eloquent\Collection')->shouldDeferMissing();
	}

	public function setUp()
	{
		parent::setUp();

		$this->attributes = Factory::tipoPermissao(['id' => 1]);
		$this->app->instance('TipoPermissao', $this->mock);
	}

	public function tearDown()
	{
		m::close();
	}

	public function testIndex()
	{
		$this->mock->shouldReceive('all')->once()->andReturn($this->collection);
		$this->call('GET', 'tipoPermissaos');

		$this->assertViewHas('tipoPermissaos');
	}

	public function testCreate()
	{
		$this->call('GET', 'tipoPermissaos/create');

		$this->assertResponseOk();
	}

	public function testStore()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(true);
		$this->call('POST', 'tipoPermissaos');

		$this->assertRedirectedToRoute('tipoPermissaos.index');
	}

	public function testStoreFails()
	{
		$this->mock->shouldReceive('create')->once();
		$this->validate(false);
		$this->call('POST', 'tipoPermissaos');

		$this->assertRedirectedToRoute('tipoPermissaos.create');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testShow()
	{
		$this->mock->shouldReceive('findOrFail')
				   ->with(1)
				   ->once()
				   ->andReturn($this->attributes);

		$this->call('GET', 'tipoPermissaos/1');

		$this->assertViewHas('tipoPermissao');
	}

	public function testEdit()
	{
		$this->collection->id = 1;
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->once()
				   ->andReturn($this->collection);

		$this->call('GET', 'tipoPermissaos/1/edit');

		$this->assertViewHas('tipoPermissao');
	}

	public function testUpdate()
	{
		$this->mock->shouldReceive('find')
				   ->with(1)
				   ->andReturn(m::mock(['update' => true]));

		$this->validate(true);
		$this->call('PATCH', 'tipoPermissaos/1');

		$this->assertRedirectedTo('tipoPermissaos/1');
	}

	public function testUpdateFails()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['update' => true]));
		$this->validate(false);
		$this->call('PATCH', 'tipoPermissaos/1');

		$this->assertRedirectedTo('tipoPermissaos/1/edit');
		$this->assertSessionHasErrors();
		$this->assertSessionHas('message');
	}

	public function testDestroy()
	{
		$this->mock->shouldReceive('find')->with(1)->andReturn(m::mock(['delete' => true]));

		$this->call('DELETE', 'tipoPermissaos/1');
	}

	protected function validate($bool)
	{
		Validator::shouldReceive('make')
				->once()
				->andReturn(m::mock(['passes' => $bool]));
	}
}
